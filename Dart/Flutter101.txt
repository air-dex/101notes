Flutter 101 :

Généralités :
	- Flutter est un framework de développement d'interfaces sur plusieurs supports :
		- Mobiles Android & iOS (stable).
		- Web (beta).
		- PCs Windows, Mac et Linux (master).
	- Site : https://flutter.dev/
	- Créé par Google.
	- Repose sur Dart.

Installer Flutter :
	- Processus :
		- S'assurer d'avoir les outils suivants, quitte à devoir les installer avec cette ligne de commande :
			- Windows : Powershell 5, git 2 (ou ultérieurs).
			- Linux : bash (rm, mkdir, which), curl, git (2.0 ou ultérieur), unzip, zip, xz-utils, libglu1-mesa.
		- Installer Flutter :
			- Méthode 1 :
				- Télécharger la tarball contenant Flutter sur le site officiel.
				- L'extraire où on veut la mettre.
			- Méthode 2 :
				- Créer le dossier de destination.
				- Y cloner Flutter : git clone https://github.com/flutter/flutter.git -b stable
		- Ajouter <dossier Flutter>/bin au path.
		- Lancer les commandes suivantes :
			- flutter precache : télécharge certains outils dont Flutter a besoin.
			- flutter doctor : vérifie que l'installation est correcte.
			- flutter upgrade : màj vers la dernière version de Flutter.
	- Les éditeurs de texte :
		- Flutter peut fonctionner avec n'importe quel éditeur de texte.
		- Néanmoins, ils ont créé des extensions pour les éditeurs suivants :
			- Android Studio & IntelliJ :
				- Extension Dart : https://plugins.jetbrains.com/plugin/6351-dart
				- Extension Flutter : https://plugins.jetbrains.com/plugin/9212-flutter
			- Visual Studio Code :
				- Extension Dart : https://marketplace.visualstudio.com/items?itemName=Dart-Code.dart-code
				- Extension Flutter : https://marketplace.visualstudio.com/items?itemName=Dart-Code.flutter
		- flutter doctor vérifie si les éditeurs ont bien leur extension officielle.

Les canaux Flutter :
	- Flutter possède plusieurs canaux de développements (channels) :
		- master : dernier commit.
			- Flutter tel qu'il était lors du dernier commit en date sur le projet GitHub.
			- Version non testée : des choses peuvent casser du jour au lendemain.
		- dev : version "sortie du four".
			- Version très avancée.
			- C'est le dernier build en date.
			- Version testée, mais n'est pas à l'abri des nouveaux bugs introduits par les derniers dévelopements.
		- beta : version avancée.
			- Màj 1 fois par mois environ. : c'est "le meilleur build dev du mois précédent".
			- Tests "avancés", avec les exemples officiels.
			- Même niveau de tests que la version stable.
			- Normalement ça marche, mais sait-on jamais.
		- stable : version (archi-)stable.
			- Màj "majeure" environ 1 fois par trimestre.
			- Hotfixes sur les bugs bloquants.
			- Conseillé pour les applications en production.
	- Changer de canal :
		- Commande flutter channel :
			- Syntaxe : flutter channel <canal>
			- <canal> : le nouveau canal : stable|beta|dev|master
		- Il est recommandé de màj Flutter après chaque changement de canal :
			flutter channel $new_channel
			flutter upgrade
