Services Web 101 :

Ici service Webs era souvent abrégé en WS (Web Service).

Généralités :
	- Les services Web reposent sur le modèle SOA.
		- SOA = Service Oriented Architecture.
	- Un service Web est composé de :
		- Une URI.
		- Des interfaces publiques.
	- Un WS (WebService) est appelé en XML.
		- Indépendance vis-à-vis des plateformes car on communique via un format normalisé.
	- Les WS communiquent via HTTP.
	- Sa description est consignée dans un annuaire.

Architecture d'un WS :
	- Repose sur les technologies suivantes :
	- HTTP : HyperText Transfer Protocol.
		- Par le Web, ses GET/POST/..., ses headers...
	- REST : REpresentational State Transfer.
		- Communication entre un client et un serveur, qui sont découplés.
		- Interface uniforme.
		- On ne garde aucun état de session entre deux transactions.
	- XML-RPC : XML-RemoteProtocolCommunication
		- Requêtes en XML via du HTTP POST.
		- Elle est renvoyée dans la réponse.
	- SOAP : Simple Object Access Protocol
		- Le protocole de communication.
		- Décrit en XML.
		- Standardise la communication des services Web WS-*
			- Le message est dans une sorte d'enveloppe.
			- L'enveloppe contient des données et des pièces jointes.
			- Elle peut aussi être signée.
	- SOAP, REST et XML-RPC sont des concurrents.
	- WSDL : Web Services Description Language
		- Langage pour décrire un WS dans les moindres détails.
	- UDDI : Universal Description, Discovery and Integration.
		- Annuaire de WS.
		- Présente les WS au monde entier.
		- 3 sous-annuaires :
			- Pages blanches : les adresses des WS.
			- Pages jaunes : secteurs d'affaires.
			- Pages vertes : données techniques.

Fonctionnement des WS :
	- S'articule autour de 3 entités :
		- L'annuaire service registry (ASR) . C'est notre annuaire.
		- Service Provider (SP) : fournit le WS, le met à disposition.
		- Service requester (SR) : le consommateur du WS.
	- Communication entre les entités :
		- Les 3 entités communiquent entre elles en respectant le protocole SOAP.
			- Donc via des messages SOAP.
		- Les communications :
			1°) Le SP publie son WS et donne les données WSDL à l'annuaire.
			2°) Le SR s'adresse par SOAP à l'annuaire pour y consulter des WS.
			3°) L'annuaire renvoie les documents WSDL demandés.
			4°) Le SR invoque le service du SP, dans le respect de son WSDL.
			5°) Le SP répond au SR, dans le respect de son WSDL.
	- Le message SOAP :
		- C'est un message HTTP. Il a donc deux parties :
			- Protocole Header : le header HTTP.
			- SOAP Envelope : le body HTTP, appelé enveloppe SOAP.
		- L'enveloppe SOAP est composée de deux (sous-)parties :
			- SOAP Header : infos spécifiques à la transaction.
			- SOAP Body : données à transporter.
				- SOAP Fault : gestion des erreurs.
		- L'enveloppe SOAP est un flux de données écrit en XML :
			<?xml version="1.0" encoding="utf-8"?>
			<soap:Envelope
				xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/"
				soap:encodingStyle="http://schemas.xmlsoap.org/soap/encoding"
			>
				<soap:Header>
					<!-- Le SOAP Header -->
				</soap:Header>
				<soap:Body>
					<!-- Le SOAP Body -->
					<soap:Fault>
						<!-- Le SOAP Fault -->
					</soap:Fault>
				</soap:Body>
			</soap:Envelope>
		- "soap" est le namespace XML du protocole.
			- SOAP utilise aussi les namespaces "xsi" et "xsl".

WSDL :
	- Document écrit en XML, avec une extension .wsdl.
	- Décrit tout un service.
	- Le profil d'un WSDL :
		<?xml version="1.0" encoding="utf-8"?>
		<!-- La définition, alias l'élément racine
		<wsdl:definition
			name="customerExemple"
			targetNamespace="http://www.stevepotts.com/customer.wsdl"
			xmlns:soap="http://www.schemas.xmlsoap.org/wsdl/soap/"
			xmlns:wsdl="http://www.schemas.xmlsoap.org/wsdl/"
			xmlns="http://www.stevepotts.com/customer.xsd"
		>
			<!-- Les types : définit les entités utilisées par le WS, les types de données. -->
			<wsdl:types>
				<xsd:schema
					targetNamespace = "http://www.stevepotts.com/customer.xsd"
					xmlns: xsd = "http://www.w3.org/2001/XMLSchema"
				>
					<xsd:element name ="customer">
						<xsd:complexType>
							<xsd:sequence>
								<xsd:element name="customer ID" type="xsd:string" />
								<xsd:element name="lastname" type="xsd:string" />
								<xsd:element name="firstname" type="xsd:string" />
							</xsd:sequence>
						</xsd:complexType>
					</xsd:element>
				</xsd:schema>
			</wsdl:types>

			<! Opérations : des opérations entre les types de wsdl:types -->
			<wsdl:operation>
				<!-- ... -->
			</wsdl:operation>

			<!-- Les messages : paramètres E/S pour les opérations. -->
			<wsdl:message name="addCustomer">
				<wsdl:part name="customerInfo" element="tns:customer"/>
			</wsdl:message>
			<wsdl:message name="confirmationResponse">
				<wsdl:part name="response" element="xsd:integer"/>
			</wsdl:message>

			<!-- Port type : lien entre opérations et messages. -->
			<wsdl:portType name ="newCustomerPortType">
				<wsdl:operation name="createNewCustomer">
					<wsdl:input message="addCustomer"/>
					<wsdl:output message="confirmationResponse"/>
				</wsdl:operation>
			</wsdl:portType>

			<!--
				Binding : informations pour se connecter au WS :
					- Protocole de communication.
					- URL à taper.
					- Comment écrire les données à envoyer.
					- Comment seront écrites les données renvoyées.
			-->
			<wsdl:binding name="newCustomerBinding" type="newCustomerPortType">
				<soap:binding style="rpc" transport="http://schemas.xmlsoap.org/soap/http"/>

				<wsdl:operation name="createNewCustomer">
					<soap:operation soapAction="http://wwww.stevepotts.com/createNewCustomer"/>
					<wsdl:input>
						<soap:body
							use="encoded"
							namespace="http://wwww.stevepotts.com/Customer" 
							encodingStyle="http://schemas.xmlsoap.org/soap/encoding"
						/>
					</wsdl:input>

					<wsdl:output>
						<soap:body
							use="encoded"
							namespace="http://wwww.stevepotts.com/createNewCustomer"
							encodingStyle="http://schemas.xmlsoap.org/soap/encoding"
						/>
					</wsdl:output>
				</wsdl:operation>
			</wsdl:binding>

			<!--
				Port : point d'accès individuel au WS.
					- Possède un nom et est lié à un binding (dont on donne le nom).
			-->
			<wsdl:port binding="newCustomerBinding" name="newCustomerPort">
				<soap:address location="http://www.stevepotts.com:1776/soap/servlet/rpcrouter">
			</wsdl:port>

			<!-- Le service : port du WS -->
			<wsdl:service name="newCustomerService">
				<documentation>Ajouter un nouveau client</documentation>
				<wsdl:port binding="newCustomerBinding" name="newCustomerPort">
					<soap:address location="http://www.stevepotts.com:1776/soap/servlet/rpcrouter">
				</wsdl:port>
			</wsdl:service>
		</wsdl:definition>

UDDI :
	- Notre annuaire.
	- 3 sections :
		- Pages blanches : organisations ayant publié des WS dans l'annuaire.
			- Alias BusinessEntity.
		- Pages jaunes : description non technique des WS proposés.
			- Alias Business Service.
		-  Pages vertes : description techniques des WS proposés.
			- Alias BindingTemplate.
			- Donnent :
				- Une URL.
				- Une description.
				- Des tModels
			- tModel : descriptions techniques, en WSDL.
	- Interface de l'UDDI :
		- Interrogation inquiry : pour consulter l'annuaire, y rechercher des trucs.
		- Publication : pour la publication de WS.
		- Sécurité : gestion de l'authentification dans les parties protégées l'annuaire UDDI
		- Contrôle d'accès : qui à le droit d'accéder à quoi sur les WS.
		- Abonnement subscription : pour être tenu au courant des évolutions, genre newsletter.

Implémentations de SOAP :
	- Intégré de base dans ASP.NET.
	- Java : JAX (JAva XML) :
		- JAX-WS : pour les Web Services.
		- JAX-RPC : SOAP en RPC.
		- JAXR (JAX Registries, pour l'UDDI).
		- JAXM (JAX Messaging, pour les messages en SOAP).
		- Dans Java EE / Jakarta EE.
	- Python : SOAPpy
	- De base dans PHP (--enable-soap).
