Guide sur le packaging :

1°) Préparer la construction du package :

bzr dh-make NOM VERSION TARBALL

Lance dh_make avec tout le stuff Bazaar en plus.

dh_make : utilitaire pour préparer le packaging Debian. Il prépare le terrain pour l'utilitaire "dh" (debhelper commands sequencer)

TARBALL=NOM-VERSION.tar.gz
TARBALL_ORIG=NOM_VERSION.orig.tar.gz

Il doit y avoir la TARBALL dans le dossier.

- Copie la tarball dans NOM_VERSION.orig.tar.gz
- Crée l'architecture Debian du package dans le dossier $PWD/NOM

Arborescence :
$PWD/ -
      |- TARBALL
      |
      |- TARBALL_ORIG
      |
      |- NOM/ -
              |- .bzr    -
              |          |- <stuff pour Bazaar>
              |
              |- debian/ -
                         |- <ce qu'il faut pour l'archi>
                       
On peut faire plusieurs types de packages :
	- single binary (s) : pour un seul package .deb "NOM-dev_VERSION.deb". Classique.
	- indep binary (i) : single binary indépendant de l'architecture.
	- multiple binary (m) : pour splitter le single binary en plusieurs packages.
	- library (l) : génère le single binary classique + un autre avec les sources "NOM-dev_VERSION.deb" avec la doc et les sources pour pouvoir faire du développement.
	- kernel module (k) : pour un module du noyau (Linux).
	- kernel patch (n) : pour un patch pour le noyau (Linux).
Dans le cas de Reyn Tweets, c'est un single binary (s).

On se fait jeter si le dossier NOM/ existe déjà.
NOM/ est versionné par Bazaar.


2°) Modifier les fichiers dans $PWD/NOM/debian/ pour les propriétés du package (control surtout).
Plus d'infos ici : http://developer.ubuntu.com/packaging/html/debian-dir-overview.html


3°) Construire le package Debian :

Commande Bazaar "builddeb" : bzr builddeb <branche> (ou bzr bd)

"-- -us -uc" : pas besoin d'une clé GPG pour signer le package.
"-- -nc -us -uc" : reconstruire le package (sans la clé GPG).
"-- <stuff>" signifie l'ajout d'arguments.
Option "-S" / "--source" pour construire un package avec des sources.

Exécuté dans le répertoire $PWD/NOM/debian/ par défaut.

Crée un dossier $PWD/build-area avec ses productions.


4°) Débugger le package :

Utilisation de l'outil "lintian" (http://lintian.debian.org/)

On le passe sur le .dsc et le .deb généré.


















