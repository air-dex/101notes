# Rust101:

Langage de Mozilla. Site : https://www.rust-lang.org

## Installer Rust :

### Via rustup :

On télécharge "rustup-init" (script ou executable) puis on l'exécute.
- S'installe à 2 endroits :
	- `~/.cargo(/bin)` : là où tous les exécutables nécessaires pour Rust sont installés.
		- Il est conseillé d'avoir ce dossier dans son PATH.
	- `~/.rustup` : le repère de rustup
- Désininstaller Rust :
	- Via rustup : `rustup self uninstall`
	- En effaçant `~/.cargo` et `~/.rustup`.

Script conseillé :
```shell
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
```

### Via un installateur officiel contenant les composant principaux.

Classique, contenant les composant principaux.

## Documentation de Rust :

Consiste en une page principale nommée "l'étagère" et une collection de livres.

Il existe un livre référence sur Rust qui explique le langage : "The Rust Programming Language" alias "The Book".

### Online :

- Site web : https://doc.rust-lang.org/
- Online : https://doc.rust-lang.org/book/title-page.html

### Offline :

- Commande : `rustup docs` (ou `rustup doc`)
	- On rajoute un argument selon le livre. Cf. `rustup docs --help`.
- The Book : `rustup docs --book`
- Documentation des crates d'un projet Rust : `cargo doc --open`

## Les outils :

Rust possède tout un écosystème d'outillage (SDK).

Outils populaires :
- `rustc` : compilateur Rust.
- `cargo` : gestionnaire de projets.
- `rust-std` : librairie standard de Rust.
- `rustfmt` : formatteur de code.
- `clippy` : linter.
	- Ensemble de règles à respecter pour améliorer la qualité du code.
- `rustdoc` : générateur de documentation pour le projet.
- `rust-analyzer` : utilitaire pour l'intégration dans les IDE.
- `rust-src` : sources de Rust.
- `rust-docs` : copie offline de la documentation de Rust.

### Rustup : 

Gestionnaire de l'installation de Rust. C'est l'outil qui va gérer notre installation de rust.

Site web : https://rustup.rs/

Exécutable via des sous-commandes : `rustup cmd ...`

#### Les commandes :

- `docs` : pour afficher la documentation offline de Rust.
- `update` : màj de Rust.

#### Les plateformes :

Rust est disponible sur différentes plateformes. Ces plateformes sont aussi appelées (triple-)targets.

Les noms des triple-targets sont normalisés.
- Syntaxe : `<arch>-<vendor>-<sys>-<abi>`
- `<arch>` : architecture système.
	- Exemples :
		- `x86_64` (x86 64 bits)
		- `i686` (x86 32 bits)
		- `wasm32` (WASM)
		- `aarch64` (ARM 64 bits)
		- `powerpc`
- `<vendor>` : nom du manufacturier de la machine.
	- `unknown` : inconnu
		- Très souvent utilisé car pas attaché à une marque en particulier.
		- Marche aussi pour Linux.
	- `pc` : vendeurs de PC (Windows).
- `<sys>` : l'OS
	- Exemples :
		-  `windows`
		- `linux`
		- `darwin` (macOS)
		- `unknown` (inconnu)
		- `android`
		- `ios`
		- `freebsd`
		- `switch` (Nintendo Switch OS)
- <abi> : l'ABI (interface application-OS), liée au compilateur
	- Exemples : `gnu` (libc), `msvc`...
	- Cette partie n'est pas toujours présente.

Les targets sont regroupées en Tier :
- Tier 1 : fonctionnement garanti.
	- Support officiel, prioritaire et complet.
	- La CI s'assure de leur compilation et que les tests dessus passent.
	- Supportent pleinement la librairie standard et les outils de l'écosystème.
- Tier 2 : compilation garantie.
	- Fiables si on veut compiler dessus.
	- La CI s'assure de leur compilation, mais pas toujours que les tests passent.
	- Peuvent ne pas avoir tout l'outillage de l'écosystème.
		- Ceux n'ayant pas l'intégralité de l'outillage sont un "Tier 2.5", un Tier dans le Tier.
	- Tout ou partie de la librairie standard est supportée.
- Tier 3 : le Tiers-Monde
	- Aucune garantie n'est donnée sur les toolchains de ce niveau.
	- Les builds ne sont pas garantis, les tests dessus encore moins.
	- Ils n'ont pas forcément le support de l'intégralité de la librairie standard et/ou de l'outillage.

#### Les canaux :

Pour chaque toolchain, on peut avoir des canaux différents :
- stable : version stable.
- nightly : version nightly, avec les tous derniers avancements.
- beta : entre stable et nightly. Avancé mais pas trop non plus.

#### Les toolchains :

Association d'un canal, d'une triple target et d'une date (facultative).

#### Reste

TODO

### Cargo : 

Gestionnaire de projets Rust, de ses différentes phases :
- Initialisation
- Compilation.
- Gestion des dépendances.
- Tests

Outil de CLI qui fonctionne avec des sous-commandes : `cargo cmd --args...`

Options générales :
- `--help` : afficher l'aide.
- `--version` : version de Cargo.
- `--verbose` : mode verbeux.
- `--package pkg_name` / `-p pkg_name` : préciser un(des) crate(s).

#### Aide de Cargo :

En CLI : `cargo --help`

Aide d'une commande de Cargo : `cargo cmd --help`

Ouvrir "The Cargo Book" : `rustup doc --cargo`

#### Création de projets Rust : 

Via les commandes `cargo new` et `cargo init`".`
- **`cargo new`** crée un projet dans un nouveau dssier.
- **`cargo init`** crée un projet Rust dans un dossier existant.

Syntaxe : `cargo new|init <args> path`
- Crée le projet dans path.
- `--name` : nom du projet, celui du path par défaut.
- `--vcs` : préciser un CVS, afin que Cargo nous initialise un repo.
	- Git par défaut (git).
	- --vcs none : pas de CVS.
- Type de projet :
	- `--bin` : exécutable (par défaut).
	- `--lib` : librairie.
- Crée les éléments suivants :
- Un dossier de sources nommé `/src`. Il contient :
	- Un fichier `main.rs` si le crate précisé dans la commande est un binaire (--bin ou rien).
		- `/src/main.rs` contient une fonction main() et c'est tout.
	- Un fichier `lib.rs` si le crate précisé dans la commande est une librairie (`--lib`).
- Le fichier de description du projet, nommé **`Cargo.toml`**.
- Un dépôt initialisé, sauf s'il y a l'option "--vcs none".

#### Compilation

Vérifier le code d'un projet sans le compiler : commande **`cargo check`**
- Grosso modo mêmes arguments que pour cargo build.

Compiler un projet : commande **`cargo build`**
- Cargo compile en mode debug (ou "dev") par défaut. Pour le mode release il faut préciser `--release`".`
- Compiler les crates :
	- Exécutables (`[[bin]]` dans le manifeste) :
		- Un binaire en particulier : `cargo build --bin <bin_name>`
		- Tous les binaires : `cargo build --bins`
	- Librairies `[lib]` dans le manifeste) : `cargo build --lib`
	- Texts (`[[test]]` dans le manifeste) :
		- Un crate de test en particulier : `cargo build --test <test_name>`
		- Tous les crates de test : `cargo build --tests`
	- Exemples (`[[example]]` dans le manifeste) :
		- Un exemple en particulier : `cargo build --example <ex_name>`
		- Tous les exemples : `cargo build --examples`
	- Benchmarks (`[[bench]]` dans le manifeste) :
		- Un crate de benchmark en particulier : `cargo build --bench <tbench_name>`
		- Tous les crates de benchmark : `cargo build --benches`
	- Compiler tous les crates : `cargo build --all-targets`
- Compilation optionnelle :
	- Utilise les features optionnelles par défaut.
	- `--features` : features à utiliser pour compiler.
	- `--no-default-features` : on n'utilise pas les featurs par défaut.
- Affichage pendant la compilation :
	- `--message-format json|short|human` : format d'affichage des infos.
		- `human` par défaut : tout est lisible clairement dans le terminal pour un humain.
		- `short` : version condensée de human.
		- `json` : le message est au format JSON.
			- Utile pour un EDI, difficilement lisible pour un humain (surtout avec du JSON non mis en forme).
	- `--verbose` : mode verbeux, pour plus de détails.
		- -v marche aussi.
		- -vv : très verbeux.
	- `--quiet` : n'affiche rien.
	- `--color` : indique s'il faut colorer ce qui est affiché.
- Autres arguments :
	- `--manifest-path` : pour préciser un manifeste en particulier.
		- Si rien, c'est le fichier Cargo.toml du dossier courant.
	- `--out-dir` : où placer le produit de la compilation.
- Cargo stocke ce qu'il produit dans un dossier `/target`
	- `/target` contient :
		- Un sous-dossier `debug` pourle mode debug.
		- Un sous-dossier `release` pourle mode release.
	- L'exécutable est `/target/debug|release/projname(.exe)`.
- TODO

#### Éxecution

Commande **`cargo run`**
- Syntaxe : cargo run <args pour Cargo> -- <args passés à lexécutable>
- Arguments pour Cargo :
	- Lui sert entre autres à compiler l'exécutable.
	- TODO
- TODO

#### Clean

Commande **`cargo clean`**

TODO

#### Tests

Commande **`cargo test`**

Pendant de "cargo run" pour les tests.

Syntaxe : `cargo test <args pour Cargo> -- <args pour l'exécutable de test>`

Arguments pour Cargo :
- Lui sert entre autres à construire l'exécutable de test.
- `--no-run` : compile les tests sans les exécuter derrière.
- Grosso modo les mêmes arguments que pour `cargo build`.
	- Les arguments de crate (`--bin`, `--bins`, `--lib`, etc.) signifient "tester seulement les crates indiqués".
- `--doc` : seulement tester la documentation.

Choisir les tests à inclure dans l'exécutable de tests :
- On peut préciser une string sans nom d'arguments.
	- Les tests inclus dans l'exécutable devront l'avoir dans son nom.
- Ne pas confondre avec le filtrage après.
	- Avant : les fonctions exclues ne seront pas dans l'exécutable, donc pas exécutables.
	- Après : les fonctions seront dans l'exéctutable, mais pas exécutées.

```shell
# Les fonctions de test dont le nom contiendra "toto" seront les seuls tests dans l'exécutable.
cargo test toto
```
Arguments pour l'exécutable de test :
- Ils sont passés à l'exécutable de tests.
- `--test-threads <n>` : nombre de threads utilisés pour passer les tests en parallèle.
	- --test-threads 1 bloque le parallélisme.
- Arguments sur la sortie :
	- `--format <format>` : format de sortie pour les résultats du test.
		- `pretty` et `terse` sont humainement lisibles.
		- `json` pour avoir les données au format JSON.
	- `--logpath <FICHIER>` : spécifier un fichier dans lequel écrire les résultats du test au lieu de stdout.
	- `--nocapture` : forcer l'affichage d'infos pour les tests qui réussissent.
		- Il est conseillé de désactiver le parallélisme.
- Forcer l'exécution des tests ignorés :
	- `--ignore` : n'exécute que les tests ignorés.
	- `--include-ignore` : ajoute les tests ignorés à la liste à tester.
- `--list` : liste des tests à exécuter.
- Choisir les tests à exécuter :
	- N'opère que sur les tests compilés,
	- On peut préciser une string sans nom d'arguments.
		- Les tests exécutés devront l'avoir dans son nom.
	- On peut préciser des tests à ne pas exécuter via --skip.

Exemples :

```shell
# Ne seront exécutées que les fonctions de test dont le nom contiendra "toto"
cargo test -- toto
# Les fonctions de test dont le nom contiendra "titi" ne seront pas exécutées.
cargo test -- toto --skip titi
```

TODO

#### Documentation

Gestion de la documentation des crates d'un projet via la commande commande **`cargo doc`**
- Créer la documentation : `cargo doc`
- Voir la documentation : `cargo doc --open`

#### Màj dépendances

Installation de binaires : "**`cargo install`**"

Les dépôt de crates peuvent contenir des crates exécutables (`[[bin]]`).
	- Rien à voir avec les dépendances d'un projet.

On peut installer les crates binaires via cargo install.

Syntaxe : `cargo install <options installation> -- <nom_crate_bin>`

Installer à un endroit précis :
- `--root` : lieu d'installation du crate. Si rien de précisé c'est au même endroit que Cargo.

Registre de l'exécutable :
- `--registry` : préciser le registre (dépôt) où se trouve le crate exécutable.
	- Les registres sont définis dans la configuration de Cargo.
	- Bien entendu crates.io est censé être le registre par défaut si on ne précise rien.
- `--version` : pour spécifier la version à installer.
	- Ne fonctionne que pour crates.io

Installer un exécutable à partir d'un dépôt git :
- `--git` : URL du dépôt git du projet Cargo.
- `--branch` : branche du dépôt git, master par défaut.
- `--tag`: tag précis pour installer.
- `--rev` : commit précis pour installer.

Installer à partir d'un projet en local :
- `--path` : dossier où se trouve l'exécutable' à installer.

Préciser les exécutables à installer :
- On peut installer des binaires et des exemples.
- Installer des binaires (`[[bin]]`) :
	- `--bin <nom_binaire>` pour un binaire.
	- `--bins` pour tous les binaires du package.
- Installer des exemples (`[[examples]]`) :
	- `--example <nom_exemple>` pour un exemple.
	- `--examples` pour tous les exemples du package.


Compilation :
- Les binaires sont compilés en mode release.
- Pour compiler en mode debug il faut préciser --debug.
- Compilation optionnelle :
	- On peut préciser des features à utiliser avec l'argument `--features`.
	- On inclut toutes les features si on met l'argument `--all-features`.
	- On désactives les features par défaut avec `--no-default-features`.

Lister les binaires installés : `cargo install --list`

Màj les dépendances : commande "**`cargo update`**"
- Peut prendre l'argument `--package pkg_name` pour màj un paquet donné.

Désinstller des binaires : "**`cargo uninstall`**"
- Syntaxe : `cargo uninstall <options installation> -- <nom_crate_bin>`
- On retrouve les options `--bin` et `--root` de `cargo install`.

#### La configuration de Cargo :

Elle est dans un fichier `.cargo/config`, écrit en TOML.

Recherche d'un point de configuration :
- Cargo recherche dans le fichier `.cargo/config` du dossier où il est exécuté (`./.cargo/config`).
- S'il ne trouve rien, il cherche dans le .cargo/config du dossier parent (`../.cargo/config`).
- Si toujours rien il regarde dans le parent encore au dessus ( `../../.cargo/config` ).
- Ainsi de suite jusqu'à `/.cargo/config` (`C:\.cargo\config` sur Windows).
- Si toujours rien, il recherche le fichier config dans son dossier d'installation (souvent `~/.cargo/config`).
- Cela permet donc de surcharger des points de conf dans un projet Rust.

Configuration des registres :
- Un registre est un dépôt de packages.
- Configuration :
	```toml
	# Registre par défaut
	[registry]
	index = "url_depot"		# URL du dépôt utilisé par défaut (crates.io si jamais modifié).
	default = "..."    		# Nom du dépôt utilisé par défaut (crates.io si jamais modifié).

	# Liste des registres
	[registries]
	nom-reg0 = {index  ="url_depot0" }
	nom-reg1 = {index  ="url_depot1" }
	# ...
	nom-regN = {index  ="url_depotN" }

	# Section pour un registre en particulier.
	[registries.nom-reg0]
	index = "url_depot0"# URL du dépôt
	token = "..."       # Token pour s'authentifier auprès du dépôt.
	```

TODO

#### Reste

TODO

### Rustc :

C'est le compilateur Rust.

Permet de connaître sa version de Rust : `rustc --version`

### Rustfmt :

Outil de formattage

TODO

### Clippy :

Linter

TODO

## Les crates

Un crate est quelque chose produit par une compilation : un exécutable binaire ou une librairie.

Un package est un ensemble de crates

Un "crate root" est un fichier source Rust contenant du code pour produire un crate donné.
- Son contenu est automatiquement placé dans un module racine nommé crate.
- Exemple :
	```rust
	// Fichier main.rs
	fn main() {
		// Le crate produit par main.rs appellera son point d'entrée "main" de manière récursive.
		crate::main();
	}
	```

Création d'un crate :
- Ils sont créés d'après le manifeste de leur package.
- On peut spécifier des options spécifique à un crate du package via des sections dédiées du manifeste.

## Les packages Rust :

Alias les projets Rust.

Un package contient au moins un crate.

### Le manifeste du package :

Un package possède un fichier principal nommé **manifeste**. C'est le fameux fichier projet **`Cargo.toml`**.

Il est :
- Toujours à la racine d'un package.
- Ecrit en [langage TOML](https://toml.io/fr), proche du `.ini`.
- Est très lié à Cargo et à ses petites conventions.

### Le fichier Cargo.lock :
	- Permet de fixer certaines choses concernant les dépendances :
		- Exemple : fixe une version donnée dans un projet.
	- Sert surtout à avoir le même build, sans version qui change sauvagement.

### La structure d'un projet Rust créé par Cargo :

```
/---+-Cargo.toml : manifeste
	|
	+-Cargo.lock : fichier lock
	|
	+-src/--+	: fichiers source
	|      	|
	|      	+-main.rs : crate root pour un exécutable
	|      	|
	|      	+-lib.rs : crate root pour la librairie
	|      	|
	|      	+-bin/	: dossier avec d'autres crate root exécutables
	|		|
	|		+-*	: d'autres sources (modules Rust, crate roots)
	|
	+-tests/	: tests d'intégration du package
	|
	+-benches/	: benches
	|
	+-examples/	: exemples d'utilisation du package
	|
	+-target/---+	: dossier à l'usage personnel de Cargo
				|
				+-debug/ : pour les compilation en mode debug
				|
				+-release/ : pour les compilation en mode debug
```

Les exécutables produits en compilant les sources sont dans `/target/debug|release/`.

Les crates root dans `/src/bin` sront ignorés si le manifeste contient a moins une section `[[bin]]`

### Les infos sur le package :

Elles sont dans la section `[package]` du manifeste.

Champs obligatoires :
- **name** : nom du package.
- **version** : version du package, formatée [SemVer](https://semver.org).

Champs facultatifs :
- **edition** : édition de Rust.
	- Une édition de Rust est une version archi-majeure, particulièrement signifiante.
	- Valeurs possibles :
		- "2015" : Rust 2015 (version 1.0), Mai 2015.
		- "2018" : Rust 2018 (version 1.31.0), Decembre 2018.
		- "2021" : Rust 2021 (version 1.56.0), Octobre 2021.
		- "2024" : Rust 2024 (version 1.85.0), Janvier 2025.
	- Equivaut à une option pour rustc : "rustc --edition 2015|2018".
- **authors** : array de strings où sont écrits les auteurs du projet.
- **build**: fichier Rust contenant un script de compilation.
- **publish** = false : pour bloquer la publication d'un package dans un dépôt.
- Des champs descriptifs du projet, notamment à l'usage de crates.io quand on publie dessus.

### Gestion des dépendances du package :

Les dépendances sont répertoriées dans la section `[dependencies]` du manifeste.
- `[dependencies]` : dépendances dans le code source.
- `[build-dependencies]` : dépendances nécessaires pour le script de compilation (champ "build" de la section [package]).
- `[dev-dependencies]` : dépendances nécessaires pour le développement, càd pour :
	- Les tests.
	- Les benchmrks.
	- Le packaging.
	- Les exemples d'utilisation.
	- La compilation à sa propre section indépendante.
- `[target.*.dependencies]` : dépendances liées à une certaine configuration (OS, toolchain...).

Une dépendance correspont à une ligne.

#### Types de dépendances

Lexique utilisée ici :
- `nom_dep` est le nom de la dépendance.
- `version` est la version de la dépendance, formatée [SemVer](https://semver.org).
- La version peut utiliser les pré-requis SemVer :
	- `^a.b.c` :
		- Toutes les versions jusqu'à ce que le 1er chiffre non nul à gauche soit incrémenté.
			- Si que des zéros, alors ça sera l'avant dernier.
				- `^0.0.0.0` : jusqu'à la `0.0.1`.
			- Si `^0`, alors c'est jusqu'à la v1.
			- Exemples :
				- Jusqu'à la prochaine version majeure si a != 0.
					- Toutes les versions dans `[a.b.c ; a+1.0.0 [`
				- Jusqu'à la prochaine version mineure si a == 0 et b != 0.
					- Toutes les versions dans `[0.b.c ; 0.b+1.0 [`
	- `~a.b.c` ou `~a.b` : jusqu'à la prochaine version mineure
	- `~a` : jusqu'à la prochaine version majeure
	- `*` : la wildcard, pouvant prendre n'importe quel valeur.
	- `>= v`, `<= v`, `< v`, `> v` et `= v` : pour les comparaisons avec version `v`.
	- `r0, r1, ..., rN` : spécifier plusieurs pré-requis.

##### Dépendances sur crates.io :
- On indique juste la version et Cargo se chargera du reste le moment venu :
	- Téléchargement de la dépendance.
	- Compilation de la dépendance si nécessaire.
- Syntaxe :
	```toml
	nom_dep = "version"
	```

##### Dépendances dans un autre registre :

Syntaxe :
```toml
nom_dep = { version = "version" , registry = "nom-registre" }
```

`nom-registre` est un registre externe présisé dans la configuration de Cargo.
- Configuration globale de l'utilisateur ou bien fichier `./.cargo/config` dans le projet, au format TOML.

##### Dépendances dans un git :

Syntaxe :
```toml
nom_dep = { git = "URL dépôt git" , branch = "branche_git", rev ="commit_git" , tag = "tag_git" }
```

`branch` permet de spécifier une branche particulière qu'on doit utiliser. ELle vaut (bien entendu) `master` par défaut.

`rev` permet de spécifier une révision par défaut. Elle vaut bien sûr la valeur de `HEAD` par défaut.

`tag` précise un tag pluôt qu'une révision.

##### Dépendances en local :

Syntaxe :
```toml
nom_dep = { path = "path/to/dep", version = "version" }
```

On peut omettre la version si on ne veut pas publier la dépendance en ligne.

Le dossier précisé dans path doit contenir un projet Rust, donc un fichier Cargo.toml.

#### Dépendances optionnelles :

Dépendances liées à des features.

Syntaxe :
```toml
nom_dep { ..., optional = true }
```

#### Surcharge d'une dépendance :

Via des sections `[patch.*]`.
- Syntaxe : `[patch.reg]`
	- `reg` est le nom du registre initial ou bien son URL.
	- Pour crates.io, c'est la section `[patch.crates-io]`

Dans chaque section, on liste les dépendances à surcharger. C'est lyntaxe normale avec la nouvelle dépendance.

Exemple :
```toml
# Au lieu d'utiliser le uuid de crates.io, on utilisera le HEAD de la branche 2.0.0 de son repo git.
[dependencies]
uuid = "1.0.1"

[patch.crates-io]
uuid = { git = 'https://github.com/rust-lang-nursery/uuid', branch = '2.0.0' }
```

#### Dépendances optionnelles :

Alias "**features**".

Utilité des features :
- Proposer des fonctionnalités expérimentales, qu'on veut inclure ou pas dans ce qu'on utilise.
- Permettent de ne pas inclure les fonctionnalités d'un package dont on n'a pas besoin.
	- Réduit les temps de compilation et la taille du programme.
	- Exemple :
		- Soit une dépendance "foo" qui permet de parser des fichiers de configuration aux formats XML, YAML, JSON et TOML.
		- foo propose 4 features, 1 par type de fichier de config : "xml" "yaml", "json" et "toml".
		- Dans notre projet Rust, on ne manipule que des fichiers YAML. On n'a pas l'utilité de foo pour du XML (par exemple)
		- On sera alors enclins à n'utiliser que la feature "yaml" de foo et exclure les 3 autres.

Cargo prend en charge les bibliothèques optionnelles.

Chaque dépendance optionnelle du projet doit avoir un argument optional valant true.

Les features optionnelles sont listées dans la section `[features]`.
- Le champ `features.default` est un tableau de features par défaut.
	- Ce sont les features conseillées pour avoir une utilisation optimale de la dépendance.
- Chaque autre champ représente une feature :
	- Syntaxe :
		```toml
		# fpI est le nom d'une autre feature ou d'un package optionnel.
		feat_name = [ fp0, fp1, ..., fpN ]
		```
	- Si la feature `feat_name` est activée, tous les `fpI` seront requis.
	- On peut utiliser les features d'une feature :
		- Exemple :
			```toml
			# La feature foo dépend de la feature baz de "bar".
			foo = ["bar/baz"]
			```

##### Préciser les features à utiliser pour une dépendance :

Elles sont dans les sections `[dependencies.nom_dep]` dans le manifeste
- On peut aussi y préciser des choses sur la dépendance qu'on écrirait dans la section `[dependencies]`.
- On est +/- dans la feature des maps du langage TOML en écrivant comme ça

Syntaxe :
```toml
[dependencies.nom_dep]
default-features = true|false
features = [ fp0, fp1, ..., fpN ]
```

`nom_dep` est le nom de la dépendance.
`default-features` spécifie si on doit utiliser les features dans `features.default` de la dépendance.
`features` : tableau de features à utiliser. Sert à surcharger si `default-features = false`.

##### Dans le code Rust :

On annote un bloc de code où l'on utilise la feature :

```rust
#[cfg(feature = "feat_name")]
{
	// Code exécuté uniquement si la feature feat_name est activée.
}
```

##### À la compilation :

**cargo build** :
- On précise les features à utiliser via l'argument `--features` de `cargo build`.
- Syntaxe : 
	```shell
	cargo build --features f0 f1 ... fN
	```

**rustc** :
- Via l'argument `--cfg`
- Syntaxe : 
	```shell
	rustc --cfg feature=feat_name
	```

Exemple :

```shell
# On compile avec les feature foo et bar
$> cargo build --features foo bar
$> rustc --cfg feature=foo --cfg feature=bar
```

### Spécification des registres :

Un registre est un dépôt de packages.

https://crates.io est le registre de référence pour Rust.

Les registres doivent être définis dans la configuration de Cargo.
- Si on veut en préciser des spéciaux pour le projet, mettre ça dans un fichier pro_root/.cargo/config

### Les profiles :

Le manifeste permet de spéciafier des options selon le profil de développement via des sections `[profile.*]` :
- `[profile.dev]` : pour le développement, en mode debug.
	- Appliqué lorsqu'on exécute "`cargo build`".
- `[profile.release]` : options pour le mode release.
	- Appliqué lorsqu'on exécute "`cargo build --release`".
- `[profile.test]` : pour les tests.
	- Appliqué lorsqu'on exécute "`cargo test`".
- `[profile.bench]` : pour le benchmark
	- Appliqué lorsqu'on exécute "`cargo bench`".

Les points de configuration sont des options pour `rustc` :
- `opt-level` : niveau d'optimisation.
	- C'est pour "`rustc --opt-level <value>`"
	- Equivalent du `-O` de gcc.
	- Valeur de 0 à 3 selon le niveau d'optimization souhaité.
	- Il existe des valeurs '`s`' et '`z`' pour réduire la taille de l'exécutable.
- `lto` : pour réduire la taille du binaire produit.
	- Pour "`rustc -C lto`".
	- C'est un booléen ou une string :
		- Si true, "`rustc -C lto`".
		- Si false, rien pour rustc.
		- Si string, "`rustc -C lto=<valeur>`".
- `debug` : inclusion des signaux de debug.
	- Booléen valan true ou false.
	- Si debug = true, c'est l'équivalent de "`rustc -C debuginfo=2" ou "rustc -g`".
- `debug-assertions` : booléen vérifiant :
	- Les macros `debug_assert!()`
	- Les arithmetic overflows, par exemple si un nombre entier dépasse sont nombre de bits ou pas.
- `panic` : stratégie en cas de panique de Rust.
	- Pour "`rustc -C panic=<valeur>`".
	- "`unwind`" : l'application essaye de se fermer proprement, en libérant la méoire et tout et tout.
	- "`abort`" : l'application crash brutalement.
- `overflow-checks` : booléen pour les overflow d'entiers.
	- Pour "`rustc -C overflow-checks=<valeur>`".
- `codegen-units` : entier pour paralléléliser la compilation.
	- Nombre de threads utilisés pour ça ?
	- Pour "`rustc -C codegen-units=<valeur>`".

### Options relatives à un crate en particulier :

On appelle ça des cibles ("targets").

Plusieurs types de sections :
- `[[bin]]` : un crate exécutable
- `[lib]` : le crate librairie.
- `[[example]]` : un exécutable produisant un exemple d'utilisation.
- `[[test]]` : un exécutable pour les tests.
- `[[bench]]` : un exécutable pour les benchmarks.

Quelques valeurs de la section :
- `name` : nom du crate.
	- Ce sera le nom du binaire.
	- C'est aussi une option pour rustc : "`rustc -o <value>`".
- `path` : path du crate root.
	- Relativement au manifeste.
- `test` : les tests unitaires sont-ils activés sur ce crate ? true|false
- `doctest` : les tests sur la documentation sont-ils activés sur ce crate ? true|false
	- Seulement valable pour [lib].
- `bench` : le benchmarking sont-ils activés sur ce crate ? true|false
- `doc` : la documentation est-elle activée sur ce crate ? true|false
- `edition` : l'édition de Rust utilisée, pour surcharger celle de `[package]`.
- `required-features` : array de features à utiliser pour la compilation.
	- OSEF pour `[lib]`.
- `crate-type` : types de crate à produire.
	- C'est un tableau de strings.
	- Option `--crate-type` de rustc : 
		```shell
		rustc --crate-type=<val_0> --crate-type=<val_1> ... --crate-type=<val_N
		```
	- Valeurs :
		- "`bin`" : exécutable.
		- Bibliothèque :
			- "`dylib`" : bibliothèque partagée (.so ou .dll).
			- "`staticlib`" : bibliothèque statique (.a ou .lib).
		- Bibliothèque Rust :
			- Bibliothèque au format .rlib.
			- Des bibliothèques utiles à rustc au moment du linkage.
			- "`lib`" : dynamique.
			- "`rlib`" : statique.

Exemples :
- Cas d'un projet avec un seul exécutable : préciser des choses le concernant :
	```toml
	[[bin]]
	path = "src/main.rs"
	# On précise les options suplémentaites ici
	``
- Gestion de la librairie du package :
	```toml
	[lib]
	path = "src/lib.rs"
	# On précise les options suplémentaites ici.
	crate-type = ["staticlib", "dylib"]		# On veut créer la .lib et la .dll.
	```

### Les workspaces (espaces de travail) :

Pour gérer un ensemble de plusieurs projets Rust (via Cargo).

Un workspace comporte un manifeste à sa racine.

Le manifeste du workspace comporte un champ `[workspace]` à la place du champ `[package]`.
- Préciser les projets du workspace :
	- Propriété `members` de la section.
	- Liste de chemins vers les projets, relativement au manifeste.
- On peut exclure des dossiers avec le champ `exclude`.
- Dans les manifestes des projets, on rajoute des champs `workspace` dans les sections `[package]`.

Les projets d'un même workspace partagent le même Cargo.lock.
- Les projets du workspace sont compilés au niveau de la racine du workspace.

Workspace et dépendances :
- Les projets du workspace ne sont pas censés dépêndre les uns des autres.
- Si on veut préciser des dépendances, il faut le faire dans les manifestes des projets.

### Les scripts de build :

Pour réaliser des choses avant la compilation du programme principal.
- Exemples :
	- Compilation de code C/C++ avant de compiler le code Rust.
	- Vérifications sur la configuration de compilation (version de Rust, toolchain...).
	- Préciser des options de compilation spécifiques selon la configuration de compilation.

On spécifie l'adresse de ce script via le champ `build` de la section `[package]` du manifeste Cargo.
- Si rien n'est écrit, Cargo cherchera un fichier "`build.rs`" à compiler et s'en servira comme script de build.
- Pour désactiver cela, il faut mettre "`build = false`" dans le manifeste.

Valeurs d'entrée du script :
- Fichier Rust dont le point d'entrée est une fonction main()
- Elles sont sous la forme de variables d'environnement.
	- Cargo, Rust et Rustup se définissent un certain nombre de variables d'environnement perso valables dans chaque programme Rust.
		- La liste est dans le livre de Cargo, à la partie 3.4 "Environment variables".
- Le dossier courant du script est son dossier source, pas l'endroit à partir duquel on l'exécute.

Les dépendances spécifiques à ce script sont dans la section `[build-dependencies]` du manifeste Cargo.

## Généralités sur le langage Rust :

### Code minimal :

Les sources en Rust vont généralement dans un fichier au format .rs.

Hello world :

```rust
fn main() {
	println!("Hello world!")
}
```

Analyse :
- La fonction `main()` est le point d'entrée.
- `println!()` sert à afficher un texte à l'écran.
- Compilation :
	- En CLI : `rustc main.rs`
	- Va créer l'exécutable main(.exe) à partir du fichier source main.rs.


### Commentaires :

```rust
/* bloc */

// monoligne
```
### Importer un module :

"Module" au sens C/C++ ou "package Java" du terme

```rust
use module;
```

Rust est livré avec une librairie standard, nommée std comme en C++.

### Les instructions :

Il y a deux types d'instructions en Rust : les statements et les expressions.

Les statements :
- Bouts de code qui font des trucs mais ne retournent rien.
- Il y a un `;` final à la fin des instructions.

Les expressions :
- Bouts de code qui font des trucs et retournent quelque chose.
- Ils sont entre accolades `{}`, dans un bloc de code.
- Il n'y a pas de `;` à la fin d'une expression.
- Pas de return comme dans les fonctions, juste une absence de `;`.
- Leur contenu est évalué lors d'une exécution.
	- Idéal pour des pseudo-lambda :
		- Exemple :
			```rust
			let a = 39;
			let b = 5;
			let c = {
				println!("{}", a);
				(a % b) - b
			}    // c == -1, après avoir affiché a.
			```
On ne met un ';' à la fin de la ligne que lorsqu'on ne veut rien retourner.

### Convention de codage et main() :

Rust aime bien séparer son code entre un crate exécutable et une librairie associée.

Le [[bin]], via son main() :
- Récupère les arguments.
- Parse les arguments.
- Met la configuration en place.
- Appelle une méthode `run()` du `[lib]` et lui passe la configuration en paramètre.
- Si `run()` renvoie une erreur, main() la gère.

Le `[lib]` se contente de contenir toute la logique du programme.
- La convention veut une méthode `run(config)` qui soit "le point d'entrée des choses sérieuses".
- `[lib]` est liée statiquement à son `[[bin]]` (enfin j'imagine).

Il existe une macro `unimplemented!()` pour dire que quelque chose n'a pas encore été implémenté.


## Les variables :

C'est une valeur liée à un nom (le nom de la variable) :
```rust
let a = 4;    // La valeur 4 est liée à la variable a
```
### Déclaration :

Se déclarent avec le mot clé **`let`** : `let a;	// Déclare la variable a.`

Nomenclature classique comme dans d'autres langages.

Convention Rust : si la variable n'est pas utilisée, la faire commencer avec `_`.

### Le type de la variable :

Il peut être précisé après le nom.
- On le met après deux-points.
- Syntaxe : `let (mut) var:type_var (= valinit);`
- Exemple :

```rust
let c = 'c';     	// auto c = 'c';
let c:char = 'c';	// char c = 'c';
```

Si on ne met rien, Rust essaye de deviner le type, comme avec "auto" en C++ ou "var" en Java.
- C'est l'inférence de type.
- Rust renvoie une erreur de compilation s'il hésite entre plusieurs types. Il faut alors préciser explicitement le type.
- Exemple :
```rust
let x = 5;     		// auto x = 5;
let x: i32 = 5;		// int x = 5;
```

### Mutabilité

Il existe des variables mutables et des immutables :
- Les variables Rust sont immutables par défaut (constantes).
- Il faut ajouter le mot-clé **`mut`** pour qu'elles soient mutables (variables).
- Exemples (avec équivalent en C++) :
	```rust
	let a: i32 = 5;		// const int a = 5;
	let mut b:i32; 		// int b;
	```

On peut les initialiser ultérieurement à leur déclaration :
- On ne peut faire ça qu'une seule fois pour les variables immutables (logique).
	- Exemple :
	```rust
	let a; a = 5;		// Autorisé
	```

Une variable ne peut pas changer de type, même quand on ne le précise pas :
```rust
let mut a = 5;
a = 6;     		// Autorisé
a = "toto";		// Interdit car on passe de int à string.
```

### Variable shadowing

On peut écraser une variable en la redéclarant :
- On apprelle ça le "variable shadowing" (éclipser une variable).
- Exemple :
	```rust
	let toto = 5;
	// ...
	let mut toto = "Bonjour";	// Ça compile et la variable toto a changé du tout au tout.
	```
- Utilisé pour convertir une variable sans avoir à en utiliser deux :
	- Exemple :
	```rust
		let a = "5";	// Ailleurs on utiliserait a_str car sous forme de string.
		let a = 5;  	// C'est bon a est sous forme de int comme on veut.
	```

### Les constantes :

Ce sont des variables immutables renforcées :
- Inférence de type interdite. Obligé de préciser le type à la déclaration.
- Obligé de l'initialiser à la déclaration.
- Shadowing interdit.

Le mot-clé **`let`** est remplacé par **`const`**.

Il est conseillé de les écrire en majuscules, et les variables en minuscule.

Exemple :
```rust
let a;
a = 5;                		// Autorisé
let mut a: u64 = 3000;		// Autorisé
const A:i32 = 5;         	// Autorisé
const A;              		// Interdit
A = 5;                		// Interdit
const A = 5;          		// Interdit
const A:u32;          		// Interdit
const A:u64 = 3000;   		// Shadowing interdit
```

### Les variables statiques :

Variables qui existent du début à la fin du programme. Ce sont les "variables globales".

Syntaxe : `static GVAR: Type = valeur;`

### Assignation d'une variable

Qu'est-ce qu'il se passe quand on assigne une variable ?
- On prend la valeur et on l'assigne à la variable de nom "mavar".
	```rust
	let mavar = 5;	// On assigne la valeur 5 à la variable mavar.
	```
- Si on "assigne une variable", alors on assigne une copie (shallow) de celle-ci dans la variable :
	```rust
	let x = 5;	// On assigne la valeur 5 à la variable x.
	let y = x;	// On assigne une copie de la valeur de la variable x à la variable y.
	```

TODO.

### Les différents types de base des variables :

Rust n'essaye pas de convertir automatiquement, donc attention d'avoir le bon type.

Les variables de ces types là sont stockées dans la pile.

#### Les entiers :

Syntaxe : `/^(u|i)\d+$/` : i ou u puis le nombre de bits.
- `i` pour entier signé.
- `u` pour entier non signé.
- Nombre de bits autorisés : `8` (byte), `16` (short), `32` (int), `64` (long), `128` (long long).
- Exemples :
	- `i32` : entier 32-bits (int).
	- `u32` : entier 32-bits positif (unsigned int).

`isize` : "i-sizeof(int)" :
- Sur les systèmes 32 bits, isize == i32.
- Sur les systèmes 64 bits, isize == i64.
- Pareil avec les unsigned et usize.

Ecritures selon la base :
- Décimal : classique
- Décimal avec séparateurs pour les milliers : _
- Binaire : `0b<nombre en base 8>`
- Octal : `0o<nombre en base 8>`
- Hexadécimal : `0x<nombre en base 16>`
- Ecriture spécial u8 : `b<char dont le code ASCII est la valeur désirée>`
- Exemple avec 1000. Tout est pareil :
	```rust
	let a = 1000;
	let a = 1_000;
	let a = 0b1111101000;
	let a = 0o1750;
	let a = 0x3e8;
	```

On peut rajouter le type après sauf pour u8. Par exemple :
```rust
let itbe = 456i32;  	// int itbe = 465;
let itbe: i32 = 456;	// Pareil
```

#### Les flottants (nombres à virgule) :

Pareil que les entiers, mais avec f :
- **`f32`** : "float".
- **`f64`** : "double".

#### Les booléens :

Type **`bool`**, pouvant prendre les valeurs **`true`** ou **`false`**.
```rust
let b:bool = false;
```

Possèdent une méthode `then()` permettant d'enchaîner pour récupérer une valeur si true.
- `.then()` prend en paramètre une closure sans arguments mais renvoyant un valeur.
- `.then()` retourne une Option<T> :
	- `Some(<résultat de la closure>)` si le booléen est true.
	- `None` si le booléen est false.

#### Les caractères :

Type **`char`**, avec le caractère entre simple quotes :
```rust
let mut c:char = '@'
```

Ecriture Unicode : '\u{nombre en Hexadécimal sur 4 ou 6 chiffres}'
```rust
let ccedilmaj = '\u{00E7}';
let ccedilmaj = 'Ç';   		// Pareil
```

Possède une taille de 4 octets.

En général on préfère travaillé avec des u8 plutôt que des char pour les caractères.

#### Les chaînes de caractères :

##### Type de strings

Trois types de strings dans Rust :
- Les strings literals (abrégées SL) : des strings écrites en dur dans le code.
	- Type **`(&'static) str`**
- Objets de type `struct std::string::String`.
- Les string slices : caractère contigus d'une String.
	- Les string slices sont de type **`str`**.
- Ici on parle des string slices et des SL (mais surtout des string slices).

#####  Points communs entre string literals et string slices

Les SL sont des string slices (ensemble d'éléments contigus d'une collection).

Pourquoi les SL sont-elles souvent de type **`&'static str`** ?
- `str` parce que ce sont des string slices, donc de type `str`.
- `&` parce que référence sur une `str`.
- `'static` parce que tout le temps valides, donc valables "pendant tout le lifetime `'static`".
	- Utile s'ils font référence à une valeur écrite en dur dans le programme.

##### Utilisation des strings

Les caractères des strings sont encodés en UTF-8.

Ils s'écrivent entre double quotes.

Leurs valeurs sont immutables.
- Cf. les méthodes qui sont toutes "en lecture (seule)" dans la documentation.
- Traduction : il n'est pas possible de faire un truc de ce genre :
	```rust
	let s = "vite";
	s[0] = 'b';     	// s == "bite"
	"vite"[0] = 'b';	// "vite" devient "bite".
	```


Déclaration avec type :
```rust
let s: &str  = "la string";
```

Conversion en String via la méthode méthode `to_string()`:
```rust
let titi = "toto";
let s = titi.to_string();
let s = "toto".to_string();		// Pareil
```

Ecriture des caractères Unicode comme pour les `char`:
- Syntaxe: `\u{code Unicode}`
- Le code Unicode possède 4 shiffres hexadécimaux.
```rust
let s: &str = "Ç";
let su: &str = "\u{00E7}";		// Pareil
```

Longueur : méthode `len()`

Possède beaucoup de méthodes de String. Cf. la doc.

#### Les tableaux :

Ici ce sont des tableaux de tailles fixe.
- Si on veut plus de dynamisme, il faut considérer les vecteurs, de type `Vec<T>`.

On peut préciser un type : `[T;n]` avec :
- `T` : type des éléments du tableau.
- `n` : nombre fixe d'éléments. Doit être un const de type usize.
- Ecriture entre crochets.
- Les éléments sont séparés par des virgules.
- Exemple :
	```rust
	let chien:[char;5] = ['c', 'h', 'i', 'e', 'n'];
	// EN C/C++ : char chien[5] = ['c', 'h', 'i', 'e', 'n'];
	const CSIZE:usize = 5;
	let chien:[char;CSIZE] = ['c', 'h', 'i', 'e', 'n'];	// Pareil
	```

Tous les éléments d'un tableau doivent avoir le même type.

Accès "classique" à un élément via `[]` :
- On commence à 0.
- Pas d'accès alambiqué avec des indices négatifs comme en Python.

Répéter un motif N fois :
```rust
let arr = [motif; N];
```

#### Les tuples :

Association de plusieurs valeurs.

Ecriture entre parenthèses et séparées par des virgules.
- Type associé : `(type0, type1, ... , typeN)`

Accès aux valeurs du tuple :
- Opérateur point : val.index
- On commence à 0.
- On est obligé d'utiliser la valeur numérique -> pas de constante.

##### Destructuration

On peut capturer les valeurs du tuple dans plusieurs variables :

Syntaxe :
```rust
let (a0, a1, ..., aN) = tuple;
```

##### Exemple 

```rust
// ℝ² couple = (3,14 ; 1,414); en mêlant maths et C++.
let couple:(f64,f64) = (3.14, 1.414);

let (prem, sec) = couple;
let prem = couple.0;     	// Pareil
let sec = couple.1;      	// Pareil
const ZERO:usize = 0;
let prem = couple.ZERO;  	// Ne marche pas
```

#### Unit :

C'est le "type void", appelé aussi "nil". C'est une sorte d'équivalent Rust du java.lang.Void de Java.

Il n'a qu'une seule valeur, notée `()`.

Il est retourné par les fonctions qui ne retournent rien.


## Les opérations :

### Mathématiques

Opérateurs classiques :
- `+` : addition.
- `-` : soustraction.
- `*` : multiplication.
- `/` : division.
- `%` : modulo.

-Rust possède également les opérateurs classiques "op=" du type `var = var op N`:
- Ce sont : `+=`, `-=`, `*=`, `/=` et `%=`.
- Par contre pas de "`++`" ni de "`--`".

Les opérateurs peuvent être surchargés en implémentant des traits de `std::ops` :

| Signe | Trait | Méthode |
|--|--|--|
| **`+`** | `Add<T>` | `fn add(self, T) -> T` |
| **`+=`** | `AddAssign<T>` | `fn add_assign(&mut self, T);` |
| **`-`** (soustraction) | `Sub<T>` | `fn sub(self, T) -> T` |
| **`-=`** | `SubAssign<T>` | `fn sub_assign(&mut self, T);` |
| **`*`** | `Mul<T>` | `fn mul(self, T) -> T` |
| **`*=`** | `MulAssign<T>` | `fn mul_assign(&mut self, T);` |
| **`/`** | `Div<T>` | `fn div(self, T) -> T` |
| **`/=`** | `DivAssign<T>` | `fn div_assign(&mut self, T);` |
| **`%`** | `Rem<T>` | `fn rem(self, T) -> T` |
| **`%=`** | `RemAssign<T>` | `fn rem_assign(&mut self, T);` |
| **`-`** (opposé d'un nombre) | `Neg<T>` | `fn neg(self, T) -> T` |

### Logique

Du classique :

| Opération logique | Opérateur combinatoire | Opérateur bit à bit |
|--|--|--|
| **AND** | **`&&`** | **`&`** |
| **OR** | **`\|\|`** | **`\|`** |
| **NOT** | **`!`** | **`!`** |
| **XOR** | **`^`** | **`^`** |

`&&` et `||` sont lazy, donc pas d'exécution si le premier booléen détermine le résultat :
- AND : Si a vaut false dans a && b, alors OSEF de b.
	- Si b est une fonction alors elle ne sera pas exécutée.
- OR : Si a vaut true dans a || b, alors OSEF de b.
	- Si b est une fonction alors elle ne sera pas exécutée.
- Equivalents en fonctions :
	```rust
	// a() & b()
	fn busy_a_and_b(a: fn() -> bool, b: fn() -> bool) -> bool {
		let res_a: bool = a();
		let res_b: bool = b();
		res_a & res_b
	}

	// a() && b()
	fn lazy_a_and_b(a: fn() -> bool, b: fn() -> bool) -> bool {
		if a() == false { false } else { b() }
	}

	// a() | b()
	fn busy_a_or_b(a: fn() -> bool, b: fn() -> bool) -> bool {
		let res_a: bool = a();
		let res_b: bool = b();
		res_a | res_b
	}

	// a() || b()
	fn lazy_a_or_b(a: fn() -> bool, b: fn() -> bool) -> bool {
		if a() == true  { true } else { b() }
	}
	```

Les bitwise sont des opérateurs bit à bit :
- Ce sont des opérations logiques dans le cadre de booléens car en théorie les bool sont sur 1 bit (true ou false).
- Dans le cadre de nombres entiers, c'est du bit à bit sur les écritures en base 2.
	- Exemples :
		- `25 & 13 = 11001 & 1101 = 11001 & 01101 = (1&0)(1&1)(0&1)(0&0)(1&1) = 01001 = 1001 = 9`
		- `25 | 13 = 11001 | 1101 = 11001 | 01101 = (1|0)(1|1)(0|1)(0|0)(1|1) = 11101 = 29`
		- `25 ^ 13 = 11001 ^ 1101 = 11001 ^ 01101 = (1^0)(1^1)(0^1)(0^0)(1^1) = 10100 = 20`
		- `!25_u8 = !11001 = !00011001 = (!0)(!0)(!0)(!1)(!1)(!0)(!0)(!1) = 11100110 = 230u8`
- Il existe également les opérateurs assign pour les bitwise : **`&=`**, **`|=`** et **`^=`**.

Les bitwises peuvent être surchargés en implémentant des traits de `std::ops` :

| Opérateur bitwise | Trait | Méthode |
|--|--|--|
| **`&`** | `BitAnd<T>` | `fn bitand(self, T) -> T` |
| **`&=`** | `BitAndAssign<T>` | `fn bitand_assign(&mut self, T);` |
| **`\|`** | `BitOr<T>` | `fn bitor(self, T) -> T` |
| **`\|=`** | `BitOrAssign<T>` | `fn bitor_assign(&mut self, T);` |
| **`^`** | `BitXor<T>` | `fn bitxor(self, T) -> T` |
| **`^=`** | `BitXorAssign<T>` | `fn bitxor_assign(&mut self, T);` |
| **`!`** | `Not<T>` | `fn not(self, T) -> T` |

### Comparaison

Du classique :
- **`==`**: égal.
- **`!=`** : différent.
- **`<`** : inférieur à.
- **`<=`** : inférieur ou égal à.
- **`>`** : supérieur à.
- **`>=`** : supérieur ou égal à.

Ils peuvent être surchargés en implémentant des traits de `std::cmp` :
- `==` et `!=` : Eq (équivalence) et PartialEq (équivalence partielle)
	- `Eq<T>` : équivalence
		- Opérateurs == et !=
	- `PartialEq<T>` : équivalence partielle
		- Equivalence sans réflexivité (a == a).
		- Méthode eq() renvoyant un `bool` : `true` si égaux, `false` sinon
- `<`, `>`, `<=` et `>=` : Ord<T> (ordre total) et PartialOrd<T> (ordre partiel)
	- `Ordering` est une enum de std::cmp pouvant prendre les valeurs Less, Equal ou Greater.
	- `Ord<T>` : ordre total.
		- Méthode `cmp()` renvoyant un `Ordering`.
	- PartialOrd<T> : ordre pariel.
		- Méthode `parital_cmp()` renvoyant un `Option<Ordering>`.
		- On renvoie :
			- `Some(Ordering)` si on a un ordre.
			- `None` si on ne peut pas comparer.

## Les conditions :

### Boucle if :

Syntaxe complète :

```rust
if cond0 { /* ... */ }
else if cond1 { /* ... */ }
else if cond2 { /* ... */ }
// ...
else if condN { /* ... */ }
else { /* ... */ }
```
Les else if et le else sont facultatifs comme ailleurs.

Les parenthèses autour de la conditions sont facultatives.

La condition est une valeur booléenne.
- Corollaire : on peut mettre une expression retournant un booléen en guise de condition.
- Exemple :
	```rust
	let a = 45;
	if {2*a < 100} { println!("a est à moitié vide"); }
	if 2*a < 100   { println!("a est à moitié vide"); } // Pareil
	```

Les blocs de codes exécutés selon les conditions sont appelés des bras ("arms")

Une boucle if est une expression. Du coup, on peut faire des ternaires avec les boucles if :
- Le else sera obligatoire.
- On peut complexifier la ternaire avec des else if.
- Tous les bras de code de la boucle if doivent :
	- Être des expressions.
	- Retourner des valeurs du même type.
- Exemple :
	```rust
	let a = 46;
	let a_pair = if a%2 == 0 { true } else if a%2 == 1 { false } else { "Peut-être" };
	```

### Boucle switch (match) :

On détourne le match de Rust.

Syntaxe :
```rust
match a {
	p0 => doP0(),
	p1 => doP1(),
	p2 => doP2(),
	// ...
	pI => doPI(),
	// ...
	pN => doPN(),
	_ => doDefault()
}
```

Chaque ligne est appelée arm ("bras", comme pour les if). Ça correspond à un "case" avec un "break;" à la fin.

Les pI sont des patterns. Le programme exécute le doPI() si le pattern est vérifié.
- Exemple :
	```rust
	match a {
		1 => println!("a=1"),
		2 => println!("a=2"),
		3 => println!("a=3")
	}
	```
- doPI() peut être remplacé par un bloc de code :
	```rust
	match a {
		1 => println!("a=1"),
		2 => println!("a=2"),
		3 => {
			println!("C'est 3.")
			println!("a=3");
		}
	}
	```
- Le match peut aussi permettre de retourner une valeur :
	```rust
	let parite: std::String = match a {
		1 => "C'est pair"
	}
	```
- Une boucle match est une expression => pseudo-ternaires possibles.
	```rust
	let a = match var {
		p0 => expr0,	// expr0 == expression ou fonction qui retourne qqch.
		p1 => expr1,	// expr1 == expression ou fonction qui retourne qqch.
		// ...
		pN => exprN, 	// exprN == expression ou fonction qui retourne qqch.
		_ => def_value
	}
	```
- _ à la fin permet d'implémenter un default.

TODO

## Les boucles :

### Interruptions volontaires de boucles :
- **`break`** : on sort de la boucle.
	- On peut se servir de break pour retourner une valeur en sortant de la boucle.
		- Syntaxe : `break val;`
- **`continue`** : on retourne au début de la boucle.

### Boucle loop :

Boucle infinie.

Syntaxe :
```rust
loop {
	// Code exécuté à l'infini
}
```

C'est une expression. On peut retourner la valeur en sortant de la boucle avec un `break`:

```rust
let mut cpt = 0;

let vingt = loop {
	cpt++;

	if cpt == 5 {
		break 20
	}
}
```

### Boucle while :

Boucle "tant que"

Syntaxe :
```rust
while cond {
	// Code exécuté tant que la condition est vraie
}
```

La boucle while N'EST PAS une expression contrairement aux boucles if et loop. Donc pas de `break val;`

### Boucle for :

Boucle foreach.

Elle sert à passer en revue les éléments d'une collection, via un itérateur Java-style.

Syntaxe :
```rust
for elt in collection.iter() {
	do_sthg(elt);
}
```

Accès aux index possible Grâce à la méthode enumerate() :
```rust
for (index, elt) in collection.iter().enumerate() {
	do_sthg(index, elt);
}
```

On peut aussi simuler une boucle for classique avec des range :
- Syntaxe : `a..b` crée un tableau d'entiers de a inclus à b exclu avec un step de 1.
```rust
println!("Les éléments de a..b : [a; b[");
let mut cpt = a;
while cpt < b {
	println!("{}", cpt)
	cpt += 1;
}
```
`a..b` est un objet de type struct std::ops::Range.

Cf. la partie "les itérateurs" pour + de détails.

Exemples de boucles for :
```rust
const LGA:usize = 4;
let a: [u8;LGA] = [0, 1, 3, 8];

println!("Itération simple");
let mut index = 0;
for elt in a.iter() {
	println!("a[{i}] = {e}", i = index, e = elt);
	index += 1;
}

// Pareil
println!("Itération avec range");
for index in 0..LGA {
	println!("a[{i}] = {e}", i = index, e = a[index]);
}

// Encore pareil
println!("Itération avec enumerate()");
for (index, elt) in a.iter().enumerate() {
	println!("a[{i}] = {e}", i = index, e = elt);
}
```

### Boucle répéter :

N'existe pas stricto sensu en Rust comme il peut exister des `do...while` dans d'autres langages.

Il existe cependant un "hack" :
```rust
while {foo(); cond} {}
```

Explication:
- On utilise une expression comme condition du while.
- L'expression de la condition exécutera toujours le contenu de la boucle.
- Si la valeur de retour de la condition est fausse, la boucle s'arrête.
- Pas de traitement dans le bloc de code du while, tout se fait dans l'expression servant de condition.

### Les boucles nommées :

Équivalent Rust des "goto".

On peut donner des noms aux boucles loop, while et for.

Syntaxe: `'nom_boucle: boucle`

Utilité : revenir au début et pouvoir arrêter une boucle supérieure.
```rust
let mut cpt = 0;
'toto_loop: loop {
	cpt += 100;

	'ouiaille: while cpt > 75 {
		cpt -= 1;

		if cpt == 90 {
			continue ouiaille;	// On recommence la boucle while
		}
		else if cpt == 80 {
			break toto_loop;	// On sort du loop.
		}

		cpt *= 2
	}
}
```

## Les boucles let :

Elles servent à éliminer les bras d'un match où il ne se passe rien.
- C'est avant tout du sucre syntaxque.
- Très utiles dans le cadre d'enums.

### Boucle if let :

Signification : si var peut emprunter ce bras de match, alors on exécute le code "if". Sinon on exécute le code "else".
Syntaxe :
```rust
// a0, a1, ... aN valeurs d'un même type.
if let a0 = var {
	// Code iflet0
}
else if let a1 = var {
	// Code iflet1
}
// ...
else if let aN = var {
	// Code ifletN
}
else {
	// Code else
}

// Equivalent match
match var {
	a0 => /* code iflet0 */,
	a1 => /* code iflet1 */,
	// ...
	aN => /* code ifletN */,
	_ => /* code else */,
}
```

L'avantage n'est pas visible dans la syntaxe, mais :
- `match` doit couvrir toutes les valeurs possibles, quitte à devoir mettre "aI => ()," ou "_ => ()".
- `if let` permet de faire sauter les bras "en => ()," comme "aI => ()," ou "_ => ()".

Exemple :
```rust
enum Chiffre { One, Two, Three, Four, Five, Six, Seven, Eight, Nine }

fn display_if_one_or_three_match(x: Chiffre) {
	match x {
		Chiffre::One => println!("One"),
		Chiffre::Three => println!("Three"),
		// or _ => (),
		Chiffre::Two => (),
		Chiffre::Four => (),
		Chiffre::Five => (),
		Chiffre::Six => (),
		Chiffre::Seven => (),
		Chiffre::Eight => (),
		Chiffre::Nine => (),
	}
}

// Idem en un peu plus court avec un default
fn display_if_one_or_three_match_bis(x: Chiffre) {
	match x {
		Chiffre::One => println!("One"),
		Chiffre::Three => println!("Three"),
		_ => ()
	}
}

// Fait la même chose.
fn display_if_one_or_three_iflet(x: Chiffre) {
	if let Chiffre::One = x {
		println!("One");
	}
	else if let Chiffre::Three = x {
		println!("Three");
	}
}
```
		
### Boucle while let :

Sert à faire sauter des break dans une boucle loop encapsulant un match.

Signification :
- Tant que la valeur peut emprunter ce bras de match, boucler.
- Plus vulgairement parlant, on boucle tant que la valeur match un pattern donné.

Syntaxe :
```rust
while let a = x {
	// Code while let
}

// Equivalent
loop {
	match x {
		a => /* Code while let */,
		_ => break,
	}
}
```

Utilisé principalement avec des `Option<T>`, où l'on boucle tant que Some :
```rust
while let Some(i) = opt {
	do_sthg(i, &mut opt);
}

// Equivalent
loop {
	match opt {
		Some(i) => do_sthg(i, &mut opt),
		None => break,
	}
}
```

## Les patterns :

TODO

## Les fonctions :

### Syntaxe 

Mot-clé **`fn`**.

Pas besoin qu'elle soit déclarée avant de l'utiliser.

Syntaxe minimale : 
```rust
// Fonctions retournant quelque chose
fn my_func(/* args */) -> type_ret {
	// Code de la fonction
	return sthg
}

// Fonctions qui ne retournent rien
fn my_func(/* args */) -> () {
	// Code de la fonction

	return ()
}

fn my_func(/* args */) {
	// Code de la fonction
}
```

Rust encourage le snake_case plutôt que le camelCase pour les noms de fonctions.

### Arguments :

Une fonction peut prendre des arguments.

Syntaxe : `nom_var0: type_var0, nom_var1: type_var1, ... , nom_varN: type_varN`

Exemple :
```rust
fn displayNb(i: isize, x:f64) {
	println!("Voici un entier : {}", i);
	println!("Voici un flottant : {}", x);
}
```

Les arguments sont shallow copiés.
- **Attention à l'Ownership si on ne passe pas de références qui borrowent.**
	- Ils sont drop à la fin de la fonction, de son scope.
	- Il faudra les retourner pour ne pas invalider ou causer des erreurs de compilations.

### Valeur de retour :

Une fonction peut retourner des choses.

Type de sortie :
- Syntaxe : `-> type_ret` après les arguments

Indication de ce qu'on retourne :
- Mot clé **`return`**

Syntaxe : `return val;`
- Le ';' final n'est pas obligatoire.
- Le `return` n'est pas obligatoire si:
	- Le moment où l'on retourne la valeur est la dernière instruction de la fonction.
	- Il n'y a pas de ';' après l'ultime instruction (sinon l'instruction est un statement et ne retourne rien).
	- Exemples :
		```rust
		fn truc(x: isize) -> isize { return x; }	// OK
		fn truc(x: isize) -> isize { return x } 	// OK
		fn truc(x: isize) -> isize { x }        	// OK
		fn truc(x: isize) -> isize { x; }       	// KO
		```

### Les pointeurs de fonctions :

Permettent à des variables de pointer sur des fonctions.

Syntaxe du type : `fn(t0, t1, ...tN) -> type_ret`
- Ils sont "de type fn".
- `t0`, `t1`, ... `tN` sont les types des arguments.
- `type_ret` est le type de retour de la fonction.

On peut appeler une fonction par le nom de son pointeur.
```rust
fn troisieme_valeur(v: &Vec<usize>) -> Option<usize> {
	v.get(2)
}

let v:Vec<usize> = vec![0, 5, 746, 89];
let third_val: fn(&Vec<usize>) -> Option<usize> = troisieme_valeur;
let vfr: Option<usize> = troisieme_valeur(&v);		// vfr == Some(746)
let ven: Option<usize> = third_val(&v);       		// ven == Some(746)
```

### Les closures (fermetures) et les fonctions anonymes :

Une closure est une fonction pouvant accéder à des paramètres non-globaux hors du scope de son exécution.
- Ne pas confondre closures et fonctions anonymes (lambdas) en théorie.
- On dit que la closure peut "capturer" son environnement. Pas la fonction.

Exemple :
```rust
fn main() {
	let x: f64;
	let y: f64;
	let z: f64;

	// Ok car la closure a le droit d'utiliser ce qu'il y a dans le main(), parce que c'est une closure.
	let cl = |t| t+x+y/z;

	// KO car la fonction n'as pas le droit d'utiliser ce qu'il y a dans le main(), parce que c'est une fonction.
	fn func(t: f64) -> f64 {t+x+y/z}

	// Rectification pour que ça soit OK
	fn func2(t: f64, x: f64, y: f64, z: f64) -> f64 {t+x+y/z}

	// En mêlant la fonction correcte dans une closure
	let clo = |t| func2(t, x, y, z);

	let a = cl(5);
	let a = func2(5, x, y, z);	// (presque) pareil
	let a = clo(5);           	// (presque) pareil
}
```

En Rust, une fonction anonyme est une closure ne capturant rien.

Syntaxe presque comme une fonction :
- Syntaxe :
	```rust
	let mavar = |arg0: type0, arg1: type1, ... , argN: typeN| -> type_ret { /* Corps fonction */ };
	mavar(arg0, arg1, ... , argN);
	```
- On n'oublie pas le ';' final car on a assigné quelque chose à une variable (ici une fonction).

Rust est capable de deviner les types tout seul, donc pas besoin de toujours les lui préciser.
```rust
let f = |x, y| { x + y };
let fres = f(0_i64, 85i64);	// Rust va deviner que f implémente "Fn (i64, i64) -> i64"

// Pareil
let f: fn (i64, i64) -> i64 = |x: i64, y: i64| { x + y };
let fres: i64 = f(0_i64, 85i64);
```

On peut omettre les accolades si l'expression n'est composée que d'une seule instruction :
```rust
let f = |x, y| x + y;
let f = |x, y| { x + y };  // Pareil
```

Si la closure est une fonction anonyme, on peut lui donner le type d'un pointeur de fonction :
- Syntaxe du type : `fn(type0, type1, ..., typeN) -> type_ret`

```rust
let mavar: fn(type0, type1, ..., typeN) -> type_ret = |arg0: type0, arg1: type1, ... , argN: typeN| -> type_ret {
	/* Code expression */
};
```

Une closure n'a rien de générique. S'il y a conflit sur le type alors il y aura erreur de compilation. Si on veut faire de la généricité alors il faudra passer par une fonction générique.
```rust
// KO
let id = |x| x;
id(0.0f32);	// id est de type fn (f32) -> i32
id(false); 	// id serait finalement de type fn (bool) -> bool ?! STAHP !

// OK
let id_float = |x| x;
let id_bool = |x| x;
id_float(0.1259f32);	// id_float est de type fn (f32) -> i32
id_bool(false);   	// id_bool est de type fn (bool) -> bool. Tout va bien.

// Compromis à base de variable shadowing
let id = |x| x;
id(0.0f32);    		// id est de type fn (f32) -> i32. OK.
let id = |x| x;		// Variable shadowing sur la variable id.
id(false);     		// Le nouveau id est de type fn (bool) -> bool. Tout va bien.

// Généricité. La seule, l'unique, la vraie.
fn idgen<T>(x: T) -> T {x}
idgen(0.0f32);
idgen(false);
```

#### Closure, traits Fn* et Ownership :

Une closure implémente des traits de `std::ops` parmi les 3 suivants : `Fn`, `FnMut` ou bien `FnOnce`.
- Les pointeurs de fonctions implémentent ces traits là.

Les 3 traits agissent sur l'Ownership :
- `Fn` : la closure capture son environnement avec des emprunts immuables (&).
	- Les closures Fn sont des closures n'agissant pas sur leur environnement ou ne capturant rien.
- `FnMut` : la closure capture son environnement avec des emprunts mutables (&mut).
	- Les closures FnMut peuvent modifier des variables qu'elles capturent.
	- FnMut est un supertrait de Fn : les closures Fn sont aussi des closures FnMut.
- `FnOnce`: la closure n'est appelable qu'une seule fois.
	- FnOnce est un supertrait de FnMut : les closures FnMut (et donc les closures Fn aussi) sont aussi des closures FnOnce.
		- Parce qu'une closure pouvant être appelée plusieurs fois ne peut qu'être appelée une seule fois.

#### Comment se passe une capture de variable ?

Si la closure modifie des variables qu'elle capture :
- Les variables capturées qui sont modifiées sont empruntées mutablement (&mut).
- La closure doit être déclarée comme une variable mutable. Exemple :
	```rust
	let mut varenv: isize;
	let mut clo = || varenv +=1;	// clo capture varenv. Elle a une &mut sur varenv
	```
- Conséquence : la closure ayant une &mut sur la variable, la variable ne peut plus être utilisée en dehors de la closure jusqu'à ce que la closure cesse son &mut.

Les variables capturées et non modifiées sont empruntées immuablement (&) ou copiées (si Copy).

On peut forcer la closure à prendre possession de ses captures non Copy en rajoutant move devant.
- Exemple :
	```rust
	let regis: String = String::from("Régis");
	let re1c = move || println!("{r} est un con. Quel con ce {r} !", r = regis);
	// re1c a déménagé regis dans son environnement. regis est désormais invalide, on ne peut plus l'utiliser désormais.
	```

### var_args et autres joyeusetés.

TODO

## Les entrées/sorties :

Module `std::io`.

### Saisie de texte :

`std::io::stdin().read_line(&mavar)` : récupère la saisie dans la variable mavar.
- Renvoie un `std::io::Result`, donc penser à lui mettre une méthode except() pour le cas d'erreur :
```rust
std::io::read_line(&mavar).except("Saisie invalide !")
```

### Récupérer les I/O standard :

- stdin : std::io::stdin()
- stdout : std::io::stdout()
- stderr : std::io::stderr()

### Formatage de strings :

Utilisation de la macro `format!(pattern, autres args)`

Premier argument : une str pattern.
- Les trous à remplir sont notés par des accolades.
	- `{}` : argument non nommé.
	- `{a}` : argument nommé 'a'.

Autres arguments : de quoi remplir.
- Les arguments nommés s'écrivent `nom_arg = val_arg`, pour le trou {nom_arg}.
- Les arguments non nommés s'écrivent `val_arg`.
	- Ils remplissent les trous dans l'ordre où ils sont rentrés :
		- Le premier non nommé va remplir le premier {} de la pattern.
		- Le deuxième non nommé va remplir le deuxième {} de la pattern.
		- Et caetera.
- Ces arguments doivent implémenter le trait (std::fmt::)Display.

Affichage debug :
- Idéal pour afficher des infos pour déboguer.
- On utilise les placeholders `{:?}` ou `{:#?}` dans la string.
	- Le second améliore l'affichage.
	- Il faut que ce qu'on y affiche implémente le trait `std::fmt::Debug`.
		- Ajouter l'annotation `#[derive(Debug)]` donne une implémentation de base pour les structs.

### Affichage de texte :

- Dans la sortie standard : `std::print!()` et `std::println!()` (avec un '\n' à la fin).
- Dans la sortie d'erreur : `std::eprint!()` et `std::eprintln!()` (avec un '\n' à la fin).

Ce ne sont pas des fonctions mais des macros. Elles prennent les mêmes arguments que `format!()`

## Rust et la gestion de la mémoire :

### Rappel : la pile et le tas.

La pile :
- Pile d'appel (de fonctions).
- Fonctionne comme une pile LIFO.
- Contient tout ce qui a une taille fixe et connue. C'est là qu'on aura les variables dont les types sont des types de base.

Le tas :
- Espace peu organisé, genre far-west.
- Dedans, on alloue de la mémoire et on retourne un pointeur sur l'espace mémoire.
- Quand on n'a plus besoin de la mémoire, on la libère.
- Le pointeur est stocké dans la pile.
- Mémoire plus lente à accéder car il faut passer par des pointeurs.

### The Ownership

Rust utilise un système de propriété pour gérer la mémoire : **The Ownership System**. L'Ownership concerne les données dans les variables, pas les variables elle-même.

<strong>
L'Ownership comporte 3 règles :
<ol>
<li>Chaque valeur est liée à une variable qui est son propriétaire (owner).</li>
<li>Chaque valeur n'a qu'un seul propriétaire à la fois.</li>
<li>La valeur est supprimée dès que l'on sort de la portée (scope) de sa variable propriétaire.</li>
</ol>
</strong>

Exemples de la règle 3.
- Quand on sort d'un bloc de code {}, les variables internes de ce bloc sont désormais hors de portée.
- Cf. les lifetimes.

Lorsqu'une valeur change de propriétaire, on dit qu'elle "move" ("déménage").

### Cycle de vie d'une variable 

Naissance : lors de sa déclaration (RAII).

Mort : lorsqu'elle devient invalide.
- Exemples :
	- Fin d'une fonction.
	- Fin d'un bloc de code {} (une expression par exemple).
	- Quand le contenu de la variable est moved.

La variable existe entre ces deux moments là.

Scope d'une variable :
- C'est le moment où la variable est valide.
- Exemples de scopes :
	- Un bloc de code `{ /* ... */ }`.
	- Une fonction. La fin du scope est la fin de l'exécution d'une fonction.

### Cycle de vie d'une valeur :

Naissance : à l'initialisation de sa variable propriétaire.
- Rust utilise le RAII comme en C++ : la valeur est créée à l'initialisation de la variable qui la contient.

Mort : règle n°3 de l'Ownership, lorsqu'on sort du scope de sa variable propriétaire.

Elle existe entre ces deux moments là, tant qu'elle a un propriétaire.

Destruction d'une valeur :
- Typiquement à la fin d'un bloc de code si on ne fait rien.
- Rust appelle un destructeur pour la détruire. Il possède une fonction spéciale pour détruire.
	- C'est la fonction `std::mem::drop()`. C'est en quelque sorte le "destructeur" de Rust au sens C++ du terme.
	- Il faut implémenter le trait `std::ops::Drop` pour pouvoir être détruit par drop.
	- Exemple :
		```rust
		struct MaStruct;
		impl Drop for MaStruct {
			fn drop(&mut self) { /* ... */ }
		}
		//...
		let x: MaStruct;
		//...
		drop(x);
		```
- Les destructions sont récursives.
- Les variables sont détruites dans l'ordre inverse de leur déclaration.

### Copies de valeurs :

Rappel : shallow copy vs. deep copy
- Shallow copy :
	- Copie "bête", champ par champ et bit par bit.
	- On copie l'adresse d'un pointeur, pas le contenu.
	- Il copie les références, càd les adresses mémoires des pointeurs, n'alloue pas une copie du contenu ailleurs.
- Deep copy :
	- Copie "intelligente", où l'on prend soin qu'il n'y ait pas de problèmes avec l'original.
	- On copie le contenu pointé et le nouveau contenu est pointé par un autre pointeur.

Par défaut Rust ne fait que des shallow copy.

Quand une variable est copiée dans une autre, la variable originale est invalidée par défaut.
- Application de la règle n°2 de l'Ownership : un seul propriétaire pour chaque valeur.
- Permet par exemple d'éviter les double free.
- C'est signalé en erreur à la compilation.
- Cette invalidation est appelée move, car on a "déménagé" l'original dans sa copie.
- Exemple :
	```rust
	{
		let s1: String = String::from("bonjour");
		// s1 est le propriétaire du tableau de caractères contenant "bonjour".
		// ...
		let s2 = s1;
		// Les droits sur le tableau contenant "bonjour" ont été transférés de s1 à s2.
		// À partir de maintenant, la variable s1 est invalide.
		// Si s1 est utilisée à partir d'ici ici => erreur de compilation !
		// ...
	}
	// Fin du scope, le tableau de caractères contenant "bonjour" est free avec la destruction de s2.
	```

Deep copy en Rust :
- Il faut implémenter le trait `std::clone::Clone`.
- Le trait Clone fournit une méthode `clone()` permettant de réaliser la deep copy.
- Exemple :
	```rust
	let s1 = String::("toto");
	let s2 = s1.clone();	// deep copy de S1 dans s2 : ça passe.
	let s3 = s1;        	// shallow copy de s1 dans s3 : ça invalide s1 et rustc peut gueuler.
	```

Il reste possible d'être shallow copié sans être invalidé.
- Il faut implémenter le trait `std::marker::Copy`.
- Signifie "on peut copier un objet bit par bit" sans que ça pose problème.
- Les types primitifs l'utilisent.
	- Peu concernés car ce sont souvent des valeurs stockées dans la pile.
- L'implémentation reste soumise à conditions :
	- Il faut aussi implémenter `std::clone::Clone` car c'est le trait parent (supertrait) de Copy.
	- Il faut que toutes les composantes de l'entité `Copy` implémentent aussi `Copy`.
		- Dans une struct, tous les champs doivent être `Copy` pour que la struct le soit aussi.
		- La présence d'un pointeur (dans la pile vers des données dans le tas) ne permettrra pas d'être `Copy`.
			- Si `Copy` bête, il y aura deux pointeurs sur les mêmes données.
				- Violation de la règle n°2 de l'Ownership donc pas possible.
				- Permet d'éviter les double free.
			- Exemple : le tableau de char d'une String.
				- Les champs de la String seront dans la pile.
				- Parmi ces champs, il y aura un pointeur vers le tableau de caractères.
					- L'adresse du pointeur sera dans la pile.
					- Le pointeur pointera des données dans le tas.

### Ownership et fonctions :

Qui dit "appel de fonction", dit "nouvel sommet dans la stack call", et donconc nouveau scope et copies dans la stack.
- Les arguments passés par valeur sont (shallow) copiés.
- Conséquence 1 : tout ce qui n'implémente pas Copy est déménagé dans le nouveau sommet de la pile.
- Conséquence 2 : ils deviennent invalides en dessous, même après le dépilement à la fin de la fonction.

Retour de fonction :
- Les valeurs retournées par une fonction sont copiées dans des variables à l'étage inférieur (futur sommet de la pile).
- Conséquence : la variable retournée par une fonction est moved à l'étage inférieur si non Copy.
- Les valeurs de retour des fonctions sont déménagées à l'étage inférieur au dépilement.

Exemple :
```rust
fn main() {
	let s1 = String::from("section");
	let s2 = yala(s1);
}

fn yala(esse: String) -> String {
	let ronin = String::from("ronin");
	return ronin
	// Fin du scope. esse est drop, son tableau de caractères est free. esse donc invalidée.
}

/*
 * Appel de yala() :
 * - Nouveau sommet dans la pile d'appel.
 * - s1 de l'étage main() est copié dans esse de l'étage yala().
 * - s1 devient invalide dans main() car déménagé dans le esse de yala(), au dessus dans la pile.
 * - Création de ronin à l'étage yala()
 * - À la fin de yala(), esse et ronin sont détruites...
 * - ... mais on retourne ronin, qui est copiée dans s2 en dessous au dépilement.
 * - ronin est de facto déménagée dans s2 avant sa destruction.
 * - s1 ne retrouve pas sa validité car rien ne transfère la valeur de esse à s1.
 */
```

Si on veut garder s1 valide :
```rust
fn main() {
	let s1 = String::from("Sam Ouraï");
	let s2 = yala(s1.clone());	// On passe un clone, donc s1 reste valide
	let (s1, s3) = yala2(s1);	// Un s1 variable shadowed récupère la valeur du s1 originel. Merci les tuples !
}

fn yala(esse: String) -> String {
	let ronin = String::from("ronin");
	return ronin
}

fn yala2(esse: String) -> (String, String) {
	let ronin = String::from("ronin");
	return (esse, ronin)
}

// À la fin de yala2(), la valeur dans esse est déménagée dans l'un des composants du tuple retourné
```

Ainsi, on préfère passer des références comme arguments dans les fonctions car il n'y a pas de transfert de valeurs avec elles. On copie la référence elle-même, pas ce qu'elle référence.

### Les références :

Rust possède le concept de références "à la C++".

Les références permettent de contourner l'Ownership car l'objet référencé ne leur appartient pas.

On dit qu'une référence emprunte une valeur. C'est le concept de Borrowing.
- Corollaire : la Règle n°2 de l'Ownership ne s'applique pas, surtout en cas de shallow copy.

Syntaxes:
- Mot-clé : **`&`**
- Références et variables :
	- Déclaration d'une référence : `&<variable à référencer>`
	- Type d'une référence : `&<type de l'objet référencé>`
	- Exemple :
		```rust
		let x: f64 = 3.14;
		let rx: &f64 = &x;	// Référence sur la variable x.
		```
	- Référence sur tableau d'éléments de type t_tab : `&[t_tab; N]`.

Les références sont immuables par défaut, càd qu'on ne peut modifier ce qu'une réference emprunte.
- On ne peut pas faire n'importe quoi avec ce qu'on emprunte.

#### Passage par référence

On passe un `&<type de l'objet référencé>` par copie.
- Corollaire : passer un shallow copiable par référence ne l'invalide pas car on ne le copie pas.
- Exemple :
	fn main() {
		let s1: String = String::from("grantatakan");
		let r1: &String = &s1;
		let s2 = yala_ref(&s1);	// s1 reste valide
		let s3 = yala_ref(r1);	// Pareil. r1 reste valide car ne possède pas s1 (Règle n°2).
		let r2 = r1;
		let s4 = yala_cp(s1);	// Invalide s1.
	}

	fn yala_cp(esse: String) -> String {
		let ronin = String::from("ronin");
		return ronin
	}

	fn yala_ref(esse_ref: &String) -> String {
		let ronin = String::from("ronin");
		return ronin
	}

#### Référence mutable :

Bien que les références & sont immuables par défaut, les références peuvent être mutables sous conditions.

Pour les rendre une référence mutables, il faut
- Rajouter `mut` : **`&mut`**.
	- Référence mutable de la variable mavar : `&mut mavar`
	- Type de la référence mutable de la variable mavar : `&mut type_mavar`

Il n'y a de référence mutable que sur les variables mutables. On peut toutefois avoir des références immutables sur des variables mutables. Mais jamais l'inverse.

Exemple :
```rust
let mut x: isize;
let rx: &mut isize = &mut x;
```
#### Cycle de vie d'une référence :

Naissance : RAII, à sa déclaration.

Mort : lorsque la variable liée à la référence meure.

La référence vit entre les deux. Le temps qu'elle existe est appelé durée de vie, ou **lifetime**.

La référence ne doit pas survivre ce à quoi elle référence. Il y a une erreur de compilation sinon (borrow checking).

#### Borrow checking

Les emprunts (par référence donc) sont strictement encadrés.

Les emprunts sont aussi vérifiés à la compilation. On appelle ça le **borrow checking**, effectué par un "borrow checker".

Règles d'emprunts :
- Une variable peut être empruntée autant de fois qu'elle le veut par des références immutables.
- Une variable mutable ne peut être empruntée que par une seule référence mutable.
- Une variable mutable ne peut être empruntée par une référence mutable ET une référence immuable en même temps.

#### Contourner le borrow checking :

L'Ownership (ici le "borrowship") est vérifié sur les références classiques à la compilation (borrow checking).
- Problème : peut râler dans certains cas de figure alors qu'il n'y aurait pas de problèmes en pratique.
- Solution : `Cell<T>` et `RefCell<T>`.

`Cell` et `RefCell` permettent de contourner le borrow checking à la compilation.
- Les emprunts sont vérifiés à l'exécution.
- La compilation passe car la `(Ref)Cell` immuable ne change pas bien qu'il y ait des &mut sur leur contenu.
- Le programme panique si les règles d'emprunt sont violées à l'exécution.
- Risk & reward : le développeur a plus de liberté sur son code, mais le programme peut planter plus facilement.
- Permet d'avoir des références mutables sur des variables qui ne le sont pas : c'est l'"interior mutability".

RefCell :
- De son nom complet struct `std::cell::RefCell<T>`.
- Création classique avec `new()`.
- Avoir des références sur le contenu :
	- Référence immuable : `recel.borrow()`
		- Renvoie une structure std::cell::Ref<T>
			- C'est une struct avec une référence parmi les champs.
		- `Ref<T>` implémente Deref, donc on peut travailler sur la variable avec le déréférencement automatique.
	- Référence muable :
		- Renvoie une structure s`td::cell::RefMut<T>`
			- C'est une struct avec une &mut parmi les champs.
		- `Ref<T>` implémente `Deref` et `DerefMut`, donc on peut travailler sur la variable avec le déréférencement automatique.
	- &mut classique : `recel.get_mut();`
	- Travailler sur les références ne modifie pas le `RefCell`, donc OSEF de si son propriétaire est let ou let mut.
	- Rappel : le programme panique si ça viole les règles d'emprunt.
- Peut être utile pour les mocks dans les tests.

Cell :
- De son nom complet struct `std::cell::Cell<T>`.
- Identique à `RefCell`, mais contient une valeur plutôt qu'une référence.
- Méthodes :
	- `get_mut()` pour avoir une &mut dessus.
	- `set()` pour changer la valeur.
	- `get()` pour avoir la valeur si T est Copy.
		- Pas de move donc.

### Les lifetimes :

Rust possède un système de lifetime permettant d'éviter les dangling references (références est une référence qui référence quelque chose de libéré).
- Exemple : fonction qui crée une String et retourne une référence immuable dessus.
	- On récupère une adresse mémoire, mais ce sur quoi elle pointe est free à la fin du scope de la fonction.
	- Problématique : le pointeur qui survit au contenu qu'il pointe.
		- Si on utilise le pointeur après la mort de son contenu alors on aura des problèmes.

Le lifetime d'une référence est le moment où une référence est considérée comme étant valide.

Syntaxe d'un lifetime : `'nom_lifetime`.
- Syntaxe d'une référence valable pendant un lifetime donné : `&'nom_lifetime (mut)`
	- Signification : référence (mutable) valable pendant la durée de vie `'nom_lifetime`.

Par définition, les références `'&nom_lifetime` sont assurées valides pendant toute la durée de vie de `'nom_lifetime`.
- Si elles deviennent dangling pendant `'nom_lifetime`, alors il y aura erreur de compilation.
- Si elles sont utilisées en dehors de `'nom_lifetime`, alors il y aura erreur de compilation.

Code illustration :
```rust
fn main() {
	let r;               	// ---------+-- 'a
							//          |
	{                    	//          |
		let x = 5;       	// -+-- 'b  |
		r = &x;          	//  |       |
	}                    	// -+       |
							//          |
	println!("r: {}", r);	//          |
}                        	// ---------+
```
- r existe pendant la durée de vie 'a.
- r référence une variable qui n'existe que pendant la durée de vie 'b, qui est englobée par la durée de vie 'a.
- r devient une dangling reference à la fin de 'b (mort de x par fin du bloc de code interne).
- r survit à x qu'elle référence.
- Pendant le println!, on utilise r, donc le contenu d'un dangling.

Lifetimes et borrow checking :
- Rust analyse les emprunts par les références à la compilation (borrow checking).
- Rust s'assure que les références sont toujours valides, i.e. "pas dangling".
- Corollaire : Rust devine le lifetime d'une référence à la compilation.
	- Si Rust n'arrive pas à deviner un lifetime, il faut donc le lui préciser.

Utilisation d'un lifetime :
- Un lifetime est utilisé comme une variable générique.
- On précise le lifetime parmi les paramètres génériques.
- Dans une fonction :
	```rust
	fn ma_func<'a, T, U, V>(r1: &'a T, r2: &'a U) -> &'a V {
		 // ...
	}
	```
	- `ma_func()` prend en arguments :
		- Une référence sur une variable de type T qui sera valable pendant le lifetime `'a`.
		- Une référence sur une variable de type U qui sera valable pendant le lifetime `'a`.
	- `ma_func()` renvoie une référence sur une variable de type V qui sera valable pendant le lifetime `'a`.
	- Cela garantit que le résultat restera valide tant que ses paramètres le seront.
		- Utile si le résultat référence des choses passées en arguments.
	- À la compilation, Rust cherchera un lifetime où les trois seront valides de concert.
		- Il prendra le plus petit lifetime des arguments.
		- Par conséquent la référence résultat ne sera valide que pendant ce lifetime.
	- Pas besoin d'utiliser les lifetimes génériques dans la fonction, Rust saura se débrouiller.

#### Lifetimes dans une struct :

Permet de :
- Ne pas posséder certains champs.
- S'assurer que tous les champs empruntés seront valides le temps de la vie de l'objet.

On précise le lifetime parmi les paramètres génériques.
```rust
struct Personne<'a> {
	nom: &'a str,
	prenom: &'a str
}
```

#### Lifetimes dans une enum :

Chaque valeur d'une enum est liée à une struct. ;-)

On précise le lifetime parmi les paramètres génériques.

```rust
enum MonEnum<'a> {
	Val0,
	Val1(&'a str),
	Val2 {
		i: &'a isize
	},
	Val3
}
```

#### Lifetimes dans un bloc d'implémentation :

On précise le lifetime parmi les paramètres génériques.
```rust
struct Personne<'a> {
	nom: &'a str,
	prenom: &'a str
}

impl<'a> Personne<'a> { /* ... */ }
```

#### Lifetime elision :

Ce sont des règles sur les lifetimes des variables d'une fonction. Ce sont ce que le complateur Rust sous-entend en matière de lifetimes.

Elles permettent d'alléger le code en ne spécifiant pas des lifetimes pour tout et n'importe quoi.

Il y a actuellement trois règles :
1. Si un lifetime n'est pas précisé pour une référence en entrée, alors c'est sous entendu être un lifetime générique en plus.
	```rust
	// Quand Rust lit :
	fn mafunc(x: &str, y: &isize, z: &bool) -> f64 { /*...*/ }
	fn mafunc2<'truc>(x: &'truc str, y: &'truc isize, z: &bool) -> f64 { /*...*/ }

	// Il comprend :
	fn mafunc<'a, 'b, 'c>(x: &'a str, y: &'b isize, z: &'c bool) -> f64 { /*...*/ }
	fn mafunc2<'truc, 'a>(x: &'truc str, y: &'truc isize, z: &'a bool) -> f64 { /*...*/ }
	```
2. Si une fonction avec un seul paramètre renvoie une référence et que rien n'est précisé, alors les lifetimes non précisés sont supposés être les mêmes que celui du paramètre d'entrée.
	```rust
	// Quand Rust lit :
	fn mafunc<T, U, V>(x: &T) -> (&U, &V) { /*...*/ }

	// Il comprend :
	fn mafunc<'a, T, U, V>(x: &'a T) -> (&'a U, &'a V) { /*...*/ }
	```
3. Si dans une méthode, si &self (ou &mut self) est présent, alors les références en sortie auront le lifetime de &self.
	```rust
	// Quand Rust lit :
	impl Truc {
		fn mafunc<T>(&self, x: &char) -> (&T, &str) { /*...*/ }
	}

	// Il comprend :
	impl Truc {
		fn mafunc<'a, 'b, T>(&'a self, x: &'b char) -> (&'a T, &'a str) { /*...*/ }
	}
	// Avec la première règle pour le paramètre x.
	```
			
#### Lifetime subtyping :

Pour s'assurer qu'un lifetime survivra à un autre, qu'il finira après lui.

Syntaxe : `'long_lt: 'short_lt`
- Le lifetime `'long_lt` est censé finir après le lifetime `'short_lt`.

#### Lifetime bounds :

Un type peut être composé de références, avec chacune leurs lifetimes respectifs.

On peut mettre une contrainte pour que toutes ces références soient valides pendant un certain lifetime.

Syntaxe : `T: 'a`.
- L'intégralité des composantes d'une variable de type T, dont ses références, seront valides pendant `'a`.

```rust
struct MaStruct<'a, 'b> {
	nom: &'a str,
	idr: &'b u128
}

struct Ref<T: 'c>(&'c T);
// Quand on utilisera Ref<MaStruct>, il faudra que 'c: 'a et 'c: 'b.
```

#### Le lifetime `'static` :

Ce lifetime s'étend du début à la fin de l'exécution du programme. La référence `&'static` sera donc tout le temps valide.

À n'utiliser que si on est vraiment CERTAIN qu'on veut que la référence soit tout le temps valide.

Exemples :
- Exemple 1 :
	```rust
	fn longest(x: &str, y: &str) -> &str {
		if x.len() > y.len() {
			x
		} else {
			y
		}
	}
	// ...
	let result = longest(string1, string2);
	// ...
	```
	- Quant on exécute longest(), Rust ne sait pas si on référencera le contenu de string1 ou bien de string2.
	- Rust ne saura pas quand la référence result va devenir dangling.
		- result sera-t-elle toujours non dangling à la mort de string1 ou string2 ? Impossible de savoir a priori.
		=> Erreur de compilation !
- Exemple 2 :
	```rust
	fn longest<'a>(x: &'a str, y: &'a str) -> &'a str {
		if x.len() > y.len() {
			x
		} else {
			y
		}
	}

	fn main() {
		let string1 = String::from("long string is long");

		{
			let string2 = String::from("xyz");                       	// -+ 'a'
			let result = longest(string1.as_str(), string2.as_str());	//  |
			println!("The longest string is {}", result);            	//  |
		}                                                             	// -+
	}
	```
	- Quand Rust va lire longest(), il va deviner le lifetime générique utilisé ici.
	- Le lifetime du bloc de code interne convient, donc ok il fera office de 'a.

#### Reste

TODO

### Le déréférencement :

On veut pouvoir suivre le contenu d'une "référence".

On "déréference" avec **`*`**, l'opérateur de déréferencement.

Déréferencement d'une référence :
- Pour obtenir le contenu de ce qui est pointé par une référence.
- Syntaxe :
	```rust
	let t:T;
	let reft: &T = &t;
	let valt: T = *reft;
	```

Déréferencement pour un type de données :
- Chaque type de données peut définir une opération de déréférencement s'il le souhaite.
- Le type peut ensuite déréférencer avec **`*`**, l'opérateur de déréférencement.
- Pour pouvoir déréférencer, il faut implémenter les traits `Deref` ou `DerefMut` (dans `std::ops`).
	- Il faut implémenter :
		- Un type "Target" qui est le type de l'élément déréférencé.
		- Une méthode de déréférencement deref(_mut) renvoyant une référence vers l'élément à déréférencer.
	- `Deref` permet de déréférencer immuablement, pour pouvoir récupérer une `&` sur l'élément déréférencé.
	- `DerefMut` permet de déréférencer mutablement, pour pouvoir récupérer une `&mut` sur l'élément déréférencé.
	- Exemple :
		```rust
		struct Container {
			val: bool
		}

		impl Deref for Container {
			type Target: bool

			fn deref(&self) -> &bool { self.val }
		}

		impl DerefMut for Container {
			type Target: bool

			fn deref_mut(&mut self) -> &mut bool { self.val }
		}

		fn main() {
			let c: Container;
			c.val = true;
			let rb: &bool = c.deref();
			let b: bool = *c;
			let b: bool = *rb;          	// Pareil
			let b: &bool = *(c.deref());	// Pareil
		}
		```

Déréférencement automatique :
- Alias "deref coercion".
- Objectif : utiliser les composantes de l'élément déréférencé à travers le type de base.
- Equivalences :
	```rust
	// T: impl Defer
	// L'élément déréférencé est de type U et possède une méthode de type a().
	let t:T;
	let ru: &U = t.deref();
	let u: U = *t;
	t.a();
	ru.a()
	(*ru).a();
	u.a();
	(*t).a();
	```
- Cela évite d'écrire des "méthodes wrapper" :
	```rust
	struct SqrtWrapper { f: f64 }

	impl Deref for SqrtWrapper {
		type Target = f64;
		fn deref(&self) -> &f64 { self.f }
	}

	// Inutile grâce au déréférencement automatique
	impl SqrtWrapper {
		fn sqrt(&self) -> f64 {
			self.f.sqrt()
		}

		fn atan(&self) -> f64 {
			self.f.atan()
		}
	}
	```

### Les slices :

Rust permet d'avoir des références sur des sous-parties de collections contenant des éléments contigus. Ces sous parties sont appelées des slices.

Les slices sont de type `&[T]`.

Obtenir une slice :
- Syntaxe : `let the_slice = &coll[a..b];`
	- `the_slice` est une référence sur la sous-partie des éléments de coll dont les index sont dans l'intervalle [a;b[.
- On peut omettra a si a == 0.
- On peut omettre b si b == coll.len().
- Si la collection change, on peut avoir des erreurs de compilation si la slice a un problème.

Exemples :
```rust
let hw = "Hello world";
let h = &hw[0..5]; 		// "Hello"
let w = &hw[6..11];		// "world"
let m = &hw[2..9]; 		// "llo wor"
let dh = &hw[..7]; 		// "Hello w"
let we = &hw[4..]; 		// "o world"
let all = &hw[..]; 		// "Hello world"
```
Cas des String :
- Il existe un type `str` représentant une slice de string.
- Les slices de String ne sont pas des `&String` mais des `&str` (d'où le type `str`).
- Les string literals sont ainsi des slices de string disponibles pendant toute la durée de vie du programme (`&'static str`).

### Autres structures de gestion de mémoire

#### Les Box

De leur nom complet struct `std::boxed::Box`.

Type servant à allouer des données sur le tas.
- La `Box` est dans la pile mais les données pointées sont dans le tas.

C'est un "pointeur intelligent".

Ownership :
- La Box possède ce qu'elle alloue dans le tas.
- "The root of all evil" : c'est un nid à moves car le contenu pointé changera de propriétaire en cas de (shallow) copie.

Types très proches :
- `String` et `Box<str>`.
- `Vec<T>` et `Box<[T]>`.

Les Box servent à avoir une taille connue pour un type alors que ce ne serait pas possible autrement.
- Parce que Rust a besoin de connaître la taille d'un type à la compilation.

Allouer sur le tas :
- Méthode `Box::new(t:T);`
- La Box alloue sur le tas une variable de type T, sur laquelle elle pointe.
- La box shallow copie la valeur avant de l'allouer sur le tas.
	- Si la valeur n'implémente as Copy, elle sera move dans la Box !
- Création à partir d'un pointeur :
	- Méthode `unsafe` Box::from_raw(*mut T);
	- Syntaxe :
		```rust
		let mut t:T;
		let bt: Box<T> = unsafe { Box::from_raw(&mut t) };

		// Pareil :
		let pt: *mut T = &t;
		let bt: Box<T> = unsafe { Box::from_raw(pt) };
		```

Libérer le tas :
- Box implémente le trait `Drop`.
- Deux étapes :
	- Destruction de l'élément alloué (via son drop).
	- Libération de l'espace mémoire.
- Géré via le compilateur.

Accès à la valeur dans la box :
- Box implémente les traits Deref et DerefMut : on peut donc utiliser l'opérateur de déréferencement.
- deref(_mut) sera une référence sur le contenu de la Box.

TODO

#### Comptage de références :

Références partagées :
- Equivalent Rust du shared pointer de C++ (std::shared_ptr).
	- Pointeurs avec compteurs de références pour une destruction intelligente.
	- Rust privilégiant les références, on partage ici les références.
- On utilise, `Rc<T>`, de son nom complet `struct std::rc::Rc<T>`.
	- C'est un pointeur intelligent qui va comptabiliser les références sur une donnée.
	- T est un type dont la taille est censée être connue (Sized).
	- Struct qui pointera vers une value dans le tas.
- Pour simuler un Ownership multiple.
	- Le `Rc` contient un pointeur (un vrai, pas une référence !) vers les données allouées.
		- L'Ownership s'effectue sur lui et non sur les données pointées.
	- Le pointeur peut être Copy, donc pas de move quand on copie le champ.
		- Le pointeur est shallow copié, donc ça pointera vers les mêmes données.
- `Rc<T>` tient une comptabilité des références sur les données pointées :
	- Lorsqu'on clone un `Rc`, le compteur de références vers le contenu pointé augmente de 1.
	- Lorsqu'un `Rc` est détruit, le compteur de références vers le contenu pointé dimiue de 1.
	- La valeur est détruite et désallouée lorsque le compteur atteint 0.
- Par convention, on utilise plus ici la notation `Rc::methode(&rc)` que la notation `rc.methode()`.
	- Les deux fonctionnent toujours cependant.
- Créer une référence partagée : méthode `new()` :
	```rust
	let rc: Rc<f32> = Rc::new(3.14_f32);
	```
- Augmenter le compteur :
	- On clone la référence :
		```rust
		let rc: Rc<f32> = Rc::new(3.14_f32);
		let rc2 = Rc::clone(&rc);
		let rc3 = rc.clone();
		```

Références faibles :
- Equivalent Rust du weak pointer de C++ (std::weak_ptr).
	- Pointeurs avec compteurs de références pour une destruction intelligente.
	- Rust privilégiant les références, on partage ici les références.
- On utilise `Weak<T>`, de son nom complet `struct std::rc::Weak<T>`.
- `Weak<T>` ne revendique aucune propriété sur son contenu pointé.
	- La 3ème règle de l'Ownership ne s'applique pas si une valeur n'a plus de Weak sur elles.
- On ne peut lire le contenu d'un Weak.
	- Utile car ce que pointe un Weak peut ne plus être valide, avoir expiré.
- Intérêt d'un Weak : éviter les référence cycliques.
	- À un moment ça ne va plus lire le contenu et briser le cycle.
- Transformation d'un Weak en Rc : méthode `upgrade()`
	- Renvoie une `Option` :
		- `Some(Rc)` si c'est OK.
		- `None` si c'est KO.
	- Ne consume pas le Weak.
- Création :
	- Méthode new() :
		- Syntaxe : `let w = Weak::new();`
		- Sans arguments.
	- À partir d'un Rc : méthode `Rc::downgrade()`
		```rust
		let rct = Rc::new("Parce que Toulon rouge !".to_string());
		let mb = Rc::downgrade(&rct);
		let mb = rct.downgrade();	// Pareil
		```

Connaître le nombre de références :
- Tout se fait à partir de Rc
- Nombre de Rc : `Rc::strong_count(&rc);`
- Nombre de Weak : `Rc::weak_count(&rc);`
- La valeur est détruite si et seulement si `strong_count() == 0`
	- OSEF de weak_count().

#### Les pointeurs :

Rust possède des vrais pointeurs "à la C/C++". Ils sont appelés "raw pointers", les vrais de vrai.

Usage conseillé en dernier recours, après :
- Les références.
- Les `Box`.
- `std::rc::Rc`.
- `RefCell` qui peut contourner certaines règles si nécessaire.

La création est safe, mais la manipulation est unsafe.

Création :
- Syntaxe :
	- Pointeurs immuables :
		```rust
		let t:T;
		let ptr_t: *const T = &t;	// Pointeur immuable sur t
		```
	- Pointeurs mutables :
		```rust
		let mut t:T;
		let ptr_t: *mut T = &mut t;    // Pointeur mutable sur t
		```
	- Les pointeurs mutables ne peuvent pointer que sur des variables mutables.
- À partir d'une Box : en déréférençant son contenu :
	```rust
	let t:T;
	let bt: Box<T> = Box::new(t);
	let pt: *const T = &* bt;

	// - Consomme la Box, qui devient invalide (pléonasme)
	// - Le résultat est forcément un pointeur mutable.
	let pt: *mut T = Box::into_raw(bt);
	```

Pointeur null :
- C'est `0` :
	```rust
	let pt_null: *const T = 0 as *const T;
	let pt_null_mut: *mut T = 0 as *mut T;
	```
- Il existe aussi des fonctions `std::ptr::null` et `std::ptr::null_mut` pour les créer :
	```rust
	use std::ptr;
	let pt_null: *const T = ptr::null();
	let pt_null_mut: *mut T = ptr::null_mut();
	```
- Tester la nullité : méthode `is_null()`.

Ownership : OSEF.
- Le pointeur ne prend pas d'Ownership sur ce qu'il pointe.
- Le pointeur n'emprunte pas ce qu'il pointe.

Lecture/écriture du contenu pointé :
- Code **`unsafe`**.
- Attention à la nullité du pointeur.
	- Le code est unsafe donc c'est la responsabilité du développeur et non de Rust.
- 3 possibilités :
	- Déréréfencement du pointeur.
	- Méthode `read()` / `write()` du pointeur.
	- Fonctions `read()` / `write()` de `std::ptr`.
- Accès en lecture : `unsafe read();`
	```rust
	// Lecture
	let t:T;
	let pt: *const T = &t;
	let pt_val: T = unsafe { pt.read() };
	let pt_val: T = unsafe { std::ptr::read(pt) };	// Pareil
	```
- Accès en écriture : `unsafe  write();`
	```rust
	// Ecriture
	let mut t2:T;
	let pt: *mut T = &t2;
	unsafe { *pt2 = newval; }
	unsafe { pt.write(newval); }
	unsafe { std::ptr::write(pt, newval); }
	```
- Références sur le contenu pointé :
	- Méthodes `as_ref()` (`&`) et `as_mut()` (&mut).
	- Là aussi c'est `unsafe`.
- Structure `std::ptr::NonNull` :
	- Océan de sûreté (et encore...) au milieu de l'`unsafe` des pointeurs.
	- Struct gérant un pointeur `*mut` non null.
	- Accès au pointeur via `as_ptr()`.
	- Là aussi on peut obtenir des références, mais c'est quand même `unsafe`.
	- Création :
		- Méthode `new(*mut)` qui renvoie une `Option<NonNull>` :
			- `Some(NonNull)` si le pointeur n'est pas null.
			- `None` sinon.
		- Exemple :
			```rust
			let t:T;
			let nnt: NonNull<T> = NonNull::new(&mut t as *mut T).unwrap();
			let pt: *mut T = nnt.as_ptr();
			```

## Les fichiers :
	- TODO

## Les vecteurs :

Ce sont des tableaux dynamiques, pas forcément de taille fixe. Tous les éléments doivent être de même type.

### Nom

`Struct std::vec::Vec<T>` de son nom complet.
- Le type T doit implémenter le trait Copy, et donc être copiable.

### Création d'un vecteur :

Méthode statique `Vec::new()` :
- Crée un vecteur vide
- Syntaxe : `let v: Vec<T> = Vec::new();`
- On est obligé de préciser un type car Rust ne connait pas a priori le type des éléments de ce vecteur vide.
	- Comme avec les `Option<T>` initialisées à `None`. ;-)

Macro `(std::)vec!` :
- Permet éventuellement d'initialiser le vecteur avec des valeurs.
- Syntaxe : `let v: Vec<T> = vec![v0, v1, ... , vN];`
	- `v0`, `v1`, ... , `vN` sont les valeurs initiales.
	- On met les paramètres entre crochets au lieu des habituelles parenthèses.
- Ne pas oublier le type quand on initialise avec un vecteur vide (`vec![]`).
- On peut initialiser en répétant un motif :
	- Comme les `[T; n]`.
	- Exemple : `let v: Vec<bool> = vec![true; 4];	// [true, true, true, true]`

Méthode statique `::with_capacity()` :
- Essaye de garantir le non-realloc si le vecteur ne dépasse pas cette taille.
- Optimisé pour créer des vecteurs de taille fixe.
- Rust peut alloue pile l'espace mémoire qu'il faut.
	- Il y a réallocation si finalement on dépasse.
- Syntaxe : `let vfixe: Vec<T> = Vec::with_capacity(size);`
	- `size` est la taille fixe, de type usize.

### Accès aux valeurs :

On accède à une référence sur l'élément dans les deux cas.

-Accès via des crochets :
- Syntaxe : `let elt: &T = v[index];`
- Rust panique en cas d'OoB.

Accès ia la méthode `get()` :
- Pas de panique en cas d'OoB mais un None. Merci les Option<T> !
- Syntaxe :
	`let elt_opt: Option<&T> = v.get(index);`
- La méthode ne retourne pas une référence sur la valeur mais une Option<&T> sur elle.
- Equivalents des crochets :
	```rust
	let elt: &T;
	if let Some(val) = v.get(index) {
		elt = val;
	}

	let elt: &T;
	let elt_opt: Option<&T> = v.get(index);
	if elt_opt.is_some() {
		elt = elt_opt.unwrap();
	}
	```
- Prévoir des else quand on récupère la valeur, sinon rustc ne compilera pas, la référence pouvant ne pas être initialisée.

Iteration avec une boucle for :
```rust
for elt in &v {
	// elt == &v[i]
}
```

Longueur d'un vecteur : méthode len() qui retourne un usize.

Référence sur la première valeur : méthode `first()`.
- Retourne la référence dans une `Option<&T>`.
- `first_mut()` si on veut une référence mutable (dans un `Option<&mut T>`).
- Retourne `None` si le vecteur est vide.

Référence sur la dernière valeur : méthode `last()`.
- Retourne la référence dans une `Option<&T>`.
- `last_mut()` si on veut une référence mutable (dans un `Option<&mut T>`).
- Retourne `None` si le vecteur est vide.

Le vecteur est-il vide ? Méthode `is_empty()` qui retourne un bool.

### Modification des valeurs d'un vecteur :

Sur vecteurs mutables.

`push(val)` : ajout de la valeur val à la fin.

`pop()` : retrait de la dernière valeur.
- Retourne une Option avec la valeur dedans :
	- None si le vecteur est vide.
	- Some(val) sinon.

`append(&mut v)` : vide le vecteur v à la fin du vecteur.

`clear()` : vidage du vecteur.

`insert(elt, index)` : insert l'élément à l'index donné.
- Rust panique si OoB.

`remove(index)` : efface la valeur à l'index donné.
- Retourne la valeur effacée.
- Rust panique si OoB.

### Gestion de la mémoire

#### Ownership : 

Le vecteur fonctionne comme une string, avec son array [T; capacity()] interne.

#### Desctruction :

Deux étapes :
- Chaque élément du vecteur est détruit.
- L'espace mémoire est rendu.

### Conversion array <=> vecteur :

De vecteur à array : via des slices :
```rust
let v: Vec<T>;
const SIZE:usize = v.len();
let arr: &[T; SIZE] = &v[..];
let arr: &[T; SIZE] = v.as_slice();
let mut arr: &mut [T; SIZE] = v.as_mut_slice();
```

D'array à vecteur : méthode `extend_with_slice()` :
```rust
let arr: [T; n];
let mut v: Vec<T> = vec![];
v.extend_with_slice(&arr);
```

### Split d'un vecteur :

Méthode `split_at(_mut)(index)`
- Rust panique en cas d'OoB.
- La méthode retourne un tuple avec des références sur les deux vecteurs suivants :
	- Un vecteur avec les valeur d'index `[0 ; index [`
	- Un vecteur avec les valeur d'index `[index ; len() [`
- Utiliser `split_at_mut(index)` pour avoir des références mutables.

## Les strings :

Deux types de strings :
- Les strings literals de type str qui sont des strings écrites en dur dans le code.
- Objets de type struct std::string::String.
- Ici on parle des String.

Les caractères des strings sont encodés en UTF-8.

Les valeurs des String peuvent être mutables. On peut changer des chars si on veut.
- Traduction : il est possible de faire un truc de ce genre :
	```rust
	let s = String::from("vite");
	s[0] = 'b';		// s == "bite"
	```

Possède beacuoup de méthodes identiques à celles de Vec.
- Ils snt +/- bâtis sur le même modèle.

### Instancier une String :

Méthode `new()` : `let s = String::new();`

Convertir une str en String : méthode `from()`
```rust
let toto = "toto";
let s = String::from(toto);
```

### Accès à un caractère :

L'accès via crochets ne marche pas.

L'encodage de travers peut donner des résultats exotiques avec les méthodes get et les slices.

Le plus simple est d'itérer sur les caractères. Exemple :

```rust
let s: String = String::from("...");

match s.chars().nth(index) {
	Some(c) => println!("s[{i}] = {val}", i = index, val = c),
	None => println!("Votre accès était OoB"),
};
```

### Modification d'un caractère :

La méthode traditionnelle à base de crochets et de = ne parche pas.

On contourne via différentes manières :
- Via la méthode `replace_range()` : `s.replace_range(index..index+1, "new char dans une str");`
- Via les méthodes `insert()` et `remove()` :
	- Exemple :
		```rust
		s.insert(index, 'new_char');
		s.remove(index + 1);
		```
	- C'est très gourmand, en O(2n).
	- Attention à la panique si OoB pour l'index.

### Concaténation :

#### Un seul caractère :
	
Via la méthode `push()`
- Syntaxe : `s.push(c);`
- Arguments :
	- `s` est la String mutable.
	- `c` est un char.

#### Une string entière

Via la méthode `push_str();`
- Syntaxe : `s.push(s2);`
- Arguments :
	- `s` est la String mutable.
	- `s2` est une string str.
- Exemple : `s.push_str("la string str");`

Via l'opérateur + :
- Syntaxe : `let s3:String = s1 + &s2;`
- On prend une référence sur la seconde string.
- L'addition déménage (move) la première String. Il vaut donc mieux la cloner :
	```rust
	let s3:String = s1 + &s2;        	// s1 invalide
	let s3:String = s1.clone() + &s2;	// s1 toujours valide
	```
- On peut aussi utiliser la méthode `add()` : `let s3:String = s1.clone().add(&s2);`
	- C'est la même chose car String implémente le trait Add qui implémente l'addition.
		-La méthode add() est la méthode appelée lorsqu'on fait une addition

### Itérer sur une String :

Avec les caractères : méthode `chars()`
- Donne un itérateur sur parcourant les caractères.

Avec les `u8` : méthode `bytes()`
- Itère sur les u8 représentant les chars en mode ASCII.

Attention à l'encodage ! ! ! ! ! !

### Les OsString :

Dans le package `std::ffi`.

String natives à l'OS cible, difficilement compatibles avec les String classiques. Leur comportement peut donc varier d'un OS à l'autre.

Il existe aussi les strings de type `OsStd`, qui sont des slices de `OsString`. `OsStd` est à `OsString` ce que str est à String.

### Ecrire une String sur plusieurs lignes :

On peut écrire une string sur plusieurs lignes.

#### Prise en compte des espaces initiaux :

On va simplement à la ligne. Cela prend en compte le `\n` de fin de ligne, ainsi que tous les espaces depuis le début de la ligne suivante.

```rust
let s = "Une string
   multiligne";
// s == "Une string\n   multiligne"
```

#### Non prise en compte des espaces initiaux :

On rajoute un antislash en fin de ligne. Il n'y a aucune prise en compte du `\n` de fin de ligne ainsi que des espaces initiaux.

```rust
let s = "Une string\
	multiligne";
// s == "Une stringmultiligne"
let s2 = "Une string \
	multiligne";
// s2 == "Une string multiligne"
```

## Les maps :

Type (ou plutôt struct) `std::collections::HashMap<K,V>` :
- K est le type des clés.
	- Il doit implémenter les traits suivants :
		- `Eq` : relation d'égalité, parce qu'il faudra bien comparer les clés pour accéder aux données.
		- `(std::hash::)Hash` : parce qu'on est dans une HASHmap, donc avec des clés hashées.
- V est le type des valeurs.

### Création :

Méthode `new()` :
- Comme pour `Vec<T>`, il faut préciser le type si pas de remplissage ensuite qui permet à Rust de deviner.

Avec une liste de clés et une autre de valeurs :
- Syntaxe :
	```rust
	let clefs: Vec<K>;
	let valeurs: Vec<V>;
	let map: HashMap<_,_> = clefs.iter().zip(valeurs.iter()).collect();
	let map: HashMap<&K, &V> = clefs.iter().zip(valeurs.iter()).collect();
	```
- Arguments :
	- `clefs` est un `Vec<K>` contenant les clés.
	- `valeurs` est un `Vec<V>` contenant les valeurs.
	- On peut mettre `HashMap<_,_>` comme type car Rust saura retrouver les bons types au bon moment.

### Accès aux valeurs :

Via les crochets : `map[key]`

Accès par clé : méthode `get()`
- Syntaxe : `let opt_val: Option<&V> = map.get(key);`
- Retourne une option.

Accès à une entrée de la map : méthode `entry()`
- Syntaxe :
	```rust
	use std::collections::hash_map::Entry;
	let the_entry: Entry<K, V> = map.entry(key);
	```
- `Entry<K, V>` est une enum prenant en compte si une entrée existe (`Occupied`) ou pas (`Vacant`) :
	```rust
	use std::collections::hash_map::Entry;
	let the_entry: Entry<K, V> = map.entry(key);
	match the_entry {
		Occupied(e) => println!("La map a une entrée avec cette clé {k}. Valeur : {v}", k = e.key(), v= e.get()),
		Vacant(e) => println!("La map n'avait pas d'entrée avec cette clé {k}.", k = e.key()),
	};
	```

### Itération :

```rust
// Sur l'ensemble de la map :
for (key, val) in map { /* ... */ }
// Sur les clés :
for key in map.keys() { /* ... */ }
// Sur les valeurs :
for val in map.values() { /* ... */ }
```

### Ajouter des valeurs :

Via la map : méthode `insert();`
- Syntaxe : `map.insert(clé, valeur);`
- Ownership :
	- Les valeurs dont le type implémente `Copy` sont deep-copiés.
	- La map prend l'ownership des non-Copy et les déménage (move) dans la map.
		- Deux solutions : `clone()` ou bien la référence.

Via une entrée : méthode `entry()` :
- Syntaxe : `let entry: Entry<K,V> = map.entry(key);`
- On récupère l'entrée et on travaille dessus.

Deux possibilités une fois l'entrée en map récupérée :
- Méthode `or_insert()` sur l'entry :
	- Syntaxe : `let val_ins: &V = entry.or_insert(val);`
	- Retourne une référence sur la valeur associée à la clé après insertion.
	- N'insère que s'il n'y a pas encore de valeur.
	- Code équivalent :
		```rust
		let entry: Entry<K,V> = map.entry(key);
		let val_ins: &V = match entry {
			Occupied(ent) => ent.get(),
			Vacant(ent) => ent.insert(val),
		};
		```
- Insert sur les données liées à l'entry :
	- Force l'insertion.
	```rust
	let entry: Entry<K,V> = map.entry(key);
	let val_ins: &V = match entry {
		Occupied(mut ent) => ent.insert(val),
		Vacant(ent) => ent.insert(val),
	};
	```
- `map.entry(key).or_insert(val)` est la méthode préférentielle si on ne veut pas écraser la valeur déjà en place.

### Effacer des valeurs :

Vider la map : méthode `clear()`

Récupérer la valeur effacée : méthode `remove()`
- Syntaxe : `let rem_val: Option<V> = map.remove(key);`
- On récupère une Option avec la valeur dedans.

Récupérer l'entrée effacée : méthode `remove_entry()`
- Syntaxe : `let rem_val: Option<(K,V)> = map.remove(key);`
- On récupère une Option avec un tuple (clé, valeur) dedans.

### Méthodes spéciales :

Nombre d'éléments dans la map : `map.len()`

La map est-elle vide ? `map.is_empty()`

La map contient-elle une valeur pour une clé donnée ? `map.contains_key(key)`

## Les ensembles :

Type `HashSet`, ou plutôt "Struct `std::collections::HashSet<T>`".

Se comporte comme une `HashMap<T, ()>`.

### Création :

Méthode statique `new()`, comme souvent.

Méthode `with_capacity(size)`, comme les autres.

À partir d'un array : 
```rust
let set: HashSet<T> = arr.iter().cloned().collect();
```

### Opérations ensemblistes :

#### Union : a ∪ b

Méthode `union()`

Syntaxe : `a.union(&b);`

Retourne un itérateur sur l'union, de type `std::collections::hash_set::Union<T>`.

On récupère l'ensemble via `.collect()` (méthode transformant un itérateur en collection).

```rust
use std::collections::HashSet;
use std::collections::hash_set::Union;

let union_iter: Union<T> = a.union(&b);
let union_set: HashSet<T> = a.union(&b).collect();
```

####  Intersection : a ∩ b

Méthode `intersect()`

Syntaxe : `a.intersect(&b);`

Retourne un itérateur sur l'intersection, de type `std::collections::hash_set::Intersection<T>`

On récupère l'ensemble via `.collect()` (méthode transformant un itérateur en collection).

```rust
use std::collections::HashSet;
use std::collections::hash_set::Intersection;

let inter_iter: Intersection<T> = a.intersect(&b);
let inter_set: HashSet<T> = a.intersect(&b).collect();
```

####  Différence : a - b

Méthode `difference()`

Syntaxe : `a.difference(&b);`

Retourne un itérateur sur la différence, de type `std::collections::hash_set::Difference<T>`

On récupère l'ensemble via `.collect()` (méthode transformant un itérateur en collection).

```rust
use std::collections::HashSet;
use std::collections::hash_set::Difference;

let diff_iter: Union<T> = a.difference(&b);
let diff_set: HashSet<T> = a.difference(&b).collect();
```

#### Tests sur les sensembles

-  Ensemble disjoints ? `a.is_disjoint(&b)`
-  Ensemble vide ? `a.is_empty()`
-  Inclusions d'ensembles :
	- a ⊆ b : a`.is_subset(&b)`
	- b ⊆ a : `a.is_superset(&b)`
	- {val} ⊆ a : `a.contains(&val)`

- Accès aux éléments :
	- Méthode get() comme habituellement.
- Ajout de valeur v : méthode insert() comme ailleurs.
- Retrait d'éléments :
	- Méthode remove() :
		- Syntaxe : a.remove(val)
		- Renvoie un bool si {val} ⊆ a vant le retrait, false sinon.
	- Méthode take() :
		- Syntaxe : a.take(val)
		- Renvoie une Option avec la valeur retirée de dans :
			- Some(val) si {val} ⊆ a vant le retrait.
			- None sinon.
	- Méthode clear() : vidage total.

## Les queues :

De type `std::collections::VecDeque<T>`.

- Marche grossomodo comme les vecteurs, à ceci près que les push et pop sont différents :
	- `push_front()` : `push()` classique.
	- `pop_front()` : `pop()` classique.
	- `push_back()` : `insert(0, )`.
	- `pop_back()` : `remove(0, )`.
	- Retourne toujours des `Option<T>`.

## Les itérateurs :

### Créer un itérateur :

Trois méthodes :
- `iter()` : accès immuable aux variables pointées par l'itérateur (&).
- `iter_mut()` : accès mutable aux variables pointées par l'itérateur (&mut).
- `into_iter()` : l'itérateur prend l'Ownership de ce qu'il pointe (move).

Un itérateur doit être mutable car on va changer son état interne en l'utilisant.

Un itérateur implémente le trait `std::iter::Iterator`.
- Si on veut créer son propre itérateur, il faut juste implémenter `next()`.
	- Le reste a des implémentations par défaut.
- Un itérateur n'est pas `Copy`, donc gare aux moves !
	- Il reste clonable cela étant.

### Réinitialiser un itérateur :

On le recrée via les méthodes iter(), iter_mut() ou into_iter().

### Partir de la fin

Via la méthodeméthode `rev()` :
```rust
	let coll: Coll<E>;
	let coll_iter = coll.iter().rev();

	// coll sera parcourue à l'envers.
	for elt in coll.iter().rev() { /*...*/ }
```

### Opérations sur ce qui est itéré :

#### Méthode `map(FnMut)` :

On applique une closure qui opérera une transformation sur la valeur renvoyée par `next()`. La closure prend en entrée l'élément pointé par l'itérateur.

```rust
let v: Vec<i8> = vec![0, 58, 94, 69];
let mut it = v.iter().map(|x| x%7);
let v0:u8 = it.next().unwrap();		// v0 = 0 car 0%7 == 0
let v1:u8 = it.next().unwrap();		// v1 = 2 car 58%7 == 2
let v2:u8 = it.next().unwrap();		// v2 = 3 car 94%7 == 3
let v3:u8 = it.next().unwrap();		// v3 = 6 car 69%7 == 6
```

Quand on applique plusieurs closures via des map(), alors il y a composition de fonctions et non remplacement :

```rust
let v: Vec<i8> = vec![0, 58, 94, 69];
let f; let g;		// Closures
let it = v.iter.map(f).map(g);
let val = it.next().unwrap();	// g(f(v[0])) == (g ∘ f)(v[0])
```

`map()` est appliquée à partir de la position courante.

#### Méthode zip(Iterator) :

On fait avancer deux itérateurs en même temps.

`next()` renverra alors un tuple avec les valeurs des deux itérateurs.

Attention à ce que ce soit bien aligné ! Si l'un des deux simples finit avant l'autre, alors `next()` renverra `None` dès que le premier aura fini.

```rust
let v1: Vec<i8> = vec![0, 58, 494, 69];
let v2: Vec<i8> = vec![50, 548, 974, 369];
let mut it = v1.iter.zip(v2.iter);
let v0:u8 = it.next().unwrap();		// Some((0, 50))
let v1:u8 = it.next().unwrap();		// Some((58, 548))
let v2:u8 = it.next().unwrap();		// Some((494, 974))
let v3:u8 = it.next().unwrap();		// Some((69, 369))

// Mauvais alignement
let mut it = v1.iter();
let v0:u8 = it.next().unwrap();		// Some((0, 50))
let v1:u8 = it.next().unwrap();		// Some((58, 548))
let v2:u8 = it.next().unwrap();		// Some((494, 974))
let v3:u8 = it.next().unwrap();		// Some((69, 369))
```

#### Méthode filter(FnMut) :

Permet de filtrer les éléments itérés avec une closure.

La closure prend les élément pointé par l'itérateur en entrée et renvoie un bool.

L'itérateur ne s'arrête que sur les éléments dont la closure renvoie true.

L'index renvoyé par `enumerate()` sera celui de la collection filtrée, pas la position dans la collection quand tout le monde est là.

```rust
// Conservation de nombres pairs dans un vecteur dédié.
let v: Vec<usize>;
let v_pair = v.iter().filter(|x| x%2 == 0).collect();
```

#### Méthode collect() :

Une fois que l'on a procédé à nos opérations, on appelle `collect()` pour mettre tout ça dans une collection.

`collect()` part de la position courante et va jusqu'à la fin.

Rust va demander de préciser un type à la variable qui recevra le résultat du `collect()`.

Ownership : `collect()` invalide un l'itérateur sur lequel elle est utilisée.

#### Performances

 Utiliser ces méthodes là est plus rapide qu'une boucle for où l'on appliquerait le traîtement sur chaque élément.

### Opérations sur tout un tableau :

Via la méthode `for_each(FnMut)` :

Ella applique la closure passée en argument sur l'intégralité des éléments de la collection itérée.
- `for_each(drop);` permet de finir de consumer un itérateur.
- `for_each()` invalide son itérateur.

### Itérateur en boucle 

Via la méthode `cycle()`. Il repart du début de la collection quand il arrive à la fin.

Syntaxe : `let mut it = coll.iter().cycle();`

### Itération style Java :

C'est la "vraie itération" de Rust, qui est donc "à la Java".

On se déplace via la méthode `next()` de l'itérateur.

Elle renvoie une `Option` sur l'élément (de la collection) pointé :
- `Some` si l'itérateur pointe encore sur quelque chose.
- `None` sinon.

Les types et comportements sont différents selon la création de l'itérateur :
```rust
// Coll<E> une collection d'élément de type E itérables.
let coll: Coll<E>;

// iter()
let coll_iter = coll.iter();
let coll_next: Option<&E> = coll_iter.next();

// iter_mut()
let coll_iter = coll.iter_mut();
let coll_next: Option<&mut E> = coll_iter.next();

// into_iter()
let coll_iter = coll.into_iter();
let coll_next: Option<E> = coll_iter.next();	// L'élément dans coll est désormais invalide.
```

#### Steps

On a la possibilité d'avancer de plusieurs cases à la fois grâce la méthode `.step_by()` :
- La méthode prend un `usize` en paramètre.
- Quand on appelle `next()`, l'itérateur avancera de plusieurs cases, comme si on l'appelait plusieurs fois sans `step_by()`.

Aller directement au dernier élément : méthode `last()`, qui invalide l'itérateur.

#### Consommation d'un itérateur :

- On dit qu'un itérateur se "consume", ou "est consommé", en utilisant `next()`.
- Un itérateur est "lazy" : il ne fait rien si on ne l'utilise pas, si on n'utilise pas `next()`.
- Les méthodes appelant next() sont appelées "consuming adaptor" car elles consomment l'itérateur.
	- Ce sont souvent des méthodes invalidant l'opérateur en en prenant l'Ownership.

#### Pouvoir deviner "la suite" :
Il faut 2 choses :
1. Rendre l'itérateur "peekable", avec la méthode `peekable()` qui invalide l'itérateur initial.
2. Utiliser `peek()` pour voir la suite.

```rust
let v = vec![10, 5, 9, 45, -12];

// Rendre l'itérateur "peekable"
let mut it = v.iter.peekable();

it.peek();	// Some(10)
it.next();	// Some(10)
it.peek();	// Some(5)
```

#### Equivalent avec une boucle for :

```rust
let coll;	// La collection

// Déclaration de l'itérateur dans le for
for elt in coll.iter() { do_sthg(elt); }

// Déclaration de l'itérateur hors du for
let coll_iter = coll.iter();
for elt in coll_iter { do_sthg(elt); }

// Avec une boucle while let
let mut coll_iter = coll.iter();
while let Some(elt) = coll_iter.next() { do_sthg(elt); }

// Avec une boucle loop
let mut coll_iter = coll.iter();

loop {
	match coll_iter.next() {
		Some(elt) => do_sthg(elt),
		None => break,
	}
}
```

### Itération style C (avec des index)

C'est du style Java déguisé, `next()` restant la méthode d'itération en coulisses.

Il est possible de connaître le nombre d'éléments de l'itération avec la méthode `count()`.

Il est possible d'accéder à un élément d'index précis avec la méthode `nth()` :
- Prend un `usize` en paramètre.
- Renvoie une `Option` :
  - `Some` avec un élément si quelque chose a été trouvé.
  - `None` sinon.
`nth()` opère sur ce qu'il reste à itérer. l ne revient pas au début à chaque fois, il faut créer un nouvel itérateur pour ça.
```rust
let coll;
let i:usize;
let mut it = coll.iter();
it.nth(i);	// Opère sur l'élément d'index i de la slice &coll[..]

// it opère désormais sur la slice &coll[i+1..]

it.nth(2) // Elément d'index 2 dans &coll[i+1..], donc élément d'index i+1 +2 dans &coll[..]

// Accès correct au second élément via nth().
let mut it = coll.iter();
it.nth(2);	// Opère sur l'élément d'index 2 de la slice &coll[..]
```

Rappel : on peut itérer sur les index avec un range :

```rust
for i in 0..n { /*...*/ }

for i in (0..n).step_by(2) { /*...*/ }	// Ici avec un step de 2.
```
#### Equivalent avec une boucle for :

```rust
let coll;
let len:usize = coll.iter().count();

for i in 0..len {
	if let Some(elt) = coll.iter().nth(i) {
		do_sthg(elt);
	}
}

let mut it = coll.iter();
for i in 0..len {
	if let Some(elt) = it.nth(0) {
		do_sthg(elt);
	}
}
```

## Les structs :

Rust utilise les structs comme en C/C++.

### Création d'une struct :

Syntaxe :
```rust
struct NomStruct {
	field_0: type_0,
	field_1: type_1,
	// ...
	field_N: type_N,
}
```

Les `field_i` sont les noms des champs, tandis que `type_i` est le type de `field_i`.

La virgule finale est facultative, comme dans les arrays PHP.

### Instanciation d'une struct :

Syntaxe :
```rust
let var: NomStruct = NomStruct {
	field_0: val_0,
	field_1: val_1,
	// ...
	field_N: val_N,
};
```

Les "`field_i`" sont les noms des champs, tandis que `val_i` est la valeur du champ `field_i`.

La virgule finale est facultative, comme dans les arrays PHP.

Il faut que la variable soit mutable si on veut toucher ne serait-ce qu'à un champ de la struct.

Si jamais `var.field_i` est initialisé avec une variable nommée `field_i`, alors on peut écrire "`field_i`" au lieu de "`field_i: field_i`". Comme en JSON.

Le CamelCase est conseillé pour les noms de structs, contrairement au snake_case qui est plus pour le reste.

### Accès à un champ :

Syntaxe : `var.field_i`
- `var` : l'instanciation de la struct.
- `field_i` : champ "`field_i`" de la struct.

Si mutable, on a bien sûr `var.field_i = newval;` pour le modifier.

### Remplir une struct avec les données d'une autre :

Il faut que la struct implémente le trait `Default`.

Syntaxe :
```rust
let var1: NomStruct = NomStruct {
	field_0: val_0,
	field_1: val_1,
	// ...
	field_N: val_N,
};
let var2: NomStruct = NomStruct {
	field_a0: val_a0,
	field_a1: val_a1,
	// ...
	field_aK: val_aK,
	..var1
};
```

Les `field_aK` sont les champs de la struct qui seront différents.

Pour tous les autres champs `field_bI`, ce sera "`var2.field_bI = var1.field_bI;`"

### Les tuple structs :

Ce sont des structs sans nom de champs. 

Elles servent à donner du sens à des tuples en leur donnant un type spécial.

#### Création :

Syntaxe : 
```rust
struct TupleStruct (type_0, type_1, /* ... */, type_N);
```

Les parenthèses remplacent les accolades des structs classiques. Il y a un ';' à la fin.

#### Instanciation :

Syntaxe :
```rust
let ts: TupleStruct = TupleStruct (val_0, val_1, /* ... */, val_N);
```

Les parenthèses remplacent les accolades des structs classiques.

#### Unit-like structs :

Tuple struct sans champ. Elles s'écrivent `struct UtStruct(),` tout simplement.

### Ajouts de méthodes à une struct :

C'est ce qui se raproche le plus de la POO dans Rust, qui en fait sans dire qu'elle en fait.

On met les méthodes dans un bloc d'implémentation. Cf. § correspondant pour + de détails.

On peut aussi mettre des pointeurs de fonctions et des closures parmi les champs.

### Déférencement automatique et référencement automatique :

Quand on utilise une référence sur un objet, Rust peut automatiquement déférencer pour accéder au champ ou à la méthode.

Pas besoin d'opérateur "`->`" comme en C/C++, Rust comprendra le "`.`" quand même.

### Ownership :

Les structs aiment bien posséder leurs champs. ;-)

Il est possible toutefois de ne pas posséder certains champs. Les champs non possédés seront alors des références qui empruntent. Il faut alors s'assurer que les références soient toujours valides le temps d'utilisation de l'objet. On utilise donc un lifetime générique pour forcer la chose.

```rust
// Invalide car rien n'assure que self.s1 et self.s2 seront tout le temps valides quand on utilisera l'objet.
struct MaStruct {
	s1: &str,
	s2: &str
}

// Correction avec un lifetime :
struct MaStruct<'a> {
	s1: &'a str,
	s2: &'a str
}
// Au pire 'a sera le temps de la vie de l'objet.
```

### Destruction :

Si la struct implémente `Drop`, alors la méthode drop() est appelée.

Si la struct n'implémente pas `Drop`, alors drop() sera appelé sur chacun des champs.

## Les enums :

Rust utilise les enums à tour de bras.

### Déclaration d'une enum :

Syntaxe :
```rust
enum MonEnum {
	VAL_0,
	VAL_1,
	// ...
	VAL_N
}
```

Il est conseillé de :
- Mettre le nom en CamelCase.
- Mettre les valeurs en SNAKE_CASE et en majuscules.

### Utilisation d'une enum :

Dans le code : `MonEnum::VAL_I`

Dans une fonction : comme une variable de type `MonEnum`.

### Enum et données associées :

On peut associer des données à une enum. 

Chaque valeur possède comme sa propre struct interne pour stocker les données. On peut ne pas avoir la même struct pour chaque valeur de l'enum.

On peut associer aux enums des blocs d'implementation avec des méthodes dedans.

Corollaire : les enums peuvent implémenter des traits.

Exemple :
```rust
enum ArchiveType {
	// Sans données associées
	ZIP,

	// Les données associées sont dans un struct tuple.
	TAR (isize),

	// Les données associées sont dans des structs classiques.
	TARBALL {
		ext_ar: String,
		ext_comp: String
	},
	7Z {
		algo_name: String,
		numero: usize,
		menum: MonEnum	// Parce que pourquoi pas.
	}
}

impl ArchiveType {
	fn message(&self) -> String {
		// code
	}
}

//...
let at:ArchiveType;
//...
at.message();
```

### Lecture des enums :

Se fait via une boucle `match` :

```rust
match enum_var {
	VAL_O => instruction_O,
	VAL_1 => instruction_1,
	// ...
	VAL_I(arg0, ar1, ..., argM) => instruction_I(arg0, ar1, ..., argM),
	// ...
	VAL_N => instruction_N,
	_ => def_ins,	// Default
}
```

Les instructions pourront être des expressions ou des statements, selon le contexte.

### Ajouts de méthodes à une enum :

C'est possible en Rust, comme pour les struct.

On met les méthodes dans un bloc d'implémentation. Cf. § correspondant pour + de détails.

On peut aussi mettre des pointeurs de fonctions et des closures parmi les champs.

Une enum peut aussi implémenter des traits

### Le discriminant d'une enum

Chaque instance d'une enum possède un discriminant, qui est un entier associé à la valeur d'une enum.

Le discriminant est indépendant des valeurs associées à la valeur de l'énumération.

#### Obtenir le discriminant :

Via la fonction `std::mem::discriminant<T>(v: &T) -> std::mem::Discriminant<T>;`
- Elle prend en paramère une référence sur une variable.
- Il est conseillé que la référence soit sur une enum car OSEF si ça ne l'est pas.
- Renvoie une struct de type `Discriminant<T>`.

La struct `Discriminant<T>` est un wrapper opaque autour du discriminant. Elle implémente les traits `(Partial)Eq`.
- Corollaire : deux valeurs sont les mêmes si elles ont le même discriminant.
- Bien sûr on ne peut pas comparer un `Discriminant<T>` avec un `Discriminant<U>`

Exemple :
```rust
use std::mem::*;

enum MonEnum {
	Sympathy(bool, f32),
	MollyGoRound(bool)
}

fn main() {
	let a = MonEnum::MollyGoRound(true);
	let b = MonEnum::MollyGoRound(true);
	let c = MonEnum::MollyGoRound(false);
	let d = MonEnum::Sympathy(false, 0.0);
	let e = MonEnum::Sympathy(false, 3.14);
	let f = MonEnum::Sympathy(true, 0.0);
	let g = MonEnum::Sympathy(true, 3.14);

	let a_b2m4ac: Discriminant<MonEnum> = discriminant(&a);

	assert_eq!(discriminant(&a), discriminant(&b));		// MollyGoRound == MollyGoRound
	assert_eq!(discriminant(&a), discriminant(&c));		// MollyGoRound == MollyGoRound
	assert_ne!(discriminant(&a), discriminant(&d));		// MollyGoRound != Sympathy
	assert_eq!(discriminant(&d), discriminant(&e));		// Sympathy == Sympathy
	assert_eq!(discriminant(&f), discriminant(&g));		// Sympathy == Sympathy
	assert_eq!(discriminant(&d), discriminant(&f));		// Sympathy == Sympathy
	assert_ne!(discriminant(&d), discriminant(&c));		// Sympathy != MollyGoRound
	assert_eq!(discriminant(&b), a_b2m4ac);
}
```

#### Forcer le discriminant :

On peut forcer le discriminant de la valeur d'une enum ssi TOUTES les valeurs de l'enum n'ont pas de données associées.

Syntaxe :
```rust
enum MonEnum { NoDataVal = DISCRIMINANT }
```

DISCRIMINANT est un entier, codé en dur ou dans une `const`.

Type du discriminant :
- On peut forcer un type d'entier en ajoutant l'attribut #[repr(<type_int>)]
	- Exemple :
		```rust
		// Les discriminants de MonEnum seront des u16.
		#[repr(u16)]
		enum MonEnum {SunStroke, Daft}
		```
- Si non précisé, l'entier est de type `isize`.

### Les enums "classiques" (à la C/C++) :

Grâce aux discriminants, on peut créer de tels enums, avec leur lien entre "`enum Truc`" et "`int`".

**Valeur entière de l'enum = discriminant de l'enum.**

L'attribution des valeur est comme en C/C++ :
- Si la première n'a pas de valeur prcisée, alors ce sera 0.
- Si la valeur suivante n'est pas précisée, alors ça sera valeur précédente + 1
- Il y a erreur de compilation si deux valeurs de l'enum ont la même valeur entière.
- Il y a erreur de compilation si arithmetic overflow sur les valeurs entières.
	- Une erreur est vite arrivée avec #[repr(u8)]. ;-)

Elles peuvent quand même implémenter des traits et avoir des méthodes, parce que ce sont des enums Rust après tout.

#### Conversions :

- De enum à int : on caste l'enum en entier.
- De int à enum : on implémente le trait `std::convert::From` dans l'enum.

#### Exemple :

```rust
const SAX: u16 = 100;

#[repr(u16)]
#[derive(Debug)]
enum MonEnum {
	SunStroke = SAX,
	Daft = 101
}

impl From<u16> for MonEnum {
	fn from(n: u16) -> Self {
		match n {
			SAX => MonEnum::SunStroke,
			101 => MonEnum::Daft,
			_ => panic!("Pas de correspondance")
		}
	}
}

fn main() {
	let x = MonEnum::SunStroke;
	println!("x = {:?}", x);
	let xint = MonEnum::SunStroke as u16;
	println!("xint = {}", xint);
	let y = MonEnum::from(101);
	println!("y = {:?}", y);
	let z = MonEnum::from(666);	// Panique !
	println!("z = {:?}", z);
}
```

### L'enum Option<T> :

De son nom complet `std::option::Option<T>`.

Rust n'a pas de valeur "`null`" comme partout ailleurs. Son rôle est endossé par l'enum `Option`.

L'enum `Option<T>` est composée de 2 valeurs :
- `Some(T)` qui est associée à un donnée de type T.
- `None` qui n'est associée à rien. C'est lui "le null de Rust", ou plutôt ce qui s'en rapproche le plus dans du code safe.

On encapsule un résultat potentiellement null dedans, puis on match selon la valeur de l'enum. L'un des intérêts est de :
1. Encapsuler quelque chose dans un `Some()`.
2. Faire des fonctions opérant sur des `Option`, qui retourneront des résultats encapsulés.
3. S'il y a une couille en route, ça passe tranquille à `None` et on peut traîter le problème.

Exemple :
```rust
// Calcul d'un logarithme népérien :
fn ln_float(x: f64) -> f64 {
	x.ln()
}

fn minus_mil_float(x: f64) -> f64 {
	x - 1000
}

fn ln_opt(xopt: Option<f64>) -> Option<f64> {
	match xopt {
		Some(x) => if x > 0 { Some(ln_float(x)) } else { None },
		None => None,
	}
}

fn minus_mil_opt(xopt: Option<f64>) -> Option<f64> {
	match xopt {
		Some(x) => Some(minus_mil_float(x)),
		None => None,
	}
}

fn main() {
	println!("Les logarithmes népériens");

	const X:f64 = 2000f64;
	const Y:f64 = 2f64;

	// Ok parce que x > 1000
	println!("[f64] x == {} > 1000", X);
	let xr:f64 = ln_float(minus_mil_float(X));
	println!("ln({} - 1000) = {}", X, xr);

	// Attention problèmes !
	println!("[f64] y == {} < 1000", Y);
	let yr:f64 = ln_float(minus_mil_float(Y));
	println!("ln({} - 1000) = {}", Y, yr);     // Un NaN sauvage apparait !

	// Safe avec les options.
	println!("[Option] x == {} > 1000", X);
	let xo = Some(X);
	match ln_opt(minus_mil_opt(xo)) {
		Some(xres) => println!("ln({} - 1000) = {}", X, xres),
		None => eprintln!("Un nombre inférieur à 1000 ou une None n'a pas de logarithme ici.")
	}

	println!("[Option] y == {} < 1000", Y);
	let yo = Some(Y);
	match ln_opt(minus_mil_opt(yo)) {
		Some(xres) => println!("ln({} - 1000) = {}", X, xres),
		None => eprintln!("Un nombre inférieur à 1000 ou une None n'a pas de logarithme ici.")
	}

	println!("[Option] ln(None - 1000)");
	match ln_opt(minus_mil_opt(None)) {
		Some(xres) => println!("ln({} - 1000) = {}", X, xres),
		None => eprintln!("Un nombre inférieur à 1000 ou une None n'a pas de logarithme ici.")
	}
}
```

#### Ecriture des valeurs :

Pas besoin d'écrire Option<T>:: devant Some ou None.

Si une variable est associée à None, il faut explicitement préciser le type à Rust :

```rust
let a = Some(5);
let a: Option<usize> = Some(5);		// Pareil
let b: Option<usize> = None;  		// Ok

// Ne marche pas parce que Rust ne peut deviner si c'est un Option<usize> ou un Option<autre chose>
let b = None;
```

#### Utilisation des valeurs :

Rust ne sait pas faire de conversion automatique entre un `Option<T>` valant `Some(T)` et un élément de type `T`.

Il faut donc passer par des options de récupération :
- Un match :
	```rust
	let o: Option<T>;
	let value_if_none: T;
	//...
	let t: T = match o {
		Some(s) => s,
		None => value_if_none,
	};
	```
- Méthode `expect(msg: &str)` :
	- Renvoie la valeur si Some.
	- Panique en affichant le message d'erreur msg si None.
- Méthode `unwrap()` : comme `expect`, mais sans message d'erreur.
- Méthode `unwrap_or(def: T)` :
	- Si `Some` : renvoie la valeur dans le `Some`, 
	- Si `None` : renvoie la valeur `def`.

Exemple :

```rust
fn minus_unsigned(x:usize, y: usize) -> Option<usize> {
	if y > x {
		return None
	}
	else {
		return Some(x-y)
	}
}

let s = minus_unsigned(4,5);
match s {
	Some(1988) => println!("La différence est de 1988. Une bonne année."),	// Cas où la valeur encapsulée vaut 1988.
	Some(d) => println!("La soustraction vaut {}", d),
	None => eprintln!("Le résultat des deux unsigned serait négatif"),
}
let d2:usize = match minus_unsigned(4,5) {
	Some(d) => d,
	None => 0,
};		// d2 == 0

// +/- pareil, à ceci près que ça panique en cas de None :
let ds2 = minus_unsigned(4,5).expect("Le résultat des deux unsigned est négatif");
```

### L'enum Result<T, E> :

De son nom complet `std::result::Result<T, E>`.

Elle sert à gérer les erreurs rattrapables. C'est "le try-catch de Rust".

L'enum possède deux valeurs :
```rust
enum Result<T, E> {
	Ok(T), 		// Enum avec la valeur de type T à retourner en cas de succès
	Err(E),		// Enum avec l'erreur (exception) de type E à retourner en cas d'échec
}
```

`Result<T, E>` possède des méthodes `unwrap()` et `expect(errmsg)` pour récupérer la bonne valeur contenue.

## Les blocs d'implémentation :

Permettent de rajouter des méthodes à une struct ou une enum (entre autres).

Mot clé **`impl`** suivi de ce qu'ils implémentent :
```rust
impl NomToImpl {
	/* Méthodes */

	// "Méthode const"
	fn methode_ro(&self, arg0: type0, arg1: type1, ..., argN: typeN) -> type_ret_mro {
		// code
	}

	// Méthode pas "const"
	fn methode_rw(&mut self, arg0: type0, arg1: type1, ..., argM: typeM) -> type_ret_mrw {
		// code
	}

	// Méthode associée
	fn methode_static(arg0: type0, arg1: type1, ..., argP: typeP) -> type_ret_ms {
		// code
	}

	// Méthode consumante
	fn methode_ownership(self, arg0: type0, arg1: type1, ..., argQ: typeQ) -> type_ret_mo {
		// code
	}

	// Méthode consumante avec self mutable
	fn methode_ownership_mut(mut self, arg0: type0, arg1: type1, ..., argR: typeR) -> type_ret_momut {
		// code
	}
}
```

Le premier argument des méthodes s'appelle `self`. C'est l'objet lui-même.
- Il doit toujours être en premier.
- On ne lui donne pas de type. Rust le retrouvera tout seul (pour une fois).
- On peut passer l'objet, ou bien une référence (mutable ou pas).
	- Il s'agit de respecter l'Ownership et que l'objet ne soit pas invalidé par sa méthode.
	- `&self` : référence immuable sur l'objet : la méthode loue.
	- `&mut self` : référence mutable sur l'objet : emprunt immutable.
		- L'emprunt se termine à la fin de l'appel de la fonction.
	- Avec `self` "sans `&`" la méthode prend le contrôle de l'objet.
		- L'objet étant déménagé dans la fonction, il devient invalide à la fin de la méthode.
		- On dit que la méthode "consume" l'objet.
		- Il est éventuellement penser à retourner quelque chose ça peut être utile. ;-)
		- mut self si on a besoin que l'objet soit mutable.
	- Sans `self` c'est une méthode statique.

On peut utiliser plusieurs blocs `impl{}` au lieu d'un seul, comme les blocs namespace en C++.

On peut utiliser les méthodes avec un opérateur de portée. Cf. exemple.

Dans les boucles impl{}, il existe le type "`Self`" qui est le type de ce qui est implémenté.

### Méthodes associées :

Alias méthodes "static", du moins c'est comme ça qu'on les appelle en C/C++/Java/...

"Méthodes sans self" car elles n'utilisent pas une instanciation particulière de la struct.

Utilisation : `NomStruct::methode_static(arg0, arg1, ..., argP);`

### Exemple :

```rust
struct NomStruct { /* ... */ }

impl NomStruct {
	fn methode_ro(&self, arg0: bool, arg1, ..., argN) { /* ... */ }

	// Méthode pas "const"
	fn methode_rw(&mut self, arg0, arg1, ..., argM) { /* ... */ }

	// Méthode associée, dite "statique"
	fn methode_static(arg0, arg1, ..., argP) { /* ... */ }

	// Méthode consumante
	fn methode_ownership(self, arg0: type0, arg1: type1, ..., argQ: typeQ) { /* ... */ }

	// Méthode consumante avec self mutable
	fn methode_ownership_mut(mut self, arg0: type0, arg1: type1, ..., argR: typeR) { /* ... */ }

	fn ref_ns(&self) -> Self { &self }
}

let x: NomStruct = NomStruct { /*...*/ };
let mut y: NomStruct = NomStruct { /*...*/ };
x.methode_ro(arg0, arg1, ..., argN);
x.methode_rw(arg0, arg1, ..., argM);            // rustc va râler car x immutable.
y.methode_ro(arg0, arg1, ..., argN);
y.methode_rw(arg0, arg1, ..., argM);
x.methode_ownership(arg0, arg1, ..., argQ);		// x devient invalide après cette instruction.
let x: NomStruct = NomStruct { /*...*/ };       // Rebirth

// Pareil "en statique" :
NomStruct::methode_ro(&x, arg0, arg1, ..., argN);
NomStruct::methode_rw(&mut x, arg0, arg1, ..., argM);	// rustc va râler car x immutable.
NomStruct::methode_ro(&y, arg0, arg1, ..., argN);
NomStruct::methode_rw(&mut y, arg0, arg1, ..., argM);
NomStruct::methode_static(arg0, arg1, ..., argP);
```

## Les traits :

Un trait est un ensemble de fonctions définissant un comportement.
- Ce sont des fonctions abstraites, à implémenter par le composant qui l'utilisera.
- Un peu comme une interface au sens Java du terme

La librairie standard fournit de nombreux traits pour de nombreux comportements. Il vaut mieux peut-être regarder ce qu'offre `std::` avant de créer un trait à soi.

### Déclaration :

Syntaxe :

```rust
trait MonTrait {
	type LeTypeDuTrait;

	fn func_0(&self, arg00 :type_arg_00, arg10 :type_arg_10, ..., argN0 :type_arg_N0) -> type_ret0;
	fn func_1(&self, arg01 :type_arg_01, arg11 :type_arg_11, ..., argN1 :type_arg_N1) -> type_ret1;
	// ...
	fn func_m(&self, arg0m :type_arg_0M, arg1M :type_arg_1M, ..., argNM :type_arg_NM) -> type_retM;

	fn func_def(&self, arg0M :type_arg_0M, arg1M :type_arg_1M, ..., argNM :type_arg_NM) -> type_retM { /* ... */ }
}
```

On garde le premier argument pour l'objet qui utilisera le trait.

Pas de bloc de code pour les fonctions à implémenter, mais seulement un ';'.

Il est toutefois possible de fournir des implémentations par défaut. Elles peuvent être redéfinies dans les implémentations.

On peut définir des types génériques dans le trait, qu'il faudra spécialiser à l'implémentation.
- Rappel : `Self` est déjà pris. C'est le type de l'objet qui implémentera.

### Implémentation du trait :

On implémentevec un bloc d'implémentation :

```rust
struct|enum Truc { /* ... */ }

impl MonTrait for Truc {  // Obligatoire pour signifier qu'on implémente le trait.
	type LeTypeDuTrait = TrucOuPas;
	fn func_0(&self, arg00 :type_arg_00, arg10 :type_arg_10, ..., argN0 :type_arg_N0) -> type_ret0 { /* ... */ }
	fn func_1(&self, arg01 :type_arg_01, arg11 :type_arg_11, ..., argN1 :type_arg_N1) -> type_ret1 { /* ... */ }
	// ...
	fn func_M(&self, arg0M :type_arg_0M, arg1M :type_arg_1M, ..., argNM :type_arg_NM) -> type_retM { /* ... */ }
}
```

Si le trait n'a que des implémentations par défaut et qu'on les utilise toutes, alors le bloc impl sera vide.

### Confidentialité :

Elle est la même pour toutes les méthodes du trait. Si le trait est `pub` alors toutes ses méthodes le seront.

La confidentialité dans l'implémentation est la même que celle du trait. Si le trait est `pub` alors ses méthodes le seront aussi dans l'implémentation.

### Orphan rule

Un type ne peut pas implémenter des traits qui sont extérieurs à son crate.

Par exemple, on ne peut pas faire implémenter des traits perso à des types de la librairie standard.

### Utiliser un trait dans une fonction :

On peut spécifier d'un argument d'une fonction qu'il implémente un trait plutôt que de spécifier un type.

Syntaxe :

```rust
fn func_arg(arg_tr: impl Montrait) -> type_ret { /* ... */ }
fn func_ret(/* ... */) -> impl MonTrait { /* ... */ }
```

On peut spécifier qu'une fonction retourne un objet implémentant un trait pluôt qu'un type en particulier.
- C'est comme si on utilisait le type "`impl MonTrait`".
- Les objets retournés devront toutefois être tous de même type, de la même implémentation du trait.
- Exemple :
	```rust
	trait MonTrait {
		fn un() -> i8 { 1u8; }
		fn this_ref(&self) -> &(impl MonTrait) { &self }
	}

	struct S { a: char }
	enum E {E1, E2}

	impl MonTrait for S {}
	impl MonTrait for E {}

	// Ok car on retourne une struct S dans tous les cas.
	fn get_struct(isa: bool) -> impl MonTrait {
		if isa {
			S { a: 'a' }
		} else {
			S { a: 's' }
		}
	}

	// Ok car on retourne une enum E dans tous les cas.
	fn get_enum(is1: bool) -> impl MonTrait {
		if is1 {
			E::E1
		} else {
			E::E2
		}
	}

	// KO car on ne retourne pas tout le temps un S ou un E bien que les 2 implémentent MonTrait.
	fn get_mt(is_struct: bool) -> impl MonTrait {
		if is_struct {
			S { a: '0' }
		} else {
			E::E1
		}
	}
	```

On peut spécifier plusieurs traits via des +.
- Exemple : 
	```rust
	fn func_arg(arg_tr: impl Montrait + MonTrait2) -> type_ret { /* ... */ }
	```

### Les traits bounds :

C'est l'utilisation des traits dans les génériques, un peu comme le `<T extends U>` de Java.

Cela permet de donner des indications supplémentaires sur le type générique.

Syntaxe :
```rust
fn func_montrait<T: MonTrait>(t: T) { t.func_0(); }
```

L'implementation de plusieurs types marche :
```rust
fn func_montrait<T: MonTrait + MonTrait2>(t: T) { t.func_0(); }
```

Dans un bloc d'implémentation, on ne met le trait bond qu'après impl :
```rust
impl<T: MonTrait> MaStruct<T> {}
```

Pour plus de lisibilité, on peut mettre le trait bond à la fin de la déclaration de la fonction/structure/énumération/impl :
- Se fait via une "clause where" (au sens SQL SELECT du terme).
```rust
fn func_montrait<T>(t: T) -> bool where T: Montrait { t.func_0(); true }

struct MaStruct<T> where T: MonTrait {}

enum MonEnum<T> where T: MonTrait {}

impl<T> MaStruct<T> where T: MonTrait {}
```

### Les blankets (mais pas de veau) :

Elles servent à implémenter un trait à partir d'un autre.

Syntaxe : 
```rust
impl<T: MonTrait> Veau for T { /*...*/ }
```

On implemente automatiquement le trait "Veau" pour toutes les struct/enum qui implémenteront MonTrait.

### Les traits objects :

Problèmes vus précédemment :
1. Une fonction retournant `impl MonTrait` doit retourner des objets du même type.
2. Le type implémentant un trait bound est lui aussi fixe.
	- Exemple : `Vec<T: MonTrait>` est un vecteurs d'éléménts tous du même type.

Les traits objects sont la solution à ces problèmes.

Un trait object permet de qualifier des objets dont le type implémente un trait.

Syntaxe : `dyn MonTrait`
- Indique que "le type de l'objet implémente Montrait".

Le type précis n'est connu qu'à l'exécution du programme. On appelle ça le "dynamic dispatch".
- D'où le mot clé **`dyn`** signifiant " **dyn**amic dispatch ".

Un trait object rempli deux condition :
1. Il n'utilise pas de paramètes génériques.
2. Les méthodes ne retournent pas des objets de type Self.

#### Utilisation :
- Problème : on a besoin de types fixes à la compilation.
- Solutions :
	- Allocation sur le tas, avec des `Box<dyn MonTrait>` (ou autres pointeurs intelligents).
	- Utilisation de références (&dyn MonTrait).

#### Exemple

```rust
trait Upload {
	fn upload(&self);
}

struct AudioUploader { /* ... */ }
impl Upload for AudioUploader { /* ... */ }

struct VideoUploader { /* ... */ }
impl Upload for VideoUploader { /* ... */ }

struct UploaderGroup {
	uploaders: Vec<Box<dyn Upload>> // Uploaders de tous types
}

struct UploaderGroupSame<T: Upload> {
	uploaders: Vec<T> // Les éléments doivent tous avoir le même type
}
```

### Reste ?

TODO

## Les modules Rust :

Un module est un peu comme un namespace C++.
- Il contient divers matériels de code : variables, fonctions, enums, structs, sous-module...
- Mais contrairement au Namespace, le module est fermé et n'expose que ce qu'il veut exposer.

### Créer un module :

On utilise le mot-clé **`mod`**.

```rust
mod my_mod {
	// Contenu du module
}
```

Il est conseillé d'écrire son nom de module en snake_case au lieu du CamelCase.

### Chemin (Path) d'un composant de module :

- Suite des noms de modules avec des opérateurs de résolution de portée (**`::`**) au milieu.

Le composant parent est désigné par **`super`**.
Le composant où l'on se situe est désigné par **`self`**.

Il y a deux types de chemins :
- Chemin absolu : part du module racine, nommé **`crate`**.
- Chemin relatif : à partir du module où l'on se situe.
	
Exemple :
```rust
mod m1 {
	pub mod m2 {
		enum E21{ E210, E211 }

		pub fn f() {
			super::g();
		}
	}

	pub fn call_m2f() {
		// Chemin absolu
		crate::m1::m2::f();

		// Chemins relatifs
		m2::f();
	}

	fn g() {}
}

fn main() {
	// Chemins absolus
	crate::m1::call_m2f();
	crate::m1::m2::f();

	// Chemins relatifs
	m1::call_m2f();
	m1::m2::f();
}
```

### Accessibilité d'un contenu :

En Rust les contenus d'un module sont privés par défaut. Pour les rendre publics, il faut ajouter le mot clé **`pub`** devant.

#### Règles de vie privée dans l'arborescence des modules :

1. Un module frère est accessible, mais pas son contenu qui n'est pas marqué `pub`.
2. Idem avec un module enfant.
3. Le contenu d'un module parent est accessible.

#### Cas des struct :

Tous les champs d'un struct sont privés par défaut. Il faut marquer les champs comme **`pub`** pour les rendre public.

Initialisation :
- Pour initialiser avec le constructeur, il faut que tous les champs soient accessibles.
- D'où l'intérêt d'une méthode statique publique, souvent nommée `new`, pour bien initialiser publiquement.

```rust
mod m1 {
	pub struct Esse {
		pub cpub: bool,
		cpriv: usize,
	}

	impl Esse {
		pub fn new(c_pub: bool, c_priv: usize) -> Esse {
			Esse {
				cpub: c_pub,
				cpriv: c_priv,
			}
		}
	}

	pub fn f() {
		// KO car le champ cpriv est privé.
		let s: Esse = Esse {
			cpub: false,
			cpriv: 27
		};

		// Tout OK car f est soeur de la struct Esse.
		let s: Esse = Esse {cpub: true, cpriv: 45};
		let s: Esse = Esse::new(true, 485);
	}
}

fn main() {
	use crate::m1::Esse;

	// Ok car Esse::new() est publique.
	let s: Esse = Esse::new(true, 945);

	// KO car cpriv n'est pas accessible à crate::main().
	let s: Esse = Esse {cpub: true, cpriv: 545};
}
```

#### Cas des enum :

Si une enum est publique, alors toutes ses valeurs le sont aussi.

#### Cas d'un bloc d'implémentation :

On ne parque pas le bloc comme publique mais ce qu'il implémente (struct ou enum).

Ne sont publiques que les méthodes marquées comme `pub`.

Est valable pour les struct et les enum.

#### Gros exemple :

```rust
mod m1 {
	mod m2 {
		pub fn g() {}
	}

	struct MaStruct {
		pub chp_public: isize,
		chp_prive: Option<bool>,
	}

	impl MaStruct {
		pub fn pub_func(&self) {}
		fn priv_func() {}
	}

	pub struct MaStruct2 {
		pub chp_public: f32,
		cpriv: Option<i8>,
	}

	pub struct MaStruct3 {
		pub c1: bool,
		pub c3: bool,
	}

	pub enum Color { Red, Blue, Green }

	impl Color {
		fn tostri(&self) -> Option<&str> {
			match self {
				Color::Red => Some("rouge"),
				Color::Blue => Some("bleu"),
				Color::Green => Some("vert"),
				_ => None,
			}
		}

		pub fn to_str(&self) -> Option<&str> {
			match self {
				Color::Red => Some("rouge"),
				Color::Blue => Some("bleu"),
				Color::Green => Some("vert"),
				_ => None,
			}
		}
	}

	pub fn f() {
		// Tout OK car au même niveau.
		let ms1: MaStruct {chp_public: 3_14, chp_prive: None};
		ms1.pub_func();
		MaStruct::priv_func();
		m2::g();
	}
}

fn main() {
	m1::f();
	m1::m2::g();	// Erreur de compilation car crate::m1::m2 est privé pour crate::main(), et donc sa fonction g() aussi.

	// RAII OK car tous les champls de m1::MaStruct3 sont publics
	let ms3 = m1::MaStruct3 {c1: true, c3: true};

	// KO car le champ cpriv est privé
	let ms2: m1::MaStruct2 = m1::MaStruct2 {
		chp_public: 2.07,
		cpriv: Some(26)
	};

	let ms1: m1::MaStruct;

	// KO car méthode privée.
	MaStruct::priv_func();

	let c: m1::Color = m1::Color::Green;
	println!("{}", c.to_str());
	println!("{}", c.tostri()); 		// KO car méthode privée.
}
```

### Importer un chemin dans le scope d'un module :

On utilise le mot-clé **`use`** :
```rust
use path::to::module::content;
// On peut ainsi utiliser "content" sans avoir à le préfixer par "path::to::module::".

// content peut aussi être un module.
```

#### Alias :

On peut donner un surnom au chemin importé grâce au mot-clé **`as`**.

```rust
use path::to::module as ptm;
use path::to::module::content as ptmc;

// Sont pareils :
path::to::module::content::subcontent;
ptm::content::subcontent;
ptmc::subcontent;
```

#### pub use :

Sert à importer un module à partir d'un autre.

```rust
mod m0 {
	pub mod m00 {
		pub mod m000 {
			pub fn f() {}
		}
	}
}

mod m1 {
	pub use m0::m00::m000;
}

// ...
m0::m00::m000::f();
m1::m000::f();		// Pareil
```

Autre exemple d'utilisation : inclusion d'un composant non module dans un fichier séparé (cf. plus bas).

#### Regropupements de use :

On peut écrire plusieurs `use` en une seule ligne, inclure plusieurs composants d'un module.

Ils s'appellent les nested paths.Ils sont écrits entre accolades et séparés par des virgules.

On peut inclure le module de base en le nommant par self.

```rust
use m0;
use m0::m00;
use m0::m01;
use m0::m02;

// Pareil que les 4 lignes au dessus.
use m0::{self, m00, m01, m02};
```

On peut inclure tous les éléments publics d'un module en utilisant une wildcard :

```rust
mod m_root {
	mod m0 {}

	pub fn f1() {}

	pub mod m2 {}

	pub struct S3;

	struct S4;

	pub enum E5 {}
}
// Les imports suivants sont tous identiques :

// Imports simples
use m_root::f1;
use m_root::m2;
use m_root::S3;
use m_root::E5;

// Nested paths
use m_root::{f1, m2, S3, E5};

// Wildcard
use m_root::*;
```

### Ecrire un module dans un (des) fichier(s) séparé(s) :

Si un module est défini ailleurs, on utilise une forward declaration :

```rust
mod my_mod;	// Module my_mod défini ailleurs
```

Les noms de module respectent des règles :
- Le module my_mod est défini par l'un des deux fichiers suivants :
	- my_mod.rs
	- my_mod/mod.rs
- Si on veut externaliser un sous-module :
	- Dans un fichier au nom du module dans le dossier du sous module.
	- Exemple : le sous-module foo du module my_mod est dans l'un des 2 fichiers :
		- my_mod/foo.rs
		- my_mod/foo/mod.rs
- Externalisation d'un composant du module (struct, enum...) :
	- Combinaison d'un sous-module fantôme et d'une pub use.
	- On écrit la struct (ou l'enum, ou ...) dans un sous-module "fantome" qui ne sera jamais exposé publiquement.
	- On rapatrie la struct (ou l'enum, ou ...) dans le module de base avec un `pub use`.

Exemple :
- Fichier main.rs
	```rust
	mod sound;		// Le module sound est défini dans sound.rs

	// Equivalent :
	mod sound {
		// Contenu de sound.rs
	}

	fn main() {
		sound::instrument::ocarina();
	}
	```
- Fichier sound.rs, qui pourrait aussi être sound/mod.rs :
	```rust
	pub mod instrument {
		pub fn ocarina() {}
	}

	pub mod musician;	// Ecrit dans sound/musician.rs

	// Inclusion de la struct crate::sound::ConcertHall dans le module
	mod concert_hall;	// Dans le fichier Rust contenant la struct dans un "module fantôme"

	// pub use qui fait que sound::concert_hall::ConcertHall et sound::ConcertHall sont la même chose
	pub use concert_hall::ConcertHall;
	```
- Fichier sound/concert_hall.rs :
	```rust
	pub struct ConcertHall { /* ... */ }

	// Ses implémentatons
	```

### Gros exemple :

```rust
mod m0 {
	pub mod m00 {
		pub fn f000() { println!("f000"); }
	}
}

mod m1 {
	use crate::m0;
	pub fn f10() {
		println!("f10");
		m0::m00::f000();
	}
}

mod m2 {
	use crate::m0::m00;
	pub fn f20() {
		println!("f20");
		m00::f000();
	}

	pub mod m21 {
		pub fn f210() { println!("f210"); }
	}
}

mod m3 {
	pub fn f30() {
		println!("f30");
		super::m0::m00::f000();
	}
}

mod m4 {
	use super::m0;
	pub fn f40() {
		println!("f40");
		m0::m00::f000();
	}
}

use m2 as other_m2;
use m2::m21;
use m2::f20;
use m2::m21::f210;

fn main() {
	use m1::f10 as ta_race;
	ta_race();
	m1::f10();
	f20();
	other_m2::f20();
	m2::f20();
	f210();
	m21::f210();
	m2::m21::f210();
	m3::f30();
	m4::f40();
}
```

## Programmation générique :

On utilise un type générique et inconnu dans la fonction, qui sera remplacé selon les besoins.

Fonctionne comme les templates en C++.
- Il compile pareil : la compilation implémente les générics que pour les combinaisons de types utilisées dans le code.

### Déclaration :

On les déclare entre `<>`.
- Syntaxe : `<T0, T1, ..., TN >`
- Les `TI` sont des types inconnus que l'on peut utiliser le temps de la déclaration.

Dans une fonction :
```rust
fn name_func<T>(arg0: T, ...) {
	// On a un nouveau type nommé "T" à disposition le temps de la fonction.
	le tval:T;
	// Du code avec tval
}
```

Dans une struct :
```rust
struct MaStruct<T> { /* ... */ }
```

Dans une enum :
```rust
enum MonEnum<T> { /* ... */ }
```

Dans un trait :
```rust
trait MonTrait<T> { /* ... */ }
```

Dans un bloc d'implémentation :
- Syntaxe :
	```rust
	impl<T0, T1, ... TN> TrucToImpl<T0, T1, ... TN> { /* ... */ }
	```
- On précise les types au nom ET après le impl.

On peut créer des méthodes spéciales en précisant certains types.
- La syntaxe reste la même, à ceci près qu'on retire le bloc `<>` devant `impl` si on n'a plus de types génériques.
- Marche aussi avec les traits bonds.
- Exemple :
	```rust
	struct Truc<T> { /* ... */ }

	impl<T> Truc<T> {
		fn f() {}
	}

	impl Truc<bool> {
		fn b() {}
	}

	impl<T> Truc<T> where T: MonTrait {
		fn truc_mt() {}
	}

	// ...
	let t: Truc<String>;
	let tb: Truc<bool>;
	t.f(); 		// OK
	t.b(); 		// KO car b() n'est définie que pour T == bool.
	tb.f();		// OK
	tb.b();		// OK
	```

### Utilisation

On remplace le type générique par le vrai type.

Dans la plupart des cas, Rust sera capable de deviner tout seul.

 S'il faut préciser des types, on rajoute "`::<types à utiliser>`" après le nom de la fonction.
- `::<T>` s'appelle "l'opérateur turbofish".

Exemples avec les déclarations ci-dessus :
```rust
fn name_func<T> (x: T) { /* ... */ }
struct MaStruct<T> { /* ... */ }

let x: isize = 2;
name_func(x);
name_func::<isize>(x);	// Pareil

let sx = MaStruct::<String> { /* ... */ } ;
let sx: MaStruct<String> = MaStruct { /* ... */ } ;	// Pareil
```

### Compilation :

Rust implémente et compile le générique pour chaque type qui l'implémentera. On appelle ça le static dispatch.

## La gestion des erreurs :

### Les erreurs irrécupérables :

Une erreur irrécupérable est appelée panique. Lorsqu'elle survient, on dit que Rust se met en état de panique.

-Il existe deux comportements possibles en cas de panique :
- `unwind` : Rust essaye de fermer proprement tout ce qu'il peut fermer proprement avant de crasher.
	- C'est le comportement par défaut.
- `abort` : arrêt brutal. Tant pis s'il y a des fuites de mémoire ou des comportements indésirés.

On peut préciser le comportement à adopter en cas de panique à la compilation :
- Manifeste Cargo :
	```toml
	[profile.*]
	panic = unwind|abort
	```
- Dans la CLI de rustc :
	```shell
	# Unwind
	rustc -C panic=unwind
	# Abort
	rustc -C panic=abort
	```

On fait paniquer Rust dans le code avec la macro **`panic!();`**. La macro prend en paramètre de quoi formater une string, à l'instar des macros `format!()` ou `print!()`.
		
### Les erreurs récupérables :

On utilise des enum **`Result<T, E>`** :
- On met le bon résultat dans une valeur `Ok(T)`.
- On met l'erreur dans une valeur `Err(E)`.

C'est l'équivalent Rust du try-catch qu'on trouve dans moult langage.

#### Propagation d'erreurs plus bas dans la pile :

##### Remonter avec `Result.`

```rust
fn called() -> Result<U, E> { /* ... */ }

fn f(u: U) -> T { /* ... */ }

fn caller() -> Result<T, E> {
	match called() {
		Ok(res) => f(res),
		Err(e) => Err(e)
	};
}
```


##### Remonter avec l'opérateur `?`

```rust
fn g() {
	// ...
	f(args)?
	// ...
}
```

f est une fonction retournant un Result<T, E>.
- Si f() retourne `Ok`, la valeur dans le `Ok` sera attribuée.
- Si f() retourne `Err`, alors :
	- L'exécution de la fonction s'arrête là. Son appel sera dépilé de la call stack.
	- L'erreur sera retournée dans un `Result<T, E>`.

Contraintes de `?` :
- `?` ne peut-être utilisé que dans des fonctions.
- Les erreurs remontées doivent être de même type.

##### Équivalences entre `Result` et `?`

Equivalent match :
```rust
let val:T = f(args)?;
let val:T = match f(args) {
	Ok(t) => t,
	Err(e) => return Err(e),	// la fonction s'arrête là et on remonte la pile d'appel.
};
```

Equivalent de l'exemple du rethrow :
```rust
fn called() -> Result<U, E> { /* ... */ }

fn f(u: U) -> T { /* ... */ }

fn caller() -> Result<T, E> {
	Ok(f(called()?))
}

// Pareil
fn caller() -> Result<T, E> {
	let c:U = called()?;
	return f(c)
}

// Encore pareil
fn caller() -> Result<T, E> {
	return match called() {
		Ok(c) => f(c),
		Err(e) => Err(e)
	}
}
```

### Quand utiliser quel type d'erreur ?

#### Paniques

1. Quand les valeurs deviennent VRAIMENT invalides et que ça peut compromettre la suite de l'exécution.
2. Quand une erreur très rare arrive.

#### Result

1. Quand un échec est prévisible et peut arriver.
2. Quand l'échec ne remet pas en cause la suite de l'exécution du programme.
3. On ne fait pas planter tout le programme au premier pet de travers.

## Les macros :

Les macros servent à faire de la métaprogrammation : écrire du code qui écrira du code. Au début de la compilation, la macro est remplacée par son véritable code.

-Les macros ne sont pas des fonctions :
- Une macro est transformée en code à la compilation.
- Une fonction est appelée à l'exécution.
- Une macro peut cependant être bien plus économe en lignes de codes qu'une fonction.


Il existe 2 types de macros, qui agissent sur des "flux de tokens" :
- Les macros déclaratives, "par l'exemple" (macro_rules!) .
	- Font ceci via la syntaxe de leur écriture, clairement définie.
- Les macros procédurales.
	- Elles utilisent les TokenStream (itérables de TokenTree) du crate proc_macro.

### Les macros déclaratives :

Alias les `macro_rules!`


Syntaxe :
- Définition :
	```rust
	macro_rules! nom_macro {
		// Une définition
		(macro_args) => {
			// Code qui remplacera la macro
		},
		// Une autre définition
		(macro_args, args2) => {
			// Code qui remplacera la macro
		}
	}
	```
- Utilisation :
	```rust
	nom_macro!(/* arguments */);
	nom_macro![/* arguments */];
	nom_macro!{/* arguments */};
	```
	- C'est souvent une question de look.
		- Par exemple, si on utilise les listes, utiliser les crochets montre ce à quoi on a affaire. ;-)

On peut lui donner autant de définitions qu'on veut à une macro. Le compilateur retroouvera ses petits et mappera avec la bonne définition.

Une macro peut s'auto-appeler, de manière récursive.

#### Les arguments des macro_rules! :

Ce sont des "flux de tokens".

Syntaxe : `$<nom token>:<spécificateur>`
- `$<nom token>` est appelée "métavariable".
- Les spécificateurs sont aussi des "designators". Ils indiquent la nature de la métavariable.

Les arguments sont analysés comme une pattern.

```rust
macro_rules! foo {
	($x mon cul sur la commode $y) => {
		println!("{}", $x + $y);
	}
}

// rustc va analyser "3 mon cul sur la commode 5" pour en déduire $x ey $y
foo!(3 mon cul sur la commode 5);
```

#### Les designators :

##### expr :

C'est une expression Rust, un morceau de code Rust qui sera réécrit tel quel dans le code final.

Exemple :
- Fichier main.rs
	```rust
	// Macro
	macro_rules! determine_and_print_x {
		($toto:expr) => {
			$toto;
			println!("{}", x);
		}
	}

	fn foo() -> u8 { /* ... */ }

	fn main() {
		let toto: u8 = foo();
		let mut x: &'static str;

		determine_and_print_x!({
			x = "bonjour";
		});

		determine_and_print_x!({
			x = match toto {
				0 => "C'est nul !",
				1 => "singulier",
				_ => "pluriel"
			};
		});
	}
	```
- Code produit :
	```rust
	fn foo() -> u8 { /* ... */ }

	fn main() {
		let toto: u8 = foo();
		let mut x: &'static str;

		x = "bonjour";
		println!("{}", x);

		x = match toto {
			0 => "C'est nul !",
			1 => "singulier",
			_ => "pluriel"
		};
		println!("{}", x);
	}
	```

##### ident : 

Utilisé pour désigner un nom de variable

Exemple :
- main.rs
	```rust
	// Macro
	macro_rules! assign_and_print {
		($mavar:ident, $val:expr) => {
			let $mavar = $val;
			println!("{}", $mavar);
		}
	}

	fn main() {
		assign_and_print!(y, 5);
	}
	```
- Code produit :
	```rust
	fn main() {
		let y = 5;
		println!("{}", y);
	}
	```

##### ty :

Pour désigner un type

- Exemple :
- main.rs
	```rust
	// Macro
	macro_rules! assign_and_print {
		($mavar:ident, $val:expr, $type:ty) => {
			let $mavar: $type = $val;
			println!("{}", $mavar);
		}
	}

	fn main() {
		assign_and_print!(y, 5, bool);
	}
	```rust
- Code produit :
	```rust
	fn main() {
		let y:bool = 5;
		println!("{}", y);
	}
	```
- Ne compilera pas car 5 n'est pas un booléen.
	- Pour que ça compile il faudra que $val soit du type $type :
		```rust
		assign_and_print!(y, 5, usize);
		```
		```rust
		assign_and_print!(y, true, bool);
		```

##### Autres désignators

| Designator | Ce qu'il représente |
|--|--|
| **`stmt`** | Un statement Rust (instruction) |
| **`block`** | Un bloc d'instruction, avec ses accolades |
| **`lifetime`** | Un lifetime Rust |
| **`item`** | Un item, genre une fonction ou une struct |
| **`vis`** | Indicateur de visibilité, genre `pub` |
| **`literal`** | Une expression littérale |
| **`pat`**,  **`pat_param`**| Une pattern |
| **`meta`** | Un attribut, càd un truc du style `#[/* ... */]` |
| **`tt`** | `TokenTree`, tel que ceux du crate `proc_macro` |

#### La répétition :

Il existe un système pour répéter les tokens dans notre fluix de tokens.

Syntaxe : `$(<pattern de ce qu'on va répéter>)<séparateur><indicateur de répétition>`
- Les indicateurs :
	- `*` : 0 ou plusieurs fois,
	- `?` : 0 ou 1 fois.
	- `+` : au moins une fois.
- Le séparateur fera le join entre ce qu'il se répétera.

Exemple-syntaxe :
```rust
$($mavar:ident, $type: ty, $val:expr); +
```
On s'attend à des ""<un ident>, <un type>, <une expression>", au moins un, séparés par des ";".

Exemple :
```rust
macro_rules! smart_vec {
	(vec![ $(($func:ident, $val:expr)) , * ]) => {
		vec!( $($func($val)), *);
	}
}
// ...
smart_vec!(vec![(foo, 5),(bar, true), (baz , 3.14) , (foo, 10)]);
vec!(foo(10), bar(true), baz(3.14), foo(10));	// Après compilation de la macro, donc pareil
```

#### Macro et modularité :

Les macros peuvent être définies dans des modules.

Si on définit des macro dans un module, il faut annoter le module avec `#![macro_use]`

Les macros à exporter dans le module doivent être annotées par `#[macro_export]`

En dehors du module, on importe la macro avec le mot clé `use`.

### Les macros procédurales :

Alias les "proc-macros"

Ce sont des fonctions qui reçoivent un flux de tokens en argument et qui en retournent un autre.

Elles ne peuvent être créées que dans des crates de type librairies.

Pour créer ces macros-là, il faut avoir accès au crate proc_macro :
- Cargo.toml :
	```toml
	[lib]
	proc-macro = true
	```
- rustc :
	```shell
	rustc --crate-type=proc-macro
	```

Un `TokenStream` est un itérable contenant des `TokenTree`.

Il y a 3 types de macros procédurales :
- Function-like macros.
- Derive macros.
- Attribute macros.

#### Function-like macros :

Syntaxe :
```rust
// Définition
#[proc_macro]
pub fn foo(_item: TokenStream) -> TokenStream {
	// Code de la macro
}
// Utilisation
foo!();
```
-
On écrit le code Rust que ça veut générer, puis on appel ".parse().unwrap()" pour tranformer ça en tokens.

#### Attributs :

Cf. chapitre dédié

#### Derive macros :

Macro étendant l'attribut `#[derive()]`. Pour rappel, cet attribut permet d'implémenter automatiquement les traits passés en paramètres.

Permet donc de générer de nouvelles implémentations par défaut.

Syntaxe :
- Définition
	```rust
	#[proc_macro_derive(Toto)]
	fn foo(ts: TokenStream) -> TokenStream {
		let mon_code: String =  /* ... */;

		return mon_code.parse().unwrap();
	}
	```
- Utilisation :
	```rust
	#[derive(Toto, etc.)]
	struct|enum Truc { /* ... */}
	```

## Les attributs :

Ce sont des metadatas destinées à être interprétées. Ce sont donc les équivalents d'une annotation Java : metadata pour modifier un comportement.

### Syntaxe :

Définition :
```rust
#[proc_macro_attribute]
fn mon_attribut(attr: TokenStream, item: TokenStream) -> TokenStream {
	// attr : les tokens en paramètre de l'attribut
	// item : l'item sur quoi est appliqué l'attribut
}
```

Utilisation :
```rust
#[mon_attribut(args)]
<un item Rust, comme une struct ou une fonction>
```

### Types d'attributs :

Il en existe deux types :
- Attributs internes : appliqués là où ils sont écrits (dans un module, une fonction...)
- Attributs externes : appliqués sur la chose qui suit l'attribut.

Syntaxe :
- Attribut interne : 
	```rust
	#![<attribut>]
	```
- Attribut externe :
	```rust
	#[<attribut>]
	la_chose_sur_laquelle_s_applique_l_attribut
	```

Syntaxe entre crochets :
- Un symple mot : `#[mot]`
	- Exemple : `#[test]` pour dire qu'une fonction sera une fonction de test unitaire.
- Une égalité : `#[truc = qqch]`
	- La metadata `truc` vaut cette valeur où doit valoir cette valeur.
- Une "fonction", avec des arguments : `#[func(arg0 = val0, arg1 = val1, ... , argN = valN)]`

### Des attributs à connaître :

#### cfg :

Utilisé pour de la compilation conditionnelle.

Les metadata rattachées à `cfg` ne sont compilées que si les conditions décrites dans `cfg` sont réalisées.

```rust
#[cfg(target_os = "windows")]
{
	// Code qui ne sera compilé que si on compile pour une version Windows.
}
```

#### deprecated : 

Indique que quelque chose est déprécié. Ils ont un argument "since" qui indique la version depuis laquelle c'est déprécié.

```rust
#[deprecated(since="3.14")]
fn old_func() {
	println!("Je suis une fonction dépréciée depuis la version 3.14 du programme");
}
```

#### derive :

Sert à implémenter certains traits automatiquement, typiquement Debug, Copy et Clone.

```rust
#[derive(Debug, Copy, Clone, Eq)]
struct MaStruct { /* ... */ }
// Plus besoin de bloc impl pour les 4 traits de derive.
```

#### inline : 

Sert à rendre une fonction inline, càd avec son code exécuté comme s'il était dans le code appelant.

```rust
#[inline]
fn func()  { /*...*/ }
```

#### warn :

Pour traiter les warnings du compilateur.

Syntaxe : `#[warn(le_warning)]`

## Le code unsafe :

Rust est un langage très sécurisé, mais on peut quand même y exécuter du code moins sûr.

unsafe <=> sécurité de la mémoire du programme.

**À ne pas utiliser à la légère**

Le code unsafe est du "risk and reward". On a plus de responsabilités, mais il y a aussi plus de dangers. Le développeurs reprend (un peu) la main sur le langage.


Le code unsafe s'effectue dans des blocas unsafe :
```rust
unsafe {
	// Code unsafe
}
```

Dans un bloc unsafe, on peut :
- Déréférencer des pointeurs (`*const` et `*mut`).
- Exécuter des fonctions et méthodes `unsafe`.
- Implémenter des traits `unsafe`.
- Modifier des variables `static mut;`

L'Ownership et le borrow checking s'appliquent quand même dans les blocs unsafe.


On peut utiliser un bloc unsafe dans une fonction safe. Ce n'est pas contaminant.

### Fonctions unsafe :

Ce sont des fonctions déclarées comme étant unsafe par le développeur.

Le corps de la fonction est un immense bloc unsafe. Pas besoin de mettre de bloc unsafe dans une fonction unsafe.

On rajoute le mot-clé unsafe devant la fonction :
```rust
unsafe fn ma_func_unsafe() { /* ... */ }
```

Une fonction unsafe n'a pas le même type qu'une fonction safe. Elles sont de type "unsafe fn".
```rust
unsafe fn fukushima(r: bool) { /* ... */ }

let pt_fukushima: unsafe fn(bool) -> () = fukushima;	// OK
let pt_fukushima: fn(bool) -> () = fukushima;       	// Erreur de compilation
```

### Traits unsafe :

Ce sont des traits déclarés comme tels.

On déclare un trait comme étant unsafe si ses implémentations peuvent impliquer des problèmes de sécurité de la mémoire.

Les blocs d'implémentation les implémentant doivent aussi être déclarés unsafe.

Syntaxe :
```rust
unsafe trait MonTrait { /* ... */ }
unsafe impl MonTrait for Truc { /* ... */ }
```

La sécurité du trait n'a rien à voir savec celle de ses fonctions :
- Un trait safe peut avoir des méthodes unsafe.
- Les méthodes d'un trait unsafe ne sont pas forcément unsafe.

## Les variables d'environnement :

NB : fonctions dans `std::env` et macros dans `std`.

### Lecture :

4 possibilités pour les récupérer :

1. Fonction `std::env::var()` :
	- Entrée : clé sous la forme d'une `OsStr` (`str` dépendant de l'OS).
	- Sortie : Result avec une String dans le Ok.

2. Fonction `std::env::var_os()` :
	- Entrée : clé sous la forme d'une `OsStr` (`str` dépendant de l'OS).
	- Sortie : Result avec une `OsString` (`String` dépendant de l'OS) dans le Ok.

3. Macro `env!()` :
	- Entrée : clé sous forme de `str`.
	- Sortie : valeur sous forme de `str`.
	- Il faut que la variable soit définie à la compilation du programme.
		- On peut rajouter un message d'erreur personnalisé avec un second champ dans `env!()`

4. Macro `option_env!()` :
	- Entrée : clé sous forme de `str`.
	- Sortie : valeur sous forme d'`Option` avec une `&str` dans le `Some`.
	- Pas besoin que la variable soit définie à la compilation du programme.

### Itération :

Obtenir un itérateur : `vars()` (non OS-dépendant) et `vars_os()` (OS-dépendant)

Itèrent sur des paires clés-valeurs.

```rust
for (key, value) in std::env::vars() {
    println!("{key}: {value}");
}
```
### Ecriture :

Rust permet de définir des variables pour le processus en cours, mais ne sait pas persister des variables d'environnement dans l'OS.

On utilise la méthode unsafe `set_var(key, val)`. `key` et `val` sont des strings, ou bien à défaut quelque chose pouvant être référencé comme une `OsStr`.

Syntaxe :
```rust
unsafe {
	set_env(key, val);
}
```



### Suppression :

Rust permet de supprimer des variables pour le processus en cours, mais la suppression ne survit pas à la fin du programme.

On utilise la méthode unsafe `remove_var(key)`. `key` est une string, ou bien à défaut quelque chose pouvant être référencé comme une `OsStr`.

Syntaxe : 
```rust
unsafe {
	remove_var(key);
}
```

### Le PATH :

Le path d'exécution possède des méthodes spéciales pour lui :
- Méthode `split_paths(path)` : pour splitter une string selon le séparateur des path (':' ou ';' selon l'OS).
	- `path` est quelque chose pouvant être référencé en `OsStr`.
	- Renvoie un itérateur sur les différents chemins.
- Méthode `join_paths(it)` : pour rajouter des path à la variable d'environnement.
	- `it` est un itérateur, sur des `Path` ou des `PathBuf` par exemple.
	- Renvoie un `Result` avec le rajout sous forme de string brute dans le `Ok`.

Exemple :

```rust
use std::env;
use std::path::PathBuf;

// La variable d'environnement brute, sous forme de string.
let p: Result<String> = env::var("PATH");
let p: Result<OsString> = env::var_os("PATH");
let p: &'static str = env!("PATH");
let p: Option<&'static str> = option_env!("PATH");

// Splité en tableau de Strings
let paths: Vec<PathBuf> = env::split_paths(env!("PATH")).collect();
```

## Programmation concurrente :

Forte utilisation du module std::thread ici.

### Les Threads :

#### Création :

```rust
let handle: std::thread::JoinHandle<T> = std::thread::spawn(|| -> T {
	// Code du thread
	
	// Retourne une valeur
});:
```

On passe une closure à la fonction `thread::spawn()`

Le spawn retourne un `JoinHandle`, entité gérant le thread :
- Permet d'accéder au thread et à ses infos.
- On peut récupérer le thread via la méthode `handle.thread()`.
- Permet aussi d'attendre qu'il finisse via la méthode `join()`.

#### Manipulation du thread

Mettre en pause le thread courant : `thread::sleep(duration);`
- `duration` est une `std::time::Duration`.
- Si on veut préciser une durée en millisecondes (par exemple) :
	```rust
	thread::sleep(Duration::from_millis(2500));    // Pause de 2,5 secondes
	```	

Bloquer le thread courant :
- Méthode `park();`
- Thread courant : `thread::park();`
- Autre thread :
	```rust
	let handle = thread::spawn(/* ... */);
	// ...
	handle.thread().park()
	```

Débloquer un thread :
- Méthode `unpark();`
- Thread courant : `thread::unpark();`
- Authre thread :
	```rust
	let handle = thread::spawn(/* ... */);
	// ...
	handle.thread().unpark()
	```

Récupérer le thread courant si vraiment nécessaire :
```rust
let current_thread: thread::Thread = thread::current();
```

#### Fin du thread :

Via la méthode `join()` du JoinHandler<T>. Elle permet d'attendre la fin du thread.

La méthode retourne un `Result<T, E>` :
- `Ok(res)` si le thread s'est déroulé correctement.
	- Le résultat wrappé est ce que retourne la closure du thread.
- `Err(e)` si le thread a paniqué.

Si on veut remonter une erreur proprement, on demandera donc à la closure de retourner un `Result<T, E>` :

```rust
let handle: thread::JoinHandle<Result<(), &'static str>> = thread::spawn(|| {
	let age: u8 = foo();

	if age < 18 {
		Err(format!("Mineur âge de {} ans.", age));
	} else {
		Ok()
	}
});

let res_proc = handle.join();
println!("Fin de la procédure");

match res_proc {
	Ok(Ok(_)) => println!("Personne autorisée"),
	Ok(Err(err)) => println!("Personne non-autorisée : {}", err),
	Err(_) => println!("Erreur dans la procédure")
}
```

#### Threads et Ownership :

La closure du thread a besoin de posséder ce qu'elle va manipuler.
- Utilisation fréquente le mot-clé `move` devant la closure pour que le thread puisse récupérer ce dont il a besoin.
- Pour que le thread principal puisse reprendre les droits, on retourne ce dont le thread a eu besoin.

```rust
let sinval = String::from("futur invalide");
let handle: thread::JoinHandler<()> = thread::spawn(move || {
	foo(sinval);	// Le thread prendra possession de sinval
});

handle.join().unwrap();

// sinval devient invalide à la destruction du thread (règle n°3 de l'Ownership)

let sval = String::from("toujours valide");

let handle: thread::JoinHandler<String> = thread::spawn(move || {
	foo(sval);    // Le thread prendra possession de sval
	sval          // On retourne sval pour que quelqu'un d'autre puisse le posséder.
});

let sval2 = handle.join().unwrap();
// Les droits de sval sont retournés à sval2. sval sera invalide, mais son contenu perdure avec sval2.
let sval = sval2;	// Variable shadowing pour que "ça persiste dans un (nouveau) sval".
```

Autre solution : passer un clone dont on se fichera ensuite.

### Les scopes de threads :

Pour gérer un pool de threads et ne continuer qu'une fois que tous sont terminés.

Il vise à simplifier l'utilisation des threads.

On utilise la méthode thread::scope()
- Prend en paramètre une closure.
- La closure prend en paramètre un objet de type `Scope`.
- Même rapport à l'Ownership que `thread::spawn()`, donc `move` fréquent.
- Peut retourner une valeur comme thread::spawn();

```rust
let retval = thread::scope(move |scope| {
	// Code paralléllisé
});
```

L'objet `scope` contient le pool de threads

Pour créer un thread dans le scope du thread, on utilise la méthode `scope::spawn()`
- Fonctionne comme `thread::spawn();`
- On peut attendre la fin d'un thread du scope dans le scope avec un `.join()`

`thread::scope()` ne se termine que lorsque tous les threads spawnés par `scope` sont terminés :
- `.join()` implicites sur tous les threads

`thread::scope()` panique si l'un des threads scopés en cours à la fin de la closure du scope panique.
- Rappels :
	- Les threads dans le scope peuvent paniquer.
	- Il y a des `.join()` implicites sur les threads du scope non-terminés à la fin de `thread::scope();`
- D'où l'intérêt de les terminer proprement.

### Messagerie entre threads :

L'idée est de se passer des données par messages plutôt que dans une mémoire partagée.

La messagerie utilise des channels MPSC.
- MPSC : **M**ulti-**P**roducer, **S**ingle **C**onsumer.
- Un channel MPSC est une file d'attente MPSC destinée à communiquer.
- C'est un canal de communication asynchrone.

#### Création du channel :

```rust
use std::sync::mpsc as MPSC;
let (tx, rx): (MPSC::Sender, MPSC::Receiver) = MPSC::channel();
```

On crée un transmetteur et un récepteur, traditionnellement appelés tx et rx.

#### Répartition des bouts du channel :

Taillé pour remonter des données dans le thread principal :
- Le récepteur reste dans le thread principal.
- Le transmetteur va dans les threads enfants.

Les threads spawnés prennent l'Ownership des transmetteurs. Donc clonage éventuel du transmetteur si nécessaire.

#### Envoi d'un message :

Via la méthode `tx.send();`. Elle renvoie un `Result<(), std::sync::mpsc::SendError<T>>` :
- `Ok` : la donnée a bien été envoyée.
	- Sa réception n'est pas garantie.
	- Rien dans le wrapper.
- `Err` : problème à l'envoi.
	- Contient un objet de type `SendError` avec la data envoyée dedans.
	- Accès à la data avec `.0`
	- Exemple :
		```rust
		use std::sync::mpsc;
		let (tx, rx) = mpsc::channel();
		// ...
		match tx.send(foo) {
			Ok(_) => println!("foo bien envoyé !")
			Err(e) => println!("Echec lors de l'envoi de foo. Il contenait la valeur {}.", e.0)
		}
		```

L'envoi est non-bloquant dans le thread où le message est envoyé.

#### Réception d'un message :

##### Réception non-bloquante :

Avec la méthode `rx.try_recv();`, qui regarde s'il y a un message disponible tout de suite là maintenant.

Elle retourne un `Result<T, TryRecvError>`
- `Ok(msg: T)` si ça se passe bien. `msg` est ce qui est reçu.
- `Err(err: TryRecvError)` si pas de message ou s'il n'y a plus de connexion.

##### Réception bloquante :

Avec la méthode `rx.recv();`, qui bloque jusqu'à ce qu'un message soit disponible ou jusqu'à déconnexion.

Retourne un `Result<T, RecvError>`
- `Ok(msg: T)` si ça se passe bien. `msg` est ce qui est reçu.
- `Err(RecvError)` si déconnexion.

##### Réception avec timeout :

Méthode `rx.recv_timeout(timeout: Duration);`, qui bloque jusqu'à ce qu'un message soit disponible ou jusqu'au timeout si toujours pas de message.

Retourne un `Result<T, RecvTimeoutError>` :
- `Ok(msg: T)` si ça se passe bien. `msg` est ce qui est reçu.
- `Err(err: RecvTimeoutError)` si déconnexion. `RecvTimeoutError` est une enum valant Timeout ou Disconnected.
	- On se sert donc de l'erreur pour traîter de la nature de la déconnexion.

##### Réception avec boucle for :

C'est une boucle bloquante qui attend des messages jusqu'à déconnexion.

###### Syntaxe :

```rust
use std::sync::mpsc;
let (tx, rx) = mpsc::channel();
for received_msg in rx {
	foo(received_msg);
}
```

###### Equivalents :

Bloquant :
```rust
use std::sync::mpsc;
let (tx, rx) = mpsc::channel();
'recv_loop: loop {
	match rx.recv() {
		Ok(received_msg) => foo(received_msg),
		Err(_) => break recv_loop
	}
}
```

Non-bloquant :

```rust
use std::sync::mpsc;
let (tx, rx) = mpsc::channel();
'recv_loop: loop {
	match rx.try_recv() {
		Ok(received_msg) => foo(received_msg),
		Err(TryRecvError::Empty) => continue recv_loop,
		Err(TryRecvError::Disconnected) => break recv_loop
	}
}
```

#### Channel synchrone :

Il est aussi possible de communiquer de manière synchrone :

```rust
use std::sync::mpsc;
let (tx, rx): (SyncSender, Receiver) = mpsc::sync_channel(buffer_size: usize);
```

Le channel comporte un buffer d'attente où le message peut être déposé.
- `buffer_size` est le nombre maximal de messages pouvant être laissés dans le buffer.

L'envoi peut être bloquant ou pas.
- Bloquant :
	- `tx.send(msg)`
	- Attend l'une des 2 situations :
		- Qu'il y ait assez de la place dans le buffer de communication pour pouvoir y pousser le message.
		- Qu'un récepteur vienne récupérer le message, via un `rx.recv()`.
	- Ne panique jamais.
	- Retourne un Result :
		- `Ok()` si bien envoyé.
		- `Err(err: SendError)` si le récepteur est déconnecté.
			- Cf. plus haut pour `send_error()`.
	- Si le buffer est de taille nulle, attend que le récepteur en face vienne chercher un message.
	- Garantit l'envoi, pas la réception.
- Envoi non-bloquant :
	- `tx.try_send(msg)`
	- Ne panique jamais.
	- Retourne un Result :
		- `Ok()` si bien envoyé.
		- `Err(err: TrySendError)` si le récepteur est déconnecté ou si le buffer est plein.
			- `TrySendError` est une enum valant Disconnected ou Full (buffer plein).
			- Chaque valeur de l'enum comporte le message en data.

La réception est la même.

#### Channels et Ownership :

L'Ownership passe du thread envoyeur au transmetteur, puis au récepteur, et enfin au thread destinataire.

Lorsqu'une variable passe dans le channel :
- À l'envoi, l'Ownership est moved dans la méthode `send()` du transmetteur.
- À la réception, l'Ownership est moved dans la méthode `recv()` du récepteur.

### Gestion des données partagées :

#### `Arc<T>`

`std::sync::Arc<T>` est la version thread safe de `Rc<T>`.

Il sera utilisé pour ne pas avoir de problèmes d'Ownership sur les objets de la mémoire partagée. `Rc<T>` n'implémente pas Send, donc ne peut être partagé entre threads pour simuler un

Cloner un Arc<T> :

```rust
let original_arc: Arc<T> = foo();
let arc_clone = Arc::clone(&original_arc);
```

#### Les Mutex :

Rappel :
- Mutex = **MUT**ual **EX**clusion
- Pour utiliser le contenu d'un mutex, il faut le verrouiller pour avoir l'exclusivité sur son contenu.
- Une fois qu'on n'en a plus besoin, on déverrouille le mutex pour que d'autres en profitent.

Le Mutex est de classe `std::sync::Mutex<T>`. `T` est le type de la donnée dans le mutex.

Mutex empoisonné :
- Un mutex est dit empoisonné si le thread qui le verrouille panique.
- Le thread qui panique ne déverrouillant pas le lock, la donnée est censée être inaccessible.

##### Création

Création avec new(val) :

```rust
use std::sync::Mutex;
let mutex: Mutex<i8> = Mutex::new(5);	// Création du mutex et initialisation de son contenu.
```

##### Lock du mutex :

Lock bloquant ou pas, on met la main sur un `std::sync::MutexGuard<T>` qui permet d'accéder au contenu.

###### Lock bloquant :

Avec la méthode .lock(), qui renvoie un `LockResult<MutexGuard>`. 

`LockResult<MutexGuard>` est un `Result`: `Result<MutexGuard, PoisonError<MutexGuard>>`.
- `Ok` si le mutex est lock en notre faveur
- `Err` si le mutex est emposonné.

L'avancement est bloqué jusqu'à ce que le lock soit obtenu.

En cas d'erreur, on peut quand même accéder au contenu du lock. `PoisonError` comporte des méthodes pour accéder au contenu du mutex :
- `into_inner()` retourne le `MutexGuard` comme si c'était `Ok`.
- `get_ref()` retourne une référence immuable (&) sur le `MutexGuard`.
- `get_mut()` retourne une référence mutable (&mut) sur le `MutexGuard`.

```rust
use std::sync::Mutex;
let mutex: Mutex<i8> = Mutex::new(5);	// Création du mutex et initialisation de son contenu.
// ...
let guard = match mutex.lock() {
	Ok(mg) => mg,
	Err(poison) => poison.into_inner()
};
```

###### Lock non bloquant :

Via la méthode `try_lock()` du mutex. Elle fonctionne comme `.lock()` sauf qu'elle n'attend pas que ce soit son tour de récupérer la ressource.

Renvoie un Result elle aussi :
- `Ok` avec le `MutexGuard` dedans.
- `Err` avec un `TryLockError` dedans. `TryLockError` est une enum avec 2 valeurs :
	- `Poisoned` avec le `MutexGuard` en donnée.
		- Renvoyé si le mutex est empoisonné.
	- `WouldBlock` s'il ne peut pas avoir le lock maintenant.

##### Accès au contenu du mutex

On déréférence le MutexGuard :

```rust
use std::sync::Mutex;
let mutex: Mutex<i8> = Mutex::new(5);
// ...
let guard = match mutex.lock() {
	Ok(mg) => mg,
	Err(poison) => poison.into_inner()
};
*guard = -49; 	// On remplace le contenu du mutex par -49.
```

##### Unlock du mutex 

Le mutex est unlocked quand le `MutexGuard` récupéré via le lock est détruit, qu'il sort du scope.

Il ne faut pas hésiter à utiliser le `MutexGuard` dans un bloc de code, pour qu'il soit benné le plus rapidement possible.

### Send & Sync :

On parle ici de deux traits : `std::marker::Send` et `std::marker::Sync`.
- Un type implémentant `Send` indique que l'Ownership de ses objets peut être transféré entre threads.
- Un type implémentant `Sync` indique que les objets de ce type peuvent être référencés (borrowed) dans plusoieurs threads.

Implémentation :
1. Quasiment tous les types implémentent ces 2 traits.
2. Tous les types ayant tous leurs champs Send le sont aussi. Idem pour Sync.

Du coup, pas besoin donc de les implémenter manuellement dns nos structs ou enums. 

Si besoin est, l'implémentation manuelle et explicite de Send et/ou Sync sont unsafes. Donc à implémenter explicitement que si on en a vraiment besoin.

## Les tests :

### Organiser ses tests :

#### Tests unitaires

Les tests unitaires sont dans le code. Pour les passer on exécute `cargo test` sur le crate :

```shell
cargo test --lib
cargo test --bins
cargo test --bin <nom_crate>
```

#### Tests d'intégration

Les tests d'intégration sont dans des crates séparés ()`[[test]]` dans le manifeste).

La convention veut qu'ils soient placés dans un dossier tests/

Par convention pour Cargo, tous les fichiers `/tests/*.rs` et `/tests/**/main.rs` sont des crates de test.

On les exécute avec ces commandes là :

```shell
cargo test --tests
cargo test --test <nom_crate>
```

### Les fonctions de test :

On marque une fonction comme étant une fonction de test en l'annotant avec l'attribut `#[test]` :

```rust
#[test]
fn test_truc() {
	// Ceci est une fonction de test.
}
```

On fait paniquer la fonction de test avec la macro `panic!()` pour faire échouer volontairement un test. 

```rust
#[test]
fn test() {
	panic!("Je plante volonairement le test parce que pourquoi pas.");
}
```

#### Tester les valeurs

On utilise des macros "`assert!()`". Si le test échoue alors le programme apellera `panic!()` pour planter la fonction de test.

`assert!(bool, fail_msg);` prend un booléen en argument. Le test réussit si `true` et échoue si `false`.

```rust
#[test]
fn test() {
	let mut val: u16;

	// ...

	// Le test réussit si val vaut 54
	assert!(val == 54u16);
}
```

`assert_eq!(real, expected, fail_msg);` réussit si "`real == expected`".
- `real` est la valeur à tester.
- `expected` est la valeur attendue.

```rust
#[test]
fn test() {
	let mut val: u16;
	// ...

	// Le test réussit si val vaut 54
	assert!(val == 54u16);
	assert_eq!(val, 54u16);		// Pareil
}
```

`assert_ne!(real, unexpected, fail_msg);` réussit si "`real != unexpected`".
- `real` est la valeur à tester.
- `expected` est la valeur à éviter.

```rust
#[test]
fn test() {
	let num: f32;
	let mut den: f32;

	// ...

	// On ne veut pas diviser par zéro
	assert!(den != 0.0);
	assert_ne!(den, 0.0_f32);	// Pareil

	let quotient: f32 = num/den;
}
```

En Rust, il n'y a pas de notions de "actual" et "expected", mais de "left" et "right". L'ordre des valeurs importe peu donc.

Les types des valeurs passées à `assert_eq!()` et `assert_ne!()` doivent implémenter les traits `PartialEq` et `Debug` :
- `PartialEq` parce que == et != sont testés pour comparer les valeurs.
- `Debug` pour afficher les valeurs en cas d'échec.

On peut préciser un message d'erreur via "l'argument" `fail_msg`. Ce n'est pas un argument à proprement parler, mais plutôt une suite d'argument comme dans `format!()`

Exemple :

```rust
#[test]
fn test() {
	let num: f32;
	let mut den: f32;
	// ...

	// On ne veut pas diviser par zéro
	assert_ne!(den, 0.0_f32, "den ne devrait pas valoir {} mais {}", den, 0.0f32);

	let quotient: f32 = num/den;
}
```

#### Tester un échec :

On utilise l'attribut `#[should_panic]`. Le test réussit si le code panique.

On peut préciser un argument expected avec un message. Le message expected doit être inclus dans le message de panique.

Exemple :

```rust
// Le test réussit si la fonction test() panique.
#[should_panic]
fn test() { /* ... */ }

// Le test russit si la fonction test2() panique et que le message de panique contient "ta chatte !".
#[should_panic(expected = "J'échoue en affichant ce message d'erreur")]
fn test2() { /* ... */ }
```

#### Ignorer (skip) un test :

On skip un test en lui mettant l'attribut `#[ignore]`.

#### Valeurs de retour d'une fonction de test :

##### `()` :

Le test est réussi si la fonction va jusqu'au bout sans paniquer.

La fonction panique si elle a un attribut `#[should_panic]`, et si le message `expected` est dans le message de panique le cas échéant.

##### `Result<(), String>` :

Le test est réussi si la fonction renvoie `Ok(())`.

Le test échoue si :
- La fonction renvoie `Err`, avec le message d'erreur dans une `String`.
- La fonction panique.

La fonction de test ne prend pas en charge l'attribut `#[should_panic]` dans ce cas.

### Compilation du code de tests :

#### Tests unitaires

Lors de la compilation de tests, Rust définit une configuration "test". C'est une sorte de "#define test" pour faire simple. Pour ça, on utilise `cfg(test)`.

L'attribut `#[cfg(test)]` permet d'indiquer que quelque chose ne sera compilé que pour les tests :
```rust
#[cfg(test)]
mod tests {
	// On met tous les tests ici
}
```
		
Quant au compilateur, il faut lui préciser via l'argument `--cfg` :
```shell
# Compilera les tests de main.rs
rustc main.rs --cfg "test"
```

L'exécutable produit consistera en l'exécution des fonctions marquées avec `#[test]`.

#### Tests d'intégration

Pas besoin de `#[cfg(test)]` dans les crates root des tests (d'intégration), c'est sous-entendu que tout le crate root l'est.

### Lancer des tests.

On utilise la commande "test" de Cargo : `cargo test`
- Cargo compile un exécutable qui lancera les tests, puis l'exécute.
- Seront exécutées toutes les fonctions annotées par `#[test]`.

Les tests sont exécutés en parallèle par défaut. Si on veut bloquer le parallélisme :
```shell
cargo test -- --test-threads=1
```

Rust n'affiche que des infos pour les tests qui ont échoué.
- S'il y a des println!() parmi les tests réussis, leur contenu n'est pas affiché par défaut.
- On peut afficher ces infos là avec l'argument --nocapture : `cargo test -- --nocapture`

#### Filtrage de fonctions de tests :

On précise les tests à inclure via une string servant de filtre. Tous les noms de fonction contenant le filtre seront du test.

Filtrage en amont :
- La sélection s'effectue parmi les fonctions dans le code source.
- On met le pattern avant le `--`.

Filtrage en aval :
- La sélection s'effectue parmi les fonctions dans l'exécutable de test.
- On met le pattern après le `--`.
- Il existe également un argument `--skip` pour exclure des tests via un filtre.

Exemples :
```shell
# Les fonctions de test dont le nom contiendra "toto" seront les seuls tests dans l'exécutable.
cargo test toto
# Toutes les fonctions de test seront dans l'exécutable, mais seulement celles dont le nom contiendra "toto" seront exécutées.
cargo test -- toto
# Les fonctions de test dont le nom contiendra "titi" ne seront pas exécutées.
cargo test -- toto --skip titi
```