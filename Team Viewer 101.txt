Team Viewer 101 :

Généralités :
	- Outil de prise en main à distance d'un poste.
	- Site : https://www.teamviewer.com
	- TODO

Installation :
	1°) Télécharger l'installateur depuis leur site.
	2°) Lors de l'installation, mettre "démarrer seulement"
		- Exécuter Team Viewer <=> lancer l'installateur.

Fonctionnement :
	- L'ordinateur où TW est installé a un ID et un mot de passe.
		- Ce sont les identifiants pour qu'on puisse se faire prendre en main.
		- Partie de gauche.
	- Il y a aussi la possibilité de prendre en main.
		- Partie de droite.
		- On rentre l'ID du partenaire, puis son MdP.
	- Une fois connecté :
		- Vérifier en bas à droite si des personnes sont connectées ou pas.
		- Menu en haut au contre si nécessaire.
			- Comme sur les VMs VMware.
			- Exemple : placer un Ctrl-Alt-Suppr.
	- Pour quitter, fermer la fenêtre.
