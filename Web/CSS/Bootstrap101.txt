Bootstrap 101 :

Généralités :
	- Librairie CSS et JS.
	- Créée par Twitter en 2011.
	- Site Web : https://getbootstrap.com/
	- "BS" pour les intimes.

Récupérer Bootstrap :
	- En récupérant les fichiers sur le site de Bootstrap.
		- On peut récupérer soit :
			- Les fichiers compilés : le CSS compilé et le JS.
				- C'est la "distribution"
			- Les sources :
				- Le Sass non transpilé en CSS.
				- Des maps permettant de remonter vers les fichiers sources.
				- Pour compiler un Bootstrap custom.
	- Via votre gestionnaire de paquet préféré :
		- npm : npm install bootstrap
		- Composer : composer require twbs/bootstrap
		- Marche aussi avec Yarn, NuGet, RubyGems et Bundler.
	- Contenu de la distribution :
		- Dossier /dist dans les sources, dossier qu'on récupère si on prend les compilés.
		- Contient 2 sous-dossiers :
			- css : fichiers CSS de Bootstrap.
				- Chaque fichier vient avec sa version minifiée, en ".min.css".
				- Chaque fichier vient avec sa map de débogage, en "(.min).css.map".
				- Les fichiers :
					- bootstrap.css : tout Bootstrap : mise en page, contenus, composants et utilitaires.
					- bootstrap-grid.css : que la mise en page en grille et l'utilitaire Flex.
					- bootstrap-reboot.css : que le contenu de reboot.
			- js : fichiers pour utiliser les composants interactifs de Bootstrap.
				- Chaque fichier vient avec sa version minifiée, en ".min.js".
				- Chaque fichier vient avec sa map de débogage, en "(.min).js.map".
				- Deux catégories de fichiers :
					- bootstrap.js : les composants de Bootstrap.
						- Nécessite jQuery et Popper.js.
						- Bootstrap 5 tente de dégager jQuery.
					- bootstrap.bundle.js : Bootstrap + Popper.js

Inclure Bootstrap dans son site :
	- Les fichiers CSS :
		- Classique via <link />
		- On inclut le fichier récupéré ou un lien vers Bootstrap sur un CDN.
		- À mettre avant son propre CSS.
			- La spécificité fera qu'en cas d'égalité ce sera notre CSS qui sera pris en compte.
	- Les fichiers JS :
		- Classique via <script src="..."></script>
		- On inclut le fichier récupéré ou un lien vers Bootstrap sur un CDN.
		- Il faut inclure jQuery avant Bootstrap.

Bootstrap et le Responsive Web Design :
	- Bootstrap utilise une conception mobile first.
		- Il est donc important de bien définir son viewport :
			<meta name="viewport" content="width=device-width,initial-scale=1.0,shrink-to-fit=no" />
	- Il y a 4 points de rupture dans BS :
		0px    |576px  |768px   |992px  |1200px
		<----->|<----->|<------>|<----->|-------------->
		       |  sm   |   md   |  lg   |     xl
		défaut | SMall | MeDium | LarGe | eXtra Large
	- BS étant mobile first, le style par défaut est valable pour les largeurs d'écran < 576px.
	- Le "par défaut" est quelques fois appelé xs (eXtra Small).

La mise en page avec Bootstrap :
	- Bootstrap calcule les tailles de boîte bordures incluses (box-sizing: border-box;).
	- Une mise en page Bootstrap se réalise dans un conteneur.
		- Il y a deux types de conteneurs : "de base" et "fluide".
		- Le conteneur de base.
			- Le conteneur a une largeur maximum fixe en fonction de la taille d'écran (sm, md, lg, xl).
				- Les marges se feront d'elles-mêmes.
			- Classe CSS .container.
		- Le conteneur fluide.
			- Le conteneur occupera toujours 100% de la largeur de son parent quelque soit sa taille.
			- Classe CSS .container-fluid.
		- Valeurs par défaut du container :
			- Marges latérales (margin-left|right) automatiques.
			- Padding latéral de 15px.
	- Un conteneur est composé de lignes :
		- Classe CSS .row.
		- Flexboxeries :
			- Chaque ligne est une flexbox horizontale à remplissage wrap.
			- On peut faire un alignement des objets avec des classes CSS :
				- Syntaxe : .<pté_flexbox>[-XY]-<val pté>
					- pté_flexbox : propriété CSS d'alignement chez Flexbox :
						- justify-content
						- align-items
					- val_pté : la valeur de la propriété pour justify-content ou align-items.
					- XY : la taille à partir de laquelle on applique.
	- Une ligne est composée de colonnes :
		- Les colonnes sont les flex-items de leur ligne, qui rappelons-le est une flexbox.
			- Ce sont des éléments physiques de la ligne.
			- Elles possèdent un offset et un colspan :
				- L'offset est le décalage avec la cellule précédente.
				- Le colspan détermine la largeur de la colonne.
				- On peut les préciser selon la taille de l'écran.
			- On peut imbriquer des lignes dans des colonnes.
			- Classes CSS des colonnes :
				- .col : indique que l'élément HTML est une colonne Bootstrap.
					- /!\ Ça introduit des propriétés Flexbox polluant les .col[-XY]-NB.
					- À ne pas mettre si on peut mettre un .col[-XY]-NB.
				- Colspan et offset :
					- .col[-XY]-NB : colspan de la colonne.
						- La cellule à un colspan de NB colonne(s) virtuelle(s) pour la taille d'écran XY.
					- .offset[-XY]-NB : offset de la colonne.
						- La cellule à un offset de NB colonne(s) virtuelle(s) pour la taille d'écran XY.
						- Techniquement, .offset-NB { margin-left: calc(100%/NB); }
					- Arguments des *-XY-NB :
						- NB : taille du colspan / de l'offset. Dans [1;12].
						- XY : taille d'écran à partir de laquelle l'offset / le colspan s'applique.
							- On ne le met pas pour une taille par défaut.
							- "À partir de" => pas besoin de le repréciser pour les tailles supérieures.
				- .w|h-PE : avoir une largeur/hauteur précise en fonction de celle de la ligne.
					- PE : pourcentage de la largeur/hauteur de la ligne, parmi 25, 50, 100 et auto.
					- w-100 permet ainsi d'avoir une colonne sur toute la largeur de la ligne.
					- Il existe aussi des mw-* et mh-* sur la lageur/hauteur maximale.
				- .align-self-XY-<val_as> :
					- align-self pour les flex-items de cette Flexbox de classe .row.
					- XY : la taille d'écran à partir de laquelle ça s'applique.
					- val_as : la valeur pour align-self.
		- À ces éléments physiques (row) sont associés une sorte de "grille virtuelle" à 12 colonnes.
			- Rien à voir avec CSS Grid.
			- L'offset et le colspan d'une colonne correspondent au nombre de colonnes virtuelles de cette grille.
				- Offset et colspan sont donc limités à 12.
				- Les classes CSS pour plus d'offset et de colspan n'existent pas de toute façon.
			- Ici, "sortir de la grille virtuelle" == "avancer au delà de la 12ème colonne virtuelle".
		- Remplissage de la ligne :
			- Les enfants sont mis les uns à la suite des autres selon leurs classes CSS.
			- Process de remplissage :
				- On se place au tout début de la grille virtuelle (rangée virtuelle n°1, colonne virtuelle n°1).
				- Foreach sur les flex-items de la .row.
					- Ordre de remplissage : ordre Flexbox des colonnes (cf. propriété CSS "order").
					- Il existe aussi des classes .order-NB, avec NB dans [0;12], .order-first et .order-last.
				- Pour chaque colonne :
					- On avance de l'offset de la colonne.
						- Si on sort de la grille virtuelle alors la colonne sera à la rangée virtuelle suivante.
					- On regarde le colspan de la colonne.
						- Si on dépasse de la grille virtuele alors la colonne ira en dessous.
						- Si on ne précise pas de colspan alors la colonne occupe l'espace restant.
							- .col-1 != "pas de colspan".
					- Pour le placement de l'enfant suivant, on part du bout de la colonne fraîchement placée.
			- Si on ne précise aucun colspan, on aura les colonnes les unes à côté des autres, même si > 12.
				- Parce que la ligne reste une Flexbox horizontale wrap.
	- Gouttières :
		- Par défaut il y a des gouttières de 30px entre colonnes.
			- Codées à la zeub avec des paddings de 15px de chaque côté de la colonne.
		- On peut les supprimer en ajoutant la classe .no-gutters à la ligne.

Gestion des marges :
	- Moult classes CSS.
	- Syntaxe : .<margin><side>[-XY]-NB
		- <margin> : le type de marge :
			- m pour "margin".
			- p pour "padding".
		- <side> : le côté concerné :
			- l pour "left".
			- r pour "right".
			- t pour "top".
			- b pour "bottom".
			- x pour horizontal (left & right).
			- y pour vertical (top & bottom).
		- XY : taille à partir de laquelle s'applique la règle.
			- Facultative.
		- NB : taille en question
			- 0 : 0.
			- 1 : 0.25rem.
			- 2 : 0.5rem.
			- 3 : 1rem.
			- 4 : 1.5rem.
			- 5 : 3rem.
			- auto : auto.
			- Si nombre, on peut rajouter n devant pour une marge négative si margin.
				- Exemple : n4 : -1.5rem.
	- Exemple :
		- .my-sm-n2 ?
			- m => margin-.
			- y => top & bottom.
			- sm => taille small.
			- n2 => -0.5rem
			- Donc un truc du style :
				@media (min-width: 576px) {
					.my-sm-n2 {
						margin-top: -0.5rem;
						margin-bottom: -0.5rem;
					}
				}
		
Bootstrap et les textes :
	- Toujours à base de classes CSS à bourrer dans les éléments HTML.
	- Bootstrap propose des choses pour uniformiser le texte sur les navigateurs.
		- Ils vont appliquer un certain nombres de propriétés de base sans qu'on ait à s'en préoccuper.
		- On appelle ça "Bootstrap Reboot".
		- On ne peut qu'inclure que cette partie de Bootstrap grâce au fichier bootstrap-rebot.css.
	- Les en-têtes :
		- Bootstrap propose un style par défaut pour les balises <h1> à <h6>
		- BS propose également des classes CSS .h1 à .h6 pour que ça ressembles aux balises du même nom ailleurs.
		- Il existe également des classes .display-1 à .display-4 pour afficher encore plus grand.
	- Mises en évidence :
		- Les paragraphes : il existe une classe CSS .lead pour mettre en évidence un paragraphe.
		- Balises HTML de mises en évidence :
			- Bootstrap stylise les balises suivantes et offre une classe du même nom pour ressembler ailleurs :
				- <mark> (surlignage de texte).
				- <small> (textes en petits caractères, les fameuses "petites lignes").
	- Abréviations :
		- Rappel HTML :
			- Balise HTML <abbr>...</abbr>
			- On met la signification de l'abréviation dans l'attribut title.
			- Exemple : <abbr title="Olympique de Marseille">OM</abbr>
		- Bootstrap possède une class .initialism pour mettre les abréviations en plus petit et en majuscules.
	- Les citations :
		- Bootstrap reboote pour les balises <blockquote>.
		- Bootstrap propose aussi 2 classes CSS :
			- .blockquote : pour singer les balises <blockquote>.
			- .blockquote-footer : pour les sources de citations.
			- Exemple :
				<blockquote class="blockquote">
					<p>Incroyable du cul !</p>
					<footer class="blockquote-footer">Xavier "Mister MV" DANG</footer>
				</blockquote>
	- Les listes :
		- Liste sans style :
			- Comme le nom l'indique : sans fioritures.
			- Classe CSS .list-unstyled dans le <ul>/<ol>/<dl>.
		- Liste en ligne :
			- Comme le nom l'indique : les éléments en lignes au lieu des uns en dessous des autres.
			- Classes CSS :
				- .list-inline dans le <ul>/<ol>/<dl>.
				- .list-inline-item dans le <li>.

Bootstrap et les images :
	- BS possède deux classes pour les images :
		- .img-fluid pour une image responsive.
		- .img-thumbnail pour une jolie bordure blanche du plus bel effet.
	- Aligner les images :
		- Classes .float-left|right pour des placement flottants.
		- Classes text-* pour faire ça avec text-align.
	- Les figures :
		- 3 classes .figure[-*]
			- .figure pour <fig>.
			- .figure-img pour l'image.
			- .figure-caption pour <figcaption>.
	- Les carousels d'images :
		- C'est un composant Bootstrap : besoin du JS (util.js) en plus du CSS.
		- Le carousel est dans un élément racine comportant les éléments suivants :
			- Un ID bien à lui.
			- Les classes .carousel et .slide
				- .carousel-fade pour un fondu enchaîné entre deux slides.
			- Des attributs HTML data-* :
				- data-ride="carousel" permet de le lancer dès le chargement de la page.
				- data-interval permet de régler le temps d'affichage d'un slide avant de passer au suivant.
		- L'élément racine comporte plusieurs enfants correspondant aux éléments du carousel :
			- Un élément de classe .carousel-inner avec les slides, qui sont ses enfants directs :
				- Ils sont tous de classe .carousel-item
				- L'un d'entre eux doit avoir la classe .active : ce sera le premier à être montré.
					- Sinon tout le carousel sera invisible.
				- Dans l'enfant, on peut mettre un élément descriptif, de classe CSS .carousel-caption.
				- On peut préciser sa durée d'affichage avec un data-interval.
				- Dedans, il est conseillé ceci pour l'image :
					- .carousel-item > img { display: block; width: 100%; }
					- Mais BS a des classes noob à mettre dans la balise <img> pour ça : class="d-block w-100".
			- Les indicateurs du carousel, dans un élément racine <ol> de classe .carousel-indicators.
				- Ses <li> enfants doivent avoir les attributs suivants :
					- data-target="#<id_du_carousel>"
					- data-slide-to="<n°du slide auquel il se réfère>"
					- L'un d'entre eux doit avoir la classe .active.
			- Les boutons "suivant" et "précédent" :
				- Balises <a> de classes CSS .carousel-control-prev|next
				- href="#<id_du_carousel>"
				- data-slide="prev|next" selon le sens où ça va
				- role="button"
				- Dans la balise, on peut mettre des éléments pour illustrer les boutons
					- Classes CSS .carousel-control-prev|next-icon
		- En JS, on récupère le carousel grâce à jQuery : $('.carousel').carousel()

Bootstrap et la navigation par menu :
	- Comme d'habitude tout se fait en bourrant les classes CSS.
	- Rappels de la structuration HTML d'un menu :
		<nav> <!-- La racine du menu -->
			<a></a> <!-- Un endroit du menu -->
			<ul> <!-- Un sous-menu -->
				<li><a></a></li> <!-- Un élément dans un sous-menu -->
				<li><a></a></li>
				<li>
					<ul>
						<li><a></a></li> <!-- Un élément dans un sous-sous-menu -->
						<li><a></a></li>
					</ul>
				</li>
				<li><a></a></li>
			</ul>
		</nav>
	- Les barres de navigation (navs) :
		- Le JavaScript n'est nécessaire que pour les onglets ou les éléments à dropdowns.
		- Classes CSS de base :
			- Une barre de navigation est signalée avec la classe BS .nav.
			- Un nœud du menu doit avoir la classe .nav-item.
			- Un lien du menu doit avoir la classe .nav-link.
			- Exemples :
				<ul class="nav">
					<li class="nav-item"><a class="nav-link" href="#foo">Foo</a></li>
					<li class="nav-item"><a class="nav-link" href="#bar">Bar</a></li>
					<li class="nav-item"><a class="nav-link" href="#baz">Baz</a></li>
				</ul>
				<nav class="nav">
					<a class="nav-item nav-link" href="#foo">Foo</a>
					<a class="nav-item nav-link" href="#bar">Bar</a>
					<a class="nav-item nav-link" href="#baz">Baz</a>
				</ul>
		- Nav-link actif/inactif :
			- Les classes :
				- .active : nav-link activé.
					- On peut l'omettre car c'est "la valeur par défaut".
				- .disabled : nav-link désactivé.
		- Alignement du menu :
			- .nav est une Flexbox.
			- C'est donc l'alignement Flexbox qui prime, avec justify-content :
				- Le CSS atomique justify-content-* de Bootstrap.
				- La propriété justify-content dans notre CSS du menu.
			- Justifier tout le menu :
				- Revient à étirer toute la Flexbox :
					- flex-grow: 1; pour les flex-items/nav-items.
				- Bootstrap propose la classe. nav-justified pour le menu.
				- Exemple : <ul class="nav nav-justified"><!-- ... --></ul>
		- Formes du menu :
			- Forme d'onglets : classe "nav-tabs" à la racine du menu :
				<ul class="nav nav-tabs"><!-- ... --></ul>
			- Avec d'onglets : classe "nav-pills" à la racine du menu :
				<ul class="nav nav-pills"><!-- ... --></ul>
		- Sous-menus : on fait ça avec des dropdowns (boutons déroulants).
		- Sens du menu :
			- .nav est une Flexbox :
				- Classes CSS atomiques .flex-row et flex-colmun de BS.
				- Propriété flex-orientation de CSS.
	- Les menus de navigation (navbars) :
		- Typiquement menu que l'on va mettre dans l'entête.
		- Le JavaScript n'est pas nécessaire.
		- Composant HTML dont la racine est un élément de classe .navbar.
			- C'est une flexbox.
		- La navbar contient plusieurs enfants :
			- La dénomination du site :
				- Élément de classe .navbar-brand.
				- Typiquement un lien vers la page d'accueil.
				- On y met l'icône et/ou le nom du site.
				- Résumé : <a class="navbar-brand" href="index.html"><!-- ... --></a>
			- Le bouton hamburger :
				- C'est le petit bouton aux 3 traits où on gare le menu responsive.
				- Caractéristiques :
					- Classe .navbar-toggler.
					- Un attribut data-target qui est l'ID de son menu.
						- Syntaxe : data-target="#<ID menu>"
					- data-toggle="collapse" : ce bouton va déplier quelque chose.
					- Pour l'accessibilité :
						- aria-controls="<ID menu>" : indique le menu contrôlé.
						- aria-label="Toggle navigation" : décrit le composant.
				- Ne pas oublier le type="button" si on utilise une balise <button> pour ça.
				- Dedans, on peut mettre un élément de classe .navbar-toggler-icon :
					- C'est une image (SVG) avec les 3 barres icôniques du menu hamburger.
				- Résumé :
					<button
							class="navbar-toggler"
							data-target="#lemenucollapse"
							data-toggle="collapse"
							type="button"
					>
						<span class="navbar-toggler-icon"></span>
					</button>
			- Le conteneur dépliable pour le menu :
				- C'est un élément pliable (collapse).
					- Donc classe .collapse.
				- Élément de classe .navbar-collapse, parce que "collapse dans une navbar".
				- Doit aussi posséder un ID pour que le bouton puisse agir sur lui.
				- Résumé :
					<div id="lemenucollapse" class="collapse navbar-collapse"><!-- ... --></div>
			- Le menu :
				- C'est un menu classique, à ceci près qu'il n'a pas la classe .nav mais .navbar-nav.
				- Résumé :
					<ul class="navbar-nav">
						<li class="nav-item"><a class="nav-link" href="#foo">Foo</a></li>
						<li class="nav-item"><a class="nav-link" href="#bar">Bar</a></li>
						<li class="nav-item"><a class="nav-link" href="#baz">Baz</a></li>
					</ul>
			- Texte sans liens :
				- Il est possible de mettre du texte sans liens.
				- Il doit être encapsulé dans des éléments de classe .navbar-text.
		- Responsive :
			- Les classes .navbar-expand-XY permettent de rendre le menu responsive.
			- En deçà de la taille XY, le menu est mis dans le hamburger.
		- Résumé HTML :
			<nav class="navbar">
				<a class="navbar-brand" href="index.html"><!-- ... --></a>
				<button class="navbar-toggler" data-target="#lemenucollapse" data-toggle="collapse" type="button">
					<span class="navbar-toggler-icon"></span>
				</button>
				<span class="navbar-text"><!-- ... --></span>
				<div id="lemenucollapse" class="collapse navbar-collapse">
					<ul class="navbar-nav">
						<li class="nav-item"><a class="nav-link" href="#foo">Foo</a></li>
						<li class="nav-item"><a class="nav-link" href="#bar">Bar</a></li>
						<li class="nav-item dropdown">
							<a class="nav-link dropdown-toggle" href="#baz">Baz</a>
							<div class="dropdown-menu">
								<a class="dropdown-item" href="#">Bazique</a>
								<a class="dropdown-item" href="#">Bazile Boli</a>
							</div>
						</li>
					</ul>
				</div>
			</nav>

Bootstrap et les onglets :
	- Cas particuliers des menus.
		- Ils amènent l'affichage d'un élément au lieu de l'ouverture d'un lien.
		- On peut mettre des .nav-tabs et des .nav-pills.
			- Ces classes ne sont là que pour l'affichage après tout.
	- Deux parties :
		- Le menu des onglets.
		- Les onglets eux-mêmes.
	- Le menu des onglets :
		- Menu avec des options supplémentaires.
		- Pour les .nav :
			- Un ID, pour qu'il puisse être rattaché aux onglets.
			- Pour l'accéssibilité :
				- role="tablist" : ce sont des boutons d'onglets.
		- Pour les a.nav-link :
			- data-toggle="tab" : bouton qui ouvre des onglets.
				- Ou data-toggle="pill" si on utilise des boutons à la place.
			- href="#<ID de l'onglet>" : pour lier l'onglet et son élément de menu.
			- Pour l'accessibilité :
				- role="tab" : c'est un onglet.
				- Un ID, pour l'attribut aria-labelledby des onglets.
				- aria-controls="<ID de l'onglet>"
	- Les onglets :
		- Regroupés dans une <div> :
			- Possède la classe .tab-content.
			- Son ID vaut <ID du Menu>Content :
				<ul class="nav nav-tabs" id="foo"><!-- ... --></ul>
				<div class="tab-content" id="fooContent"><!-- ... --></div>
		- Les onglets sont les enfants (directs) de l'élément .tab-content.
			- <div> de classe .tab-pane.
			- Possèdent un ID qui est l'ancre qui les référence dans le menu.
			- Pour l'accessibilité :
				- role="tabpanel" : c'est un onglet.
				- aria-labelledby="<ID du bouton d'onglet>" : pour lier l'onglet à son bouton.
	- HTML synthèse :
		<ul class="nav nav-tabs" id="foo">
			<li class="nav-item"><a class="nav-link" href="#o1" data-toggle="tab">Onglet 1</a></li>
			<li class="nav-item"><a class="nav-link" href="#o2" data-toggle="tab">Onglet 2</a></li>
			<!-- ... -->
			<li class="nav-item"><a class="nav-link" href="#oN" data-toggle="tab">Onglet N</a></li>
		</ul>
		<div class="tab-content" id="fooContent">
			<div class="tab-pane" id="o1"><!-- Contenu de l'onglet n°1 --></div>
			<div class="tab-pane" id="o2"><!-- Contenu de l'onglet n°2 --></div>
			<!-- ... -->
			<div class="tab-pane" id="oN"><!-- Contenu de l'onglet n°N --></div>
		</div>
	- Le JavaScript :
		- On accède au menus d'onglets comme ceci : $('#foo.nav').tab();
		- Méthodes :
			- $('#foo.nav .nav-link#idtab').tab('show'); : afficher un onglet.
			- $('#foo.nav .nav-link#idtab').tab('dispose'); : détruire un onglet.
		- Événements :
			- hide.bs.tab : sur un onglet que l'on n'affiche plus
			- show.bs.tab : sur un onglet que l'on va afficher, avant l'affichage.
			- hidden.bs.tab : sur l'onglet que l'on arrête d'afficher
			- shown.bs.tab : sur un onglet que l'on va afficher, après l'affichage.
			- Syntaxe : $('a[data-toggle="tab"]').on(event, callback);

Généralités sur les boutons :
	- .btn : classe générale pour les boutons.
	- .btn-lg : grand bouton.
	- .btn-sm : petit bouton.
	- TODO

Les boutons déroulants (dropdowns) :
	- Nécessitent le JavaScript.
		- Nécessite Popper.js (dans bootstrap.bundle[.min].js).
	- Structure du bouton déroulant :
		- La racine :
			- Une <div> de classe .dropdown.
			- Contient deux enfants :
				- Le bouton lui-même.
				- Le menu à dérouler.
		- Le bouton en lui-même:
			- Une <div> de classe .dropdown-toggle.
			- Doit posséder les attributs suivants :
				- Un ID.
				- data-toggle="dropdown" : qui en fait un bouton déroulant.
			- On peut rajouter les attributs suivants pour l'accessibilité (ARIA) :
				- C'est un bouton : role="button"
				- Le bouton ouvre un menu : aria-haspopup="true"
				- Le menu n'est pas affiché "au repos" : aria-expanded="false"
		- Le menu déroulant :
			- Une <div> de classe .dropdown-menu.
			- On peut rajouter les attributs suivants pour l'accessibilité (ARIA) :
				- aria-labelledby="<ID du bouton>"
		- Les éléments du menu :
			- Plusieurs classes possibles :
				- .dropdown-item : élément interactif.
				- .dropdown-item-text : élément non-interactif.
				- .dropdown-divider : séparateur dans le menu.
		- Résumé HTML :
			<div class="dropdown">
				<!--
					- Le bouton déroulant lui-même, souvent un <button></button>.
					- On peut aussi mettre un lien <a> à la place.
				-->
				<div id="lebouton" class="dropdown-toggle" data-toggle="dropdown">Le bouton</div>

				<!-- Le menu -->
				<div class="dropdown-menu" aria-labelledby="lebouton">
					<a class="dropdown-item" href"#destmenu">Lien interactif</a>	<!-- Élément du dropdown -->
					<div class="dropdown-divider"></div>	<!-- Séparateur dans le menu -->
					<span class="dropdown-item-text">Élément non-interactif</span>
				</div>
			</div>
	- Alignement du menu :
		- Par défaut le menu est aligné avec le bord du bouton :
			- dropdown-menu-left : alignement avec le bord gauche.
			- dropdown-menu-right : alignement avec le bord droit.
		- Pour aligner le menu avec le bord de la flèche :
			- Ajouter la classe .dropdown-toggle-split au bouton.
			- Exemple :
				<div class="dropdown">
					<div id="lebouton" class="dropdown-toggle dropdown-toggle-split" data-toggle="dropdown">
						Le bouton
					</div>
					<div class="dropdown-menu" aria-labelledby="lebouton"><!-- ... --></div>
				</div>
	- Orientation du menu :
		- Selon l'orientation de la flèche.
		- Attribut à la racine (à la place de .dropdown si nécessaire) :
			- .dropdown : le menu s'affiche en bas.
			- .dropup : le menu s'affiche en haut.
			- .dropleft : le menu s'affiche à gauche.
			- .dropright : le menu s'affiche à droite.
	- Comme pour les menus, on peut activer/déactiver un élément avec les classes .active et .disabled.
	- JavaScript :
		- Récupérer le dropdown : $('.dropdown-toggle').dropdown()
		- Les méthodes :
			- Afficher le menu : $('.dropdown-toggle').dropdown('show')
			- Cacher le menu : $('.dropdown-toggle').dropdown('hide')
			- Alterner l'affichage : $('.dropdown-toggle').dropdown('toggle')
			- Màj le dropdown : $('.dropdown-toggle').dropdown('update')
			- Détruire le dropdown : $('.dropdown-toggle').dropdown('dispose')
		- Les événements :
			- show.bs.dropdown : quand on demande l'affichage.
			- shown.bs.dropdown : quand le menu est visible.
			- hide.bs.dropdown : quand on demande de cacher le menu.
			- hidden.bs.dropdown : quand le menu est caché.
			- Syntaxe : $('.dropdown-menu').on(event, callback);

Les scroll spy :
	- Élément màj automatiquement l'élément sélectionné dans un menu (nav) ou un list group.
	- JavaScript est requis pour manipuler cela.
	- Espionner :
		- La zone à espionner doit avoir les attributs suivants :
			- data-spy="scroll"
			- data-target="#<ID du menu espion>"
			- data-offset="0"
		- Les "breakpoints" (éléments de la zone faisant bouger le menu) doivent avoir des IDs.
		- Le menu nav est on ne peut plus classique.
			- Les navs links doivent avoir les ID des breakpoints en href.
		- Exemple récapitulatif :
			<div data-spy="scroll" data-target="#lespymenu" data-offset="0">
				<h1 id="foo">Titre fou</h1>
				<p>Lorem ipsum dolor sit amet, et blablabla et blablabla.</p>
				<h1 id="bar">Titre barr&eacute;</h1>
				<p>Lorem ipsum dolor sit amet, et blablabla et blablabla.</p>
				<h1 id="baz">Titre bas&eacute;</h1>
				<p>Lorem ipsum dolor sit amet, et blablabla et blablabla.</p>
			</div>
			<nav id="lespymenu" class="nav">
				<a class="nav-link" href="#foo">Fou</a>
				<a class="nav-link" href="#bar">Barr&eacute;</a>
				<a class="nav-link" href="#baz">Bas&eacute;</a>
			</nav>

Les cartes :
	- Containers pour afficher des choses, avec typiquement un header, un body et un footer.
		- Rien à voir avec les cartes géographiques.
	- La racine de la carte est un bloc de classe .card.
	- Parties de la carte :
		- Représentées par des classes CSS.
		- .card-header : en-tête de la carte.
		- .card-body : contenu de la carte.
		- .card-footer : pied de page de la carte.
		- .card-img-* : image pour illuster la carte.
	- Éléments de la carte :
		- .card-title : titre de la carte.
		- .card-subtitle : sous-titre pour la carte.
		- .card-text : le texte de la carte.
		- .card-link : lien à mettre dans la carte, typiquement en bas.

Les éléments pliables (collapse) :
	- Nécessite JavaScript.
	- Pour afficher ou pas un élément en cliquant sur quelque chose.
	- Ce qui est nécessaire :
		- Sur l'élément cliquable pour étendre ou pas :
			- data-toggle="collapse" pour dire qu'on peut dérouler.
			- data-target="<sel>", avec <sel> qui est un sélecteur sur ce qui peut être déroulé.
				- Typiquement un ID visant un seul élément.
				- Peut aussi être mis dans href si on utilise une balise <a>.
		- Sur l'élément à dérouler :
			- Avoir le sélecteur référencé dans le data-target du bouton.
				- Typiquement un ID.
			- La classe ".collapse".
				- Si on veut que ça soit affiché de base, on rajoute la classe ".show".
				- Classe .collapsing utilisée durant les transitions.
	- Pour manipuler plusieurs éléments, on utilisera plutôt une classe dans le data-target.
	- Accessibilité :
		- Dans le bouton :
			- aria-expanded="true|false" selon si le .collapse est ouvert ou pas.
			- role="button" dans le bouton si la balise n'est pas <button>
			- aria-controls="bar", avec bar étant l'identifiant du .collapse.
		- Dans le collapse :
			- aria-labelledby="foo", avec foo étant l'identifiant du bouton.
	- Exemple minimal :
		<span data-toggle="collapse" data-target="#foo">Let's collapse!</span>
		<div class="collapse" id="foo">Till the roof comes off.</div>
		<span data-toggle="collapse" data-target="#bar">Voir au début</span>
		<div class="collapse show" id="bar">Eminem</div>

Les accordéons :
	- Cas particuliers de collapse.
	- On a plusieurs collapses et seulement un seul s'affiche.
	- Utilisé en combinaison avec les cartes.
		- Un élement collapse == une carte.
		- Le bouton sera dans le card-header et le collapse dans le card-body
	- Classes supplémentaires :
		- La classe ".accordion" à la racine.
			- Les cartes collapse sont ses enfants directs
		- Pour les .collapse : data-parent="<selacc>", où <selacc> est un sélecteur désignant la base.
	- Exemple minimal :
		<div class="accordion" id="foo">
			<div class="card">
				<div class="card-header" data-toggle="collapse" data-target="#verchuren">L'homme</div>
				<div class="card-body collapse" id="verchuren" data-parent="#foo">André Verchuren</div>
			</div>
			<div class="card">
				<div class="card-header" data-toggle="collapse" data-target="#horner">La femme</div>
				<div class="card-body collapse" id="horner" data-parent="#foo">Yvette Horner</div>
			</div>
		</div>

Les formulaires :
	- Des classes pour les styles de formulaire.
	- En aucun cas un framework pour des formulaires.
	- Ne nécessite pas JavaScript.
	- Là aussi on bourre les classes.
	- Les classes sont de la forme ".form-*" :
		- .form-group : .
		- .form-control-* : pour les inputs.
			- .form-control : pour les inputs textuels.
			- .form-control-file : pour les <input type="file" />.
			- .form-control-plaintext : pour les <input readonly />, sert à afficher en "plain text".
				- Rappel : readonly est un attribut HTML valide pour les <input/>
			- .form-control-range : pour les <input type="range" />.
		- .form-check-* : pour les cases à cocher et les boutons radio.
			- .form-check : container de base.
			- .form-check-inline : pour afficher les cases en ligne au lieu d'en colonne.
				- En plus de .form-check.
			- .form-check-label : pour le label.
			- .form-check-input : pour l'input.
		- .form-group : classe container pour grouper des choses sur un champ du form (genre label et input).
		- .form-inline : pour que les blocs du forumlaire soient inline.
		- .form-text : pour les petits textes indiquant des infos sur le champ.
			- Exemple : "votre mot de passe doit contenir au moins 10 caractères dont 1 n° et 2 majuscules."
	- Forumaires et grille Bootstrap :
		- .form-row : la classe .row de la grille Bootstrap optimisée pour les formulaires.
			- Remplace .row dans les formulaires. Elle ne s'ajoute pas.
			- Les colonnes enfantes sont toujours en .col-*.
		- .col-form-label :  Pour les labels dans une colonne.
	- Formulaires et validation :
		- .was-validated : classe chapeau pour :valid et :invalid.
			- Appliquée à <form>
		- .is-valid : classe chapeau pour :valid.
			- Au niveau de l'<input>.
			- Utile pour quand on renvoie une page avec le formulaire, pour indiquer ce qui va.
		- .is-invalid : classe chapeau pour :invalid.
			- Au niveau de l'<input>.
			- Utile pour quand on renvoie une page avec le formulaire, pour indiquer ce qui ne va pas.
		- .valid-* : classe pour des éléments à afficher si c'est valide.
			- .valid-feedback pour un texte
			- .valid-tooltip pour un tooltip.
		- .invalid-* : classe pour des éléments à afficher si c'est invalide.
			- .invalid-feedback pour un texte
			- .invalid-tooltip pour un tooltip.
	- Personnalisation avancée :
		- .custom-control : remplace .form-control.
		- .custom-control-label : remplace .form-control-label.
		- .custom-control-input : remplace .form-control-input.
		- .custom-switch : aux côtés de .custom-control pour un switch
		- .custom-select pour les <select>
		- .custom-range : remplace .form-range.
		- Cas des <input type="file" />.
			- .custom-file-input : remplace .form-control-file.
			- .custom-file-label : la classe pour le label au lieu de l'input.
