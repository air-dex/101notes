CSS avancé :

Organiser son CSS :
	- Principe DRY :
		- Regrouper des propriétés communes par classes.
		- Le CSS aura 1 sélecteur par classe.
		- Une balise HTML appartiendra ainsi une tétrachiée de classes CSS.

La spécificité :
	- Cas où plusieurs règles CSS avec des propriétés communes s'appliquent à un même élément.
		- Conflit de valeurs pour une (ou plusieurs) propriétés entre les différentes règles.
		- Quelle valeur choisir ?
	- Le navigateur choisit la valeur de ce qui est le plus spécifique à l'élément.
		- Se fait via le calcul d'un score, la spécificité.
	- Calcul de la spécificité :
		- Les règles CSS sont divisées en 4 niveau de spécificité. Du plus spécifique au moins spécifique :
			1°) Les style inline :
				- Dans la propriété "style" d'une balise HTML.
				- Exemple : <strong style="color: red;">...</strong>
			2°) Les identifiants :
				- Propriété "id" d'une balise HTML.
				- Sélecteurs avec un #.
				- Exemple :
					- Dans le HTML : <span id="lolo"></span>
					- Dans le CSS : #lolo { /* ... */ }
			3°) Les classes et pseudo-classes CSS :
				- Classes CSS.
				- Pseudo classes genre :hover
				- Attributs se référant à une propriété HTML.
				- Exemples :
					- Classes : .maclasse { /* ... */ }
					- Pseudo-classes : span:hover { /* ... */ }
					- Attributs : .maclasse[href="https://google.com"] { /* ... */ }
			4°) Les éléments et pseudo-éléments HTML :
				- Éléments HTML.
				- Parties déléments HTML.
				- Exemples :
					- Classes : div { /* ... */ }
					- Pseudo-classes : p::first-line { /* ... */ } (1ère ligne d'un paragraphe)
		- Pour chaque sélecteur, le navigateur compte le nombre de fois où une catégorie est implémentée.
			- Exemples :
				- Un style inline va marquer 1 pt dans la catégorie "inline"
				- Une règle genre .c1.c2 va marquer 2 pts en catégorie "classes".
				- Une règle genre #id.c2 va marquer :
					- 1 pt en catégorie "identifiants".
					- 1 pt en catégorie "classes".
				- Une règle genre elt1 elt2 va marquer 2 pts en catégorie "éléments".
				- Une règle genre elt.c2 va marquer :
					- 1 pt en catégorie "classes".
					- 1 pt en catégorie "éléments".
		- Choix de la valeur à choisir :
			- Le navigateur calcule les scores de tout le monde dans chaque catégorie.
			- Le navigateur regarde les scores dans la catégorie inline.
				- Le plus fort l'emporte.
			- Les ex-aequo sont départagés avec leurs scores dans la catégorie "identifiants".
				- Le plus fort l'emporte.
			- Si encore ex-aequo, ils sont départagés avec les scores de la catégorie "classes".
				- Le plus fort l'emporte.
			- Si encore ex-aequo, ils sont départagés avec les scores de la catégorie "éléments".
				- Le plus fort l'emporte.
			- Si encore ex-aequo, c'est le dernier déclaré dans le code qui l'emporte.
	- Résumé approximatif : sont pris en compte par ordre de priorité :
		- Le code inline.
		- Les règles liées à l'ID.
		- Les règles liées à une classe.
		- Les règles liées à un élément HTML en général.
		- Les règles du dernier déclaré.
	- Savoir jouer de la spécificité :
		- En temps normal, c'est bien de garder une spécificité aussi basse que possible.
		- Ne créer du spécifique que si c'est justifié.
		- Il vaut mieux donner plusieurs classes à un élément.
			- Factorisation du style.
			- Une règle combinant plusieurs de ses classes sera plus spécifique et donc prioritaire.
	- En fin de déclaration, on peut rajouter !important pour forcer le choix de cette règle.

Le BEM :
	- Convention de nommage des sélecteurs en CSS.
	- Signification : Bloc, Élément, Modificateur.
	- Définitions :
		- Bloc : composant autonome, indépendant du teste de la page.
			- Exemples : un menu, un footer...
		- Élément : élément d'un bloc.
			- Exemple : le titre d'un bloc, une illusatration...
		- Modificateur : sélecteur modifiant l'apparence d'un bloc ou d'un élément.
			- Exemple : pour faire ressortir un bloc spécial parmi ses congénères.
	- Règles de nommage BEM :
		- Un sélecteur BEM est toujours sous forme de classe, afin d'avoir une spécificité basse.
			- C'est plus qu'un bête div ou span.
			- Ce n'est pas un bloc ou un élément en particulier dans toute la page.
		- Un bloc est nommé en décrivant sa fonction.
		- Un élément d'un bloc est nommé comme ceci : .nom-bloc__elt
			- Avec 2 underscores entre le nom du bloc et celui de l'élément.
		- Un modificateur est nommé comme ceci : .nom-bloc-ou-elt--modif
		- Si un élément est un bloc, alors on ne considère comme un bloc.
		- Si on doit augmenter la spécificité, alors il est conseillé d'utiliser un combinateur en plus.
		- Exemples :
			/* Bloc contenant une recommandation logicielle */
			.soft-recom { /* le CSS */ }
			
			/* Nom du logiciel dans la recommandation */
			.soft-recom__name { /* le CSS */ }

			/* Les mots écrivant le nom du logiciel */
			.soft-recom__soft-title { /* le CSS */ }
			.soft-recom__name span { /* le CSS */ }		/* Conseillé car + spécifique que soft-recom__soft-title */

			/* Tag d'une recommendation */
			.soft-tag { /* le CSS */ }

			/* Tags décrivant mon logiciel principal à l'utilisation */
			.soft-tag--main-soft { /* le CSS */ }

Les combinateurs :
	- Quand plusieurs éléments de CSS se combinent dans un sélecteur.
	- Les différents combinateurs :
		- Combinateur descendant : pour décrire un élément contenu dans un autre.
			- Syntaxe : E F { /* le CSS */ }
			- S'applique à F contenu dans E.
		- Combinateur enfant : descendant direct.
			- Alias "combinateur parent > enfant".
			- Syntaxe : E > F { /* le CSS */ }
			- S'applique aux F enfants de E.
		- Combinateur adjacent : élément frère venant juste après.
			- Alias "combinateur voisin".
			- Syntaxe : E + F { /* le CSS */ }
			- S'applique aux F venant après un E.
		- Combinateur de frères : élément frère venant après d'une manière générale.
			- Syntaxe : E ~ F { /* le CSS */ }
			- S'applique aux F qui sont précédés par un E, pas forcément juste avant.
	- Spécificité :
		- Marque les points pour le sélecteur E + ceux du sélecteur F.
		- Plus spécifiques que des éléments de E à qui on aurait mis une pseudo-classe ou un pseudo-élément.

Les custom properties :
	- Alias les variables CSS.
	- Depuis Chrome 49 (mars 2016) et Firefox 31 (juillet 2014).
	- Déclaration :
		- À l'intérieur d'un bloc CSS.
		- Syntaxe : --mavar: val;
	- Utilisation :
		- Via la fonction CSS var()
		- Syntaxe : pte: var(--mavar);
	- La fonction CSS var() :
		- Pour utiliser une variable CSS.
		- Prend le nom de la variable comme premier argument.
		- Les arguments suivants sont des valeurs de secours, si jamais on n'a pas accès à la valeur.
			- 1 argument supplémentaire suffit en général.

Les règles @ ("At-Rules") :
	- Règles CSS indiquant des choses au moteur de rendu, pas en lien avec le HTML où sont appliquées les règles.
	- Datent de CSS 2.
	- Certaines sont dites imbriquées, càd qui peuvent être utilisées au plus au niveau du CSS.
		- Exemple :
			@media /* des trucs en rapport avec @media */ {
				/* Règle CSS qui sera appliquée si la règle @ est respectée, ici du @media */
				sel1 {
					/* du CSS */
				}
			}
	- Des règles @ connues. Et à connaître ?
		- @import : pour inclure une feuille de CSS externe dans une autre.
			- Syntaxe : @import <url> <media-req>;
				- <url> désigne l'URL de la feuille à inclure.
					- Généralement on utilise la fonction CSS url(), avec la feuille CSS en argument.
				- <media-req> est une Media Query, pour inclure sous conditions.
		- @media : pour des Media Queries.
			- La base pour le Responsive Design.
		- @charset : l'encodage de la feuille de style.
			- Exemple : @charset 'utf-8';	/* Force à interpréter le CSS comme étant encodé en UTF-8. */

Centrer horizontalement un bloc :
	div {
		margin-left:  auto;
		margin-right: auto;
	}

Garder le ratio d'une image quand on modifie sa largeur :
	img {
		display: block;
		width: 100%;
		height: auto;
	}

Masquer un élément :
	- Deux propriétés s'affrontent : display et visibility.
	- Différences :
		- "display:none;" rend l'espace occupé par l'élément, comme s'il n'avait jamais existé.
		- "visibility: hidden;" laisse l'espace de l'élément, comme s'il était totalement transparent.

Utiliser des attributs customisés :
	- Dans le HTML, on peu mettre des attributs data-*.
		- Ça a été standardisé avec HTML 5.
	- On peut donc les cibler avec des sélecteurs CSS [data-*].
		- Exemples :
			[data-foo] { /* Règles pour tous les éléments possédant un champ customisé nommé data-foo */ }
			div[data-bar] { /* Règles pour tous les éléments <div data-bar="...">...</div> */ }
	- On peut utiliser la valeur de l'attribut customisé en CSS grâce à la fonction CSS attr().
		- Exemple :
			/* Le pseudo-élément ::before d'un <span data-foo="..."> contiendra la valeur de data-foo */
		 	span[data-foo]::before {
				content: attr(data-foo);
			}

Définir une police personnalisée :
	- Règle-At @font-face.
	- Syntaxe :
		@font-face {
			src: <URL source de la police>;
			font-family: <Nom de la police>;
			/* D'autres propriétés font-* qui la définissent aussi */
		}

Les compteurs CSS :
	- Propriétés counter-*
	- counter-reset : définir un compteur.
		- Dans le <ol>/<ul> : nom du compteur.
		- Dans le <li> : nom du compteur puis valeur à laquelle on le définit dans ce <li>
	- counter-increment : incrémenter un compteur de manière spécifique.
		- Dans le <li>
		- Syntaxe : <nom du compteur> <valeur d'incrémentation>
	- Fonction counter(<nom cpt>) : pour récupérer la valeur actuelle du compteur.
	- Le reste n'est que propriétés spécifiques à Firefox.

Définir des formes et des contours :
	- Il existe plein de fonctions CSS pour définir des formes.
		- Polygones : fonction polygon().
			- Arguments : remplissage puis suite de points
				- Un point est représenté par sa valeur x et sa valeur y
				- Remplissage : nonzero et evenodd, pour déterminer si un point est à l'extérieur ou pas.
			- Syntaxe complète : polygon(remplissage, x1 y1, x2 y2, ... , xN yN)
			- Il existe une fonction rect() pour les rectangles mais elle est dépréciée.
		- Cercles : fonction circle()
			- Syntaxe : circle(<rayon> at <position>)
			- La position par défaut est le centre de la boîte.
			- On peut mettre les extend-keywords des dégradés pour le rayon.
		- Ellipses : fonction ellipse()
			- Syntaxe : ellipse(<rayonX> <rayonY> at <position>)
			- La position par défaut est le centre de la boîte.
	- Contours :
		- inset() :
			- Syntaxe : inset(pt pr pb pl round tlrad trrad brrad blrad)
			- Les valeurs p* désignent des paddings
				- On peut n'en préciser qu'un seul (valeur commune au 4)
				- On peut n'en préciser que 2 (valeur commune à top bottom et à left/right).
			- Les valeurs *rad désignent des radius pour les coins.
		- path() : pour un chemin SVG, à éviter donc en CSS.

Centrer sur le viewport :
	// Horizontal center
	.viewport-hcenter {
		left: 50vw;		// 50% marche aussi.
		transform: translateX(-50%);
	}

	// Vertical center
	.viewport-vcenter {
		top: 50vh; 		// 50% marche aussi.
		transform: translateY(-50%);
	}
	
	// Center H&V
	.viewport-center {
		top: 50vh; 		// 50% marche aussi.
		left: 50vw;		// 50% marche aussi.
		transform: translate(-50%, -50%);
	}
