Les propriétés CSS :

Cheat sheet simple pour bien se souvenir des propriétés CSS.

* {
	/* Alignement des flex-items sur l'axe secondaire d'une Flexbox & l'axe vertical d'une Grid. */
	align: {
		content: pour une flexbox qui wrappe, comment sont réparties les lignes sur l'axe secondaire : <enum>
			- Flexbox :
				- Pour faire simple, c'est un "justify-content sur une flexbox de lignes".
				- Valeurs possibles :
					- flex-start : les lignes/colonnes sont regroupées au début de la direction secondaire.
						- Flexbox horizontale : les lignes sont regroupées en haut.
						- Flexbox verticale : les colonnes sont regroupées à gauche.
					- flex-end : les lignes/colonnes sont regroupées à la fin de la direction secondaire.
						- Flexbox horizontale : les lignes sont regroupées en bas.
						- Flexbox verticale : les colonnes sont regroupées à droite.
					- center : les lignes/colonnes sont regroupées au milieu de la flexbox.
						- Flexbox horizontale : les lignes sont regroupées à mi-hauteur.
						- Flexbox verticale : les colonnes sont regroupées au milieu, à mi-largeur.
					- space-between : les lignes/colonnes sont en space-between sur la direction secondaire.
					- space-around : les lignes/colonnes sont en space-around sur la direction secondaire.
					- stretch : les lignes/colonnes sont étirées pour occuper toute la direction secondaire.
						- Valeur par défaut.
						- Flexbox horizontale : la hauteur de chaque ligne est ajustée.
						- Flexbox verticale : la largeur de chaque ligne est ajustée.

		items: pour une flexbox, gère l'alignement sur l'axe secondaire: <enum>
			- Valeurs possibles (flexbox) :
				- stretch : les bords collent au paddings latéraux.
					- Valeur par défaut.
				- flex-start : collé au padding au début de la direction opposée.
					- Flexbox horizontale : collés en haut.
					- Flexbox verticale : collés à gauche.
				- flex-end : collé au padding à la fin de la direction opposée.
					- Flexbox horizontale : collés en bas.
					- Flexbox verticale : collés à droite.
				- center : les flex-items sont au milieu de la flexbox.
				- baseline : dans le cadre d'une flexbox horizontale, les textes sont alignés sur une même ligne.

		self: align-items pour un flex-item (Flexbox) : <enum>
			- Mêmes valeurs possibles et mêmes significations que pour align-items.
	}

	/* Animations CSS */
	animation: {
		*: <animation-duration> <animation-timing-function> <animation-delay>
		   <animation-iteration-count> <animation-direction> <animation-fill-mode>
		   <animation-play-state> <animation-name>;

		name: animations concernées par la transition: <@keyframes[]>
			- Liste d'animations CSS séparées par des virgules.

		duration: durées pour les animations: <durées[]>
			- Liste de durées séparées par des virgules.

		timing-function: fonctions d'avancement pour chaque animation: <enum[]>
			- Liste de "fonctions" CSS séparées par des virgules.
			- Avancement continu géré par courbes de Bézier :
				- cubic-bezier(p1, p2, p3, p4) : pour paramétrer précisément la courbe.
				- linear : avancement continu, à un rythme de croisière.
				- ease : rapidement rapide puis ralentit à la fin.
				- ease-in : lent au début puis accélère.
				- ease-out : rapide au dbut mais ralentit.
				- ease-in-out : lent au début et à la fin, rapide au milieu.
				- linear et les ease-* sont des valeurs particulières de cœfficients pour cubic-bezier().
			- Avancement par paliers :
				- 0% au début, 100% à la fin.
				- steps(<nb_palers>, <jump-style>)
				- <jump-style> :
					- [jump-]start : continu à gauche.
					- [jump-]end : continu à droite.

		delay: délais pour chaque animation: <durées[]>
			- Liste de durées séparées par des virgules.

		iteration-count: nombre de fois qu'on répète l'animation: <nb>|infinite
			- On met lme nombre de fois fini ou bien infinite si on veut qu'elle se fasse en boucle.

		direction: sens de parcours de l'animation : <enum>
			- normal : tout le temps de from à to.
			- alternate : de from à to, puis de to à from en alternance une fois sur deux.
			- reverse : tout le temps de to à from.
			- alternate-reverse : de to à from, puis de from à to en alternance une fois sur deux.

		fill-mode: valeurs des propriétés à la fin de l'animation : <enum>
			- none : valeurs définies hors animation.
			- backwards : valeurs au début de l'animation.
				- Typiquement celles de from {} mais ça dépend de animation-direction.
			- forwards : valeurs à la fin de l'animation.
				- Dépend de animation-direction et de animation-iteration-count
				- Typiquement celles de to {} (si direction normal et si iteration-count vaut 1).

		play-state: exécution de l'animation : running|paused
			- running : peut s'exécuter.
			- paused : en pause.
	}

	/* Arrière-plan des boites */
	background: {
		clip: à quoi l'arrière-plan se limite-t-il ? <enum>
			- border-box : content-box, padding et border.
			- padding-box : content-box et padding.
			- content-box : content-box seulement.
			- text : seulement dans le texte.
				- Pleinement supportée par Firefox et Edge seulement.
				- Dans le cas de Chrome, rajoutée le préfixe navigateur : -webkit-background-clip: text;
				- IE ne connait pas cette valeur.

		color: couleur de l'arrière plan : <couleur>

		/* Propriétés utiles si on a une image */

		image: URL de l'image de fond : <url>

		attachment: l'image est-elle fixe ? fixed|scroll
			- fixed : image fixe
			- scroll : on scrolle l'image avec le contenu de la boite.

		/* Définit des ancres par rapport aux bords de l'image et des marges internes. */
		position: {
			x: Positionnement horizontal
				- Une taille.
				- Pourcentage de la valeur de la boite (à partir de la gauche).
				- left : ancrée sur la gauche (0% *).
				- center : centrage vertical (milieu horizontal de l'image = milieu horizontal de la boite).
				- right : ancrée sur la droite.
				- Non pris en charge par Chrome, seulement Firefox et IE.

			y: Positionnement vertical
				- Une taille
				- Pourcentage de la hauteur de la boite (à partir du haut)
				- top : ancrée sur le haut (* 0%).
				- middle : centrage vertical (milieu vertical de l'image = milieu vertical de la boite).
				- bottom : ancrée sur le bas.
				- Non pris en charge par Chrome, seulement Firefox et IE.

			*: <b-p-x> <b-p-y>
				- Valeur sur x puis valeur sur y.
				- Seule méthode prise en charge par Chrome.
		}

		repeat: l'image doit-elle être répétée ? no-repeat|repeat-x|repeat-y|repeat
			- no-repeat : pas de répétitions
			- repeat-x : répétition horizontale.
			- repeat-y : répétition verticlae.
			- repeat : répétition horizontale et verticale.
	}

	/* Bordures des boites */
	border: {
		collapse: si "bordures mitoyennes", doivent-elles être communes ou séparées ? collapse|separate
			- Dans un tableau, au niveau du <table>.

		color: couleur de la bordure : <couleur>

		style: style de bordure : <enum>
			- Valeurs possibles :
				- none|hidden : rien
				- solide : ligne continue
				- dotted : pointillés
				- dashed : tirets
				- double : double ligne
				- inset : impression 3D "trou dans le document"
				- outset : impression 3D "trou dans le document"
				- groove : impression 3D "bordure inset"
				- ridge : impression 3D "border outset"

		width: taille de la bordure : <size>

		/* Bordures latérales */
		top|left|right|bottom: {
			color: border-color pour la bordure en question : <couleur>
			style: border-style pour la bordure en question : <enum border-style>
			width: border-width pour la bordure en question : <size>
		}

		/* Les arrondis des coins */
		radius: rayon (de courbure) pour les coins : <size>
			- Taille absolue ou relative
			- Si pourcentage, pourcentage de la longueur/largeur.

		#{V}-#{H}-radius: border-radius pour un coin donné: <size>
			- #{V} : verticalité du coin : top|bottom.
			- #{H} : horizontalité du coin : left|right.
			- Les 4 coins : top-left, top-right, bottom-left, bottom-right.
	}

	bottom: si positionnement non statique, décalage par rapport au bas (au cul) de la référence: <size>

	box: {
		shadow: mettre un effet d'ombre : <size> <size> <color>
			- Syntaxe : box-shadow: <offset-x> <offset-y> <shadow-color>;
				- offset-x : décalage horizontal. Offset positif : décalage vers la droite.
				- offset-y : décalage vertical. Offset positif : décalage vers le bas.
				- shadow-color : couleur de l'ombre.

		sizing: stratégie de calcul de la taille des boites: content-box|border-box
			- content-box : size(box) == size(content-box)
			- border-box  : size(box) == size(content-box) + padding + border
	}

	/* Gestion des fragments d'un contenu éclaté entre deux colonnes ou pages */
	break: {
		before : peut-on avoir un saut avant le contenu ? <enum>
			- auto : on peut.
			- avoid : NON ! Saut interdit avant le contenu.
				- Internet Explorer utilise aussi "avoid-column" pour les colonnes.
				- Edge utilisait également avoid-column avant son passage à Chromium.
			- avoid-page : avoid pour les pages.
			- column : saut de colonne obligatoire avant le contenu.
				- Non supporté par Firefox et Safari.
			- page : saut de page obligatoire avant le contenu.
			- Autrefois nommée -webkit-column-break-before sur Chrome et Safari.

		after : peut-on avoir un saut après le contenu ? <enum>
			- auto : on peut.
			- avoid : NON ! Saut interdit après le contenu.
				- Internet Explorer utilise aussi "avoid-column" pour les colonnes.
				- Edge utilisait également avoid-column avant son passage à Chromium.
			- avoid-page : avoid pour les pages.
			- column : saut de colonne obligatoire après le contenu.
				- Non supporté par Firefox et Safari.
			- page : saut de page obligatoire après le contenu.
			- Autrefois nommée -webkit-column-break-after sur Chrome et Safari.

		inside : peut-on avoir un saut au beau milieu d'un contenu ? <enum>
			- auto : oui.
			- avoid : non pour tout.
			- avoid-column : non pour les colonnes.
				- Non supporté par Firefox.
			- avoid-page : non pour les pages.
				- Non supporté par Firefox et IE.
			- Support tardif pour Firefox : depuis Firefox 65 en Janvier 2019.
			- Autrefois nommée -webkit-column-break-inside sur Chrome et Safari.
	}

	caption-side: où placer la légende d'un tableau (table caption) ? top|bottom
		- Il existe d'autres valeurs, mais seulement supportées par Firefox.

	/* Curseur dans une zone de texte */
	caret: {
		color : couleur du curseur dans une zone de saisie de texte : <color>
			- Non pris en charge par IE, ni par Edge avant Chromium.
	}
	
	clear: pour ne pas se faire flotter dessus: <enum>
		- none : on ne bloque pas le flottement.
		- left : bloque le flottement à gauche.
		- right : bloque le flottement à droite.
		- both : bloque le flottement des deux côtés.

	/* Zone visible d'un élément */
	clip: {
		* : définition d'une zone pour des éléments placés de façon absolue : <shape>
			- Propriété dépréciée.
			- <shape> est définie avec la fonction rect(), elle aussi dépréciée :
				rect(haut bas gauche droite)

		path: version récente, avec les contours modernes : <basic-shape>
			- <basic-shape> est définie avec les fonctions inset(), circle(), polygon() et ellipse().
	}

	color: couleur du texte : <couleur>

	/* Disposition en colonnes */
	column: {
		count: nombre de colonnes: <int>

		width: largeur idéale d'une colonne : <size>

		gap: largeur des gouttières verticales entre deux colonnes : <size>
			- Marche aussi pour les éléments enfants dans CSS Grid et Flexbox.

		span : un élément doit-il s'étendre sur toutes les colonnes : none|all
			- none : non.
			- all : toutes les colonnes.
			- Sur les éléments fils du container des colonnes.

		/* Séparateur entre 2 colonnes */
		rule: {
			*: <column-rule-style> <column-rule-width> <column-rule-color>;

			style: style du séparateur : <enum>.
				- Comme border-style.

			width: épaisseur du séparateur : <size>.
				- Comme border-width.

			color: couleur du séparateur : <color>.
				- Comme border-color.
		}
	}

	columns: <column-width> <column-count>;

	/* Compteur dans les listes */
	counter: {
		increment : incrémentation du compteur : <nom compteur> <incval>
			- Dans le <li>
			- <incval> est une valeur d'incrémentation, valant 1 par défaut.
		
		reset: remet un compteur à zéro : <nom compteur>
			- Dans le <ol/ul> : nom du compteur utilisé pour la liste
			- Dans le <li> : là où on réinitialise le compteur.

		/* Les autres propriétés ne sont prises en charge que par Firefox */
	}

	cursor : type de curseur de souris sur l'élément : <enum>
		- Non pris en charge par Fennec et la WebView Android.

	display: mode d'affichage d'un bloc : <enum>
		- Propriété complexe, dont la valeur dépend du mode de layout qu'on veut utiliser.
		- Valeurs :
			- none : on n'affiche pas.
			- block : force la balise à être une balise bloc.
			- inline : force la balise à être une balise inline :
				- inline-block : balise bloc à comportement inline.
					- Le beurre et l'argent du beurre.
				- inline-flex : Flexbox inline.
				- inline-grid : Grid inline.
			- flex : Flexbox bloc.
			- grid : Grid bloc.
			- table : comme un <table> HTML.

	filter: effet "filtre Insta" pour une image : <fonction()>
	
	/* This is a Flexbox world! */
	flex: {
		*: <flex-grow> <flex-shrink> <flex-basis>

		basis: taille souhaitée pour un flex-item : <size>|auto
			- width pour un flexbox horizontale et height pour une flexbox verticale.
			- Valeurs possibles :
				- taille : la taille écrite.
				- auto : taille définie automatiquement en fonction du contenu.

		direction: dans quelle direction range-t-on les éléments de la flexbox ? <enum>
			- row : sur une ligne, de gauche à droite.
				- Les contenus sont "alignés à gauche".
			- row-reverse : sur une ligne, de droite à gauche.
				- Les contenus sont "alignés à droite".
			- column : en colonne, de haut en bas.
				- On empile à partir du.
			- column-reverse : en colonne, de bas en haut.
				- On "empile" à partir du bas de la flexbox.
		
		flow: <flex-direction> <flex-wrap>

		grow : facteur d'agrandissement : <entier>
			- 0 : pas d'agrandissement possible.
				- Valeur par défaut.
			- n>0 : cœfficient d'agrandissement.
				- L'espace supplémentaire est accordé au prorata des flex-grow de tous les flex-items.

		shrink : facteur de rétrécissement : <entier>
			- 0 : pas de rétrécissement possible.
			- n>0 : on peut réduire le flex-item jusqu'à n fois sa taille si nécessaire.
			- Valeur par défaut : 1.

		wrap: Que faire en cas de débordement ? <enum>
			- nowrap : ça dépasse.
			- wrap : on va à la ligne.
				- Flexbox horizontale : sur une nouvelle ligne en dessous.
				- Flexbox verticale : dans une nouvelle colonne à droite.
			- wrap-reverse : on va à la ligne de l'autre côté.
				- Flexbox horizontale : sur une nouvelle ligne au dessus.
				- Flexbox verticale : dans une nouvelle colonne à gauche.
	}

	float: pour flotter au dessus des autres : <enum>
		- none : on ne flotte pas.
		- left : flottement à gauche.
		- right : flottement à droite.

	/* Police de caractères */
	font: {
		family: le nom de la police : <nom_police>, <nom_font_fallback_1>, <nom_font_fallback_2>...

		size: taille de la police : <size>

		style: police en italique : normal|italic|oblique|oblique <angle>
			- Février 2020 : oblique <angle> n'est supporté que par Firefox >= 61 (juin 2018).

		/* Options de style pour les lettres. */
		variant: { 
			caps: Utilisation des majuscules de petites tailles: normal|small-caps|all-small-caps
				- Autrefois font-variant tout court.
				- Désormais "font-variant-caps" depuis CSS Fonts Module Level 3 et ses "font-variant-*".
				- Non supporté par Safari et IE, qui en est encore à font-variant.
				- small-caps : seulement les minuscules.
				- all-small-caps : même les majuscules.

			/* Les autres apparaissent avec CSS Fonts Module Level 3 */
			numeric: style pour les nombres : normal|slashed-zero|diagonal-fractions|stacked-fractions|...
				- normal : rien
				- slashed-zero : une barre au milieu du zéro.
				- diagonal-fractions : les fractins écrites en diagonale
				- stacked-fractions : les fractions écrites en vertical

			ligatures: pour les ligatures, dans les trucs genre "ff" ou les deux 'f' sont entremêlés.
				- (no-)common-ligatures : force/désactive les ligatures courantes.
				- (no-)discretionary-ligatures : force/désactive les ligatures liées à la police.

			etc.
		}

		weight: taille de la graisse : <nombre de 1 à 1000>|normal|bold|bolder|lighter
	}

	gap: tailles des gouttières horizontales et verticales.
		- 2 syntaxes possibles :
			- gap: <row-gap> <column-gap>;
				- Propriété chapeau
			- gap: <size>;
				- Pour une largeur commune.
				- Équivaut à :
					$size: une taille commune;
					row-gap: $size;
					column-gap: $size;

	/* This is a CSS Grid world! */
	grid: {
		/* Déclarations des éléments de grille */
		template: {
			*: propriété chapeau
				- Syntaxes possibles :
					- <grid-template-rows> / <grid-template-columns>
					- liste de (string, size) / <grid-template-columns>
						- Dans le cadre de zones nommées
						- La string représente les cellules de la rangée (cf. grid-template-areas).
						- La taille représente celle de la ligne.
						- Idéal pour des représentations très visuelles de la grille, de ce style :
							grid-template: "ali ali ali bob" 250px
							               " .   .  zoe bob" auto
							               "ben tim tim tom" 200px /
										    10% 40% 30% 20%;

			columns: déclaration des colonnes : <sizelist>
				- Liste des tailles de chaque colonne de la grille.
				- Entre deux tailles, on peut avoir une série d'alias pour désigner une ligne.
					- Syntaxe : [nom 1 nom2 ... nomN]

			rows: déclaration des rangées : <sizelist>
				- Liste des tailles de chaque rangée de la grille.
				- Entre deux tailles, on peut avoir une série d'alias pour désigner une ligne.
					- Syntaxe : [nom 1 nom2 ... nomN]

			areas: déclaration des zones de la grille : <stringlist>
				- Une string = 1 rangée de la grille où chaque cellule est désignée.
				- Chaque cellule est représentée :
					- Soit par un nom, correspondant à une zone (nommée).
					- Soit par un point, qui signifie que la cellule est vide.
		}

		/* Délimitations verticales d'une Grid Area */
		column: {
			*: <grid-column-start> / <grid-column-end>

			start: n° de la ligne de gauche: <n° ligne>

			end: n° de la ligne de droite: <n° ligne>

			gap: largeur des gouttières vericales : <size>
				- Dépréciée au profit de column-gap.
		}

		/* Délimitations horizontales d'une Grid Area */
		row: {
			*: <grid-row-start> / <grid-row-end>

			start: n° de la ligne du haut: <n° ligne>

			end: n° de la ligne du bas: <n° ligne>

			gap: largeur des gouttières horizontales : <size>
				- Dépréciée au profit de row-gap.
		}

		area: propriété chapeau des délimitations d'une Grid Area.
			- Syntaxes :
				- <grid-row-start> / <grid-row-end> / <grid-column-start> / <grid-column-end>
				- <nom zone> (nom de la zone dans grid-template-areas).

		/* Placement automatique des Grid Items */
		auto: {
			rows: hauteur des colonnes si on doit en rajouter: <size>
				- Valeur par défaut : auto, qui est la hauteur des éléments quand on rajoute.

			columns: largeur des colonnes si on doit en rajouter: <size>
				- Valeur par défaut : auto, qui est la largeur des éléments quand on rajoute.

			flow: comment remplir ? row|column sparse|dense
				- row|column : dimension préférentielle
					- Si row, on met les nouveaux éléments dans une nouvelle rangée.
					- Si column, on met les nouveaux éléments dans une nouvelle colonne.
				- sparse|dense : comportement de remplissage
					- dense : on cherche à combler les trous avant d'agrandir la grille si nécessaire.
					- sparse : on place systématiquement à la suite, sans chercher à combler les trous.
		}
	}

	height: hauteur d'une boite : <size>|auto|%
		- Valeur auto : calculée automatiquement selon le contenu et la stratégie de calcul (box-sizing).
		- Pourcentage : pourcentage de la hauteur de l'élément parent.

	left: si positionnement non statique, décalage par rapport à la gauche de la référence: <size>

	/* Alignement des flex-items sur l'axe principal d'une Flexbox & l'axe horizontal d'une Grid. */
	justify: {
		content: dépend de Flexbox ou Grid <enum>
			- Flexbox : Gère l'alignement des flex-items sur l'axe principal.
			- Valeurs possibles :
				- flex-start : à partir du coin de départ de la flexbox.
				- flex-end : à partir du coin opposé au coin de départ de la flexbox.
				- center : au milieu.
				- space-between : justifié, avec les éléments au bord ancrés sur le padding.
				- space-around : justifié, avec tous les éléments ayant une sorte de margin virtuelle autour.
					- La margin virtuelle est la même pour tout le monde, même les éléments sur le bord.
					- Pour les éléments au bord, la margin virtuelle commence au padding, pas à la border.
	}

	letter-spacing: espacement entre les lettres : normal|<size>

	line-height: hauteur d'une ligne de texte : normal|<size>
		- Si pas d'unité : cœfficient multiplicateur par rapport à la taille de police.

	/* Style des listes à puces et des listes numérotées. */
	list-style: {
		type: type de nombre (ol) ou de puce (ul): <enum>
			- Valeurs possibles :
				- none : rien (surtout pour ul).
				- ul : disc|circle|square|none
				- ol :
					- decimal : le nombre avec un point.
					- decimal-leading-zero : decimal + des zéros devant.
					- lower|upper-roman : chiffres romains en minuscules|majuscules.
					- lower|upper-alpha : alphabet latin en minuscules|majuscules.
					- lower|upper-greek : alphabet grec en minuscules|majuscules.

		image: image personnalisée pour une puce : <url>

		position: le nombre/la puce (li::marker) est-elle dans le <li> ? inside|outside
	}

	/* Épaisseur de la marge extérieure. */
	margin: {
		*: épaisseur commune à toute les marges extérieures : <size>.

		bottom: épaisseur de la marge extérieure inférieure : <size>.

		left: épaisseur de la marge extérieure gauche : <size>.

		right: épaisseur de la marge extérieure droite : <size>.

		top: épaisseur de la marge extérieure supérieure : <size>.
	}

	/* Dimensions maximales */
	max: {
		height: hauteur maximale (comme son nom l'indique) : <size>

		width: longueur maximale (comme son nom l'indique) : <size>
	}

	/* Dimensions minimales */
	min: {
		height: hauteur minimale (comme son nom l'indique) : <size>
		
		width: longueur minimale (comme son nom l'indique) : <size>
	}

	/* Éléments replacés (typiquement <img> ou <video>) dans une boîte */
	object: {
		fit: redimensionnement de l'élément dans la content-box ? fill | contain | cover | none | scale-down
			- Équivalent à des options de papier-peint.
			- Valeurs possibles :
				- none : on ne cherche pas à redimensionner.
					- Élément rogné si plus grand que la boîte.
					- Élément non déformé (proportions conservées).
					- Ce qu'on voit dépend de object-position.
				- fill : on le redimensionne aux dimensions de la content-box.
					- Élément non rogné.
					- Élément déformé si ses proportions ne sont pas celles de la content-box.
						- L'élément prend les proportions de la content-box.
						- Ne cherche donc pas à conserver la proportion de l'élément.
					- L'élément prend les dimensions de la content-box.
				- contain : on le fait rentrer dans la content-box en conservant sa proportion.
					- Élément redimensionné
					- Élément non rogné (proportion conservées).
					- La dimension la plus grande de l'élément s'étale sur toute celle de la content-box.
				- cover : on agrandit l'image de manière à ce que toute la content-box soit remplie.
					- Élément redimensionné.
					- Élément rogné si ses proportions sont différentes de celles de la content-box.
					- La dimension la plus petite de l'élément s'étale sur toute celle de la content-box.
					- Ce qu'on voit dépend de object-position.
				- scale-down: image la plus petite entre none et contain.

		position: position de l'objet dans la boîte : <position>
			- On précise d'abord le décalage horizontal puis le décalage vertical.
			- Décalage : un bord puis une taille.
				- Les bords : left, right, bottom, top, center (dépend de la dimension).
				- Si pas de taille alors elle est nulle.
			- Exemple :
				object-position: center bottom 10%;		/* Milieu horizontal + à 10% du bas */
	}

	opacity: opacité d'un élément, float de 0 (totalement transparent) à 1 (totalement opaque)

	order: ordre de traîtement des enfants d'une grille ou d'une Flexbox: <index entier>
		- Notes sur l'index :
			- L'index peut-être positif, nul ou négatif, du moment qu'il est entier.
			- Les index les plus faibles sont traîtés les premiers.
			- En cas d'index égaux, l'ordre dans le DOM fait foi.
		- Dans le cadre de Flexbox :
			- Ordre des flex-items dans la flexbox.
		- Dans le cadre de CSS Grid :
			- Ordre pour placer les grid-items À PLACEMENT AUTOMATIQUE dans la grille.
			- Inutile sur les éléments placés explicitement (placement défini).
		- Valeur par défaut : 0.

	orphans : nombre minimal de lignes orphelines d'un paragraphe en bas de colonne/page : <integer>
		- Si ce ne peut être respecté, le paragraphe commencera après le saut.

	/* Gestion du scroll */
	overflow: {
		*: gestion du scroll sur les deux axes : <enum_scroll>
			- Aussi un raccourci pour overflow-x overflow-y dans cet ordre.

		x: gestion du scroll sur l'axe horizontal : <enum_scroll>

		y: gestion du scroll sur l'axe vertical : <enum_scroll>

		<enum_scroll> :
			- visible : affiche le contenu qui dépasse.
			- hidden : masque le contenu qui dépasse.
			- scroll : met des ascenseurs pour scroller.
			- auto : automatique en fonction du navigateur.
	}

	/* Épaisseur de la marge intérieure. */
	padding: {
		*: épaisseur commune à toute les marges intérieures : <size>.

		bottom: épaisseur de la marge intérieure inférieure : <size>.

		left: épaisseur de la marge intérieure gauche : <size>.

		right: épaisseur de la marge intérieure droite : <size>.

		top: épaisseur de la marge intérieure supérieure : <size>.
	}

	page-break-* : fragmentation des paragraphes entre plusieus pages.
		- Dépréciée au profit des break-* qui fonctionnent pareil.

	position: type de positionnement de l'élément dans la page : <enum>
		- Valeurs possibles :
			- static : positionnement classique, par rapport au parent.
			- absolute : positionnement par rapport à la page Web (élément fixe dans la page).
				- Par rapport à la boîte en position absolute parente, au pire <body>.
			- fixed : positionnement par rapport à l'écran (élément fixe sur l'écran).
			- relative : positionnement par rapport à sa position théorique.

	right: si positionnement non statique, décalage par rapport à la droite de la référence: <size>

	row-gap: largeur des gouttières horizontales : <size>

	/* Gestion du comportement du scroll dans la page */
	scroll: {
		behavior : gestion du scroll jusqu'à un lien interne ('#') : auto|smooth
			- auto : instantané
			- smooth : défilement doux.
	}

	/* Zone à l'intérieur de laquelle un texte n'a pas le droit d'aller. */
	shape: {
		margin : distance autour de la zone : <size>

		outside : définition de la zone : <basic-shape>
			- <basic-shape> est un contour défini avec des fonctions comme inset() ou polygon()
			- Définie dans un élément qui est la zone à contourner.
	}

	/* Texte */
	text: {
		align: Alignement du texte : left|center|right|justify

		/* Gestion du barré/souligné */
		decoration: {
			line: Positionnement de la ligne qui barre : none|underline|line-through|overline|blink
				- Autrefois text-decoration tout court, désormais "text-decoration-line".

			/* Les autres apparaissent avec CSS Text Decoration Module Level 3. */
			/* Pris en charge par tout le monde, sauf IE. */
			color: couleur du trait : <couleur>

			style: style du trait : solid|double|dotted|dashed|wavy
		}

		indent: Indentation au début d'un paragraphe : <size>

		transform: gestion des lettres majuscules : capitalize|uppercase|lowercase|none
			- capitalize : Première lettre de chaque mot en majuscule.
			- none : bloque les modifications.
	}

	top: si positionnement non statique, décalage par rapport au sommet de la référence: <size>

	/* Transitions CSS */
	transition: {
		*: <transition-property> <transition-duration> <transition-timing-function> <transition-delay>;

		property: propriétés concernées par la transition: <propriétés[]>
			- Liste de propriétés CSS séparées par des virgules.
			- all: toutes les propriétés.

		duration: durées pour les transitions de chaque propriété de la transition: <durées[]>
			- Liste de durées séparées par des virgules.

		timing-function: fonctions d'avancement pour chaque propriété de la transition: <enum[]>
			- Liste de "fonctions" CSS séparées par des virgules.
			- Avancement continu géré par courbes de Bézier :
				- cubic-bezier(p1, p2, p3, p4) : pour paramétrer précisément la courbe.
				- linear : avancement continu, à un rythme de croisière.
				- ease : rapidement rapide puis ralentit à la fin.
				- ease-in : lent au début puis accélère.
				- ease-out : rapide au dbut mais ralentit.
				- ease-in-out : lent au début et à la fin, rapide au milieu.
				- linear et les ease-* sont des valeurs particulières de cœfficients pour cubic-bezier().
			- Avancement par paliers :
				- 0% au début, 100% à la fin.
				- steps(<nb_palers>, <jump-style>)
				- <jump-style> :
					- [jump-]start : continu à gauche.
					- [jump-]end : continu à droite.

		delay: délais pour chaque propriété concernée par la transition: <durées[]>
			- Liste de durées séparées par des virgules.
	}

	visibility: gère la visibilité d'un élément : visible|hidden|collapse
		- visible : l'élément est visible.
		- hidden : l'élément est caché, mais l'espace qu'il est censé occupé est totalement vide.
			- Comme s'il était transparent.
		- collapse : l'élément est caché, mais l'espace qu'il est censé occupé est rendu aux autres.
			- Comme s'il n'avait jamais existé.
			- À éviter. Il vaut mieux lui préférer "display: none;"
				- Ne marche vraiment que pour les tableaux et les Flexbox.
				- Traîté comme "hidden" dans les autres cas.

	widows : nombre minimal de lignes veuves d'un paragraphe en haut de colonne/page : <integer>
		- Si ce ne peut être respecté, le paragraphe finira avant le saut.

	width: largeur d'une boite : <size>|auto|%
		- Valeur auto : calculée automatiquement selon le contenu et la stratégie de calcul (box-sizing).
		- Pourcentage : pourcentage de la largeur de l'élément parent.
		- fit-content : ne s'étend pas au delà de son contenu.
			- Ne marche pas pour firefox. Lui préférer -moz-fit-content

	word-spacing: espacement entre les lettres : normal|<size>

	z-index: dans le cadre d'un positionnement non statique, index vertical : <nombre>
		- Celui qui est visible sera celui qui est le plus haut.
}

Valeurs particulières :
	- inherit : TODO
	- initial : TODO
	- unset : TODO
