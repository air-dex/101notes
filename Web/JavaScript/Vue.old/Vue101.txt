Vue.js 101 :

Généralités :
	- Vue.js est un framework JS pour construire des interfaces Web et des applications Web monopages.
		- On a un template qu'on manipule à partir du JS.
	- Objectifs :
		- Progressif/versatile/polyvalent : on peut l'introduire progressivement dans notre projet.
			- On peut n'utiliser Vue que sur une partie de notre projet si ça nous chante.
				- Angular et React veulent la main sur tout le projet, pas Vue.
			- Vue a une stack bien à lui, mais on ne peut en utiliser qu'une seule partie si on veut.
		- Performant :
			- Peu de code JS à embarquer dans sa page Web, donc se charge rapidement.
			- Rapide
		- Code facilement maintenable et testable.
			- Avec des composants réutilisables.
		- Serait plus facile à apprendre qu'Angular ou React.
	- S'écrit "Vue", mais se prononce "View".
	- Créé par Evan You, un ancien de chez Google qui a voulu "ne garder que le meilleur d'Angular".
	- Supporté par Alibaba.
	- Site Internet : https://vuejs.org/

Variantes de Vue :
	- Vue possède plein de variantes, 10 au total.
	- Versions dev/prod :
		- Versions de développement : versions non-minifiées, en ".min.js". Idéal pour déboguer.
		- Versions de production : versions minifiées, en ".min.js".
	- Versions full/runtime-only :
		- Versions full : versions intégrales, avec :
			- Le compilateur qui transforme les templates en fonctions JS render();
			- Le runtime qui crée les instances de Vue, s'occupe du DOM virtuel et d'exécuter render();
		- Versions runtime-only : version sans compilateur.
			- Fichiers avec un ".runtime" dedans.
			- Utiles dans les environnements où le CSP est activé.
				- Environnement avec des headers HTTP en "[X-]Content-Security-Policy:"
				- Parce que la version intégrale n'y fonctionne pas.
	- Versions dépendant du style pour les modules :
		- UMD : modules sauce UMD.
			- Fichiers : vue[.rumtime][.min].js
			- Vue est développé comme ça de base.
		- CommonJS : modules sauce CommonJS (Node, browserify, Webpack 1)
			- Fichiers : vue[.runtime].js
		- ES Modules :
			- Pour empaqueteurs (bundlers : Webpack 2+, Parcel, Rollup) : vue[.runtime].esm.js
			- Pour navigateurs : vue.esm.browser[.min].js
				- À mettre dans les balises <script type="module"></script>

Récupération de Vue.js :
	- On récupère le fichier, on l'embarque avec <script> et c'est bon.
	- Récupération via un CDN :
		<!-- Dernière version -->
		<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
		<!-- Version particulière -->
		<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js@X.Y.Z"></script>
		<!-- En tant qu'ES Module -->
		<script type="module">
			import Vue from 'https://cdn.jsdelivr.net/npm/vue/dist/vue.esm.browser.js'
			// Pour une version particulière
			import Vue from 'https://cdn.jsdelivr.net/npm/vue@X.Y.Z/dist/vue.esm.browser.js'
		</script>
	- Récupération via NPM : npm install vue
	- Via Bower : bower install vue
		- Seulement les variantes UMD de Vue.

Principe de Vue :
	- On a un template HTML et du JS dedans qui le manipule :
		<!doctype html>
		<html>
			<head>
				<!-- Le contenu de notre head -->
				<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
			</head>
			<body>
				<!-- Le contenu de notre body -->
				<script type="text/javascript">
					// On manipule le reste de la page à partir d'ici.
				</script>
			</body>
		</html>
	- Le template = la page HTML elle-même
	- On n'est même pas à l'échelle d'un projet.
	- Vue manipule la vue avec un DOM virtuel.

L'instance de Vue :
	- Abrégée ici "IV".
	- C'est l'instance qui va permettre de faire le lien entre la vue et le modèle.
		- VM dans le pattern MVVM (Mode-View-ViewModel).
	- C'est un objet de type "Vue" :
		let vm = new Vue(opts);
	- Les options de l'instance de Vue sont dans un object JS.
		- Les champs relatifs aux données :
			- data : contient les données que la vue utilisera pour l'affichage.
				- Elles sont dans un objet JS.
		- Les champs relatifs à la vue (DOM virtuel) :
			- el : élément sur lequel agit la vue.
				- C'est une string ou un élément du DOM.
				- Il est conseillé de cibler l'élément à diriger via l'IV avec un ID.
				- Exemple :
					let mv = new Vue({ el: "#elt" });
					// Pareil
					let mv = new Vue({ el: window.document.querySelector('#elt') });
					// Encore pareil
					let mv = new Vue({ el: document.getElementById('elt') });
	- Accès aux  données de l'IV :
		- Options de configuration : vm.$optconfig
			- optconfig est un champ de l'objet passé au constructeur.
		- Données dans l'IV : vm.dataname
			- dataname est un champ de l'objet data passé au constructeur.
			- vm.dataname === vm.$data.dataname
		- Exemple :
			let vm = new Vue({
				data: {
					foo: 26,
					bar: true,
				}
				foo: 42,
				baz: 'Enrico !'
			});
			console.log(vm.$foo);     	// "42"
			console.log(vm.foo);      	// "26"
			console.log(vm.$data.foo);	// "26"
			console.log(vm.bar);      	// "true"
			console.log(vm.$baz);     	// "Enrico !"
			console.log(vm.baz);      	// "undefined"
	- L'instance est réactive sur les données :
		- Elle s'arrange via des bindings pour propager la modification.
		- Si on rajoute des propriétés supplémentaires via l'IV alors elles ne seront pas rajoutées.
		- Si on gèle l'objet de départ (avec Object.freeze()), alors il ne propagera plus ses modifications.
		- Exemple :
			let d = yolo();               		// d a un champ "foo", mais pas de champ "toto". 
			let vm = new Vue({ data: d });		// L'IV
			// Les champs d.foo et vm.foo sont désormais bindés. Si l'un change, l'autre aussi.
			console.log(vm.foo === d.foo);      	// "true"
			vm.foo = bar()
			console.log(vm.foo === d.foo);      	// Encore "true" (valeur des deux : bar())
			d.foo = baz()
			console.log(vm.foo === d.foo);      	// Toujours "true" (valeur des deux : baz())
			vm.toto = trucNotUndefined()
			console.log(vm.toto === d.toto);    	// "false"
			console.log(vm.toto === undefined); 	// "false"
			console.log(d.toto === undefined);  	// "true"
			Object.freeze(d);
			vm.foo = zabNotBaz()
			console.log(vm.foo === d.foo);      	// "false"
			console.log(vm.foo === zabNotBaz());	// "true"
			console.log(d.foo === baz());       	// "true"
	- Le cycle de vie d'une IV :
		- Le cycle :
			1°) Instanciation :
				- C'est le moment où elle est instanciée avec new Vue()
			2°) Initialisation des événements et du cycle de vie.
				- Les hooks sont repérés ici.
			3°) Initialisation des données et de la réactivité.
				- Met en place les champs relatifs aux données.
			4°) L'IV construit son DOM virtuel à partir des options "el" et "template".
				- L'attribut templ
			5°) Màj du DOM avec le DOM virtuel.
				- Création de vm.$el et il met le contenu de "el" dedans.
			6°) L'IV écoute les potentiel changements de données.
			7°) Une donnée change.
			8°) Màj du DOM virtuel.
			9°) Actualisation du DOM.
			10°) Répétition des étapes 7°) à 9°)
			11°) On invoque la destruction de l'IV, quand le composant est retiré du DOM.
			12°) On détruit (presque) tout :
				- Le DOM virtuel
				- Les IV enfantes
				- Les watchers
				- La boucle d'événements
			13°) Comme la carte du tarot de Marseille : c'est La Mort !
		- Les hooks :
			- Il existe des hooks où on peut agir dans le cycle de vie.
				- Dans les hooks, "this" désigne l'IV.
				- Éviter les fonctions fléchées.
			- beforeCreate() : l'instance vient à peine d'être créée.
				- Entre les étapes 2°) et 3°).
				- Il n'y a que les hooks.
				- Absolument RIEN n'est en place :
				- On n'a pas accès à ses options de configuration ("computed", "data", "watch"...).
				- La réactivité n'est pas encore en place.
			- created() : tout est en place, SAUF $el.
				- Entre les étapes 3°) et 4°).
				- L'IV est désormais solide sur son accès aux données.
				- Rien n'est encore visible.
				- Idéal pour lancer des choses asynchrones sur les données.
					- L'IV travaille déjà en arrière-plan sur les données en attendant de s'afficher.
					- Exemple : appels AJAX à une API Web.
			- beforeMount() : avant que le DOM virtuel n'actualise le DOM réel.
				- Entre les étapes 4°) et 5°).
				- Le template est compilé, mais pas encore affiché.
				- Appelé juste avant render();
			- mounted() : après que le DOM virtuel ait actualisé le DOM réel.
				- Entre les étapes 5°) et 6°).
				- On a désormais accès au dom via "this.$el"
				- Équivalent du $(sel).ready(function() { /* ... */ }) de jQuery.
			- beforeUpdate() : avant là màj d'une donnée.
				- Entre les étapes 8°) et 9°).
				- Le DOM virtuel a changé mais le DOM réel n'est pas encore màj.
				- C'est le moment de "nettoyer" le DOM réel avant les màj :
					- Retirer des event listeners, par exemple.
			- updated() : après la màj du DOM réel.
				- Entre les étapes 9°) et 10°).
				- C'est le moment pour faire nous-même des changements sur le DOM réel.
					- Ajouter de nouveaux event listeners, par exemple.
			- beforeDestroy() : juste avant la destruction.
				- Entre les étapes 11°) et 12°).
				- C'est le dernier moment où on pourra accéder au composant avec toutes ses capacités.
				- C'est le moment idéal pour stopper proprement les event listeners.
			- destroy() : composant visuellement inexistant.
				- Entre les étapes 12°) et 13°).
				- On n'a plus accès à $el.
				- Les instances Vue enfantes sont mortes.
				- La boucle d'événements est débranchée.
				- On peut annoncer la mort du composant ici.
	- TODO

Les templates Vue :
	- Qui peut-être la template Vue ?
		- Le HTML d'un élément ciblé par vm.$el.
		- Le contenu de vm.$template dans vm.$el.
		- Si vm.$el n'est pas préciser ça peut être la page entière.
	- Via des zones d'interpolation et des directives :
		- Zones d'interpolation : trous textuels de la template à remplir.
			- Le design pattern template dans toute sa splendeur.
		- Directives : attributs HTML v-*.
			- Gros problème : incompatible avec la validation W3C.
	- Remplir les trous 
		- Les zones d'interpolation :
			- Syntaxe moustache : {{ /* ... */ }}
			- Vue évalue ce qu'il y a entre les délimiteurs et remplace le tout par cette évaluation.
			- Évaluation sécurisée :
				- L'évaluation est interprêtée comme du texte pur.
				- Si l'évaluation renvoie de l'HTML alors celui-ci sera échappé pour apparaître comme du texte.
				- Permet donc de se prémunir des failles XSS à base d'injection de code HTML.
					- Rappel : XSS veut dire "CROSS-Site Scripting".
			- On peut modifier les délimiteurs :
				- Propriété "delimiters" de l'IV.
				- Syntaxe: delimiters: [delim_o, delim_f]
					- delim_o : marqueur de le début de la zone d'interpolation.
					- delim_f : marqueur de la fin de la zone d'interpolation.
				- Par défaut : delimiters: ["{{", "}}"]
					- D'où la syntaxe moustache.
				- Exemple :
					<script>
						let vm = new Vue({
							el: "cirufe",
							data: {
								melodie: 'nelson'
							},
							delimiters: ["£!", ";%"]
						})
					</script>
					<!-- Rendu : "<div id="cirufe">{{ melodie }}</div>" -->
					<div id="cirufe">{{ melodie }}</div>
					<!-- Rendu : "<div id="cirufe">nelson</div>" -->
					<div id="cirufe">£! melodie ;%</div>
		- Directive v-html :
			- Syntaxe : <div|span v-html="truc"></div>
				- truc correspond à vm.truc / vm.$data.truc.
			- Vue remplace le contenu de la balise par le contenu de truc.
			- Évaluation non sécurisée :
				- Vue n'échappe pas le HTML.
				- Potentielle faille XSS pour des injections HTML.
	- TODO

Vue CLI :
	- TODO
