Vue 101 :

Vue CLI :
	- Le projet ne doit pas avoir de lettre majuscule !
	- Créer un projet : vue create nom-proj
	- Arbo générée :
		/---+---/public/ : page de l'index
			|
			+---/src/ : sources de notre appli Vue
			|	|
			|	+---/components/ : là où on place nos composants Vue.
			|	|	|
			|	|	+---/Foo.vue : composant Vue "Foo"
			|	|
			|	+---/App.vue : composant racine
			|	|
			|	+---/main.js : ficier JS "main" de l'application
			|
			+---/package[-lock].json : Stuff de NPM
			|
			+---/babel.config.js : stuff de Babel ?
			[
			+---/jsconfig.json : stuff de Babel ?
	- Vaut mieux créer avec la CLI à la main, puis import project from folder.
	- PWA :
		- Webpack va faire ses petites affaires pour compiler le site.
	- index.html va taper /src/main.js, qui chargera le composant racine "App" (/src/App.vue)
	
Le composant Vue :
	- Fichier.vue
	- Plusieurs parties, "entre balises HTML" :
		- template : le template HTML
		- style : le style (S)CSS du composant
			- Attribut HTML "scoped" pour le limiter au composant.
				- La classe sera préfixée avec un ID unique généré à la compilation.
		- script : instanciation du composant

En pratique WAM-Zenon :
	- Le style sera très lié à Vuetify et ses classes prémâchées. Sera fait rarement.

Le template HTML :
	- On y met les props entre double accolades.
		- Syntaxe moustache, sécurisée.
		- Exemple :
			<p>Je m'appelle {{ nom }}</p>
	- Binder une valeur avec une prop :
		- Directive v-bind
		- Syntaxe : <div v-bind:foo="bar"><!-- ... --></div>
			- Dès que Vue rencontrera "bar" dans le texte enfant, il le remplacera par la valeur de foo.
	- Affichage conditionnel :
		- Directive v-if :
			- Syntaxe : <div v-if="condition"><!-- ... --></div>
			- condition est une condition de boucle if.
			- La div s'affiche ssi la condition vaut true.
			- Pas besoin de tout mettre dans une valeur.
			- Exemple :
				<!-- Dans un composant avec 2 props booléennes "toto" et "titi" -->
				<p v-if="toto">Toto !</p>
				<p v-if="titi">Titi !</p>
				<p v-if="titi && toto">Titi &amp; toto !</p>
		- v-else : la balise suivant celle du v-if peut avoir un v-else.
			- Cette balise s'affichera si la valeur de la condition du v-if est fausse.
		- v-else-if : balises intermédiaires entre un v-if et un v-else pour une boucle if complète.
			- Syntaxe similaire à celle de v-if.
	- Affichage itératif :
		- Itération sur une collection.
		- Directive v-for :
			- Syntaxe :
				 <ul>
					<li> v-for="elt in coll">{{ elt }}</li>
				 </ul>
			- La balise contenant le v-for se répétera pour chaque elt de la liste.
	- Événements :
		- Directive v-on :
			- Syntaxe :
				<button v-on:click="foo">Clique !</button>
			- Nom des événements : mots séparés par des tirets => kebab-case
			- La méthode "foo" sera appelée lorsqu'on cliquera sur le boution.
			- NE PAS OUBLIER les ":" !
			- Le callback appelé doit être situé dans un champ "methods" du composant :
				{
					name: 'Foo',
					props: {
						msg: String,
						toto: Boolean,
						titi: Boolean
					},
					methods: {
						foo: function() {
							alert('Foo foghters !')
						}
					}
				}
	- 2-ways binding :
		- Dès qu'on change un état, c'est automatiquement màj.
			- Exemple : input dans un formulaire.
		- Directive v-model :
			- Syntaxe :
				<foo v-model="bar"><!-- ... --></foo>	
		- Dès que la valeur de la balise change (typiquement dans un onChange pour un <input />), l'étrat est màj.
			- Exemple :
				<!-- Quant l'input fini changera de valeur, l'état fufu le fera aussi -->
				<form>
					<input type="text" name="fifi" v-model="fufu" />
				</form>
		- La variable en question est un attribut, dans les méthode du composant on peut l'utiliser avec "this.mavar".
		- On peut mettre un @ à la place.
		- C'est un v-bind évoué.
	- Binding de variables :
		- Quand on veut binder une prop d'un composant/un attribut de balise HTML avec une variable.
		- Directive v-bind :
			- Syntaxe :
				<!-- Dans le template d'un composant Foo ayant des frops fooid et link -->
				<div v-bind:id="fooid"> <!-- foo#<valeur de fooid> -->
					<a v-bind:href="link">{{ link }}</a>
				</div>
				
La partie script :

// Dans le <script> du fichier <nom du composant>.vue
export default {
	name: '<nom du composant>',
	props: {	// Pour chaque prop
		nomprop: typeprop,
	},
	methods: {	// Cf. binding des événements
		foo: function() { /* ... */ }
	}
}


 Champs d'un composant :
	- Champ computed : variables calculées
	- Champ data : variables locales au composant
	- Objets JSON avec les noms de variables en champ.
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
