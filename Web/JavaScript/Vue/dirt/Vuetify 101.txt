Vuetify 101 :

Généralités :
	- Bibliothèques de composants graphiques pouvant être utilisés avec Vue.
	- Un peu le Bootstrap de Vue.

Ajouter Vuetify à un projet Vue :
	- Via "vue add" de Vue CLI : vue add vuetify
		- Va mettre un template de projet Vuetify. Le faire rapidement après le vue create.
	- Marche aussi via NPM : npm install vuetify
	
Utiliser Vuetify :
	- import Vuefify from "vuetify";
	
Paramétrer Vuetify :
	// Fichier avec les options pour Vuetify

	import Vue from 'vue'
	import Vuetify from 'vuetify'
	import 'vuetify/dist/vuetify.min.css'

	Vue.use(Vuetify)

	const opts = {}

	export default new Vuetify(opts)

Vuetify & SASS :
	- Prérequis : inclure SASS dans le projet Vue. Cf. Vue 101.
	- Exemple de code :
		<style lang="scss">
		// On met l'import des styles de Vuetify dans un namespace afin d'éviter les conflits avec son propre code.
		@use '~vuetify/src/styles/styles.sass' as vuetify;

		.cliplist-form {
		  display: grid;
		  grid-template:
			  "tag-label       tag-input"
			  "family-label    family-input"
			  "label-label     label-input"
			  "create-label    create-input"
			  "broadcast-label broadcast-input"
			  "submit-btn      submit-btn" /
			  1fr               1fr;
		  place-items: center;
		  // Utilisation d'une variable SASS de Vuetify
		  gap: vuetify.$form-grid-gutter;

		  @each $label in ['tag', 'family', 'label', 'create', 'broadcast'] {
			[data-gridname='#{$label}-label'] {
			  grid-area: #{$label}-label;
			}

			[data-gridname='#{$label}-input'] {
			  grid-area: #{$label}-input;
			}
		  }

		  [data-gridname='submit-btn'] {
			grid-area: submit-btn;
		  }
		}
		</style>
	- Doc des variables de Vuetify : https://vuetifyjs.com/en/api/vuetify/#sass
	
Paramétrer Vuetify :
	- Surcharger des valeur par défaut :
	- Dans le "fichier de plugin" :
		// /src/plugins/vuetify.js
		import Vue from 'vue';
		import Vuetify from 'vuetify/lib/framework';

		Vue.use(Vuetify);

		export default new Vuetify({
			theme: {
				themes: {
					// Modifs du thème "light"
					light: {
						primary: "#0f527d"
					}
					// Possibilités avec le thème "dark"
				}
			}
		});
	- Dans le main de l'appli Vue :
		// /src/main.js
		import Vue from 'vue'
		import App from './App.vue'
		import router from './router'
		import store from './store'
		import vuetify from './plugins/vuetify'
		import i18n from './i18n'

		Vue.config.productionTip = false

		new Vue({
		  router,
		  store,
		  vuetify,
		  i18n,
		  render: h => h(App)
		}).$mount('#app')
	- Pourra aussi se faire en écrasant des valeurs de this.$vuetify.

Des composants :
	- v-card : composant fourre-tout, comme une <div> HTML.
	- v-container : Équivalent Vuetify de l'immonde grille à 12 colonnes de Bootstrap.
		- Avec des v-row, qui contiennent des v-col.
		- Tout le responsive se fait dans les v-col.
		- C'est mobile-first, donc quand tu précise un changement c'est pour une taille et au dessus.
		- Mêmes possibilités que pour sa cousine à chier de chez BS.
	- Layout de base d'une application :
		- v-app : composant racine de l'application.
		- Composant v-app-bar (menu de l'application)
		- Vient accompagnée d'un v-navigation-drawer (menu hamburger).
			- mini-variant pour savir si on l'affiche ou pas.
		- Le gros de l'application est mis dans un v-main.
		- On a aussi un v-footer pour le footer de l'application.
	- v-date-picker :
		- Comme son nom l'indique.
		- Manipule les dates sous forme de strings et non pas d'objet JS Date.
	- v-select :
		- On charge les options possibles via sa prop "items".
		- Dans l'idéal, liste d'objets JSON suivants :
			[
				{ text, value, disabled, divider, header }
			]
			- text : le texte à afficher.
			- value : la valeur à retourner.
			- disabled : pour que la valeur ne puisse être sélectionnée, modifiée.
			- divider : mettre un divider dans le select ?
		- multiple : pour rendre le select multiple.
		- readonly : pour que sa valeur ne puisse être changée.
		- chips : afficher les valeurs sélectionnées dans des puces.
	- v-list :
		- Hiérarchie d'inclusion :
			- v-list
			- v-list-group
			- v-list-item-group
			- v-list-item
			- v-list-item-*
	- Les icônes :
		- Reprennent la nomenclature de "Material Design Icons"
			- Site : https://materialdesignicons.com/
		- Nom des icônes dans Vuetify : mdi-<nom chez MDI>
	- v-flex : composant Flexbox
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
