Drupal :

CMS libre et open-source, sous licence GPL.

Site : http://drupal.org

Version actuelle : Drupal 7 (7.28 début juillet 2014).
	- Drupal 8 est en développement et se basera sur Symphony 2.
	
Acquia est l'entreprise derrière Drupal.

Sur le code de Drupal :
	- Il sépare le code correctement : logique, visuel et contenus.
	- Drupal 7 est très procédural. Par contre Drupal 8 devrait plus embrasser l'objet.

Notes sur l'arborescence des fichiers :
	- Le Virtual Host se place à la racine du dossier contenant Drupal.
	- Tous les dossiers et fichiers sont pour le core de Drupal, à l'exception du dossier /sites/
	- Dossier /sites/ :
		- L'utilisateur d'Apache ("daemon" pour XAMPP), doit y avoir les droits en écriture.
		- Il comporte plusieurs dossiers, un par site hébergé sur cette installation de Drupal.
			- S'il n'y a qu'un site, il est en général dans /sites/default/
			- Chaque dossier de site contiendra les fichiers uploadés dans un sous-dossier files/, comme par exemple les images.
				Ce sont là certains contenus, les autres étant en BDD.
			- Ces dossiers ont aussi un fichier settings.php pour la configuration du site.
		- Il y a aussi un dossier all/, commun à tous les sites hébergés sur ce Drupal
			- C'est lui qui contient la configuration Drupal, les thèmes et les modules.

Installation de Drupal :
	- Sur le site de Drupal (ou ailleurs), récupérer Drupal dans son archive (ZIP ou tarball)
	- On décompresse l'archive fraîchement téléchargée et on a un dossier "drupal-7.28" (ici pour Drupal 7.28).
	- On place le dossier à la racine web, dans le www / htdocs (ça dépend où on est).
	- On peut faire un VHost pointant à la racine de Drupal.
	- On va à la racine du navigateur avec Drupal : http://localhost/drupal/ (ou l'URL du VHost).
	- On suit alors les étapes pour l'installation, vers laquelle on a été redirigé.
		- Dupliquer le fichier /sites/default/default.settings.php en /sites/default/settings.php
		- Donner les droits d'écriture sur /sites/ à Apache.
		- Créer une BDD pour Drupal, et aussi un utilisateur de la BDD si nécessaire.
		- Penser à noter les identifiants de l'admin ("Site Maintainer").
		- Penser à un "AllowOverride All" pour la config d'Apache dans un .htaccess ou dans le VHost.
	- Installation du français (et plus généralement de n'importe quelle langue) :
		- Tuto (plein de fautes d'orthographes) ici : http://learning-drupal.com/tuts/comment-drupal-7-francais
		- Modules > Core, cocher "Locale" et sauvegarder.
		- Aller dans Configuration > Regional and Language > Languages. Cliquer sur "Add language", choisir "French" et sauvegarder.
		- Activer le français et le mettre par défaut.
		- Sur http://localize.drupal.org, télécharger le français pour la version de Drupal que l'on a (ici 7.28).
		- Aller dans Configuration > Regional and Language > Translate interface > Import sur le Drupal. Sélectionner le
			fichier de traductions télécharger (finissant en .po) puis cliquer sur Import.
		- C'est bon dans l'ensemble.

Distribution Drupal :
	- Drupal est composé d'un core, de modules/thèmes standards et d'autres modules/thèmes.
	- Composée d'un Drupal standard et de composants supplémentaires (configuration, thèmes, modules...) pour être prêt à l'emploi.
	- Un peu comme les distros Linux : le Noyau Linux est le core et des composants viennent se greffer autour pour faire un OS complet.

Drush :
	- Drush signifie "DRUpal SHell".
	- C'est un shell pour Drupal, indépendant de la plateforme.
	
Les sites :
	- TODO
	
Les types de contenus :
	- Menu Structure > Content Types. On a alors la liste des types de contenu existants.
	- Ajout d'un type :
		- Cliquer sur "Add content type".
		- On renseigne alors les informations suivantes :
			- Nom du type. Drupal créera à partir de ça un "Machine name" pour lui, que l'on peut éditer.
			- Description (qui sera en tooltip).
			- Des options par défaut pour la gestion de contenus + des options pour la création de contenu.
	- Édition d'un type de contenu :
		- Aller dans la liste des types de contenus.
		- Plusieurs onglets :
			- "edit" pour l'edit des infos entrées lors de l'ajout.
			- Gestion des champs. Ce sont des parties de l'article affichées dans des widgets.
			- Affichage des champs.
				- Gère l'affichage dans des entités appelées Format.
				- On peut définir des affichages pour différentes parties (l'article, son accroche...).
				- Affichage des images :
					- Avec les styles d'images
					- Un style d'image = une taille d'image + une opération pour la mettre à l'échelle.
					- Ils se définissent dans Configuration > Media > Style d'images.
			- Idem avec les commentaires (2 onglets différents là aussi).
	- Suppression d'un type de contenu :
		- Structure > Content Types puis "delete" dans les opérations du type de contenu
		- Bouton "Delete Content Type" dans l'opération "edit".
	- Deux types de contenus out of the box :
		- Articles :
			- Il s'agit d'articles comme des articles de blogs.
			- On leur donne un titre (obligatoire), des tags (pour la taxonomie).
			- En dessous, on écrit le contenu.
			- En dessous, on a de quoi uploader des images nécessaires pour l'article.
			- Après on a les options (cf. plus loin).
		- Pages basiques :
			- Même chose que les articles, sans les tags.
			- On ne peut uploader d'images.

Gestion des contenus :
	- Drupal est centré autour des contenus. Ils prennent le pas sur le reste.
	- Créer un contenu :
		- Dans l'admin de Drupal ("barre en haut") :
			- Content > Add Content. Déjà affiché si on est sur la home.
			- On peut créer des pages à partir des types de contenu disponibles.
			- Ces contenus là sont des noeuds, stockés dans la table node.
			- On remplit les différents champs du type et les options de fin de page avant d'enregistrer.
			- Options à la fin des pages (articles et pages de base) :
				- Menu settings : pour mettre le contenu dans un menu
					- On peut spécifier un titre et une descriprion.
					- On choisit un item parent :
						- Soit <Main_Menu> (barre d'onglets en haut).
						- Soit un autre Item qui sera son parent.
				- Revision Information : pour créer une révision de l'article (un peut comme dans un CVS).
				- URL path settings : spécification d'une URI (alias) pour la page.
				- Comment settings : Ouvrir / fermer les commentaires.
				- Authoring information : auteur et date de l'article (+/- les dates UTC)
				- Publishing options : options de publication de la page.
	- Trouver un contenu :
		- Content > Find Content (à côté de Add Content).
		- Cliquer sur le contenu pour y accéder (colonne TITLE).
	- Éditer un contenu :
		- Quand on est sur la page d'un contenu, il y a un onglet "Edit" pour ça.
		- Sinon, aller dans Content > Find Content, rechercher le contenu et cliquer sur "edit" dans la dernière colonne ("OPERATIONS").
	- Supprimer un contenu :
		- Quand on est sur la page d'un contenu, aller dans son Edit puis cliquer sur "Delete" en bas.
		- Sinon, aller dans Content > Find Content, rechercher le contenu et cliquer sur "delete" dans la dernière colonne ("OPERATIONS").
		
Les blocs :
	- Un gabarit de page (template) consiste en l'agencement de blocs.
	- Tout se passe dans Structure > Blocs.
	- Un onglet par thème Drupal.
	- On édite le gabarit en faisant du drag and drop entre les blocs.
	- Ajout de blocs via le bouton qui va bien, celui avec un '+' au dessus du tableau (comme toujours).
		- Besoin d'une description (obligatoire), du corps de bloc (obligatoire aussi).
		- Titre facultatif.
		- Paramètres de région : où afficher un bloc par défaut pour un thème donné.
		- En dessous, on règles des options si on veut restreindre son affichage (pages, types de contenus, utilisateurs).
	- Possibilité d'édition classiques, via des action "edit" ou un truc de conf dans le coin si connecté en admin...
	
Taxonomie :
	- Indexation des contenus dans Drupal.
	- Fait par le module Taxonomy du Core.
	- Deux notions importantes :
		- Terme : mot-clé pour indexer. Si on a du bon multi-langue on peut le traduire dans d'autres langues.
		- Vocabulaire : ensemble de termes.
	- Les termes peuvent être organisés de manière arborescente à l'intérieur d'un vocabulaire.
	- Quand on crée un terme, on précise le terme (normal).
		- Description facultative
		- URL alias facultative aussi
		- On précise la parenté dans Relations.
	- Gestion des vocabulaires :
		- À l'ajout, nom et description seulement.
		- À l'édition, on peut aussi modifier les champs d'affichage qui présentent la page du vocabulaire.
			Cf. les types de contenu.

Les menus :
	- Arborescence de liens formant un menu.
		- Un lien contient a minima un titre et une URL (absolue ou bien une URI sur le site Drupal).
		- On peut également l'activer / le désactiver et lui choisir un parent.
	- Pour l'affichage, un menu possède un bloc équivalent.

Gestion des utilisateurs :
	- Dans le menu Drupal du même nom ("Personnes" en français)
	- Dans ce menu, les utilisateurs se gèrent à partir de l'onglet "Lister" et les droits/permissions à partir de l'onglet "Droits".
	- Les utilisateurs ont des rôles et des permissions :
		- Rôle : groupe d'utilisateurs avec certains droits
			- Modifier les rôles : Personnes > Droits (onglet) > Rôles.
			- Trois rôles out of the box : utilisateur anonyme, utilisateur authentifié et administrateur.
		- Permission :
			- Modifier les permissions : Personnes > Droits (onglet) > Droits
		- Dans le tableau d'edit des droits, on peut modifier les droits selon les rôles.
	- Comme pour Piwik, l'e-mail est unique pour chaque utilisateur.
	- À l'ajout/edit, on peut paramétrer :
		- Le login (obligatoire)
		- L'e-mail (obligatoire)
		- Le statut (utilisateur bloqué ou bien actif)
		- Les rôles de l'utilisateur.
		- La langue
		- L'avatar
		- Les paramètres régionaux genre fuseaux horaires.
	- Supprimer un compte, dans l'edit, aller dans "Annuler le compte".
		- On peut, au choix, simplement désactiver le compte ou bien le supprimer totalement.
		- Pour le contenu, plusieurs possibilités :
			- En cas de désactivation du compte, agir sur la publication du contenu en le dépubliant ou pas.
			- En cas de suppression du compte, attribuer le contenu à "Anonyme" ou bien le supprimer avec son utilisateur.

La BDD :
	- Beaucoup de tables, au moins 60.
	- Ne différencie pas les différents environnements (devs, recette, prod, différences entre devs).
		- Drupal 8 arrêtera le massacre.
	- BDD compatibles : MySQL, MariaDB, PostgreSQL, SQLite.
		- MySQL recommandé.
		- Oracle et Microsoft SQL Server possibles via des modules.
	- Une BDD par site hébérgé sur le Drupal.

La configuration :
	- Tout est en BDD. Il faut faire attention !
	- Si problèmes, utiliser phpMyAdmin ou la CLI de la BDD.
	- Dans Drupal 8, la configuration ira dans des fichiers.
	- Dans le code, il existe les variables persistantes, des points de configuration stockés dans une table. Trois fonctions :
		- <?php variable_get($name, $default = NULL); ?> : Getter sur le point de configuration <?php $name ?>.
			Il y a une valeur par défaut (<?php $default ?>) si le point de conf n'existe pas.
		- <?php variable_set($name, $value); ?> : Donner au point de conf <?php $name ?> la valeur <?php $value ?>.
		- <?php variable_del($name); ?> : Effacer le point de conf <?php $name ?>.

Les modules :
	- Les modules du Core de Drupal sont dans /modules
	- Les modules hors Core de Drupal sont dans /sites/all/modules/
	- Les modules sont tous à la base de /sites/all/modules/, à l'exception des modules maison dans un sous-dossier custom/
	- Un module à cette arborescence minimale :
		- /sites/all/modules/[custom/]mymodule/ : dossier de base
			- mymodule.module : fichier PHP de base du module.
			- mymodule.info : fichier d'informations sur le module.
			- mymodule.install : pour installer le module Drupal.
	- Installer un module :
		- En mode graphique :
			- Méthode mains dans le cambouis :
				- Aller sur la page Drupal du module. Elle commence par http://drupal.org/project/<nom du module>
				- En bas, il y a la section des téléchargements.
				- Le module est récupéré dans une tarball, on le décompresse.
				- Aller dans le menu Modules pour l'activer et enregistrer cela.
			- Méthode graphique :
				- Dans la page des modules, cliquer sur "Installer un nouveau module"
				- Pour installer, plusieurs options :
					- Donner l'URL de la tarball
					- Passer la tarball du module fraîchement uploadée.
	- Créer un module Drupal :
		- Faire le dossier avec le nom "système" du module.
			- Attention ! Pas de tirets '-'. Leur préférer des underscores '_'.
		- Faire les fichiers minima (.module, .info)
		- Dans le .info, définir les champs suivants :
			- name : nom du module qui sera affiché
			- description : une courte description qui sera affichée
			- core : version du core de Drupal. Mettre "core = 7.x" pour Drupal 7.
		- Les commentaires dans le .info sont sur une ligne après un point-virgule : ; Mon commentaire
		- Dans le .module, on met les fonctions qui seront appelées quand Drupal déclenchera les siens.
			- La fonction monmodule_XXX() est appelée quand Drupal déclenche son hook "hook_XXX" :
				<?php
					// Dans le fichier mymod.info d'un module Drupal nommé "mymod"
					
					function mymod_XXX() {
						echo 'Drupal vient de déclencher son hook nommé hook_XXX.');
					}
				?>
				- La liste des hooks de Drupal 7 est disponible ici : https://api.drupal.org/api/drupal/includes!module.inc/group/hooks/7
		- On peut mettre un fichier .install qui gèrera l'install/uninstall du module :
			<?php
				// mymod.install
				
				function mymod_install() {
					// Installation du module.
					// Appelé à la première activation
				}
				
				function mymod_enable() {
					// Activation du module
				}
				
				function mymod_disable() {
					// Désactivation du module
				}
				
				function mymod_uninstall() {
					// Désinstallation du module
				}
			?>
			- Il implémente des hooks comme le .module.
			- Cas de gestion de BDD :
				- Création / suppression de tables se fait grâce à la Schema API (voir plus loin).
		- Si on veut faire un menu, il faut implémenter le hook_menus :
			<?php
				function mymod_menus() {
					$items['admin/config/mymod'] = array(
						'title' => 'Titre du menu de configuration',
						'description' => 'Décrire le menu de configuration',
						'page callback' => "nom de la fonction appelée par l'endpoint <url Drupal><index de l'item>",
						'page arguments' => array('arguments passés'),
						'access arguments' => array('arguments passés à la fonction Drupal user_access();'),
						'file' => 'piwik_autologin.admin.inc',
						'type' => flags_sur_les_types_de_menus_combinales_avec_des | entre_eux
					);
					
					return $items;
				}
			?>
			- URL du hook : https://api.drupal.org/api/drupal/modules!system!system.api.php/function/hook_menu/7
			- La convention veut que la page de configuration d'un module soit à admin/config/<nom du module>
			- Pour vérifier qu'il s'agit d'un admin, mettre <?php 'access arguments' => array('administer site configuration'), ?> 
			- Cas de <?php 'page callback' => 'drupal_get_form' ?> :
				- Si le premier argument est une fonction, alors elle servira de callback pour créer un formulaire
				- La fonction en question est dans le script PHP à <?php $items['admin/config/mymod']['file'] ?>
				- Cas d'un formulaire.
					- La fonction suivante, passée en callback, a cette forme :
					<?php
						/* Dans mymod_menus */
						'page callback' => 'drupal_get_form',
						'page arguments' => array('func_for_form'),
						'file' => 'mymod.admin.inc',
						
						/* Dans mymod.admin.inc */
						function mymod_func_for_form($form, &$form_state) {
							// Actions sur le $form
							$form['nom_du_champ_dans_le_form'] = array(
								'#type' => 'textfield',		// Type de champ
								'#title' => 'contenu du label, entre les balises <label></label>',	
								'#default_value' => Valeur par défaut,
								'#size' => largeur du champ,
								'#required' => est-ce obligatoire ?,
							);

							// Cas d'un bouton submit
							$form['submit'] = array(
								'#type' => 'submit',
								'#value' => 'Mettre à jour',
								'#submit' => array('mymod_submit'),
							);
							
							return $form;
						}
						
						function mymod_submit($form, &$form_state) {
							// TODO
						}
					?>
					- Les champs des formulaires sont définis par la FAPI (Forms API). Cf. plus loin dans le code.
					- Callback du formulaire :
						- 2 arguments :
							- <?php $form ?> : le formulaire
							- <?php &$form_state ?> : une variable sur le formulaire.
						- Le champ <?php $form_state['values'] ?> contient toutes les valeurs du submit.
			- Dans le tableau des 'page arguments', mettre un entier <?php $n ?> revient à mettre l'argument <?php func_get_arg($n) ?>
				pour la fonction mise dans 'page callback'.
	- Publier un module Drupal :
		- Le module doit avant tout respecter les conventions de codage de Drupal : https://www.drupal.org/node/1354

Les thèmes :
	- Les modules du Core de Drupal sont dans /themes
	- Les modules hors Core de Drupal sont dans /sites/all/themes
	- Chaque thème a cette arborescence :
		-/sites/all/themes/montheme/ : dossier de base du thème
			- template.php : recensement des hooks visuels du thème
			- templates/ : dossier avec les fichiers PHP représentant les structures d'une page.
				- Plein de fichiers *.tpl.php

Drupal et la BDD :
	- Schema API :
		- API pour manipuler les schémas de BDD.
		- URL : https://www.drupal.org/node/146843
		- Création du schéma :
			<?php
				// Typiquement dans un mymod.install
				
				function mymod_schema() {
					$schema['table1'] = array(
						'description' => 'La description de la table {table1}',
						'fields' => array(
							// Clé primaire
							'id' => array(
								'description' => 'La clé primaire',
								'type' => 'serial',
								'not null' => true,
							),
							
							'champ' => array(
								'description' => 'Description du champ',
								'type' => 'type du champ en BDD. Les noms sont ceux de Drupal, indépendant du SGBD',
								'size' => 'tiny, big, small... pour plus de précisions sur le type comme en MySQL',
								'length' => 50, // Longueur du champ. C'est l'entier qui est entre parenthèses dans le type
								'not null' => true si non nul false sinon,
								'unsigned' => true si non signé false sinon,
								'default' => 'Valeur par défaut',
							)
						),
						'primary key' => array('id'),
					);
					
					$schema['table2'] = array(
						'description' => 'La description de la table {table2}',
						'fields' => array(
							// Clé primaire
							'id' => array(
								'description' => 'La clé primaire',
								'type' => 'serial',
								'not null' => true,
							),
							
							'champ1' => array(
								'description' => 'Description du champ',
								'type' => 'type du champ en BDD. Les noms sont ceux de Drupal, indépendant du SGBD',
								'size' => 'tiny, big, small... pour plus de précisions sur le type comme en MySQL',
								'length' => 50, // Longueur du champ. C'est l'entier qui est entre parenthèses dans le type
								'not null' => true si non nul false sinon,
								'unsigned' => true si non signé false sinon,
								'default' => 'Valeur par défaut',
							),
							
							'champ2' => array(
								'description' => 'Description du champ',
								'type' => 'type du champ en BDD. Les noms sont ceux de Drupal, indépendant du SGBD',
								'size' => 'tiny, big, small... pour plus de précisions sur le type comme en MySQL',
								'length' => 50, // Longueur du champ. C'est l'entier qui est entre parenthèses dans le type
								'not null' => true si non nul false sinon,
								'unsigned' => true si non signé false sinon,
								'default' => 'Valeur par défaut',
							),
							
							'table1_id' => array(
								'description' => 'La clé primaire de {table 1} en clé étrangère.',
								'type' => 'serial',
								'not null' => true,
							),
						),
						'primary key' => array('id'),
						'unique key' => array('id', 'champ2'),
						'foreign key' => array(
							'table' => 'table1',
							'columns' => array('table1_id' => 'id'),
					);
					
					return $schema;
				}
			?>
			- Implémenter hook_schema (typiquement dans un module).
			- On retourne une variable tableau
				- Les indices sont le nom des tables.
				- Les valeurs sont des tableaux avec les données pour chaque table
					- 'description' : description de la table.
					- 'fields' : champs de la table. C'est un tableau avec plein de sous-champs :
					- 'primary key' : liste de colonnes formant la clé unique de cette table.
					- 'foreign key' : tableau des clés étrangères.
					- 'unique key' : tableau pour la clé unique.
					- 'indexes' : les indexes, là aussi en tableau.
		- Le hook_schema est appelé
			- À la première activation, juste avant le hook_install.
			- Dans <?php drupal_install_schema('mymod'); ?>
		- On peut aller plus loin avec le module Schema : https://www.drupal.org/project/schema
	- Database API :
		- Configuration : <?php $databases ?> dans le settings.php du site.
		- Création de la requête :
			- <?php db_query($sql, $args); ?> : Exécution de la requête SQL <?php $sql ?>.
				- Dans la requête SQL, on peut mettre des placeholders du style <?php ':qqch' ?>.
					Ils seront remplacés par <?php $args[':qqch'] ?> dans la requête finale
				- Noms des tables entre accolades.
				- Cette fonction construit et exécute aussi la requête.
			- <?php db_select($table, $alias = NULL); ?> : annonce que la requête sera un SELECT sur la table <?php $table ?>.
				- On peut donner un alias à la table avec <?php $alias ?>.
			- <?php db_insert($table, $alias = NULL); ?> : annonce que la requête sera un INSERT sur la table <?php $table ?>.
				- On peut donner un alias à la table avec <?php $alias ?>.
				- Les valeurs à insérer sont ajoutées avec la méthode fields();
				- Pour faire plusieurs insertions, l'argument passé à fields(); sera un tableau de tableaux de valeurs à insérer.
			- <?php db_update($table, $alias = NULL); ?> : annonce que la requête sera un UPDATE sur la table <?php $table ?>.
				- On peut donner un alias à la table avec <?php $alias ?>.
				- Les valeurs à màj sont ajoutées avec la méthode fields();
			- <?php db_delete($table, $alias = NULL); ?> : annonce que la requête sera un DELETE sur la table <?php $table ?>.
				- On peut donner un alias à la table avec <?php $alias ?>.
		- Création de conditions :
			- <?php db_and(); ?> : crée une condition AND.
			- <?php db_or(); ?> : crée une condition OR.
			- <?php db_xor(); ?> : crée une condition XOR.
		- Construction de conditions :
			- <?php condition($condition); ?> : Rajoute la condition SQL <?php $condition ?>.
				- Idéal pour parenthéser des conditions (xxx_where_open|close() en FuelPHP).
			- <?php condition($field, $value, $operator); ?> : Rajoute une condition SQL "<?php $field ?> <?php $operator ?> <?php $value ?>".
				- Opérateur '=' par défaut.
				- Permet de préciser des valeurs pour <?php db_and(); ?>, <?php db_or(); ?> et <?php db_xor(); ?>.
				- <?php $value ?> peut aussi être un db_select() dans le cas d'un SELECT imbriqué.
			- <?php isNull($field); ?> : pour la nullité du champ <?php $field ?>.
			- <?php isNotNull($field); ?> : pour la nullité du champ <?php $field ?>.
		- Construction de la requête : ici les méthodes s'appliquent sur un objet <?php $query ?> 
			- <?php addField($table, $field, $alias); ?> : ajouter le champ <?php "$table.$field" ?> dans les champs d'un SELECT.
				- On peut utiliser l'alias de la table au lieu du nom de table.
				- On peut donner un alias au champ avec <?php $alias ?>.
			- <?php fields($table, array $fields); ?> : ajouter les champs de <?php $table ?> dont le nom est dans la liste <?php $fields ?>.
				- On peut utiliser l'alias de la table au lieu du nom de table.
				- Équivaut à : <?php foreach($fields as $field) { $query->addField($table, $field); } ?>
			- <?php useDefaults(array $fields); ?> : utiliser les valeurs par défaut de la BDD pour les champs de la liste <?php $fields ?>.
				- Surtout utile pour les INSERT.
			- <?php condition(...); ?> : ajouter des conditions à la requête.
				- Dans le cadre de <?php $query->condition($field, $value, $operator); ?>, équivaut à :
					<?php $query->condition(db_and()->condition($field, $value, $operator)); ?>
			- <?php where($sql, $args); ?> : pour rajouter le bout de code SQL <?php $sql ?> dans la clause WHERE.
				- <?php $args ?> fonctionne comme dans <?php db_query(); ?>
			- <?php having($sql, $args); ?> : pour rajouter le bout de code SQL <?php $sql ?> dans la clause HAVING.
				- <?php $args ?> fonctionne comme dans <?php db_query(); ?>
			- <?php havingCondition($field, $value, $operator); ?> : comme <?php condition($field, $value, $operator); ?>,
				mais pour les clauses HAVING.
			- <?php join($table, $alias, $condition_sql, $args); ?> : jointure de la table <?php $table ?> d'alias <?php $alias ?>
				selon les conditions SQL de <?php $condition_sql ?>.
				- <?php $args ?> fonctionne comme dans <?php db_query(); ?>
				- On peut mettre un db_select à la place de <?php $table ?>.
				- <?php join(); ?> est pour les jointures JOIN. Pour les autres, il existe des méthodes <?php leftJoin(); ?>,
					<?php rightJoin(); ?> et <?php innerJoin(); ?>
				- Ne peut pas être chaînée avec d'autres méthodes de construction de requête.
			- <?php distinct(); ?> : clause DISTINCT.
			- <?php countQuery(); ?> : pour compter le nombre de lignes résultat.
			- <?php addExpression($sql, $alias, $args); ?> : Permet de rajouter des expressions style "CONCAT(a, b) AS truc" dans un SELECT.
				- <?php $sql ?> est l'expression et <?php $alias ?> est l'alias du AS.
				- <?php $args ?> fonctionne comme dans <?php db_query(); ?>
			- <?php groupBy($field); ?> : GROUP BY sur le champ <?php $field ?>.
				- GROUP BY multiple <=> plusieurs appels.
			- <?php orderBy($field, $order); ?> : ORDER BY sur le champ <?php $field ?> trié selon <?php $order ?>.
				- ORDER BY multiple <=> plusieurs appels.
			- <?php orderRandom(); ?> : ordre aléatoire. Peutêtre combiné à des <?php orderBy(); ?>.
			- <?php range($offset, $limit); ?> : clauses OFFSET et LIMIT de la requête SQL.
		- <?php $query->execute(); ?> exécution de la requête.
			- Dans le cadre d'un INSERT, renvoie l'ID que l'objet inséré a en BDD.
			- Dans le cadre d'un UPDATE / DELETE, renvoie le nombre de lignes affectées.
			- Pas besoin de cette méthode dans le cadre d'un <?php db_query(); ?>.
		- Fetch du résultat : <?php $query->execute(); ?> a créé un objet résultat <?php $result ?> sur lequel s'applique les méthodes suivantes :
			- Méthode classique de parcours : le foreach :
				<?php
					$result = $query->execute();
					foreach ($result AS $ligne) {
						f($ligne);
					}
				?>
			- Fetcher ligne par ligne :
				- <?php fetch(); ?> : on récupère la ligne avec le mode de fetch par défaut.
				- <?php fetchObject(); ?> : on récupère la ligne en tant qu'array.
				- <?php fetchAssoc(); ?> : on récupère la ligne en tant qu'objet.
				- <?php fetchField($field); ?> : on ne récupère que le champ <?php $field ?> dans la liste.
			- Tout récupérer d'un coup :
				- <?php fetchAll(); ?> : un tableau d'objets :
					<?php
						$fetchAll = array();
						
						while ($obj = $result->fetchObject()) {
							$fetchAll[] = $obj;
						}
						
						return $fetchAll;
					?>
				- <?php fetchAllAssoc($field); ?> : on récupère une liste des champs <?php $field ?>. Une vraie colonne :
					<?php
						$fetchAllAssoc = array();
						
						// Jusqu'à la fin de la table, même les NULL
						while ($val = $result->fetchField($field)) {
							$fetchAllAssoc[] = $val;
						}
						
						return $fetchAllAssoc;
					?>
				- <?php fetchAllKeyed(); ?> : avoir une table avec une colonne en indexes et une autre en valeurs.
					- On peut préciser les colonnes en rajoutant leurs indexes en paramètre. D'abord celle des indexes puis celle des valeurs.
				- <?php fetchCol(); ?> : +/- comme <?php fetchAllAssoc(); ?> ?
				- <?php rowCount(); ?> : nombre de lignes affectées. Utilisée pour les INSERT, UPDATE et DELETE.
					- Pour les SELECT :
						<?php
							$query = db_select(/* ... */);
							
							// Remplissage de la requête...
							
							$query->countQuery();
							return $query->execute()->fetchField();
						?>
		- Plein d'utilitaires sympa ici : https://api.drupal.org/api/drupal/includes!database!database.inc/7

Drupal et les formulaires :
	- Les champs des formulaires sont définis par la FAPI (Forms API)
		- URL de la FAPI : https://api.drupal.org/api/drupal/developer!topics!forms_api_reference.html
		- Des valeurs pour #type :
			- 'textfield' : champ texte <input type="text"/>.
			- 'submit' : bouton de submit <input type="submit"/>.
		- #default_value : Valeur par défaut du champ.
		- #size : largeur du champ.
		- #required : true si champ obligatoire, false sinon.
		- #value : texte du bouton submit
		- #submit : fonction de callback appelée lors du submit
	- Modification de formulaire :
		- Implémenter le hook hook_form_alter() pour tous les formulaires.
		- Pour viser un form en particulier, on prend hook_form_FORM_ID_alter().
			- Généralement FORM_ID est l'ID du form dans le HTML en changant les tirets '-' par des underscores '_'.
		- Le premier argument du hook est le formulaire <?php $form ?>. Pour ajouter les champs, on le modifie mais on ne le retourne pas.

Traduction (de modules Drupal) :
	- La traduction est basée sur gettext.
	- Drupal ne lit que les fichiers.po, pas les .mo contrairement à WordPress.
	- Process :
		- Installer le module POTX.
		- Dans l'onglet de traduction de Drupal (Configuration > Régionalisation et langue > Traduire l'interface), aller dans l'onglet "Extraire".
		- Sélectionner son module et extraire. POTX peut générer soit le .pot, soit le .po pour la langue désirée.
		- Finir le travail de traduction avec poedit ()ou tout autre éditeur de traductions gettext).
		- On place le .po (et le .pot) dans un dossier translations/ du module.
	- Il faut placer les fichiers dans un sous dossier translations/ du module.
	- Pour traduire, on peut utiliser le logiciel Poedit ( http://www.poedit.net ).
		- On commence par un catalogue mymod.pot qui servira de template aux fichiers .po.
		- On va dans Fichier > Nouveau catalogue depuis un fichier POT...
		- On remplit les champs.
			- Pour la langue, mettre "fr" pour la France.
			- Pour les pluriels, écrire "nplurals=2; plurals=(n>1);".
		- On enregistre le fichier .po sous le nom de <code ISO>.po.
		- On traduit.
	- Bon tuto ici : http://stackoverflow.com/questions/5231496/how-to-generate-translation-file-for-a-custom-drupal-7-module

Diverses notes de code Drupal :
	- <?php drupal_set_message($mgs, $type) ?> : fonction pour afficher un message à l'utilisateur.
		- 3 types, <?php 'status' ?> (information / confirmation), <?php 'warning' ?> (avertissement) et <?php 'error' ?> (erreur).
		- Il existe un troisième paramètre mais OSEF.
	- Rien pour les sessions. Faire comme d'habitude avec la superglobale <?php $_SESSION ?>.
	- Le hook_menu est rarement appelé. Dès que le module est activé, le hook est appelé à cet occasion. Les résultats sont alors cachés en BDD.
		- Pour rafraîchir le menu, rien ne vaut un bon <?php menu_rebuild(); ?>.
