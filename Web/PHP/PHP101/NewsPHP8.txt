Nouveautés de PHP 8 :

PHP 8.0 :
	- Compilation JIT.
	- Microsoft ne s'occupe plus de la version Windows de PHP dès la version 8.
	- Les union types :
		- Comme pour les regroupements d'exceptions de PHP 7.1
		- Cette fois-ci appliqué aux types des attributs de classe.
		- Exemple : <?php
			class UnionLibre {
				protected int|float $toto;

				public function getToto() : int|float {
					return $this->toto;
				}

				public function setToto(int|float $val): void {
					$this->toto = $val;
				}
			}
		?>
	- Les paramètres promus :
		- Sucre syntaxique pour ne pas à avoir à répéter plusieurs fois les attrobuts d'une classe.
		- Syntaxe-exemple : <?php
			// PHP 8
			class Personne8 {
				public function __construct(
					protected string $nom,
					protected string $prenom,
					public int $age
				) {}

				public function identite() : string {
					return "{$this->prenom} {strtoupper($this->nom)}";
				}
			}

			// Équivalent PHP 7
			class Personne7 {
				protected string $nom;
				protected string $prenom;
				protected int $age;


				public function __construct(string $nom, string $prenom, int $age) {
					$this->nom = $nom;
					$this->prenom = $prenom;
					$this-> age= $age;
				}

				public function identite() : string {
					return "{$this->prenom} {strtoupper($this->nom)}";
				}
			}
		?>
	- static comme self dans les résultats de méthode.
		- static est ainsi un peu plus l'équivalent de self pour les parties statiques d'une classe.
		- Exemple : <?php
			class Singleton {
				protected static $INSTANCE = null;

				protected function __construct() { /* ... */ }

				public static function getInstance(): static {
					if (static::INSTANCE == null) {
						static::INSTANCE = new static();
					}
					return static::INSTANCE;
				}
			}
		?>
	- Virgule de fin dans les déclarations de fonction aussi.
		- En plus des tableaux et des appels de fonctions.
	- On peut nommer les arguments dans les appels de fonction : <?php
		function min($a, $b) {
			return $a < $b ? $a : $b;
		}

		// Tout pareil :
		$m = min(10, 4);
		$m = min(a: 10, b: 4);
		$m = min(b: 4, a: 10);
	?>
	- Opérateur nullsafe :
		- Protection anti "null->qqch".
		- 2 cas :
			- Méthode derrière la flèche : la méthode n'est pas appelée.
			- Attribut derrière la flèche : on renvoie null.
		- Syntaxe : <?php
			$mavar = bar();

			// Attributs
			$a = $mavar?->a;
			// Équivalent PHP 7
			$a = $mavar == null ? null : $mavar->a;

			// Méthodes
			$mavar?->foo();
			// Équivalent PHP 7
			if (!is_null($mavar)) {
				$mavar->foo();
			}
		?>
	- Expression match :
		- Un peu comme le match de Rust.
		- Syntaxe-exemple : <?php
			$a = foo();

			// PHP 8
			$b = match ($a) {
				0 => "C'est nul !",
				10 => "Sois positif !",
				true, false => "Ceci est un booléen.",
				default => "Je sais pas..."
			};

			// PHP 7
			switch ($a) {
				case 0:
					$b = "C'est nul !";
					break;

				case 10:
					$b = "Sois positif !";
					break;

				case true:
				case false:
					$b = "Ceci est un booléen.";
					break;

				default:
					$b = "Je sais pas...";
					break;
			}
		?>
		- On peut finir le match sur une virgule. C'est pas grave
		- Si rien ne matche, une erreur de type UnhandledMatchError est déclenchée.
	- Attributes, comme les annotations ailleurs (TS, Java).
	- Type "mixed".
		- Équivalent du "any" de TypeScript.
	- Gestion différente des index négatifs.
		- Quand on remplit un tableau en commençant par un index négatif :
			- PHP 7 : l'index de l'élément suivant est 0.
			- PHP 8 : l'indes de l'élément suivant est l'entier au dessus
	- Fonction str_contrains() :
		- Comme son nom l'indique.
		- Elle cherche une sous-string dans une plus grande.
		- true si ça y est, false sinon.
		- Syntaxe : <?php str_contains(string $haystack, string $needle); ?>
			- $needle : la string à rechercher (l'aiguille dans...)
			- $haystack : la grosse string où l'on cherche (...la meule de foin).
		- Exemples : <?php
			str_contains('Romain', 'main');   	// true
			str_contains('Françoise', 'main');	// false
		?>

PHP 8.1 :
	- Types d'intersection :
		- Comme les types d'unions, mais là c'est de l'intersection ensembliste.
		- Le type du truc doit être de tous les types cités.
		- Idéal si le truc implémente plusieurs interfaces ou traits donc.
		- Syntaxe : <?php Type1&Type2 ?>
			- PHP utilise ici le "&" du "and".
			- Contrepied du "|" des unions signifiant "ou" (un type parmi ceux cités).
	- Les énumérations débarquent (enfin) dans PHP !
		- Déclaration :
			- Mot-clé enum
			- Chaque valeur énumérable est défnie par un case.
			- On les appelle avec l'opérateur de portée.
			- Syntaxe : <?php
				enum Jours {
					case Lundi;
					case Mardi;
					case Mercredi;
					case Jeudi;
					case Vendredi;
					case Samedi;
					case Dimanche;
				}

				$lundi = Jours::Lundi;
			?>
			- Deux types d'enums :
				- Pure enums : comme vue ci-dessus.
				- Backed enums : avec une valeur associée.
					- int ou string, à préciser.
					- Exemple : <?php
						enum HauteurInt: int {
							case As = 1;
							case Deux = 2;
							case Trois = 3;
							case Quatre = 4;
							case Cinq = 5;
							case Six = 6;
							case Sept = 7;
							case Huit = 8;
							case Neuf = 9;
							case Dix = 10;
							case Valet = 11;
							case Cavalier = 12;
							case Dame = 13;
							case Roi = 14;
						}

						enum HauteurStr: string {
							case As = 'A';
							case Deux = '2';
							case Trois = '3';
							case Quatre = '4';
							case Cinq = '5';
							case Six = '6';
							case Sept = '7';
							case Huit = '8';
							case Neuf = '9';
							case Dix = '10';
							case Valet = 'V';
							case Cavalier = 'C';
							case Dame = 'D';
							case Roi = 'R';
						}
					?>
			- Sont amenées à encore évoluer.
				- Objectif : se rapprocher de celles de Rust.
	- Type de retour "never" :
		- Comme le "never" de TypeScript.
		- Pour être retournée par les fonctions qui ne retournent jamais rien.
			- Si une exception est fatalement lancée, par exemple.
			- Si ça finit en exit();
			- S'il y a redirection HTTP.
	- Introduction des fibers
		- Comme les threads.
	- Des nouveaux mots-clés venus d'ailleurs pour les classes :
		- final :
			- Comme en Java.
			- Ne peut être modifié par les classes filles.
		- readonly :
			- Comme en C# ou en QML.
			- Attribut en lecture seule.
	- Fonction array_is_list()
		- Qui renvoie true si le tableau passé en argument n'a que des entrées numérotées à partir de 0.

PHP 8.2 :
	- Prévue pour fin 2022.
		- On parle donc ici de nouveautés prévues juisqu'à nouvelle ordre.
	- Type de retour null :
		- Pour les fonctions qui renvoient toujours null.
	- Méthodes magiques __get() et _set() (cf. PHP 101) dépréciées.
	- Plus généralement, dépréciation de trucs qui faisaient que PHP était très dynamique.
		- Plus statique, plus de force dans les types, plus contraignant.
