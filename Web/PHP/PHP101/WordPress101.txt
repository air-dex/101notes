WordPress 101 :

Généralités :
	- CMS PHP très populaire, bien plus que Drupal.
	- Avec un core, mais extnsible grâce à moult extensions.
	- Sites officiels :
		- https://wordpress.org/ : site de la technologie.
			- https://codex.wordpress.org/ : référence de WP.
		- https://wordpress.com/ : site pour créer son site à base de WP.
	- Concurrent : Drupal
	- Sous licence GPL

Installer WP :
	- Le site :
		- Récupérer les fichiers sur wordpress.org
		- Les mettre sur son serveur.
	- La BDD :
		- Créer une BDD MySQL/MariaDB.
		- Encodage de caractères "utf8_general_ci"
	- Installation du site :
		- Aller à l'URL du site (ici http://mon.wp/ ).
		- WP va demander les infos pour sa BDD, afin de créer le fichier wp-config.php (à la racine du site).
		- Après la création de wp-config, va demander des infos pour un 1er utilisateur WP, l'admin du site.
		- L'admin du WP sera disponible à http://mon.wpmon.wp/wp-admin (ou http://mon.wp/wp-login )

Les articles :
	- Contenus dynamiques.
	- Contenu pouvant être daté.
	- Ses attributs principaux :
		- Son titre.
		- Son état dans le process (brouillon, en attente de relecture, à publier, publié...).
		- Date de publication.
		- Ses taxons (catégorie et tags).
		- Une image pour le mettre en avant.
		- Un extrait pour les previews.
		- Autorisation des commentaires WP.
		- Sa visibilité (contenu réservé à des abonnés, par exemple).

Les pages :
	- Contenus statiques.
	- Conternu "intemporel", dont on se fiche de la date.
	- Ses attributs principaux :
		- Beaucoup en lien avec les articles car très semblables.
		- Son titre.
		- Son état dans le process (brouillon, en attente de relecture, à publier, publié...).
		- Ses taxons (catégorie et tags).
		- Une image pour le mettre en avant.
		- Un extrait pour les previews.
		- Autorisation des commentaires WP.
		- Une page parente.
		- Sa visibilité (contenu réservé à des abonnés, par exemple).

Les médias :
	- Contenus multimédias : sons, photos, vidéos, documents écrits...

Les thèmes :
	- Partie visuelle du site.
	- Si on modifie un thème, on crée un thème enfant.

Les commentaires :
	- Au sens "commentaires WP".
	- Caractérisés par :
		- Un pseudo
		- Une adresse mail
		- Son contenu
		- Une date de publication
		- Un état lié à sa modération
		- Un commentaire à qui il répond (si c'est une "réponse à").
	- L'utilisateur peut-être reconnu par son compte WP sur le site ou via Gravatar.

Les widgets :
	- Alias "les blocs".
	- Ce sont des parties d'articles.
		- Exemples : une vidéo, un fil d'actu, des commentaires Disqus, la bio de l'auteur...

Les menus :
	- TODO

Taxonomie :
	- Manière de classer les contenus.
	- WP en propose plusieurs :
		- Les catégories
		- Les tags (étiquettes)
		- Les menus
	- Les catégories :
		- Arborescence classique "dossier/sous-dossier".
		- Un contenu appartien à une seule catégorie.
	- Les tags :
		- Classification transverse.
		- Un articles peut avoir plusieurs tags.

Les utilisateurs :
	- Utilisateurs du site WP.
	- Peuvent avoir différents rôles (admin, abonné, rédacteur...).
	- Pour se connecter, ont besoin d'un login/mdp.
	- Ont besoin aussi d'une adresse mail pour s'inscrire.

Extensions populaires :
	- Woocommerce : pour transformer son WP en site e-commerce.
	- Akismet : anti-spam pour les commentaires WP.
