<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8"/>
		<title>Résultats des actions du formulaire.</title>
	</head>
	<body>
		<h1><a href="formulaires.php">Cliquez ici pour revenir à la page initiale</a></h1>
		
<?php
	$htmlstr = "";
	try {
		if (!isset($_POST["nForm"])) {
			throw new Exception("Il n'y a rien de posté.", -1);
		}
		
		$nForm = htmlentities($_POST["nForm"]);
		
		function writeIDform() {
			$htmlstr = "<h1>Vos renseignements :</h1>";
			
			// Nom et prénom
			if(!isset($_POST["nom"]) || isset($_POST["nom"]) && $_POST["nom"] === "") {
				throw new Exception("Vous n'avez pas de nom.");
			}
			
			if(!isset($_POST["prenom"]) || isset($_POST["prenom"]) && $_POST["prenom"] === "") {
				throw new Exception("Vous n'avez pas de prénom.");
			}
			
			$nom = htmlentities($_POST["nom"]);
			$prenom = htmlentities($_POST["prenom"]);
			$htmlstr .= "Vous vous appelez $prenom $nom.";
			
			// Sexe
			if(isset($_POST["sexe"])) {
				$sexe = htmlentities($_POST["sexe"]);
				
				switch ($sexe) {
					case "masculin";
						$htmlstr .= " Vous êtres un homme.";
						break;
						
					case "feminin";
						$htmlstr .= " Vous êtres une femme.";
						break;
						
					case "transsexuel";
						$htmlstr .= " Vous êtes transsexuel.";
						break;
						
					case "eunuque";
						$htmlstr .= " On vous a tout coupé entre les jambes.";
						break;
						
					case "rocco";
						$htmlstr .= " Vous êtres vantard.";
						break;
						
					case "autre";
						$htmlstr .= "Vous êtes foutus comment au milieu de votre corps ?";
						break;
						
					default:
						throw new Exception("Sexe inconnu.");
						break;
				}
			} else {
				throw new Exception("Sexe inconnu.");
			}
			
			// Date de naissance
			if (isset($_POST["bday"]))
			{
				$anniv = htmlentities($_POST["bday"]);
				
				// $regexanniv == /^[1-9][0-9]*\-((0[0-9])|(1[0-2]))\-(([0-2][0-9])|(3[01]))$/
				$regexanniv = "/^[1-9][0-9]*".preg_quote("-")."((0[0-9])|(1[0-2]))".preg_quote("-")."(([0-2][0-9])|(3[01]))$/";
				
				if (preg_match($regexanniv, $anniv)) {
					sscanf($anniv, "%d-%d-%d", $annee, $mois, $jour);
					$htmlstr .= " Vous êtes né le ".date("d/m/Y", mktime(0, 0, 0, $mois, $jour, $annee)).".";
				} else {
					throw new Exception("Date d'anniversaire invalide");
				}			
			} else {
				throw new Exception("Pas de date d'anniversaire");
			}
			
			// Heure du geek
			if (isset($_POST["geekhour"]))
			{
				$geekhour = htmlentities($_POST["geekhour"]);

				// $regexgeekhour == /^(([01][0-9])|(2[0-3]))\:[0-5][0-9]$/
				$regexgeekhour = "/^(([01][0-9])|(2[0-3]))".preg_quote(":")."[0-5][0-9]$/";
				
				if (preg_match($regexgeekhour, $geekhour)) {
					// Est-ce 13h37 ?
					sscanf($geekhour, "%d:%d", $heure, $minute);
					$connheure = ($heure == 13 and $minute == 37);
				} else {
					$connheure = false;
				}			
			} else {
				$connheure = false;
			}
			
			$htmlstr .= ($connheure ? " Vous connaissez l'heure du geek." : " Vous ne connaissez pas l'heure du geek.");
			
			// Année du permis
			if (isset($_POST["permis"]))
			{
				$permis = htmlentities($_POST["permis"]);
				
				if (is_numeric($permis)) {
					$htmlstr .= " Vous avez obtenu le permis en ".((integer) $permis).'.';
				} else {
					throw new Exception("Année de pemis invalide ($permis).");
				}
							
			} else {
				throw new Exception("Année de pemis non disponible.");
			}
			
			// Région auvergnate favorite
			if (isset($_POST["dpt"]))
			{
				$dpt = htmlentities($_POST["dpt"]);
				
				if (is_numeric($dpt)) {
					$dpt = (integer) $dpt;
					
					switch ($dpt)
					{
						case 3:
							$dpt = "Allier";
							break;
							
						case 15:
							$dpt = "Cantal";
							break;
							
						case 43:
							$dpt = "Haute-Loire";
							break;
							
						case 63:
							$dpt = "Puy de Dôme";
							break;
							
						default:
							$dpt = null;
							break;
					}
					
					$htmlstr .= (is_null($dpt) ? " Vous n'aimez pas l'Auvergne. Honte à vous !" : " Vous aimez $dpt.");
					
				} else {
					throw new Exception("Département invalide ($dpt).");
				}
							
			} else {
				$htmlstr .= " Vous n'aimez pas les départements. Honte à vous !";
			}
			
			// Les OS mobiles
			if (isset($_POST["osmob"]) and is_array($_POST["osmob"])) {
				$osmob = $_POST["osmob"];
				
				// Sécurisation de $osmob
				foreach ($osmob as &$os) {
					$os = htmlentities($os);
				}
				
				$pluriel = (count($osmob) > 1) ? "s" : "";
				
				$htmlstr .= " Vous aimez le$pluriel OS mobile$pluriel ".implode(", ", $osmob).".";
			} elseif (isset($_POST["osmob"]) and !is_array($_POST["osmob"]) ) {
				throw new Exception("Les OS mobiles devraient être dans un tableau");
			} else {
				$htmlstr .= " Vous n'aimez aucun OS mobile.";
			}
			
			return $htmlstr;
		}
		
		// Écrire les infos d'un fichier
		function infoFichier($name) {
			if (isset($_FILES[$name]) and is_array($_FILES[$name])) {
				$htmlstr = "Infos sur le fichier $name<ul>";
				
				foreach($_FILES[$name] as $cle => $valeur) {
					$htmlstr .= "<li><strong>".htmlentities($cle)."</strong> : ".htmlentities($valeur)."</li>" ;
				}
				$htmlstr .= "</ul>";
			} else {
				throw new Exception("Pas de fichier '$name' attendu reçu");
			}
			
			return $htmlstr;
		}
		
		function writeUploadfic() {
			$htmlstr = "<h1>Upload d'un fichier</h1>";
			$htmlstr .= infoFichier("fichier");
			
			if (move_uploaded_file($_FILES["fichier"]["tmp_name"], "mon cul.txt")) {
				$htmlstr .= "Fichier commité.";
			} else {
				$htmlstr .= "Fichier toujours en transit.";
			}
			
			
			return $htmlstr;
		}
		
		function writeUpload2fic() {
			print_r($_FILES);
			$htmlstr = "<h1>Upload de deux fichiers</h1>";
			
			foreach($_FILES as $name => $val) {
				$htmlstr .= infoFichier($name);
			}
			
			return $htmlstr;
		}
		
		function writeUpload2ficbis() {
			print_r($_FILES);
			
			$htmlstr = "<h1>Upload de deux fichiers (bis)</h1>";
			/*
			foreach($_FILES as $name => $val) {
				$htmlstr .= infoFichier($name);
			}//*/
			
			return $htmlstr;
		}
		
		// Traîtement selon le formulaire
		switch ($nForm) {
			case "idform":
				$htmlstr = writeIDform();
				break;
				
			case "uploadfic":
				$htmlstr = writeUploadfic();
				break;
				
			case "upload2fic":
				$htmlstr = writeUpload2fic();
				break;
				
			case "upload2ficbis":
				$htmlstr = writeUpload2ficbis();
				break;
			
			default:
				throw new Exception("On veut traiter un formulaire.", -2);
		}
	} catch (Exception $ex) {
		$htmlstr = "Problème de génération de la page :<br/>Erreur ".$ex->getCode()." : ".$ex->getMessage();
	}
	
	echo $htmlstr;
?>
	</body>
</html>
