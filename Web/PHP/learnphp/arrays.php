<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8"/>
		<link media="screen" rel="stylesheet" type="text/css" title="Style tableau" href="styletableau.css" />
		<title>Les tableaux</title>
	</head>
	<body>
		Création :<br>
		<?php
			$tab[] = "qqch";
			echo 'Création de $tab avec <code>$tab[] = "qqch"</code> : $tab est de type <code>',gettype($tab),"</code>.<br/>";
			
			$tab2 = [];
			echo 'Création de $tab avec <code>$tab = []</code> : $tab est de type <code>',gettype($tab2),"</code>.<br/>";
			
			$tab3 = array();
			echo 'Création de $tab avec <code>$tab = array()</code> : $tab est de type <code>',gettype($tab3),"</code>.<br/>";
		?>
		<hr/>
		Initialisation :<br>
		<?php
			$monopoly = [
				"nom" => "Monopoly",
				"éditeur" => "Hasbro",
				"Excellent jeu" => true,
				"Nombre de cases" => 38,
				1929 => "Année de l'évènement ayant déclenché la naissance du Monopoly"
			];
			
			echo "print_r : ";
			print_r($monopoly);
			echo "<br/>var_dump : ";
			var_dump($monopoly);
		?>
		<hr/>
		Range :<br/>
		<?php
			$alphabet = range("a", "z");
			echo "L'alphabet : ",implode(", ", $alphabet),".<br/>";
			
			$alphabet2 = range("a", "z", 2);
			echo "L'alphabet une lettre sur deux : ",implode(", ", $alphabet2),".<br/>";
			
			$alphabet3 = range("auv", "bzh");
			echo "Une suite : ",implode(", ", $alphabet3),".<br/>";
		?>
		<hr/>
		<table>
			<caption><code>each();</code> sur <code>$monopoly</code></caption>
			<thead>
				<tr>
					<td><code>0</code></td>
					<td><code>1</code></td>
					<td><code>"key"</code></td>
					<td><code>"value"</code></td>
				</tr>
			</thead>
			<tbody>
				<?php
					for(reset($monopoly);$elt = each($monopoly);) {
						echo "<tr><td>",$elt[0],"</td><td>",$elt[1],"</td><td>",$elt["key"],"</td><td>",$elt["value"],"</td></tr>";
					}
					
					/* Même chose que :
					
					reset($monopoly);
					
					while($elt = each($monopoly)) {
						echo "<tr><td>",$elt[0],"</td><td>",$elt[1],"</td><td>",$elt["key"],"</td><td>",$elt["value"],"</td></tr>";
					}
					//*/
				?>
			</tbody>
		</table>
		<hr/>
		Création / dégommage de variable :<br/>
		<?php
			$str = 'La variable $str';
			
			echo 'La variable $str ',(isset($str) ? "existe" : "n'existe pas"),".</br>";
			
			unset($str);
			
			echo 'La variable $str ',(isset($str) ? "existe encore" : "n'existe plus"),".</br>";
		?>
		<hr/>
		Tri :<br>
		<?php
			$tab = [2, -5, 3.6, 12, 0, "Henri-Jean", 259];
			$tab["Riton"] = "Toto";
			$tab[-1] = "Et mon cul c'est du poulet ?";
			
			function compi($a, $b) {
				if ($a < $b)
					return -1;
				else if ($a == $b)
					return 0;
				else 	// $a > $b
					return 1;
			}
			
			function afftab($tab) {
				echo "[",implode(", ", $tab),']<br/>';
				print_r($tab);
				echo "<br/>";
			}
			
			afftab($tab);
			$tab2 = $tab;
			usort($tab2, "compi");
			afftab($tab2);
			uasort($tab, "compi");
			afftab($tab);
		?>
	</body>
</html>
