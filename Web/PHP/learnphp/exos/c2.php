<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8"/>
		<title>Exercices du chapitres 2</title>
		
		<!-- Style pour les tableaux -->
		<link media="screen" rel="stylesheet" type="text/css" title="Style tableau" href="../styletableau.css" />
		<link media="screen" rel="stylesheet" type="text/css" title="Style exos" href="stylexos.css" />
	</head>
	<body>
		<!-- <h1><?= $GLOBALS["TITLE"] ?></h1> -->
		Exercice 1 :<br/>
		Noms valides ou invalide pour une variable :<ul>
			<li><code>mavar</code> : invalide (mais valide pour unr constante).</li>
			<li><code>$mavar</code> : valide.</li>
			<li><code>$var5</code> : valide.</li>
			<li><code>$_mavar</code> : valide.</li>
			<li><code>$_5var</code> : valide.</li>
			<li><code>$__élément1</code> : invalide (pas de lettres accentuées).
			=> <strong class="errorstrong">ERROR !</strong> (mais c'est pas conseillé non plus. Na!)</li>
			<li><code>$hotel4*</code> : invalide (à cause de <code>'*'</code> à la fin).</li>
		</ul>
		<hr>
		Exercice 2 :<br>
		Script PHP : (pas écrit)
		<?php
			$x="PostgreSQL";
			$y="MySQL";
			$z=&$x;
			$x="PHP 5";
			$y=&$x;
		?>
		<br>Valeurs :<ul>
			<li><code>$x</code> : <code>"PHP 5"</code>.</li>
			<li><code>$y</code> : <code>"PHP 5"</code>.</li>
			<li><code>$z</code> : <code>"PHP 5"</code>.</li>
		</ul>
		Réponses :<ul>
			<li><code>$x</code> : <code><?= $x ?></code>.</li>
			<li><code>$y</code> : <code><?= $y ?></code>.</li>
			<li><code>$z</code> : <code><?= $z ?></code>.</li>
		</ul>
		<hr>
		Exercice 3 :<br>
		Valeurs de l'exercice 2 avec <code>$GLOBALS</code> :<ul>
			<li><code>$x</code> : <code>$GLOBALS['x']</code>.</li>
			<li><code>$y</code> : <code>$GLOBALS['y']</code>.</li>
			<li><code>$z</code> : <code>$GLOBALS['z']</code>.</li>
		</ul>
		<br>
		<hr>
		Exercice 4 :<br>
		<strong>TODO</strong>
		Avec les constantes prédéfinies. => <strong class="errorstrong">JUSTE !</strong> (ou presque)
		<ol>
			<li>Version de PHP : <code>PHP_VERSION (<?= PHP_VERSION ?>)</code>.</li>
			<li>OS du serveur : <code>PHP_OS (<?= PHP_OS ?>)</code>.</li>
			<li>Langue du navigateur : <code>BROWSER_LANG</code>. => <strong class="errorstrong">ERROR !</strong>
			C'est <code>$_SERVER["HTTP_ACCEPT_LANGUAGE"]</code> (<code><?= $_SERVER["HTTP_ACCEPT_LANGUAGE"] ?></code>),
			donc rien à voir.</li>
		</ol>
		<hr>
		Exercice 5 :<br>
		<div>
		Script PHP : (pas écrit)
		<?php
			$x="PHP 5";
			$a[]=&$x;
			$y=" 5e version de PHP.";
			$z=$y*10;
			$x.=$y;
			$y*=$z;
			$a[0]="MySQL";
		?>
		<br>
		<table>
			<caption>Lignes et valeurs</caption>
			<thead>
				<tr>
					<td>N° de ligne</td>
					<td><code>$x</code></td>
					<td><code>$y</code></td>
					<td><code>$z</code></td>
					<td><code>$a</code></td>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>1</td>
					<td>"PHP 5"</td>
					<td>Non défini</td>
					<td>Non défini</td>
					<td>Non défini</td>
				</tr>
				<tr>
					<td>2</td>
					<td>"PHP 5"</td>
					<td>Non défini</td>
					<td>Non défini</td>
					<td>{ 0: "PHP 5" }</td>
				</tr>
				<tr>
					<td>3</td>
					<td>"PHP 5"</td>
					<td>" 5e version de PHP."</td>
					<td>Non défini</td>
					<td>{ 0: "PHP 5" }</td>
				</tr>
				<tr>
					<td>4</td>
					<td>"PHP 5"</td>
					<td>" 5e version de PHP."</td>
					<td>50</td>
					<td>{ 0: "PHP 5" }</td>
				</tr>
				<tr>
					<td>5</td>
					<td>"PHP 5 5e version de PHP."</td>
					<td>" 5e version de PHP."</td>
					<td>50</td>
					<td>{ 0: "PHP 5 5e version de PHP." }</td>
				</tr>
				<tr>
					<td>6</td>
					<td>"PHP 5 5e version de PHP."</td>
					<td>250</td>
					<td>50</td>
					<td>{ 0: "PHP 5 5e version de PHP." }</td>
				</tr>
				<tr>
					<td>7</td>
					<td>"MySQL"</td>
					<td>250</td>
					<td>50</td>
					<td>{ 0: "MySQL" }</td>
				</tr>
			</tbody>
		</table>
		<table>
			<caption>Lignes et types</caption>
			<thead>
				<tr>
					<td>N° de ligne</td>
					<td><code>$x</code></td>
					<td><code>$y</code></td>
					<td><code>$z</code></td>
					<td><code>$a</code></td>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>1</td>
					<td>string</td>
					<td>Non défini</td>
					<td>Non défini</td>
					<td>Non défini</td>
				</tr>
				<tr>
					<td>2</td>
					<td>string</td>
					<td>Non défini</td>
					<td>Non défini</td>
					<td>array (avec une string en 0)</td>
				</tr>
				<tr>
					<td>3</td>
					<td>string</td>
					<td>string</td>
					<td>Non défini</td>
					<td>array (avec une string en 0)</td>
				</tr>
				<tr>
					<td>4</td>
					<td>string</td>
					<td>string</td>
					<td>integer</td>
					<td>array (avec une string en 0)</td>
				</tr>
				<tr>
					<td>5</td>
					<td>string</td>
					<td>string</td>
					<td>integer</td>
					<td>array (avec une string en 0)</td>
				</tr>
				<tr>
					<td>6</td>
					<td>string</td>
					<td>integer</td>
					<td>integer</td>
					<td>array (avec une string en 0)</td>
				</tr>
				<tr>
					<td>7</td>
					<td>string</td>
					<td>integer</td>
					<td>integer</td>
					<td>array (avec une string en 0)</td>
				</tr>
			</tbody>
		</table>
		</div>
		<hr>
		Exercice 6 :<br>
		Script PHP : (pas écrit)
		<?php
			$x6="7 personnes";
			$y6= (integer) $x6;
			$x6= "9E3";
			$z6= (double) $x6;
		?>
		<br>Valeurs :<ul>
			<li><code>$x</code> : <code>9.000</code>. => <strong class="errorstrong">ERROR !</strong> ("9E3" est une string !)</li>
			<li><code>$y</code> : <code>7</code>.</li>
			<li><code>$z</code> : <code>9.000,000.000 (etc)</code>.</li>
		</ul>
		Réponses :<ul>
			<li><code>$x</code> : <code><?= $x6 ?></code>.</li>
			<li><code>$y</code> : <code><?= $y6 ?></code>.</li>
			<li><code>$z</code> : <code><?= $z6 ?></code>.</li>
		</ul>
		<hr>
		Exercice 7 :<br>
		Script PHP : (pas écrit)
		<?php
			$a7 = "0";
			$b7 = "TRUE";
			$c7 = FALSE;
			$d7 = ($a7 OR $b7);
			$e7 = ($a7 AND $c7);
			$f7 = ($a7 XOR $b7);
		?>
		<br>Valeurs finales :<ul>
			<li><code>$a</code> : <code>true</code>. => <strong class="errorstrong">ERROR !</strong> (variable vide donc false)</li>
			<li><code>$b</code> : <code>true</code>.</li>
			<li><code>$c</code> : <code>false</code>.</li>
			<li><code>$d</code> : <code>true</code>.</li>
			<li><code>$e</code> : <code>false</code>.</li>
			<li><code>$f</code> : <code>false</code>. => <strong class="errorstrong">ERROR !</strong> (à cause de <code>$a</code>)</li>
		</ul>
		Réponses :<ul>
			<li><code>$a</code> : <code><?php if ($a7) { echo "true"; } else { echo "false"; } ?></code>.</li>
			<li><code>$b</code> : <code><?php if ($b7) { echo "true"; } else { echo "false"; } ?></code>.</li>
			<li><code>$c</code> : <code><?php if ($c7) { echo "true"; } else { echo "false"; } ?></code>.</li>
			<li><code>$d</code> : <code><?php if ($d7) { echo "true"; } else { echo "false"; } ?></code>.</li>
			<li><code>$e</code> : <code><?php if ($e7) { echo "true"; } else { echo "false"; } ?></code>.</li>
			<li><code>$f</code> : <code><?php if ($f7) { echo "true"; } else { echo "false"; } ?></code>.</li>
		</ul>
	</body>
</html>
