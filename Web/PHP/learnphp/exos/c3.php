<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8"/>
		<title>Exercices du chapitre 3</title>
		<link media="screen" rel="stylesheet" type="text/css" title="Style exos" href="stylexos.css" />
	</head>
	<body>
		Exercice 1 :<br/>
		Code solution : (pas visible)
		<?php
			// Le nombre à tester s'appelle $nb
			$nb = "Un nombre";
			
			$est_multiple =	($nb % 3 == 0) AND ($nb % 5 == 0);
			
			// Autre solution, 3 et 5 étant premiers entre eux
			$est_multiple_bis = $nb % 15 == 0;
		?>
		<hr/>
		Exercice 2 :<br/>
		Code solution : (pas visible)<br/>
		<?php
			$age = 22;
			$sexe = "f";
			
			if ($age >= 21 AND $age <= 40 and $sexe === "f") {
				echo "Bonjour madame !<br/>";
			}
		?>
		<hr/>
		Exercice 3 : => JUSTE ! <br/>
		<strong class="errorstrong">mais c'était pas forcément demandé comme ça.
		Ma soltuion ne fait qu'optimiser le nombre de tirages.</strong>
		<div>
			Code solution : (pas visible)<br/>
			<ol>
				<li>Avec une boucle do...while : (pas visible)<br/>
					<?php
						// Les deux premiers tirages
						$tirage[0] = rand();
						$tirage[1] = rand();
						echo "Tirages : $tirage[0] $tirage[1]";
						$index = 1;
						
						do {
							$index++;
							$tirage[$index] = rand();
							echo " ".$tirage[$index];
						} while(($tirage[$index -2] %2 == 1) OR ($tirage[$index -1] %2 == 0) OR ($tirage[$index] %2 == 0));
						echo "<br/>Suite trouvée !";
					?>
				</li>
				<li>Optimisation 1, sans <code>$index</code> : (pas visible)<br/>
					<?php
						// Les deux premiers tirages
						$tirage[] = rand();
						$tirage[] = rand();
						echo "Tirages : $tirage[0] $tirage[1]";
						
						do {
							$tirage[] = rand();
							echo " ".$tirage[count($tirage) -1];
						} while(($tirage[count($tirage) -3] %2 == 1) OR ($tirage[count($tirage) -2] %2 == 0) OR ($tirage[count($tirage) -1] %2 == 0));
						echo "<br/>Suite trouvée !";
					?>
				</li>
				<li>Optimisation 2, avec boucle for (mais variable <code>$index</code>) : (pas visible)<br/>
					<?php
						// Les deux premiers tirages
						echo "Tirages :";
						for ($index = 0;
							 $index < 2 OR (($index >=2) and ($tirage[$index-2] %2 == 0) and ($tirage[$index-1] %2 == 1) and ($tirage[$index] %2 == 1));
							 $index++)
						{
							$tirage[$index] = rand();
							echo " ".$tirage[$index];
						}
						echo "<br/>Suite trouvée !";
					?>
				</li>
			</ol>
		</div>
		<hr/>
		Exercice 4 :<br/>
		<div>
			Code solution : (pas visible)<br/>
			MOYEN ! Il fallait toutes les générer.<br/>
			<?php
				echo "Je ne comprends pas cet exercice ! Je fais ce que j'ai compris.<br/>";
				
				// Création d'une plaque
				
				// Les 3 lettres
				$alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
				define("indP", 15);		// Index de P
				define("indH", 7);		// Index de H
				$lettres = "";
				$lettre = "";
				
				// 1ère lettre
				do {
					$indexalpha = rand(0, 25);
					$lettre = $alphabet[$indexalpha];
				} while ($indexalpha < indP);
				$lettres.=$lettre;
				
				// 2ème lettre
				if ($lettre == "P") {
					do {
						$indexalpha = rand(0, 25);
						$lettre = $alphabet[$indexalpha];
					} while ($indexalpha < indH);
				} else {
					$lettre = $alphabet[rand(0, 25)];
				}
				$lettres.=$lettre;
				
				// 3ème lettre
				if ($lettres == "PH") {
					do {
						$indexalpha = rand(0, 25);
						$lettre = $alphabet[$indexalpha];
					} while ($indexalpha < indP);
				} else {
					$lettre = $alphabet[rand(0, 25)];
				}
				$lettres.=$lettre;
				
				// Le nombre
				$nombre = rand(10, 999);
				
				// L'affichage
				echo "La plaque : $nombre $lettres 75<br/>";
				
				// Stockage dans le tableau
				echo "On stocke ".(($nombre % 100 == 0) ? "" : "pas ")."la plaque dans un tableau.";
			?>
			</div>
		<hr/>
		Exercice 5 :<br/>
		<div>
			Code solution :<ul>
				<li>Avec la boucle <code>while</code> : (pas visible)<br/>
					<?php
						$base = rand(100, 999);
						$cpt = 0;
						$nb = 99;	// Nombre invalide pour faire échouer le premier test.
						
						while ($nb != $base) {
							$nb = rand(100, 999);
							$cpt++;
						}
					?>
					Il aura fallu <?= $cpt ?> essai<?= ($cpt > 1) ? "s" : "" ?> pour revenir au nombre <?= $base ?>.
				</li>
				<li>Avec la boucle <code>for</code> : (pas visible) => <strong class="errorstrong">ERREUR ! Ça commence à 1 et non 0</strong>.<br/>
					<?php
						$base = rand(100, 999);
						$nb = 99;	// Nombre invalide pour faire échouer le premier test.
						
						for ($cpt = 0; $cpt != $nb; ++$cpt) {
							$nb = rand(100, 999);
						}
					?>
					Il aura fallu <?= $cpt ?> essai<?= ($cpt > 1) ? "s" : "" ?> pour revenir au nombre <?= $base ?>.
				</li>
				<li><strong>Bonus</strong> avec la boucle <code>do...while</code> : (pas visible)<br/>
					<?php
						$base = rand(100, 999);
						$cpt = 0;
						
						do {
							$nb = rand(100, 999);
							$cpt++;
						} while ($nb != $base);
						
					?>
					Il aura fallu <?= $cpt ?> essai<?= ($cpt > 1) ? "s" : "" ?> pour revenir au nombre <?= $base ?>.
				</li>
			</ul>
		</div>
		<hr/>
		Exercice 6 :<br/>
		<div>
			Code solution : (pas visible) => <strong class="errorstrong">"triche" avec <code>$alphabet</code></strong><br/>
			<?php
				for ($i = 11; $i <= 36; $i++) {
					// Je reprends la variable $alphabet de l'exercice 4
					$tab[$i] = $alphabet[$i - 11];
				}
				
				// Lecture du tableau avec la boucle for
				for ($i = 11; $i <= 36; $i++) {
					echo $tab[$i]." ";
				}
				echo "<br/>";
				
				// Lecture du tableau avec la boucle foreach
				foreach ($tab as $lettre) {
					echo "$lettre ";
				}
				echo "<br/>";
				
				// Lecture indices et valeurs
				foreach ($tab as $ind => $lettre) {
					echo '$tab['.$ind."] = $lettre<br/>";
				}
			?>
		</div>
		<hr/>
		Exercice 7 :<br/>
		<div>
			Code solution :<ul>
				<li>Avec une boucle <code>while</code> : (pas visible)<br/>
					<?php
						$nb = rand(1, 1000);	// Le nombre donné

						while (true) {
							$mult = rand();
							
							if ($mult % $nb == 0)	break;
						}
					?>
					<?= $mult ?> est un multiple de <?= $nb ?>.
				</li>
				<li>Avec une boucle <code>do...while</code> : (pas visible)<br/>
					<?php
						$nb = rand(1, 1000);	// Le nombre donné
						
						do {
							$mult = rand();
						} while ($mult % $nb != 0);
					?>
					<?= $mult ?> est un multiple de <?= $nb ?>.
				</li>
			</ul>
		</div>
		<hr/>
		Exercice 8 :<br/>
		<div>
			Code solution : (pas visible)<br/>
			<?php
				// Détermination des nombres
				try {
					$a = rand();
					$b = rand();
					
					if (!is_int($a)) {
						throw new Exception ("On n'a pas deux nombres entiers pour calculer le PGCD.", $a);
					}
					
					if (!is_int($b)) {
						throw new Exception ("On n'a pas deux nombres entiers pour calculer le PGCD.", $b);
					}
					
					// Calcul du PGCD
					echo "PGCD($a, $b) = ";
					
					// Avoir $a >= $b
					if ($b > $a) {
						// Swap
						$c = $a;
						$a = $b;
						$b = $c;
					}
					
					if (($a == $b) or ($b == 0)) {
						$pgcd = $a;
					} else {
						// Calcul par l'algorithme d'Euclide.
						do {
							$r = $a % $b;
							
							if ($r != 0) {
								$a = $b;
								$b = $r;
							}
						} while ($r != 0);
						
						$pgcd = $b;
					}
					
					// Affichage du résultat
					echo $pgcd;
				} catch (Exception $ex) {
					echo "Le PGCD ne peut être calculé car ".$ex->getCode()." n'est pas entier.";
				}
			?>
		</div>
	</body>
</html>
