<!DOCTYPE html>
<html>
	<head>
		<!-- <meta type="text/html; charset=utf-8"/> -->
		<meta charset="utf-8"/>
		<link media="screen" rel="stylesheet" title="Style exos" type="text/css" href="stylexos.css" />
		<title>Exercices du chapitre 4</title>
	</head>
	<body>
		Exercice 1 :<br/>
		<div>
			<?php
				$str = "Down fROm the InsIde !";
				$newstr = ucwords($str);
				
				echo "Chaîne initiale : '$str'.<br/>";
				echo "Chaîne transformée : '$newstr'.";
			?>
		</div>
		<hr/>
		Exercice 2 :<br/>
		<div>
			<?php
				$str = "PHP 5";
				echo "'$str' avec un caractère par ligne :<br/>";
				
				for ($i = 0; $i < strlen($str); $i ++) {
					echo $str[$i],"<br/>";
				}
			?>
		</div>
		<hr/>
		Exercice 3 :<br/>
		<div>
			<p style="font-family: Courier New">
				<?php
					// Le tableau de noms
					$tab[] = "Ducher Romain";
					$tab[] = "Clavel Isabelle";
					$tab[] = "Porte Louane";
					$tab[] = "Porte Hortense-Gertrude-Albertine";
					$tab[] = "Martinet Françoise";
					$tab[] = "Clavel André";
					$tab[] = "Ducher Gérard";
					
					// L'affichage
					foreach ($tab as $personne) {
						sscanf($personne, "%s %s", $nom, $prenom);
						
						// Ce qui serait bien
						//printf("%20s %20s<br/>", $prenom, $nom);
						
						// Bidouille
						
						// Prénom
						$lstr = strlen($prenom);
						if ($lstr < 20) {
							// On bourre avec des espaces
							for ($i = $lstr; $i < 20; $i++) {
								$prenom.='&nbsp;';
							}
						} elseif ($lstr > 20) {
							// On coupe
							$prenom = substr($prenom, 0, 20);
						}
						
						// Nom
						$lstr = strlen($nom);
						if ($lstr < 20) {
							// On bourre avec des espaces
							for ($i = $lstr; $i < 20; $i++) {
								$nom.='&nbsp;';
							}
						} elseif ($lstr > 20) {
							// On coupe
							$nom = substr($nom, 0, 20);
						}
						
						printf("%s&nbsp;%s<br/>", $prenom, $nom);
					}
				?>
					
				<strong>Mais va te faire encoder espèce de sale PHP !</strong>
			</p>
			<p>
				<strong class="errorstrong">MOYEN !</strong><br/>
				Corrigé : il fallait utiliser des symboles <code>%'_-20s</code> :<br/>
				<div style="font-family: Courier New">
				<?php
					foreach ($tab as $personne) {
						sscanf($personne, "%s %s", $nom, $prenom);
						
						// Ce qui serait bien
						printf("%'_-20s %'_-20s<br/>", $prenom, $nom);
					}
				?>
				</div>
			</p>
		</div>
		<hr/>
		Exercice 4 :<br/>
		<div>
			<?php
				$str = '<form action="script.php">';
				echo htmlentities($str);
			?>
		</div>
		<hr/>
		Exercice 5 :<br/>
		<div>
			<?php
				$s1 = "Bateau";
				$s2 = "arbre";
				
				if (strtolower($s1) < strtolower($s2)) {
					echo "$s1 $s2";
				} else {
					echo "$s2 $s1";
				}
			?>
		</div>
		<hr/>
		Exercice 6 :<br/>
		<div>
			<?php
				$texte = "Ah bin zut alors ! J'ai écrit bordel de merde !";
				echo "Texte original : $texte<br/>";
				$censure = htmlentities("<censored>");
				echo "Texte censuré : ",str_replace("zut", $censure, $texte),"<br/>";
			?>
			<strong class="errorstrong">NB :</strong> l'exo attendait des regex comme <code>/Zut/i</code>.
		</div>
		<hr/>
		Exercice 7 :<br/>
		<div>
			<?php
				/*
				 * URL = <scheme>://<authority>/<chemin>/<nom><query><fragment>
				 * 
				 * <scheme> : HTTP ou FTP ?
				 * <authority> = <user_info>@<host>:<port>
				 * 		<user_info> = <user>:<password>
				 * 		user_info et le port sont facultatifs.
				 * 
				 * Tout ce qu'il y a après l'authority est facultatif.
				 * 
				 * <chemin> : comme un chemin absolu sous Linux / UNIX
				 * 
				 * nom : au moins une lettre avec éventuellement une extension
				 * 
				 * query = ?<query_arg>&<query_arg2>&...
				 * 		<query_arg> = <nom>=<valeur>, nom et valeur url-encodés.
				 * 
				 * fragment = #<qqch>
				 */
				
				$achar = "[a-zA-Z0-9".preg_quote("-_%")."]";	// [a-zA-Z0-9\-_%]
				
				// Regex finale
				
				// Scheme : HTTP ou FTP ?
				$scheme = "((http)|(ftp))";
				
				// Authority :
				
				// user_info :
				$user = "$achar+";	// user
				$password = "$achar+";	// password
				$userinfo = "$user(".preg_quote(":")."$password)?";	// $user(:$password)?
				
				// host
				$domain =  "[a-zA-Z0-9".preg_quote("-_")."]+";
				$tld = "[a-z]{2,4}";	// Top Level Domain
				$host = "$domain(".preg_quote(".")."$domain)*".preg_quote(".")."$tld";	// $domain(\.$domain)*\.$tld
				
				// port
				$port="((80)|(21))";
				
				// Assemblage
				$authority = "($userinfo@)?$host(".preg_quote(":")."$port)?";
				
				// After : TODO
				
				$chemin = "([a-zA-Z0-9".preg_quote("+-_%")."]".preg_quote("/", '/').")*";
				$nom = "($achar)+(".preg_quote(".")."($achar)+)?";
				$queryarg = "$achar".preg_quote('=')."$achar";
				$query = preg_quote("?")."($queryarg)+";
				$fragment = preg_quote("#")."$achar";
				
				$after = preg_quote("/", '/')."($chemin)?($nom)?($query)?($fragment)?";
				
				$regex = "/^$scheme".preg_quote("://", '/')."$authority($after)?$/";
				echo $regex;
				echo "<br/>TODO";
				
				
				
			/*
				// HTTP ou FTP ?
				$scheme = "(http)|(ftp)";
				
				// Domain : mot minuscule avec éventuellement un tiret au milieu (mais pas la fin).
				$domain = "[a-z][a-z".preg_quote('-')."]+[a-z]";
				$baseurl = "($domain)(".preg_quote('.')."$domain)+".preg_quote('.')."[a-z]{2,4}";
				
				$regex="/".$scheme.preg_quote("://", '/').$baseurl."/";
				*/
			?>
			<p>
				<strong>BOF !</strong> Étaient attendues des URLs de type "www.qqch.com" (BEAUCOUP plus simple).
			</p>
		</div>
		<hr/>
		Exercice 8 :<br/>
		<div>
			<?php
				//$regexage = "/[1-9]?[[:digit:]]/";
				$regexage = "/^[1-9]?[0-9]$/";
				
				$patternage = "Mamy a %s ans.";
				
				$gm[] = "Mamy a 2 ans.";
				$gm[] = "Mamy a 18 ans.";
				$gm[] = "Mamy a 95 ans.";
				$gm[] = "Mamy a 102 ans.";
				
				foreach ($gm as $mamy) {
					// Récupération de l'âge de mamy
					sscanf($mamy, $patternage, $age);
					
					echo "$mamy : ";
					if (preg_match($regexage, $age)) {
						echo "valide";
					} else {
						echo "invalide";
					}
					echo ".</br>";
				}
			
				echo "La regex : $regexage";
			?>
		</div>
		<hr/>
		Exercice 9 :<br/>
		<div>
			<?php $ch = "PHP \n est meilleur \n que ASP \n et JSP \n réunis."; ?>
			
			Chaîne de base : '<?= $ch ?>'<br/>
			<ol>
				<li>Avec une fonction (<code>nl2br()</code>) : '<?= nl2br($ch) ?>'.</li>
				<li>Avec un remplacement à base de regex : '<?= preg_replace("/\n/", "<br/>", $ch) ?>'.</li>
				</li>
			</ol>
		</div>
	</body>
</html>
