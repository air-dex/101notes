<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8"/>
		<title>Exercices du chapitre 5</title>
		
		<!-- Style pour les tableaux -->
		<link media="screen" rel="stylesheet" type="text/css" href="../styletableau.css" />
		<link media="screen" rel="stylesheet" type="text/css" href="stylexos.css" />
	</head>
	<body>
		Exercice 1 :<br/>
		<div>
			<?php
				$annuaire1 = [
					"Ducher"   => ["Romain",      "Seychalles", 25 ],
					"Porte"    => ["Louane",      "Seychalles", 11 ],
					"Favy"     => ["Gaëlle",      "Seychalles", 38 ],
					"Martinet" => ["Fabien",      "Seychalles", 39 ],
					"Vautrin"  => ["Marie-Laure", "Pagnant",    30 ]
				];
				
				print_r($annuaire1);
			?>
		</div>
		<hr/>
		Exercice 2 :<br/>
		<div>
			<?php
				//
				$annuaire2 = [
					"Ducher"   => ["prenom" => "Romain",      "ville" => "Seychalles", "age" => 25 ],
					"Porte"    => ["prenom" => "Louane",      "ville" => "Seychalles", "age" => 11 ],
					"Favy"     => ["prenom" => "Gaëlle",      "ville" => "Seychalles", "age" => 38 ],
					"Martinet" => ["prenom" => "Fabien",      "ville" => "Seychalles", "age" => 39 ],
					"Vautrin"  => ["prenom" => "Marie-Laure", "ville" => "Pagnant",    "age" => 30 ]
				];
				
				print_r($annuaire2);
			?>
		</div>
		<hr/>
		Exercice 3 :<br/>
		<div>
			<p>
				Affichage du tableau 1 :<br/>
				<?php
					foreach ($annuaire1 as $nom => $personne) {
						echo "{$personne[0]} $nom a {$personne[2]} ans et vit à {$personne[1]}.<br/>";
					}
				?>
			</p>
			<p>
				Affichage du tableau 2 :<br/>
				<?php
					foreach ($annuaire2 as $nom => $personne) {
						echo "{$personne["prenom"]} $nom a {$personne["age"]} ans et vit à {$personne["ville"]}.<br/>";
					}
				?>
			</p>
		</div>
		<hr/>
		Exercice 4 :<br/>
		<div>
			<p>
				Affichage du tableau 1 :<br/>
				<?php
					reset($annuaire1);
					
					/*
					while (list($nom, $personne) = each($annuaire1)) {
						echo "{$personne[0]} $nom a {$personne[2]} ans et vit à {$personne[1]}.<br/>";
					}//*/
					
					while (list($nom, list($prenom, $ville, $age)) = each($annuaire1)) {
						echo "$prenom $nom a $age ans et vit à $ville.<br/>";
					}
				?>
			</p>
			<p>
				Affichage du tableau 2 :<br/>
				<?php
					reset($annuaire2);
					
					while (list($nom, $personne) = each($annuaire2)) {
						echo "{$personne["prenom"]} $nom a {$personne["age"]} ans et vit à {$personne["ville"]}.<br/>";
					}
				?>
			</p>
		</div>
		<hr/>
		Exercice 5 :<br/>
		<div>
			<?php
				$recoms = [
					"https://twitter.com",
					"http://google.fr",
					"http://one.ubuntu.com",
					"http://bit.ly",
					"http://lacentrale.fr",
					"http://nokia.com/fr-fr/",
					"http://fr.gravatar.com/airdex",
				];
				
				$lienalea = &$recoms[0];
				printf("Tableau initial : [%s].<br/>", implode(", ", $recoms));
				printf("Le lien aléatoire : [%s].<br/>", $lienalea);
				
				// Tri en ordre aléatoire
				shuffle($recoms);
				
				$lienalea2 = &$recoms[0];
				printf("Tableau shufflé : [%s].<br/>", implode(", ", $recoms));
				printf("Le lien aléatoire : [%s].<br/>", $lienalea);
				printf("Le lien aléatoire 2 : [%s].<br/>", $lienalea2);
			?>
			<strong class="errorstrong">
				FAUX ! Il fallait utiliser array_rand();
				et indexer les sites avec ce qu'ils recoomandent (Twitter, Google, Ubuntu One...).
			</strong>
		</div>
		<hr/>
		Exercice 6 :<br/>
		<div>
			<?php
				$ints = range(0, 63);	// Normalement "$ints = range(1, 63);" mais c'est pas ce qui sera attendu.
				printf("Tableau d'entiers : [%s].<br/>", implode(", ", $ints));
				
				// Tableau de 0 à 6.3 :
				
				// Fonction pour transformer
				function divdix($elt) {
					return $elt / 10.0;
				}
				
				// Transformation
				
				/* Avec array_walk
				$decis = $ints;
				if (array_walk($decis, "divdix")) {
					printf("Tableau décimé : [%s].", implode(", ", $decis));
				} else {
					echo "Tableau raté.";
				}
				echo "<br/>";
				//*/
				
				//* Avec un foreach dégueulasse
				foreach($ints as $index => $i) {
					$decis[$index] = divdix($i);
				}
				printf("Tableau décimé : [%s].<br/>", implode(", ", $decis));
				//*/
				
				// Création du tableau de sinus
				
				//* Avec un array_combine();
				foreach($decis as $deci) {
					$sinuses[(string) $deci] = sin($deci);
				}
				$tableau = array_combine($decis, $sinuses);
				//*/
				
				/* Avec un foreach seul
				foreach($decis as $deci) {
					$tableau[(string) $deci] = sin($deci);
				}
				//*/
				
				/* Suprême (mais faux)
				$sinuses = $decis;
				array_walk($sinuses, "sin");
				$tableau = array_combine(range(0, 6.3, 0.1), $sinuses);
				//*/
				
			?>
			
			<table>
				<caption>Table de sinus</caption>
				<thead>
					<tr>
						<td>X</td>
						<td>sin(X)</td>
					</tr>
				</thead>
				<tbody>
					<?php
						foreach ($tableau as $x => $sinx) {
							printf("<tr><td>%f</td><td>%f</td></tr>", $x, $sinx);
						}
					?>
				</tbody>
			</table>
		</div>
		<hr/>
		Exercice 7 :<br/>
		<div>
			<?php 
				$mails = [
					"ducher.romain@sfr.fr",
					"ducher.romain@gmail.com",
					"louaneporte@gmail.com",
					"gfavy@groupama-ra.fr",
					"sophie.bardet@almerys.com",
					"rene.favy@gmail.com",
					"vincent.huart@poste.isima.fr",
					"jeremy.coatelen@hotmail.com",
					"air-dex@ovi.com",
					"francoise.martinet@sfr.fr",
					"larry.page@google.com",
					"steve.ballmer@microsoft.com",
					"steve.jobs@apple.com"
				];
			?>
			
			Les mails :<ul>
				<?php
					foreach ($mails as $mail) { 
						echo "<li>$mail</li>";
					}
				?>
			</ul>
			
			<?php
				// Récupération des serveurs
				function getMailServer($courriel) {
					return explode('@', $courriel)[1];
				}
				
				foreach ($mails as $mail) {
					$servers[] = getMailServer($mail);
				}
				
				// Élimination des doublons
				$serversunique = array_unique($servers);
			?>
			
			Les serveurs mails :<ul>
				<?php
					foreach ($serversunique as $server) { 
						echo "<li>$server</li>";
					}
				?>
			</ul>
			
			<?php
				// Occurences
				
				/* Iter 0
				foreach ($serversunique as $server) {
					$occ[$server] = 0;
				}
				foreach ($mails as $mail) {
					$occ[getMailServer($mail)]++;
				}
				//*/
				
				/* Iter 1
				foreach ($mails as $mail) {
					$mailserver = getMailServer($mail);
					
					if (isset($occ[$mailserver])) {
						$occ[$mailserver]++;
					} else {
						$occ[$mailserver] = 1;
					}
					
				}
				//*/
				
				//* Iter2
				$occ = array_count_values($servers);
				//*/
			?>
			
			Les occurences des serveurs :<ul>
				<?php
					foreach ($occ as $server => $nb) {
						$pluriel = $nb > 1 ? "s" : "";
						echo "<li>Serveur $server : $nb adresse$pluriel mail$pluriel.</li>";
					}
				?>
			</ul>
		</div>
	</body>
</html>
