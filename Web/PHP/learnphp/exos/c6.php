<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8"/>
		<link media="screen" rel="stylesheet" title="Style exos" type="text/css" href="stylexos.css" />
		<title>Exercices du chapitre 6</title>
	</head>
	<body>
		<?php $actionpage = "c6form.php"; ?>
		Exercice 1 :<br/>
		<div>
			<form name="exo1" method="post" action="<?= $actionpage ?>" >
				<fieldset>
					<legend>Adresse client</legend>
					<input type="hidden" name="nbexo" value="1" />
					<label for="nom">Nom</label> <input type="text" name="nom" /> <br/>
					<label for="prenom">Prénom</label> <input type="text" name="prenom" /> <br/>
					<label for="adresse">Adresse</label> <input type="text" name="adresse" /> <br/>
					<label for="ville">Ville</label> <input type="text" name="ville" /> <br/>
					<label for="cpostal">Code postal</label> <input type="text" name="cpostal" /> <br/>
					<input type="submit" value="Envoyer l'adresse" />
				</fieldset>
			</form>
			<p>Résultat : sur <?= $actionpage ?></p>
		</div>
		<hr/>
		Exercice 2 :<br/>
		<div>
			<p>
				Idée 1 : avec des patterns dans le formulaire
				<form name="exo2" method="post" action="<?= $actionpage ?>">
					<fieldset>
						<legend>Adresse client</legend>
						<?php $pattern2 = "?{1,}" ?>
						<input type="hidden" name="nbexo" value="2" />
						<label for="nom">Nom</label> <input type="text" name="nom" pattern="<?= $pattern2 ?>"/> <br/>
						<label for="prenom">Prénom</label> <input type="text" name="prenom" pattern="<?= $pattern2 ?>" /> <br/>
						<label for="adresse">Adresse</label> <input type="text" name="adresse" pattern="<?= $pattern2 ?>" /> <br/>
						<label for="ville">Ville</label> <input type="text" name="ville" pattern="<?= $pattern2 ?>" /> <br/>
						<label for="cpostal">Code postal</label> <input type="text" name="cpostal" pattern="<?= $pattern2 ?>" /> <br/>
						<input type="submit" value="Envoyer l'adresse" />
					</fieldset>
				</form>
				<p>Résultat : sur <?= $actionpage ?></p>
			</p>
			<p>
				Idée 2 avec vérifs côté serveur (sur <?= $actionpage ?>).
				<form name="exo2bis" method="post" action="<?= $actionpage ?>">
					<fieldset>
						<legend>Adresse client</legend>
						<input type="hidden" name="nbexo" value="2" />
						<label for="nom">Nom</label> <input type="text" name="nom" /> <br/>
						<label for="prenom">Prénom</label> <input type="text" name="prenom" /> <br/>
						<label for="adresse">Adresse</label> <input type="text" name="adresse" /> <br/>
						<label for="ville">Ville</label> <input type="text" name="ville" /> <br/>
						<label for="cpostal">Code postal</label> <input type="text" name="cpostal" /> <br/>
						<input type="submit" value="Envoyer l'adresse" />
					</fieldset>
				</form>
				<p>Résultat : sur <?= $actionpage ?></p>
			</p>
		</div>
		<hr/>
		Exercice 3 :<br/>
		<div>
			<p>
				Les deux, <code>.html</code> de préférence.<br/>
				Le traitement des données se fait dans le fichier <code>ajout.php</code> situé dans le même dossier.
			</p>
			<p>
				Réponse : c'est juste.
			</p>
		</div>
		<hr/>
		Exercice 4 :<br/>
		<div>
			<p>
				Mes deux solutions :<ul>
					<li>Utiliser <?= $_SERVER["PHP_SELF"] ?> pour le champ action du formulaire.</li>
					<li>Écrire en dur le nom de la page PHP dans le champ action du formulaire.</li>
				</ul>
			</p>
			<p>
				Réponse : c'est juste.
			</p>
		</div>
		<hr/>
		Exercice 5 :<br/>
		<div>
			<form name="exo5" method="post" action="<?= $_SERVER["PHP_SELF"] ?>">
				<fieldset>
					<legend>Adresse email</legend>
					<input type="hidden" name="nbexo" value="5"/>
					<label for="email">Veuillez entrer votre adresse email : </label><input type="email" name="email"/><br/>
					<input type="submit" value="Envoyer l'adresse"/>
					<input type="hidden" name="navigo" value="<?= $_SERVER["HTTP_USER_AGENT"] ?>"/>
				</fieldset>
			</form>
			<p>
				Résultat :<br/>
				<?php
					if (isset($_POST["nbexo"]) and is_numeric($_POST["nbexo"]) and ((integer) $_POST["nbexo"]) == 5) {
						// Ce sont bien les données pour cet exercice.
						
						// Adresse email
						if (isset($_POST["email"])) {
							echo "Votre courriel : ",htmlentities($_POST["email"]),"<br/>";
						} else {
							echo "<strong>Il manque l'adresse email !</strong><br/>";
						}
						
						// Navigateur
						if (isset($_POST["navigo"])) {
							echo "Vos identifiants navigateur (user agent) : ",htmlentities($_POST["navigo"]),"<br/>";
						} else {
							echo "<strong>On ne peut identifier votre navigateur</strong>";
						}
					} else {
						// Ce ne sont pas les données pour cet exercice.
						echo "pas de résultat ici";
					}
				?>
			</p>
		</div>
		<hr/>
		Exercice 6 :<br/>
		<div>
			<form name="exo6" method="post" action="<?= $_SERVER["PHP_SELF"] ?>">
				<fieldset>
					<legend>Saisie de prix</legend>
					<input type="hidden" name="nbexo" value="6"/>
					<label for="ht">Prix HT : </label><input type="number" min="0.00" step="0.01" name="ht" value="<?= (isset($_POST["ht"]) ? htmlentities($_POST["ht"]) : 0.00 ) ?>"/>
					<label for="tva">Taux de TVA : </label><input type="number" min="0.0" step="0.1" name="tva" value="<?= (isset($_POST["tva"]) ? htmlentities($_POST["tva"]) : 19.6 ) ?>"/>
					<input type="submit" value="Calculer"/>
				</fieldset>
			</form>
			<p>
				Résultat :
				<?php
					if (isset($_POST["nbexo"]) and is_numeric($_POST["nbexo"]) and ((integer) $_POST["nbexo"]) == 6) {
						// Ce sont les données pour cet exercice.
						
						if (isset($_POST["ht"]) and is_numeric($_POST["ht"])) {
							$ht = (double) $_POST["ht"];
							
							if (isset($_POST["tva"]) and is_numeric($_POST["tva"])) {
								$tva = (double) $_POST["tva"];
								
								$ttc = $ht * (1 + $tva / 100);
								printf("Taux de TVA : %.1f.<br/>", $tva);
								printf("Prix TTC : %.2f.", $ttc);
							} else {
								echo "Pas de TVA =&gt; pas de calcul.";
							}
						} else {
							echo "Pas de prix HT =&gt; pas de calcul.";
						}
					} else {
						// Ce ne sont pas les données pour cet exercice.
						echo "pas de résultat ici";
					}
				?>
				<br/>
				<strong class="errorstrong">FAUX ! L'exercice attendait dans le formulaire une zone avec le montant de la TVA et
				une autre avec le prix TTC.Une fois qu'il a tout vérifié, l'utilisateur peut alors confirmer.</strong>
			</p>
		</div>
		<hr/>
		Exercice 7 :<br/>
		<div>
			<?php $mimeZIP = "application/zip"; ?>
			<form name="exo7" method="post" action="<?= $_SERVER["PHP_SELF"] ?>" enctype="multipart/form-data">
				<fieldset>
					<legend>Transfert de ZIP.</legend>
					<input type="hidden" name="nbexo" value="7"/>
					<input type="hidden" name="MAX_ZIP_SIZE" value="1000000" />
					<label for="zipfile">Votre archive ZIP : </label><input type="file" name="zipfile" accept="<?= $mimeZIP ?>" /><br/>
					<input type="submit" value="Téléverser"/>
				</fieldset>
			</form>
			<p>
				Résultat :<br/>
				<?php
					if (isset($_POST["nbexo"]) and is_numeric($_POST["nbexo"]) and ((integer) $_POST["nbexo"]) == 7) {
						// Ce sont les données pour cet exercice.
						
						if (isset($_FILES["zipfile"])) {
							// Infos
							$zipfile = $_FILES["zipfile"];
							$realname = htmlentities($zipfile["name"]);
							
							echo "Nom du fichier : ",$realname,"<br/>";
							echo "Taille du fichier : ",$zipfile["size"]," octet(s).<br/>";
							
							// Upload
							if ($zipfile["error"] == UPLOAD_ERR_OK and $zipfile["type"] === $mimeZIP
								and isset($_POST["MAX_ZIP_SIZE"]) and is_numeric($_POST["MAX_ZIP_SIZE"])
								and $zipfile["size"] < ((integer) $_POST["MAX_ZIP_SIZE"]))
							{
								$uploadOK = move_uploaded_file($zipfile["tmp_name"], $realname);
								echo "Réception réussie, upload ",($uploadOK ? "victorieux" : "raté"),".";
							} else {
								echo "Mauvais upload";
							}
						} else {
							echo "Le fichier n'a pas été uploadé.";
						}
					} else {
						// Ce ne sont pas les données pour cet exercice.
						echo "pas de résultat ici";
					}
				?>
				<br/><strong class="errorstrong">Juste dans l'ensemble.</strong>
			</p>
		</div>
		<hr/>
		Exercice 8 :<br/>
		<div>
			<form name="exo8" method="get" action="">
				<fieldset>
					<legend>Vous et votre futur chez vous</legend>
					<input type="hidden" name="nbexo" value="8" />
					<label for="choix">Que voulez-vous faire ? </label>
					<input type="submit" name="choix" value="Vendre" />
					<input type="submit" name="choix" value="Acheter" />
					<input type="submit" name="choix" value="Louer" />
				</fieldset>
			</form>
			<p>
				Résultat :<br/>
				<?php
					if (isset($_GET["nbexo"]) and is_numeric($_GET["nbexo"]) and ((integer) $_GET["nbexo"]) == 8) {
						// Ce sont les données pour cet exercice.
						
						if (isset($_GET["choix"])) {
							$newURL = "http://ducher-acer/~ducher/apprentissage/exos/fic_c6e8/".strtolower(htmlentities($_GET["choix"])).".html";
							echo "<script type=\"application/javascript\">window.open('$newURL')</script>";
						}
					} else {
						// Ce ne sont pas les données pour cet exercice.
						echo "pas de résultat ici";
					}
				?>
				<br/><strong class="errorstrong">Juste mais pas le code attendu (ici JavaScript alors que PHP était attendu).</strong>
			</p>
		</div>
	</body>
</html>
