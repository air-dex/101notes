<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8"/>
		<title>Des résultats du chapitre 6</title>
		<link media="screen" rel="stylesheet" type="text/css" href="../styletableau.css" />
	</head>
	<body>
		<?php
			function newline() {  echo "<br/>"; };
		
			if (isset($_POST["nbexo"]) and is_numeric($_POST["nbexo"])) {
				$nbexo = (integer) $_POST["nbexo"];
			}
			
			// Exercice 1
			function resexo1() {
				function getData($cle) {
					return isset($_POST[$cle]) ? htmlentities($_POST[$cle]) : "inconnu";
				}
				
				// Récupération des 5 données dans un tableau
				foreach (array_keys($_POST) as $cle) {
					$tab[htmlentities($cle)] = getData($cle);
				}
				unset($tab["nbexo"]);
				
				// Affichage du tableau
				echo "<table>";
				
				foreach ($tab as $cle => $val) {
					echo "<tr><th>$cle</th><td>$val</td></tr>";
				}
				
				echo "</table>";
			}
			
			// Exercice 2
			function resexo2() {
				function getData2($cle) {
					if (isset($_POST[$cle]) and $_POST[$cle] != "") {
						return htmlentities($_POST[$cle]);
					} else
					{
						return "<script type=\"application/javascript\">alert('Valeur de ".htmlentities($cle)." inconnue')</script>";
					}
				}
				
				echo "<table>";
				
				foreach (array_keys($_POST) as $cle) {
					$cle = htmlentities($cle);
					if ($cle == "nbexo") continue;
					
					echo "<tr><th>$cle</th><td>",getData2($cle),"</td></tr>";
				}
				
				echo "</table>";
			}
		?>
		
		Résultats de l'exo <?= $nbexo ?> :<br/>
		<?php
			$funcexo = "resexo".$nbexo;
			$funcexo();
		?>
	</body>
</html>
