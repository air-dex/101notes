<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8"/>
		<link media="screen" rel="stylesheet" type="text/css" href="../styletableau.css" />
		<link media="screen" rel="stylesheet" type="text/css" href="stylexos.css" />
		<title>Exercices du chapitre 7</title>
	</head>
	<body>
		Exercice 1 :<br/>
		<div>
			<?php
				function alerte($msg) {
					echo '<script type="application/javascript">alert("',$msg,'");</script>';
				}
			
				//alerte("Une alerte");	// Commenté car c'est chiant au bout d'un moment
			?>
			Il y a une alerte ici.
		</div>
		<hr/>
		Exercice 2 :<br/>
		<div>
			<?php
				// Écrit le code HTML du tableau
				function writeTab($tab, $caption = "", $legcle = "Clés", $legval = "Valeurs", $footer = false) {
					echo "<table>";
					
					// Écriture de la légende
					if (is_string($caption) and strlen($caption) > 0 ) {
						echo "<caption>$caption</caption>";
					}
					
					function writeLine($cle, $val) {
						printf("<tr><td>%1\$s</td><td>%2\$s</td></tr>",
								htmlentities($cle),
								htmlentities($val));
					}
					
					// Écriture du header
					echo "<thead>",writeLine($legcle, $legval),"</thead>";
					
					// Écriture du footer
					if ($footer) {
						echo "<tfoot>",writeLine($legcle, $legval),"</tfoot>";
					}
					
					// Écriture du corps
					echo "<tbody>";
					
					foreach ($tab as $clef => $val) {
						writeLine($clef, $val);
					}
					
					// Fermetures finales
					echo "</tbody></table>";
				}
				
				$tab = [
					"nom" => "Ducher",
					"prenom" => "Romain",
					"age" => 25,
					"ville" => "Seychalles",
					"mon cul" => "sur la commode"
				];
				
				writeTab($tab, "Sur air-dex", "Type d'info", "Info");
			?>
		</div>
		<hr/>
		Exercice 3 :<br/>
		<div>
			<?php
				// S(n) = SUM(u(i), 0 <= i <= n)
				
				//* Calcul de n!
				function fact($n) {
					if ($n < 0) {
						throw new Exception("On calcul des factorielles de nombres positifs !", $n);
					} elseif ($n == 0) {
						return 1;
					} else {
						$res = 1;
						
						for ($i = 1; $i <= $n; ++$i) {
							$res *= $i;
						}
						
						return $res;
					}
				}//*/
				
				/*
				function u($n, $x) {
					if ($n < 0) {
						throw new Exception("On calcul des factorielles de nombres positifs !", $n);
					} else {
						return pow($x, 2*$n + 1)/fact($n);
					}
				}
				
				function s($n, $x) {
					if ($n < 0) {
						return 0;
					} else {
						$res = 0;
						
						for ($i = 0; $i <= $n; ++$i) {
							$res += u($i, $x);
						}
						
						return $res;
					}
				}
				
				// s($n, $x) optimisée.
				function sopt($n, $x) {
					if ($n < 0) {
						throw new Exception("On calcul des factorielles de nombres positifs !", $n);
					} else {
						// Le premier terme, S(0) == u(0) == $x.
						$res = $x;
						
						for ($i = 1, $factnb = 1; $i <= $n; ++$i) {
							$factnb *= $i;
							$terme = pow($x, 2*$i + 1) / $factnb;
							$res += $terme;
						}
						
						return $res;
					}
				}
				//*/
				
				// s($n, $x) encore optimisée.
				function sopt2($n, $x) {
					if ($n < 0) {
						throw new Exception("On calcul des factorielles de nombres positifs !", $n);
					} else {
						// Le premier terme, S(0) == u(0) == $x.
						for ($i = 1, $terme = $x, $res = $terme; $i <= $n; ++$i) {
							$terme *= ($x * $x / $n);
							$res += $terme;
						}
						
						return $res;
					}
				}
				
				echo sopt2(42, 10),"<br/>";
			?>
			<strong class="errorstring">FAUX ! Il manque les décimales à la fin. Il faut retourner <code>round($res, $d);</code>.</strong><br/>
			La version de la correction correspond grosso modo à <code>sopt();</code>.
		</div>
		<hr/>
		Exercice 4 :<br/>
		<div>
			<?php
				function upperFirst(&$tab) {
					if (!is_array($tab)) {
						return;
					}
					
					foreach ($tab as &$phrase) {
						$phrase = strtolower($phrase);
						$phrase = ucfirst($phrase);
					}
				}
				
				$tab = ["PHP", "mysQL", "asp.NET"];
				echo "Tableau avant : ";
				print_r($tab);
				
				echo "<br/>";
				
				upperFirst($tab);
				echo "Tableau après : ";
				print_r($tab);
			?>
			<br/><strong>Juste mais lui utilise les <code>$tab[$i]</code> et non une référence dans le foreach.</strong>
		</div>
		<hr/>
		Exercice 5 :<br/>
		<div>
			<?php
				function sinusse($mes, $unit = 'r') {
					// Obtention de la mesure en radians
					switch ($unit) {
						case 'r' :
							// $mes en radians
							$mesrad = $mes;
							break;
							
						case '°' :
						case 'd' :
							// $mes en degrés
							$mesrad = deg2rad($mes);
							break;
							
						case 'g' :
							// $mes en grades
							$mesrad = $mes * pi() / 200;
							break;
							
						default:
							throw new Exception("Unité d'angle '".$unit."' non valide.");
					}
					
					return sin($mesrad);
				}
				
				echo "sin 30° = ",sinusse(30, '°'),"<br/>";
				echo "sin(pi/6 rad) = ",sinusse(pi()/6, 'r'),"<br/>";
				echo "sin(100/3 gon) = ",sinusse(100/3, 'g'),"<br/>";
			?>
		</div>
		<hr/>
		Exercice 6 :<br/>
		<div>
			<?php
				function createForm($formArgs = [], $legend = "",
									$labelTxt = "", $textArgs = [],
									$labelRad = "", $radioArgs = [],
									$submitArgs = [], $resetArgs = [])
				{
					/*
					 * Arguments :
					 * 	- $formArgs : Attributs de la balise <form>. S'il n'y a pas l'attribut "action",
					 *    La fonction fixera sa valeur à $_SERVER["PHP_SELF"].
					 * 	- $legend : Titre du formulaire.
					 * 	- $labelTxt : Label pour la zone de texte
					 * 	- $textArgs : Attributs pour la zone de texte. L'attribut 'type', s'il y en a un,
					 *    verra sa valeur ecrasée au profit de "text".
					 * 	- $labelRad : Label pour les boutons radio
					 * 	- $radioArgs : Attributs pour les boutons radio. L'attribut "value" devra être un
					 *    array avec toutes les valeurs possibles. L'attribut 'type', s'il y en a un,
					 *    verra sa valeur ecrasée au profit de "radio".
					 * 	- $submitArgs : Attributs pour le bouton submit. L'attribut 'type', s'il y en a un,
					 *    verra sa valeur ecrasée au profit de "submit".
					 * 	- $resetArgs : Attributs pour le bouton reset. L'attribut 'type', s'il y en a un,
					 *    verra sa valeur ecrasée au profit de "reset".
					 */
					
					
					// En-tête du formulaire
					$attrList = [];
					
					// Valeur par défaut pour l'attribut action
					if (!isset($formArgs["action"])) {
						$formArgs["action"] = $_SERVER["PHP_SELF"];
					}
					
					foreach ($formArgs as $name => $attr) {
						$attrList[] = $name.'="'.addslashes($attr).'"';
					}
					
					echo "<form ",implode(" ", $attrList),"><fieldset>";
					
					if (is_string($legend) and $legend != "") {
						echo "<legend>",htmlentities($legend),"</legend>";
					}
					
					
					// Saisie de texte
					if (!isset($textArgs["name"])) {
						$textArgs["name"] = "text";
					}
					
					$textArgs["type"] = "text";
					
					if (is_string($labelTxt) and $labelTxt != "") {
						echo '<label for="',addslashes($textArgs["name"]),'">',htmlentities($labelTxt),'</label>';
					}
					
					$attrList = [];
					foreach ($textArgs as $name => $attr) {
						$attrList[] = $name.'="'.addslashes($attr).'"';
					}
					
					echo "<input ",implode(" ", $attrList),"/><br/>";
					
					// Boutons radio
					$radioValues = (isset($radioArgs["value"]) and is_array($radioArgs["value"])) ? $radioArgs["value"] : [];
					
					if ($radioValues !== []) {
						// Radio input only if there are choices.
						if (!isset($radioArgs["name"])) {
							$radioArgs["name"] = "radio";
						}
						
						$radioArgs["type"] = "radio";
						
						
						if (is_string($labelRad) and $labelRad != "") {
							echo '<label for="',addslashes($radioArgs["name"]),'">',htmlentities($labelRad),'</label><br/>';
						}
						
						// List of attributes
						$attrList = [];
						unset($radioArgs["value"]);	// Not in attributes list
						foreach ($radioArgs as $name => $attr) {
							$attrList[] = $name.'="'.addslashes($attr).'"';
						}
						$attrStr = implode(" ", $attrList);
						
						foreach ($radioValues as $value) {
							printf('<input value="%s" %s/><label for="%s">%s</label><br/>',
									addslashes($value),
									$attrStr,
									addslashes($value),
									htmlentities($value)
							);
						}
					}
					
					// Submit
					if (!isset($submitArgs["name"])) {
						$submitArgs["name"] = "submit";
					}
					
					$submitArgs["type"] = "submit";
					
					$attrList = [];
					foreach ($submitArgs as $name => $attr) {
						$attrList[] = $name.'="'.addslashes($attr).'"';
					}
					
					echo "<input ",implode(" ", $attrList),"/>";
					
					// Reset
					if (!isset($resetArgs["name"])) {
						$resetArgs["name"] = "reset";
					}
					
					$resetArgs["type"] = "reset";
					
					$attrList = [];
					foreach ($resetArgs as $name => $attr) {
						$attrList[] = $name.'="'.addslashes($attr).'"';
					}
					
					echo "<input ",implode(" ", $attrList),"/><br/>";
					
					// Fin du formulaire
					echo "</fieldset></form>";
				}
			?>
		</div>
		<hr/>
		Exercice 7 :<br/>
		<div>
			<?php
				// UTILS : Construire la partie avec les attributs dans la balise
				function getHTMLAttributes($attrMap = []) {
					$attrList = [];
					
					foreach ($attrMap as $name => $attr) {
						$attrList[] = $name.'="'.addslashes($attr).'"';
					}
					
					return implode(" ", $attrList);
				}
				
				// En-tête du formulaire
				function writeFormHeader($formArgs = [], $legend = "")
				{
					/*
					 * Arguments :
					 * 	- $formArgs : Attributs de la balise <form>. S'il n'y a pas l'attribut "action",
					 *    La fonction fixera sa valeur à $_SERVER["PHP_SELF"].
					 * 	- $legend : Titre du formulaire.
					 */
					
					// Valeur par défaut pour l'attribut action
					if (!isset($formArgs["action"])) {
						$formArgs["action"] = $_SERVER["PHP_SELF"];
					}
					
					echo "<form ",getHTMLAttributes($formArgs),"><fieldset>";
					
					// Titre du formulaire
					if (is_string($legend) and $legend != "") {
						echo "<legend>",htmlentities($legend),"</legend>";
					}
				}
				
				// Saisie de texte
				function writeTextInput($labelTxt = "", $textArgs = [])
				{
					/*
					 * Arguments :
					 * 	- $labelTxt : Label pour la zone de texte
					 * 	- $textArgs : Attributs pour la zone de texte. L'attribut 'type', s'il y en a un,
					 *    verra sa valeur ecrasée au profit de "text".
					 */
					 
					if (!isset($textArgs["name"])) {
						$textArgs["name"] = "text";
					}
					
					$textArgs["type"] = "text";
					
					if (is_string($labelTxt) and $labelTxt != "") {
						echo '<label for="',addslashes($textArgs["name"]),'">',htmlentities($labelTxt),'</label>';
					}
					
					echo "<input ",getHTMLAttributes($textArgs),"/><br/>";
				}
				
				// Boutons radio
				function writeRadioInput($labelRad = "", $radioArgs = [])
				{
					/*
					 * Arguments :
					 * 	- $labelRad : Label pour les boutons radio
					 * 	- $radioArgs : Attributs pour les boutons radio. L'attribut "value" devra être un
					 *    array avec toutes les valeurs possibles. L'attribut 'type', s'il y en a un,
					 *    verra sa valeur ecrasée au profit de "radio".
					 */
					
					$radioValues = (isset($radioArgs["value"]) and is_array($radioArgs["value"])) ? $radioArgs["value"] : [];
					
					// Radio input only if there are choices.
					if ($radioValues == []) {
						return;
					}
					
					if (!isset($radioArgs["name"])) {
						$radioArgs["name"] = "radio";
					}
					
					$radioArgs["type"] = "radio";
					
					if (is_string($labelRad) and $labelRad != "") {
						echo '<label for="',addslashes($radioArgs["name"]),'">',htmlentities($labelRad),'</label><br/>';
					}
					
					// List of attributes
					unset($radioArgs["value"]);	// This array is not in attributes list
					$attrStr = getHTMLAttributes($radioArgs);
					
					foreach ($radioValues as $value) {
						printf('<input value="%s" %s/><label for="%s">%s</label><br/>',
								addslashes($value),
								$attrStr,
								addslashes($value),
								htmlentities($value)
						);
					}
				}
				
				// Submit & Reset
				function submitAndReset($submitArgs = [], $resetArgs = [])
				{
					/*
					 * Arguments :
					 * 	- $submitArgs : Attributs pour le bouton submit. L'attribut 'type', s'il y en a un,
					 *    verra sa valeur ecrasée au profit de "submit".
					 * 	- $resetArgs : Attributs pour le bouton reset. L'attribut 'type', s'il y en a un,
					 *    verra sa valeur ecrasée au profit de "reset".
					 */
					
					// Submit
					if (!isset($submitArgs["name"])) {
						$submitArgs["name"] = "submit";
					}
					
					$submitArgs["type"] = "submit";
					
					echo "<input ",getHTMLAttributes($submitArgs),"/>";
					
					// Reset
					if (!isset($resetArgs["name"])) {
						$resetArgs["name"] = "reset";
					}
					
					$resetArgs["type"] = "reset";
					
					echo "<input ",getHTMLAttributes($resetArgs),"/><br/>";
				}
				
				// Fermeture du formulaire
				function closeForm()
				{
					// Fin du formulaire
					echo "</fieldset></form>";
				}
				
				function createForm7($formArgs = [], $legend = "",
									 $textMap = [], $radioMap = [],
									 $submitArgs = [], $resetArgs = [])
				{
					/*
					 * Arguments :
					 * 	- $formArgs : Attributs de la balise <form>. S'il n'y a pas l'attribut "action",
					 *    La fonction fixera sa valeur à $_SERVER["PHP_SELF"].
					 * 	- $legend : Titre du formulaire.
					 * 	- $textMap : Map avec les attributs des champs de saisie de texte indexés selon leur légende.
					 * 	- $radioMap : Map avec les attributs des choix via boutons radio indexés selon leur légende.
					 * 	- $submitArgs : Attributs pour le bouton submit. L'attribut 'type', s'il y en a un,
					 *    verra sa valeur ecrasée au profit de "submit".
					 * 	- $resetArgs : Attributs pour le bouton reset. L'attribut 'type', s'il y en a un,
					 *    verra sa valeur ecrasée au profit de "reset".
					 */
					
					
					// En-tête du formulaire
					writeFormHeader($formArgs, $legend);
					
					
					// Saisie de texte
					foreach ($textMap as $labelTxt => $textArgs) {
						writeTextInput($labelTxt, $textArgs);
					}
					
					// Boutons radio
					foreach ($radioMap as $labelRad => $radioArgs) {
						writeRadioInput($labelRad, $radioArgs);
					}
					
					// Submit & Reset
					submitAndReset($submitArgs, $resetArgs);
					
					// Fin du formulaire
					closeForm();
				}
				
			?>
		</div>
		<hr/>
		Exercice 8 :<br/>
		<div>
			<?php
				// Récursive en O(n²)
				function coefPascal($n) {
					if ($n < 0) {
						throw new Exception("Pas de coef de Pascal pour les nombre négatifs", $n);
					} else if ($n == 0) {
						return [1];
					} else {
						// Les coefs précédents
						$cpnm1 = coefPascal($n-1);
						
						$cpn = [1];
						
						for ($i = 1; $i < count($cnpm1); $i++) {
							$cpn[$i] = $cnpm1[$i] + $cnpm1[$i-1];
						}
						
						$cpn[] = 1;
						
						return $cpn;
					}
				}
				
				// Avec les "p parmi n", en O(n²)
				function coefPascal2($n) {
					if ($n < 0) {
						throw new Exception("Pas de coef de Pascal pour les nombre négatifs", $n);
					} else if ($n == 0) {
						return [1];
					} else {
						for ($p = 0; $p <= $n; $p++) {
							$cpn[] = fact($n) / fact($p) / fact($n-$p);
						}
						
						return $cpn;
					}
				}
				
				// Avec les "p parmi n" optimisé en O(n)
				function coefPascal2opt($n) {
					if ($n < 0) {
						throw new Exception("Pas de coef de Pascal pour les nombre négatifs", $n);
					} else if ($n == 0) {
						return [1];
					} else {
						// Tableau des factorielles, pour aller plus vite
						$facttab = [1];
						for ($p = 1; $p <= $n; $p++) {
							$facttab[$p] = $facttab[$p-1] * $p;
						}
						
						for ($p = 0; $p <= $n; $p++) {
							$cpn[] = $facttab[$n] / $facttab[$p] / $facttab[$n - $p];
						}
						return $cpn;
					}
				}
				
				for ($i = 0; $i < 5; $i++) {
					echo "Cœfficients de Pascal d'ordre $i : [",implode(", ", coefPascal2opt($i)),"]<br/>";
				}
			?>
			<strong class="errorstring">Juste mais lui voulais un coefficient en particulier ("p parmi n" récursif).</strong>
		</div>
		<hr/>
	</body>
</html>
