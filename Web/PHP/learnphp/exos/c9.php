<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8"/>
		<link media="screen" rel="stylesheet" title="Style exos" type="text/css" href="stylexos.css" />
		<title>Exercices du chapitre 9</title>
	</head>
	<body>
		Exercice 1 :<br/>
		<div>
			<?php
				class Ville {
					public $nom;
					public $departement;

					public function afficher() {
						echo "La ville de $this->nom est dans le département $this->departement.<br/>";
					}
				}

				$marseille = new Ville();
				$marseille->nom = "Marseille";
				$marseille->departement = "Bouches du Rhône";
				$marseille->afficher();

				$clermont = new Ville;
				$clermont->nom = "Clermont-Ferrand";
				$clermont->departement = "Puy de Dôme";
				$clermont->afficher();
			?>
		</div>
		<hr/>
		Exercice 2 :<br/>
		<div>
			<?php
				class Ville2 {
					protected $nom;
					protected $departement;

					public function __construct($nom, $departement) {
						$this->nom = $nom;
						$this->departement = $departement;
					}

					public function afficher() {
						echo "La ville de $this->nom est dans le département $this->departement.<br/>";
					}
				}

				$marseille2 = new Ville2("Marseille", "Bouches du Rhône");
				$marseille2->afficher();

				$clermont2 = new Ville2("Clermont-Ferrand", "Puy de Dôme");
				$clermont2->afficher();
			?>
		</div>
		<hr/>
		Exercice 3 :<br/>
		<div>
			<?php
				class Personne {
					protected $nom;
					protected $prenom;
					protected $adresse;

					public function __construct($nom, $prenom, $adresse) {
						$this->nom = $nom;
						$this->prenom = $prenom;
						$this->adresse = $adresse;
					}

					public function __destruct() {
						echo "Kill $this->prenom $this->nom !<br/>";
					}

					public function getAdresse() {
						return "$this->nom $this->prenom<br/>$this->adresse";
					}

					public function setAdresse($newAdresse) {
						$this->adresse = $newAdresse;
					}
				}

				$romain = new Personne("Ducher", "Romain", "Rue du Moustier 63190 Moissat-Bas");
				echo '<p>Adresse de $romain :<br/>'.$romain->getAdresse().'</p>';
				echo "Déménagement !";
				$romain->setAdresse("6, rue de la Mairie 63190 Seychalles");
				echo '<p>Nouvelle adresse de $romain :<br/>'.$romain->getAdresse().'</p>';

				unset($romain);
			?>
			<strong class="errorstrong">Juste mais c'est <code>Personne::getPersonne();</code> et pas <code>Personne::getAdresse();</code>.</strong>
		</div>
		<hr/>
		Exercice 4 :<br/>
		<div>
			<?php
				class form {
					// Header du formulaire
					protected $formHeader;

					// Boutons texte (dans des arrays), indexés selon leur noms.
					// Si le nom est déjà pris, il rajoute un "-X" devant pour différencier.
					// Si pas de nom, un est créé.
					protected $textInputs = [];

					// Boutons submit (dans des arrays), indexés selon leur noms.
					// Si le nom est déjà pris, il rajoute un "-X" devant pour différencier.
					protected $submitButtons = [];

					// Constantes
					const formHeaderPattern = '<form method="%s" action="%s" enctype="%s"><fieldset>';
					const legendPattern = "<legend>%s</legend>";
					const formFooter = "</fieldset></form>";

					// Constructeur
					public function __construct($action, $method = "get", $legend = "",
												$enctype = "application/x-www-url-encoded")
					{
						$this->formHeader = sprintf(self::formHeaderPattern,
													addslashes($method),
													addslashes($action),
													addslashes($enctype));

						// Form legend
						if (is_string($legend) and $legend != "") {
							$this->formHeader .= sprintf(self::legendPattern, $legend);
						}
					}

					// Un nom est-il déja réservé ?
					protected function nameRes($name, $allInputs) {
						foreach ($allInputs as $inputArray) {
							if (isset($inputArray[$name]))
								return true;
						}

						return false;
					}

					// Un nom est-il déja réservé ?
					protected function nameReserved($name) {
						return $this->nameRes($name, [$this->textInputs, $this->submitButtons]);
					}

					// Ajout d'un input dans le tableau des inputs
					protected function setInput(array $newInput,		// Input à ajouter
												array &$inputsArray,	// Tableau des inputs de même type
												$inputType)				// Type d'imput
					{
						// Si pas de nom, un est créé.
						$baseInputName = isset($newInput["name"]) ? $newInput["name"] : $inputType;
						$inputName = $baseInputName;

						// Si le nom est déjà pris, il rajoute un "-X" devant pour différencier.
						for ($i = 1; $this->nameReserved($inputName); $i++) {
							$inputName = "$baseInputName-$i";
						}

						$newInput["name"] = $inputName;
						$newInput["type"] = $inputType;

						$inputsArray[$inputName] = $newInput;
					}

					// Ajout d'un texte input
					public function setText(array $textInput) {
						$this->setInput($textInput, $this->textInputs, "text");
					}

					// Ajout d'un bouton submit
					public function setSubmit(array $submitbutton) {
						$this->setInput($submitbutton, $this->submitButtons, "submit");
					}

					// Construction du code HTML d'un input
					protected function buildInput(array $input, $writeLabel,
												  $labelAttribute = "name", $labelAfter = false)
					{
						$inputAttrList = [];

						foreach($input as $name => $value) {
							$inputAttrList[] = strtolower($name).'="'.addslashes($value).'"';
						}

						// Écriture du code HTML
						$inputcode = sprintf('<input %s/>', implode(" ", $inputAttrList));
						$labelCode = "";
						if ($writeLabel) {
							$labelCode .= sprintf('<label for="%s">%s</label>', $input[$labelAttribute], $input[$labelAttribute]);
						}

						return $labelAfter ? $inputcode.$labelCode : $labelCode.$inputcode;
					}

					// Construction du code HTML d'un input
					protected function buildTextInput() {
						$htmlInputs = [];

						// Saisies de texte
						foreach ($this->textInputs as $input) {
							$htmlInputs[] = $this->buildInput($input, true);
						}

						return implode("<br/>", $htmlInputs);
					}

					// Construction du code HTML des boutons submit
					protected function buildSubmits() {
						$htmlInputs = [];

						// Boutons submit
						foreach ($this->submitButtons as $input) {
							$htmlInputs[] = $this->buildInput($input, false);
						}

						return implode("<br/>", $htmlInputs);
					}

					// Construction du code HTML de tous les inputs
					protected function buildAllInputs() {
						$htmlInputs = [];

						// Saisies de texte
						$htmlInputs[] = $this->buildTextInput();

						// Boutons submit
						$htmlInputs[] = $this->buildSubmits();

						return implode("<br/>", $htmlInputs);
					}

					// Écriture du code HTML formulaire
					public function getForm() {
						// Code HTML du formulaire : header, inputs et fermeture du formulaire
						return $this->formHeader.$this->buildAllInputs().self::formFooter;
					}
				}

				// Code test
				$formulaire = new form($_SERVER["PHP_SELF"], "post", "Votre identité");

				$inputNom = [
					"name" => "nom",
					"type" => "text",
					"value" => "Ducher"
				];
				$formulaire->setText($inputNom);

				$inputPrenom = [
					"name" => "prenom",
					"type" => "text",
					"value" => "Romain"
				];
				$formulaire->setText($inputPrenom);

				$submitButton = [
					"name" => "envoyer",
					"type" => "submit"
				];
				$formulaire->setSubmit($submitButton);

				echo $formulaire->getForm();
			?>
		</div>
		<hr/>
		Exercice 5 :<br/>
		<div>
			<?php
				class form2 extends form {
					// Boutons radio (dans des arrays), indexés selon leur noms.
					// Si pas de nom, un est créé.
					protected $radioInputs = [];

					// Cases à cocher (dans des arrays), indexés selon leur noms.
					// Si pas de nom, un est créé.
					protected $checkBoxes = [];

					// Constructeur
					public function __construct($action, $method = "get", $legend = "",
												$enctype = "application/x-www-url-encoded")
					{
						parent::__construct($action, $method, $legend, $enctype);
					}

					// Ajout d'un input à choix multiples
					protected function setMultipleInput(array $newInput,		// Input à ajouter
														array &$inputsArray,	// Tableau des inputs de même type
														$inputType)				// Type d'imput
					{
						// Si pas de nom, un est créé.
						$inputName = isset($newInput["name"]) ? $newInput["name"] : $inputType;

						$newInput["type"] = $inputType;

						if (isset($inputsArray[$inputName])) {
							$inputsArray[$inputName][] = $newInput;
						} else {
							$inputsArray[$inputName] = [$newInput];
						}
					}

					// Ajouter un bouton radio
					public function setRadio(array $radioInput) {
						$this->setMultipleInput($radioInput, $this->radioInputs, "radio");
					}

					// Ajouter une case à cocher
					public function setCheckBox(array $checkBox) {
						$this->setMultipleInput($checkBox, $this->checkBoxes, "checkbox");
					}

					// Construction du code HTML des boutons radio
					protected function buildRadios() {
						$htmlInputs = [];

						// Boutons radio
						foreach ($this->radioInputs as $inputGroup) {
							foreach ($inputGroup as $input) {
								$htmlInputs[] = $this->buildInput($input, true, "value", true);
							}
						}

						return implode("<br/>", $htmlInputs);
					}

					// Construction du code HTML des cases à cocher
					protected function buildCheckboxes() {
						$htmlInputs = [];

						// Cases à cocher
						foreach ($this->checkBoxes as $inputGroup) {
							foreach ($inputGroup as $input) {
								$htmlInputs[] = $this->buildInput($input, true, "value", true);
							}
						}

						return implode("<br/>", $htmlInputs);
					}

					// Construction du code HTML de tous les inputs
					protected function buildAllInputs() {
						$htmlInputs = [];

						// Saisies de texte
						$htmlInputs[] = $this->buildTextInput();

						// Boutons radio
						$htmlInputs[] = $this->buildRadios();

						// Cases à cocher
						$htmlInputs[] = $this->buildCheckboxes();

						// Boutons submit
						$htmlInputs[] = $this->buildSubmits();

						return implode("<br/>", $htmlInputs);
					}

					// Méthode de clonage pour l'exercice 6
					public function __clone() {}
				}

				// Code test
				$formulaire2 = new form2($_SERVER["PHP_SELF"], "post", "Votre identité");

				$formulaire2->setText($inputNom);
				$formulaire2->setText($inputPrenom);
				$formulaire2->setSubmit($submitButton);

				$radio1 = [
					"name" => "radio",
					"type" => "radio",
					"value" => "RTL2",
				];
				$formulaire2->setRadio($radio1);

				$radio2 = [
					"name" => "radio",
					"type" => "radio",
					"value" => "Jazz Radio",
				];
				$formulaire2->setRadio($radio2);

				$case = [
					"name" => "info",
					"type" => "checkbox",
					"value" => "Recevoir newsletter",
				];
				$formulaire2->setCheckBox($case);

				echo $formulaire2->getForm();
			?>
		</div>
		<hr/>
		Exercice 6 :<br/>
		<div>
			<?php
				$formulaire3 = clone $formulaire2;

				$case2 = [
					"name" => "foff",
					"type" => "checkbox",
					"value" => "Allez vous faire foutre ?",
				];
				$formulaire3->setCheckBox($case2);

				echo "Le formulaire 2 :";
				echo $formulaire2->getForm();

				echo "Le formulaire 3 :";
				echo $formulaire3->getForm();
			?>
		</div>
		<hr/>
		Exercice 7 :<br/>
		<div>
			<?php
				abstract class PersonneAbs {
					protected $nom;
					protected $prenom;

					public function __construct($nom, $prenom) {
						$this->nom = $nom;
						$this->prenom = $prenom;
					}
				}

				class Client extends PersonneAbs {
					protected $adresse;

					public function __construct($nom, $prenom, $adresse) {
						parent::__construct($nom, $prenom);
						$this->adresse = $adresse;
					}

					public function setCoord() {
						return "$this->nom $this->prenom<br/>$this->adresse";
					}
				}

				class Electeur extends PersonneAbs {
					protected $nom;
					protected $prenom;
					protected $bureau_de_vote;
					protected $vote;

					public function __construct($nom, $prenom, $bureau_de_vote, $vote) {
						parent::__construct($nom, $prenom);
						$this->bureau_de_vote = $bureau_de_vote;
						$this->vote = $vote;
					}

					public function avoter($vote) {
						$this->vote = $vote;
					}
				}

				$airdex = new Client("Ducher", "Romain", "Rue du Moustier 63190 Moissat-Bas");
				echo '<p>Adresse de $airdex :<br/>'.$airdex->setCoord().'</p>';

				$romain = new Electeur("Ducher", "Romain", "Seychalles", false);
				var_dump($romain);
			?>
		</div>
		<hr/>
		Exercice 8 :<br/>
		<div>
			<?php
				require("./ns_c9e8e9/client.php");

				$riri = new Firme\Client\Personne("Seychalles", "R2D2");
				$riri->getAdresse();
				echo "<br/>";
				$riri->getCode();
				echo "<br/>";

				require("./ns_c9e8e9/commercial.php");

				$roro = new Firme\Commercial\Personne("Moissat", "C-6PO");
				$roro->getAdresse();
				echo "<br/>";
				$roro->getCode();
				echo "<br/>";
			?>
			<strong class="errorstrong">FAUX ! On ne demandait pas TOUS les getters et TOUS les setters
			mais bel et bien des méthodes <code>get();</code> et <code>set();</code> (sans les __ au début
			comme dans l'exercice 9.</strong>
		</div>
		<hr/>
		Exercice 9 :<br/>
		<div>
			<?php
				require("./ns_c9e8e9/client9.php");

				$riri = new Firme\Client9\Personne("Seychalles", "R2D2");
				$riri->code;
				echo "<br/>";
				$riri->code = "C-6PO";
				$riri->code;
			?>
		</div>
	</body>
</html>
