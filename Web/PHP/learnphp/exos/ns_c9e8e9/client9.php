<?php

namespace Firme\Client9;

class Personne {
	protected $adresse;
	protected $code;

	public function __construct($adresse, $code) {
		$this->adresse = $adresse;
		$this->code = $code;
	}

	public function getAdresse() {
		echo $this->adresse;
	}

	public function setAdresse($newAdresse) {
		$this->adresse = $newAdresse;
	}

	public function getCode() {
		echo $this->code;
	}

	public function setCode($newCode) {
		$this->code = $newCode;
	}

	public function __get($prop) {
		switch ($prop) {
			case "adresse":
			case "code":
				echo $this->$prop;
				break;

			default:
				echo "Propriété '$prop' inconnue.";
		}
	}

	public function __set($prop, $val) {
		$this->$prop = $val;
	}
}

?>

