<?php

namespace Firme\Commercial;

class Personne {
	protected $adresse;
	public $code;

	public function __construct($adresse, $code) {
		$this->adresse = $adresse;
		$this->code = $code;
	}

	public function getAdresse() {
		echo $this->adresse;
	}

	public function setAdresse($newAdresse) {
		$this->adresse = $newAdresse;
	}

	public function getCode() {
		echo $this->code;
	}

	public function setCode($newCode) {
		$this->code = $newCode;
	}
}

?>
