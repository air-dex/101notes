<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8"/>
		<title>Les fonctions</title>
	</head>
	<body>
		<?php 
			$tabext = get_loaded_extensions();
			natcasesort($tabext);
		?>
		Les <?= count($tabext) ?> modules PHP installés sur XAMPP :<ul>
		<?php 
			$nbfuncs = 0;
			foreach ($tabext as $valeur ) {
				$nbfuncext = count(get_extension_funcs($valeur));
				printf("<li>%s : %d fonctions</li>", $valeur, $nbfuncext);
				$nbfuncs += $nbfuncext;
			}
		?>
		</ul>
		Il y a au total <?= $nbfuncs ?> fonctions PHP dans les modules de XAMPP.
		<hr/>
		Emprunts :<br/>
		<?php
			function mensualite($capital, $tauxann, $nbmois) {
				return $capital * $tauxann / ( 1- pow(1 + $tauxann, -$nbmois));
			}
		?>
		<?= mensualite(400.000, 1.5, 48); ?>
		<hr/>
		Inscriptions :<ul>
			<?php
				function inscrire() {
					foreach (func_get_args() as $inscrit) {
						echo "<li>$inscrit est inscrit.</li>";
					}
				}
				
				inscrire("Thomas", "Marion", "Romain", "Jean-André", "André-Pierre", "Jean-Louis");
			?>
		</ul>
		
		Réinscriptions :<ul>
			<?php
				// Même chose (ou presque ^^)
				function reinscrire() {
					for ($i = 0; $i < func_num_args(); $i++) {
						printf("<li>%s est réinscrit.</li>", func_get_arg($i));
					}
				}
				
				reinscrire("Thomas", "Marion", "Romain", "Jean-André", "André-Pierre", "Jean-Louis");
			?>
		</ul>
		<hr/>
		Appels :<br/>
		<?php 
			function appel() {
				static $nbappel = 0;
				$nbappel++;
				echo "Appel n°$nbappel de la fonction.<br/>";
			}
			
			appel();
			appel();
			appel();
			appel();
			appel();
			appel();
			appel();
			appel();
			appel();
			appel();
			appel();
			appel();
			appel();
			appel();
			appel();
		?>
		<hr/>
		Fonctions dynamiques :<br/>
		<?php 
			$f1 = "inscrire";
			$f1("André-Pierre Gignac", "André Pierre Clavel");
			$f2 = "appel";
			$f2(true);
			// "appel"();	// FAUX
		?>
		<hr/>
		Fonctions imbriquées :<br/>
		<?php 
			function root($argu = "Bonjour !") {
				echo "Rootz Kawootz !<br/>";
				function son($hector) {
					echo $hector,"<br/>";
					echo $argu,"<br/>";
				}
			}
			
			root();
			@son("Dors encore !");
			son("Tellement con !");
		?>
	</body>
</html>
