<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8"/>
		<title>Les inclusions</title>
	</head>
	<body>
		Ceci est le début.<br/>
		Première inclusion :<br/>
		<?php
			include("./sinova.inc");
			echo "<br/>Dotraki";
		?>
		<hr/>
		Seconde inclusion :<br>
		<?php
			include("sinova2.inc.php")
		?>
		<hr/>
		Truc plus sioux avec du JavaScript dedans :<br/>
		
		<!-- Écrit le script JS entre les balises -->
		<script type="text/javascript">
		<?php include("./sinova.js"); ?> 
		</script>
		
		<!-- Même chose que ci-dessus mais sans passer par PHP. -->
		<script src="./sinova2.js"></script> 
		
		<!-- Encore la même chose mais avec la balise dans le fichier PHP. -->
		<?php include("sinova3.inc.php") ?>
		
		<a href="javascript:bonjour();">Dire bonjour.</a><br/>
		<a href="javascript:aurevoir();">Dire au revoir.</a><br/>
		<a href="javascript:direuntruc();">Dire un truc.</a>
		<hr/>
		Autres méthodes :<ul>
			<li><code>include</code> (ici avec erreur) : <?php include("sinovapas.inc") ?></li>
			<li><code>include_once</code> :
				<?php include_once("sinovaonce.php") ?>
				<?php include_once("sinovaonce.php") ?></li>
			<li><code>require_once</code> :
				<?php require_once("sinovastronger.inc.php") ?>
				<?php require_once("sinovastronger.inc.php") ?></li>
			<li><code>require</code> (ici avec erreur) : <?php require("sinovapas.inc") ?></li>
		</ul>
	</body>
</html>
