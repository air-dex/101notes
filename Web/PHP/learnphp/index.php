<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<title>Mes pages HTML</title>
	</head>
	<body>
		<?php
		echo "Bienvenue sur les pages de Romain."
		?>

		<br>
		La date : <?= date("d/m/y")?><br>

		<?= "Voici ce que je fais :" ?>

		<ol>
			<li><a href="inclusions.php">Les inclusions</a></li>
			<li><a href="variables.php">Les variables</a></li>
			<li><a href="instructions.php">Les instructions</a></li>
			<li><a href="strings.php">Les strings</a></li>
			<li><a href="arrays.php">Les arrays</a></li>
			<li><a href="formulaires.php">Les formulaires</a></li>
			<li><a href="fonctions.php">Les fonctions</a></li>
			<li><a href="poo.php">La programmation orientée objet</a></li>

			<li><a href="ressources.php">Les ressources</a></li>
		</ol>
		<hr>
		Exercices de mon apprentissage :<ul>
			<?php
				$chapexos = [2, 3, 4, 5, 6, 7, 9];

				foreach ($chapexos as $chapno) {
			?>
				<li><a href="exos/c<?= $chapno ?>.php">Chapitre <?= $chapno ?></a></li>
			<?php } ?>
		</ul>

		Les corrigés sont <a href="http://www.funhtml.com/php5/Corrig%C3%A9s_PHP5_4.doc">ici sur Internet</a> ou
		<a href="file:///home/ducher/Bureau/101notes/pHp/Corrig%C3%A9s_PHP5_4.doc" target="_blank"
		download="Corrigés_PHP5_4.doc">là en local</a> (format .doc).
	</body>
</html>
