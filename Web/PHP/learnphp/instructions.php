<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8"/>
		<title>Les instructions</title>
		
		<!-- Style pour les tableaux -->
		<link media="screen" rel="stylesheet" type="text/css" title="Style tableau" href="styletableau.css" />
	</head>
	<body>
		Boucle if:<br>
		<div>
		<?php
			echo "Quand ma tente en a :<br>";
			$ma_tante_en_avait = true;
			if ($ma_tante_en_avait) {
				echo "Ma tante est mon oncle.<br>";
			}
			
			echo "Quand ma tente n'en a pas :<br>";
			$ma_tante_en_avait = false;
			if ($ma_tante_en_avait) {
				echo "Ma tante est mon oncle.<br>";
			} else {
				echo "Ma tante n'est pas mon oncle.<br>";
			}
			
			echo "Et sinon ?<br>";
			$sinon = true;
			if ($ma_tante_en_avait) {
				echo "Ma tante est mon oncle.<br>";
			}
			elseif ($sinon) {
				echo "Sinon mais oui non mais oui non non (va comprendre !).<br>";
			}
			else {
				echo "Ma tante n'est pas mon oncle.<br>";
			}
			
			echo "Détaché le elseif ?<br>";
			$sinon = true;
			if ($ma_tante_en_avait) {
				echo "Ma tante est mon oncle.<br>";
			}
			else if ($sinon) {
				echo "Sinon mais oui non mais oui non non (va comprendre !).<br>";
			}
			else {
				echo "Ma tante n'est pas mon oncle.<br>";
			}
			$sinon = !$sinon;
			$a_part_ca = true;
			if ($ma_tante_en_avait) {
				echo "Ma tante est mon oncle.<br>";
			}
			else if ($sinon) {
				echo "Sinon mais oui non mais oui non non (va comprendre  !).<br>";
			}
			else if ($a_part_ca) {
				echo "À part ça tout va bien. Je suis l'enfant du juron et fils de pute !<br>";
			}
			else {
				echo "Ma tante n'est pas mon oncle.<br>";
			}
			
			echo "En une ligne et sans accolades ?<br>";
			$enfantDuJuron = true;
			if ($enfantDuJuron) echo "Enfant du juron et fils de pute !<br>";
			if (true)
				echo "Enfant du juron et fils de pute !<br>";
		?>
		</div>
		<hr>
		Boucle switch :<br>
		<div>
			<?php
				define("MAJEUR", 18);
				
				$age=15;
				switch ($age) {
					case -1:
						echo "Vous avez un âge bien bizarre dites donc !";
						break;
						
					case 15:
						echo("Vous avez 15 ans");
						break;
						
					case MAJEUR:
						echo("Vous êtes majeur désormais.");
						break;
						
					case 21:
					case 25:
						echo("Vous êtes majeur désormais et avec la vingtaine.");
						break;
						
					case "Un âge":
						echo "Vous êtes une bonne femme pour cacher votre âge ?";
						break;
						
					case true:
						echo "C'est un booléen, pas un âge";
						break;
						
					case null:
						echo "Bon à rien";
						break;
						
					default:
						echo "fon";
						break;
				}
				
				echo "<br>";
				
				$age=MAJEUR;
				switch ($age) {
					case -1:
						echo "Vous avez un âge bien bizarre dites donc !";
						break;
						
					case 15:
						echo("Vous avez 15 ans");
						break;
						
					case MAJEUR:
						echo("Vous êtes majeur désormais.");
						break;
						
					case 21:
					case 25:
						echo("Vous êtes majeur désormais et avec la vingtaine.");
						break;
						
					case "Un âge":
						echo "Vous êtes une bonne femme pour cacher votre âge ?";
						break;
						
					case true:
						echo "C'est un booléen, pas un âge";
						break;
						
					case null:
						echo "Bon à rien";
						break;
						
					default:
						echo "fon";
						break;
				}
				
				echo "<br>";
				
				$age=25;
				switch ($age) {
					case -1:
						echo "Vous avez un âge bien bizarre dites donc !";
						break;
						
					case 15:
						echo("Vous avez 15 ans");
						break;
						
					case MAJEUR:
						echo("Vous êtes majeur désormais.");
						break;
						
					case 21:
					case 25:
						echo("Vous êtes majeur désormais et avec la vingtaine.");
						break;
						
					case "Un âge":
						echo "Vous êtes une bonne femme pour cacher votre âge ?";
						break;
						
					case true:
						echo "C'est un booléen, pas un âge";
						break;
						
					case null:
						echo "Bon à rien";
						break;
						
					default:
						echo "fon";
						break;
				}
				
				echo "<br>";
				
				$age="Un âge";
				switch ($age) {
					case -1:
						echo "Vous avez un âge bien bizarre dites donc !";
						break;
						
					case 15:
						echo("Vous avez 15 ans");
						break;
						
					case MAJEUR:
						echo("Vous êtes majeur désormais.");
						break;
						
					case 21:
					case 25:
						echo("Vous êtes majeur désormais et avec la vingtaine.");
						break;
						
					case "Un âge":
						echo "Vous êtes une bonne femme pour cacher votre âge ?";
						break;
						
					case true:
						echo "C'est un booléen, pas un âge";
						break;
						
					case null:
						echo "Bon à rien";
						break;
						
					default:
						echo "fon";
						break;
				}
				
				echo "<br>";
				
				$age = true;
				switch ($age) {
					case -1:
						echo "Vous avez un âge bien bizarre dites donc !";
						break;
						
					case 0:
						echo "Bienvenu au monde !";
						break;
						
					case 1:
						echo "Première bougie ! !";
						break;
						
					case 15:
						echo("Vous avez 15 ans");
						break;
						
					case MAJEUR:
						echo("Vous êtes majeur désormais.");
						break;
						
					case 21:
					case 25:
						echo("Vous êtes majeur désormais et avec la vingtaine.");
						break;
						
					case "Un âge":
						echo "Vous êtes une bonne femme pour cacher votre âge ?";
						break;
						
					case true:
						echo "C'est un booléen, pas un âge";
						break;
						
					case null:
						echo "Bon à rien";
						break;
						
					default:
						echo "fon";
						break;
				}
				
				echo "<br>";
				
				//$age = null;
				switch ($age) {
					case -1:
						echo "Vous avez un âge bien bizarre dites donc !";
						break;
						
					case 0:
						echo "Bienvenu au monde !";
						break;
						
					case 1:
						echo "Première bougie ! !";
						break;
						
					case 15:
						echo("Vous avez 15 ans");
						break;
						
					case MAJEUR:
						echo("Vous êtes majeur désormais.");
						break;
						
					case 21:
					case 25:
						echo("Vous êtes majeur désormais et avec la vingtaine.");
						break;
						
					case "Un âge":
						echo "Vous êtes une bonne femme pour cacher votre âge ?";
						break;
						
					case true:
						echo "C'est un booléen, pas un âge";
						break;
						
					case null:
						echo "Bon à rien";
						break;
						
					default:
						echo "fon";
						break;
				}
				
				echo "<br>";
				
				$age2 = "fatal";
				switch ($age2) {
					case -1:
						echo "Vous avez un âge bien bizarre dites donc !";
						break;
						
					case 0:
						echo "Bienvenu au monde !";
						break;
						
					case 1:
						echo "Première bougie ! !";
						break;
						
					case 15:
						echo("Vous avez 15 ans");
						break;
						
					case MAJEUR:
						echo("Vous êtes majeur désormais.");
						break;
						
					case 21:
					case 25:
						echo("Vous êtes majeur désormais et avec la vingtaine.");
						break;
						
					case "Un âge":
						echo "Vous êtes une bonne femme pour cacher votre âge ?";
						break;
						
					case true:
						echo "C'est un booléen, pas un âge";
						break;
						
					case null:
						echo "Bon à rien";
						break;
						
					default:
						echo "fon";
						break;
				}
				
				echo "<br>";
				
				$age2 = -56;
				switch ($age2) {
					case -1:
						echo "Vous avez un âge bien bizarre dites donc !";
						break;
						
					case 0:
						echo "Bienvenu au monde !";
						break;
						
					case 1:
						echo "Première bougie ! !";
						break;
						
					case 15:
						echo("Vous avez 15 ans");
						break;
						
					case MAJEUR:
						echo("Vous êtes majeur désormais.");
						break;
						
					case 21:
					case 25:
						echo("Vous êtes majeur désormais et avec la vingtaine.");
						break;
						
					case "Un âge":
						echo "Vous êtes une bonne femme pour cacher votre âge ?";
						break;
						
					case true:
						echo "C'est un booléen, pas un âge";
						break;
						
					case null:
						echo "Bon à rien";
						break;
						
					default:
						echo "fon";
						break;
				}
				
				echo "<br>";
				
				$age3 = false;
				switch ($age3) {
					case -1:
						echo "Vous avez un âge bien bizarre dites donc !";
						break;
						
					case 0:
						echo "Bienvenu au monde !";
						break;
						
					case 1:
						echo "Première bougie ! !";
						break;
						
					case 15:
						echo("Vous avez 15 ans");
						break;
						
					case MAJEUR:
						echo("Vous êtes majeur désormais.");
						break;
						
					case 21:
					case 25:
						echo("Vous êtes majeur désormais et avec la vingtaine.");
						break;
						
					case "Un âge":
						echo "Vous êtes une bonne femme pour cacher votre âge ?";
						break;
						
					case true:
						echo "C'est un booléen, pas un âge";
						break;
						
					case null:
						echo "Bon à rien";
						break;
						
					default:
						echo "fon";
						break;
				}
				
				echo "<br>";
			?>
		</div>
		<hr>
		Boucle for :<br>
		<div>
			Forme simple :<br>
			<?php
				echo "les nombres simples : ";
				for ($i = 0; $i < 21; $i++) {
					echo "$i ";
				}
				
				// Avec une instruction plus complexe dans le for
				
				echo "<br/>Des carrés : ";
				for ($i = 2; $i < 21 * 21; $i = pow($i, 2)) {
					echo "$i ";
				}
				echo "<br>";
			?>
			Forme composée :<br>
			<?php
				echo "Avec deux variables<br>";
				for ($i = 0, $j = 10;
					 $i < 10, $j > 0;
					 $i++, --$j)
				{
					echo "$i $j<br/>";
				}
				
				echo "Avec un décalage<br>";
				for ($i = 0, $j = 15;
					 $i < 10, $j > 0;
					 $i++, --$j)
				{
					echo "$i $j<br/>";
				}
			?>
			<table>
				<caption>La table de Pythagore</caption>
				<thead><tr>
					<td>x</td>
					<?php
						// Têtes des colonnes, avec le multiplicande
						for ($i = 0 ; $i <= 10 ; ++$i) {
							echo "<td>$i</td>";
						}
					?>
				</tr></thead>
				<tbody>
					<?php
						// Têtes des colonnes, avec le multiplicande
						for ($i = 0; $i <= 10 ; ++$i) {
							echo "<tr>";	// Début de la ligne du tableau
							
							// Écriture du multiplicateur
							echo "<td>$i</td>";
							
							// Écriture des produits
							for ($j = 0 ; $j <= 10; $j++) {
								echo "<td>",$i * $j,"</td>";
							}
							
							echo "</tr>";	// Fin de la ligne
						}
					?>
				</tbody>
			</table>
		</div>
		<hr>
		Boucle while : On sait faire !
		<hr>
		Boucle do...while :<br>
		<div>
			<?php
				$i = 10;
				do {
			?>
				La valeur vaut <?= $i ?>.<br/>
			<?php
				$i--;
				} while ($i > 5);
			?>
		</div>
		<hr>
		Boucle foreach :<br>
		<div>
			<?php
				$min = 5;
				$max = 17;
				
				for ($i = $min ; $i <= $max ; $i++) {
					$tab[$i] = $i * $i;
				}
				
				// Affichage des carrés
				foreach ($tab as $x => $y) {
					echo $x,"² = $y<br/>";
				}
			?>
		</div>
		<hr>
		goto :<br>
		<div>
			<?php
				$i = 1;
				
				echo "Boucle while à base de goto.<br/>";
				if ($i < 10) {
				laboucle:
					$i++;
					
					if ($i < 10) {
						echo "Qu'est-ce que j'ai fait au bon Dieu pour devoir utiliser un goto ?<br/>";
						goto laboucle;
					}
				}
				
				echo "Boucle do...while à base de goto.<br/>";
				
				$j = 4;
				lautreboucle:
				$j--;
				
				if ($j > -4) {
					echo "Mais pourquoi encore et toujours devoir utiliser un goto ?<br/>";
					goto lautreboucle;
				}
				
			?>
			
			<?php
				echo "Retour dans la page (devient à la limite du lisible).<br/>";
				$k = 26;
				pointdanslapage:
				
				if ($k < 45) { ?>
					Le logarithme népérien de <?= $k ?> : <?= log($k) ?>.<br/>
			<?php
				$k++;
					goto pointdanslapage;	// Et aussi mon poing dans ta gueule ça suffit les goto !
				} else
					echo "Enfin finis ces putains de goto ! ! ! C'est y pas trop tôt !";
			?>
		</div>
		<hr>
		Les exceptions :<br>
		<div>
			<?php
				try {
					echo "Je lance une exception.<BR/>";
					throw new Exception();
				} catch (Exception $ex) {
					echo "Exception capturée.<br/>";
				}
				
				$e = new Exception("Erreur !", 42);
				
				try {
					echo "On lance une exception !<br/>";
					throw $e;
				} catch (Exception $ex) {
					echo "Le message : ".$ex->getMessage()."<br/>";
					echo "Le code : ".$ex->getCode()."<br/>";
					echo $ex."<br/>";
				}
			?>
		</div>
	</body>
</html>

