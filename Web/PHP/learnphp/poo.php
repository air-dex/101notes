<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8"/>
		<title>La programmation objet</title>
	</head>
	<body>
		Premiers tests avec les classes :<br/>
		<?php
			class ma_class {
				// Constantes de la classe
				const PI = 3.14;

				// Attributs
				public $attr1;   		// Attribut public
				protected $attr2;		// Attribut protégé
				private $attr3;  		// Attribut privé
				//$toto;         			// FAUX car pas de modificateur
				public $attr4 = "Mon cul sur la commode.";		// On peut init la variable ici
				public static $attrStat = "Je suis statique.";		// Attribut statique
				public static $NB_INSTANCES = 0;
				public $attrobj;

				// Méthodes
				public function doit() {
					echo "Je suis beau.<br/>";
				}

				public function printConst() {
					echo "<br/>Constante avec du self : ",self::PI,"<br/>";
				}

				public function getAttr3() {
					return $this->attr3;
				}

				public function setAttr3($arg) {
					$this->attr3 = $arg;
				}

				public static function methstat() {
					echo "L'attribut statique vaut : ",self::$attrStat,"<br/>";
				}

				public function set2(array $a) {
					$this->attr2 = $a;
				}

				public function setObj(ma_class $arg) {
					$this->attrobj = $arg;
				}

				// Constructeur
				public function __construct($arg2 = null, $arg3= []) {
					self::$NB_INSTANCES++;
					$this->attr2 = $arg2;
					$this->attr3 = $arg3;
				}

				// Destructeur
				public function __destruct() {
					self::$NB_INSTANCES--;
					echo "DESTROY ! ! !<br/>";
				}
			}

			$mc = new ma_class("Coucou tu veux voir ma bite ?");
			$mc->doit();
			echo $mc->attr4,"<br/>",ma_class::PI,"<br/>";
			ma_class::doit();
			echo "Le type de ma classe : ",gettype($mc),"<br/>";

			if ($mc instanceof ma_class) {
				echo "YES ! On a le instanceof !<br/>";
			} else {
				echo "Et merde ! On n'a pas le instanceof.<br/>";
			}

			$mc->attr1 = "Salut ! C'est monsieur Sucre.";
			echo "var dump sur l'objet :";
			var_dump($mc);
			echo "<br/>print_r sur l'objet :";
			print_r($mc);

			$mc->printConst();
			$mc->setAttr3(25);
			echo "Des attributs :<ul><li>Attribut 1 : $mc->attr1</li><li>Attribut 3 : ",$mc->getAttr3(),"</li></ul>";
			echo ma_class::$attrStat,"<br/>";
			ma_class::methstat();
			//echo $mc->$attrStat,"<br/>"; // FAUX !
			echo "<br/>FIN... Ou pas !<br/>";

			$mc->tutu = 26;
			echo "Nouvel attribut 'tutu' : $mc->tutu<br/>";
			//echo "Accès via tableau (ici 'tutu') : ",$mc["tutu"],"<br/>";		// FAUX !
			unset($mc);
			echo "FIN ! (pour de vrai)";
	    ?>
		<hr/>
		(L'amour en) héritage :<br/>
		<?php
			// Personnage
			class Personnage {
				protected $nom;
				protected $pv;
				protected static $fonction;

				public function __construct($name, $hp) {
					$this->nom = $name;
					$this->pv = $hp;
				}

				public function isMort() {
					if ($this->pv == 0) {
						unset($this);
					}
				}

				public function decrire() {
					echo "Je m'appelle $this->nom et je suis $this->fonction.<br/>";
				}
			}

			// Guerrier
			class Guerrier extends Personnage {
				protected $arme;
				protected $force;

				public function __construct($name, $hp, $arme, $strength) {
					parent::__construct($name, $hp);
					$this->fonction = "Guerrier";
					$this->arme = $arme;
					$this->force = $strength;
				}
			}

			// Magicien
			class Magicien extends Personnage {
				protected $sceptre;
				protected $pm;

				public function __construct($name, $hp, $sceptor, $mp) {
					$this->fonction = "Magicien";
					$this->sceptre = $sceptor;
					$this->pm = $mp;
					parent::__construct($name, $hp);
				}
			}

			$reyn = new Guerrier("Reyn", 5689, "Gardar", 526);
			$melia = new Magicien("Melia", 2615, "Canne magique", 425);
			$meyneth = new Magicien("Lady Meyneth", 9999, "Monado de Mékonis", 999);

			$reyn->decrire();
			$melia->decrire();

			$meyneth = new Magicien("Lady Meyneth", 9999, "Monado de Mékonis", 999);
			$reyn->decrire();
			$meyneth->decrire();

			echo "<h2>Les traits</h2>";

			trait Combat {
				protected $arme;
				protected $force;

				/* NON!
				public function __construct($arme, $strength) {
					$this->arme = $arme;
					$this->force = $strength;
				}//*/
			}

			trait Magie {
				protected $sceptre;
				protected $pm;

				/* NON!
				public function __construct($sceptor, $mp) {
					$this->sceptre = $sceptor;
					$this->pm = $mp;
				}//*/
			}

			/* Équivalent avec des traits
			class Magicien extends Personnage {
				use Magie;

				// Code
			}

			class Guerrier extends Personnage {
				use Combat;

				// Code
			}
			//*/

			// Utilise Combat et Magie
			class MageRouge extends Personnage {
				use Magie, Combat;

				public function __construct($name, $hp, $arme, $mp, $strength) {
					parent::__construct($name, $hp);
					$this->fonction = "Mage Rouge";

					//Magie::__construct($arme, $mp);
					$this->sceptre = $arme;
					$this->pm = $mp;
					//Combat::__construct($arme, $strength);
					$this->arme = $arme;
					$this->force = $strength;
				}
			}

			$riki = new MageRouge("Riki", 8934, "Taptap doré", 125, 200);

			$reyn->decrire();
			$melia->decrire();
			$meyneth->decrire();
			$riki->decrire();
		?>
	</body>
</html>
