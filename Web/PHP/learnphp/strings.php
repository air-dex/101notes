<!DOCTYPE html>
<html>
	<head>
		<meta type="text/html; charset=iso-8859-1"/>
		<title>Les strings</title>
		<link media="screen" rel="stylesheet" type="text/css" title="Style tableau" href="styletableau.css" />
	</head>
	<body>
		<?php
			echo "Une string avec echo.<br/>";
			print "Une string avec print.<br/>";
			
			printf("%s de %d ans �crit une string avec printf.<br/>", "Un con", 25);
			$vtab[0] = "Un con";
			$vtab[1] = 25;
			vprintf("%s de %d ans �crit une string avec vprintf.<br/>", $vtab);
			
			$x = 1000;
			echo "Je ne te le r�p�terai pas $x fois !<h6>",str_repeat(" Ou pas.", $x),"</h6>";
			echo "<br/>";
			
			$str = sprintf("La string (et non pas la %s) cr��e avec sprintf();", "culotte");
			echo $str,"<br/>";
			printf('$str mesure %d caract�res.<br/>', strlen($str));
		?>
		<hr/>
		<table>
			<caption>De code � caract�re (imprimable)</caption>
			<thead>
				<tr>
					<td>Code</td>
					<td>Caract�re</td>
				</tr>
			</thead>
			<tbody>
				<?php
					for($i = ord('!'); $i < 128; ++$i) {
						printf("<tr><td>%d</td><td>%s</td></tr>", $i, chr($i));
					}
				?>
			</tbody>
		</table>
		<br/>
		<table>
			<caption>Lettres de l'alphabet et codes ASCII</caption>
			<thead>
				<tr>
					<td rowspan="2">Lettre</td>
					<td colspan="2">Casse</td>
				</tr>
				<tr>
					<td>Majuscule</td>
					<td>Minuscule</td>
				</tr>
			</thead>
			<tbody>
				<?php
					for($i = 0; $i < 26; ++$i) {
						$maj = chr(ord('A') + $i);
						$min = chr(ord('a') + $i);
						
						echo "<tr><td>$maj / $min</td><td>",ord($maj),"</td><td>",ord($min),"</td></tr>";
					}
				?>
			</tbody>
		</table>
		<hr>
		<table>
			<caption>Des caract�res sp�ciaux pour l'HTML.</caption>
			<thead>
				<tr>
					<td>Caract�re normal</td>
					<td>Caract�re en HTML</td>
				</tr>
			</thead>
			<tbody>
				<?php
					$chars = "<>��������������������������������$@\"'&\\";
					
					for ($i = 0; $i < strlen($chars); $i++) {
						$char = $chars[$i];
						$htmlchar = htmlentities($char, ENT_COMPAT, "iso-8859-1");
						
						// Pour emp�cher l'interpr�tation de &, & => &amp;
						if ($htmlchar[0] == "&") {
							$htmlchar = str_replace('&', '&amp;', $htmlchar);
						}
						
						echo "<tr><td>",$char,"</td><td>",$htmlchar,"</td></tr>";
					}
				?>
			</tbody>
		</table>
		<hr/>
		Sous cha�ne :<br/>
		<?php
			$mcslc = "Mon cul sur la commode.";
			echo "$mcslc<br/>";
			$cul = substr($mcslc, 4, strlen("cul"));
			echo "Extraction de fesses : '$cul'.";
		?>
		<hr/>
		Le scan :<br/>
		<?php
			$desc = "Je m'appelle Romain et j'ai 25 ans.";
			$nb = sscanf($desc, "Je m'appelle %s et j'ai %d ans.", $nom, $age);
			echo "R�cup�r� $nb info",($age > 1 ? "s" : "")," :<br/>";
			echo "$desc<br/>";
			echo "Nom : $nom<br/>�ge : $age an",($age > 1 ? "s" : "");
		?>
		<hr/>
		Comparaisons :<ol>
			<li>59 et "59" :
			<?php
				if (59 == "59") {
					$tab[] = '59 == "59"';
				}
				
				if (59 === "59") {
					$tab[] = '59 === "59"';
				}
				
				if (59 !== "59") {
					$tab[] = '59 !== "59"';
				}
				
				if ("59" === "59") {
					$tab[] = '"59" === "59"';
				}
				
				echo " ",implode(", ", $tab),".";
			?>
			</li>
			<li>59 et "62" :
				<?php
					$tab = null;
				
					if (59 != "62") {
						$tab[] = '59 != "62"';
					}
					
					if (59 !== "62") {
						$tab[] = '59 !== "62"';
					}
					
					if ("59" !== "62") {
						$tab[] = '"59" !== "62"';
					}
				
					echo " ",implode(", ", $tab),".";
				?>
			</li>
		</ol>
		<hr/>
		Test d'indices n�gatifs, � la Python :<br/>
		<?php
			$str = "Monopoly";
			
			for ($i = -strlen($str); $i < strlen($str); ++$i) {
				echo "Lettre n�$i : {$str[$i]}<br/>";
			}
		?>
		C'est donc pas possible.
	</body>
</html>
