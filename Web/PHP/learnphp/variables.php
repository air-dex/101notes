<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8"/>
		<title>Les variables</title>
	</head>
	<body>
		<div>
		Les variables commencent par un $.<br>
		<?php $a = 3; ?>
		Affichage de la variable <code>$a</code> générée dans cette page PHP. Elle vaut <?= $a ?>.
		</div>
		<hr>
		<div>
		Affichage comme en BASH :<ul>
			<li><code>"</code> interprète la valeur.
				<?php
					echo "\$a vaut $a avec \"."
				?>
			</li>
			<li><code>'</code> n'interprète pas la valeur.
				<?php
					echo '"$a" vaut $a avec \'.'
				?>
			</li>
		</ul>
		</div>
		<hr>
		<div>
		Deux types d'affectation :<ul>
			<li>Par valeur :<br/>Avant l'affectation :
				<?php $b = 4; $c = -5; ?>
				<code>$b == <?= $b ?>; $c == <?= $c ?>;</code></br>
				Affectation par valeur de $b à $c : <?php $c = $b ?>
				<code>$b == <?= $b ?>; $c == <?= $c ?>;</code></br>
				Réaffectations :
				<?php $b = "toto"; $c = 14.02; ?>
				<code>$b == <?= $b ?>; $c == <?= $c ?>;</code>
			</li>
			<li>Par référence :
				Avant l'affectation :<br/>
				<?php $d = 4; $e = 7 ?>
				<code>$d == <?= $d ?>; $e == <?= $e ?>;</code></br>
				Affectation par référence de $d à $e :<br/>
				<?php $e = &$d ?>
				<code>$d == <?= $d ?>; $e == <?= $e ?>;</code></br>
				Réaffectation de $d :
				<?php $d = "toto"; ?>
				<code>$d == <?= $d ?>; $e == <?= $e ?>;</code></br>
				Réaffectation de $e :
				<?php $e = -7; ?>
				<code>$d == <?= $d ?>; $e == <?= $e ?>;</code></br>
			</li>
		</ul>
		</div>
		<hr>
		<div>
		Opérations :<br/>
		<?php
			// Concaténation de strings
			$a = "Bonjour";
			$b = "toto";
			echo $a.' '.$b;	// "Bonjour toto"
			echo "<br/>";
			echo $a + $b;
			echo "<br/>";
			
			// Concaténation avec un nombre
			$c = "Rambo";
			echo $c.' '."2";
			echo "<br/>";
			
			// Concaténation avec un Booléen
			$d = true;
			echo $d;
			echo "<br/>";
			echo "Vrai : ".$d;
			$d = false;
			echo $d;
			echo "<br/>";
			echo "Faux : ".$d;
			
			// ++ et --, dans les 2 sens.
			$e = 5;
			echo $e;
			echo "<br/>";
			$e++;
			echo $e;
			echo "<br/>";
			++$e;
			echo $e;
			echo "<br/>";
			$e += 1;
			echo $e;
			echo "<br/>";
			
			$e--;
			echo $e;
			echo "<br/>";
			--$e;
			echo $e;
			echo "<br/>";
			$e -= 1;
			echo $e;
			echo "<br/>";
			
			// Avec le reste
			$e *= 8;	// 40
			echo $e;
			echo "<br/>";
			$f = 9;
			$g = $e * $f;	// 360
			echo $g;
			echo "<br/>";
		?>
		</div>
		<hr>
		<div>
			Les constantes :<br/>
			<?php
				// Constante sensible à la casse
				$cree = define("PI", 3.1419, false);
				
				if ($cree) {
					echo "PI a été défini.";
					echo "<br/>";
				}
				
				echo PI; echo "<br/>";
				echo Pi; echo "<br/>";	// Message d'erreur
				echo $PI; echo "<br/>";	// Message d'erreur
				echo "PI"; echo "<br/>";
				echo 'PI'; echo "<br/>";
				
				// Constante insensible à la casse
				$cree = define("omega", "Ohm", true);
				echo omega; echo "<br/>";
				echo OmeGA; echo "<br/>";
				
				if (defined("PI")) {
					echo "PI définie.<br/>";
				}
				
				if (defined("smultron")) {
					echo "smultron défini.<br/>";
				} else {
					echo "smultron non défini.<br/>";
				}
				
				// Sensible à la casse par défaut
				$pardefOK = define("pardef", -626);
				echo pardef; echo "<br/>";
				echo paRdef; echo "<br/>";
				
				// Les constantres déjà définies
				/*
				echo "Les constantes prédéfinies :<br/>";
				print_r(get_defined_constants());
				//*/
			?>
		</div>
		<hr>
		<div>
		<code>isset()</code> si une variable existe déjà.<br/>
		<?php
			if(isset($a)) {
				echo '$a'," est définie (et vaut \"$a\").<br/>";
			} else {
				echo '$a n\'est pas définie.<br/>';
			}
			if(isset($smultron)) {
				echo '$smultron', " est définie (et vaut $smultron).<br/>";
			} else {
				echo '$smultron n\'est pas définie.<br/>';
			}
		?>
		</div>
		<hr>
		<div>
			Les types de variables :<ul>
				<li>Les entiers : <?= gettype(5) ?>.</li>
				<li>Les nombres à virgule : <?= gettype(5.4) ?>.</li>
				<li>Les chaînes de caractères : <?= gettype("bonjour") ?>.</li>
				<li>Les booléens : <?= gettype(TRUE) ?>. Ils valent <code>true</code>
					et <code>false</code> et sont insensibles à la casse.</li>
				<li><code>null</code>, la valeur nulle et insensible à la casse : <?= gettype(nuLl) ?>.</li>
				<li>Des objets : ???.</li>
				<li>Des tableaux : ???.</li>
				<li>Des ressources : ???.</li>
			</ul>
			<?php if (is_scalar(null)) { ?>
				null est scalaire.
			<?php } else { ?>
				null n'est pas scalaire.
			<?php } ?>
			<br>
			Type d'un alias : 
			<?php
			  $a = 5;
			  $b = &$a;
			  echo gettype($b);
			?>.
		</div>
		<hr>
		<div>
			Variables vides ou pas :<br/>
			<?php
				// $a, non vide.
				$a = 5;
				if (isset($a)) {
					$res = " est initialisée.";
				} else {
					$res = " n'est pas initialisée.";
				}
				
				echo '$a'.$res."<br/>";
				if (empty($a)) {
					$res = " est vide.";
				} else {
					$res = " n'est pas vide.";
				}
				echo '$a'.$res."<br/>";
				
				// $smultron, vide et non init.
				if (isset($smultron)) {
					$res = " est initialisée.";
				} else {
					$res = " n'est pas initialisée.";
				}
				
				echo '$smultron'.$res."<br/>";
				if (empty($smultron)) {
					$res = " est vide.";
				} else {
					$res = " n'est pas vide.";
				}
				echo '$smultron'.$res."<br/>";
				
				// 0
				echo "Zéro<br/>";
				$capar = 0;
				if (isset($capar)) {
					$res = " est initialisée.";
				} else {
					$res = " n'est pas initialisée.";
				}
				
				echo '0'.$res."<br/>";
				if (empty($capar)) {
					$res = " est vide.";
				} else {
					$res = " n'est pas vide.";
				}
				echo '0'.$res."<br/>";
				
				// false
				echo "false<br/>";
				$capar = false;
				if (isset($capar)) {
					$res = " est initialisée.";
				} else {
					$res = " n'est pas initialisée.";
				}
				
				echo 'false'.$res."<br/>";
				if (empty($capar)) {
					$res = " est vide.";
				} else {
					$res = " n'est pas vide.";
				}
				echo 'false'.$res."<br/>";
				
				// null
				echo "null<br/>";
				$capar = null;
				if (isset($capar)) {
					$res = " est initialisée.";
				} else {
					$res = " n'est pas initialisée.";
				}
				
				echo 'null'.$res."<br/>";
				if (empty($capar)) {
					$res = " est vide.";
				} else {
					$res = " n'est pas vide.";
				}
				echo 'null'.$res."<br/>";
				
				// Chaîne vide
				echo "Zéro<br/>";
				$capar = "";
				if (isset($capar)) {
					$res = " est initialisée.";
				} else {
					$res = " n'est pas initialisée.";
				}
				
				echo '""'.$res."<br/>";
				if (empty($capar)) {
					$res = " est vide.";
				} else {
					$res = " n'est pas vide.";
				}
				echo '""'.$res."<br/>";
				
				// Chaîne zéro "0"
				echo "Chaîne Zéro<br/>";
				$capar = "0";
				if (isset($capar)) {
					$res = " est initialisée.";
				} else {
					$res = " n'est pas initialisée.";
				}
				
				echo '"0"'.$res."<br/>";
				if (empty($capar)) {
					$res = " est vide.";
				} else {
					$res = " n'est pas vide.";
				}
				echo '"0"'.$res."<br/>";
				
				// Chaîne FALSE "FALSE"
				echo "Chaîne FALSE<br/>";
				$capar = "FALSE";
				if (isset($capar)) {
					$res = " est initialisée.";
				} else {
					$res = " n'est pas initialisée.";
				}
				
				echo '"FALSE"'.$res."<br/>";
				if (empty($capar)) {
					$res = " est vide.";
				} else {
					$res = " n'est pas vide.";
				}
				echo '"FALSE"'.$res."<br/>";
				
				// Chaîne null "null"
				echo "Chaîne null<br/>";
				$capar = "null";
				if (isset($capar)) {
					$res = " est initialisée.";
				} else {
					$res = " n'est pas initialisée.";
				}
				
				echo '"null"'.$res."<br/>";
				if (empty($capar)) {
					$res = " est vide.";
				} else {
					$res = " n'est pas vide.";
				}
				echo '"null"'.$res."<br/>";
				
				// Test sur des constantes
				/*
				echo "L'odyssée de PI:<br/>";
				if (isset(PI)) {
					$res = " est initialisée.";
				} else {
					$res = " n'est pas initialisée.";
				}
				
				echo 'PI'.$res."<br/>";
				if (empty(PI)) {
					$res = " est vide.";
				} else {
					$res = " n'est pas vide.";
				}
				echo 'PI'.$res."<br/>";
				//*/
			?>
		</div>
		<hr>
		<div>
			Notations scientifiques et décimales : nombre 5.421.<br>
			<?php
				echo "Base 10 (5421) <br>";
				$nb = 5421;
				echo $nb; echo "<br>";
				echo "Base 2 (1010100101101) <br>";
				$nb = 0b1010100101101;
				echo $nb; echo "<br>";
				echo "Base 8 (12455) <br>";
				$nb = 012455;
				echo $nb; echo "<br>";
				echo "Base 16 (152D) <br>";
				$nb = 0x152D;
				echo $nb; echo "<br>";
			?>
			PHP ne mélange pas écriture en base non décimale et nombres à virgule ou notations scientifiques et décimales (pas con le gars).<br>
		</div>
		<hr>
		<div>
			Les tableaux (type <code>array</code>) :<br>
			Proche des <code>dict</code> de Python.<br>
			Le simple fait de mettre des crochets à une variable fait que c'est un tableau.
			<?php
				$tab[0] = "Bojuour";
				$tab[2] = 456;
				$tab["technologie"] = "PHP";
				$tab[] = pi(); /* Remplit $tab[3] */
				$tab[true] = false;
				$tab[false] = "Happy debugging suckers!";
				$tab[null] = "Nul!";
				
				print_r($tab);
				echo ((string) $tab); // Sert à rien
				echo $tab[0]."<br/>";
				echo "C'est ".$tab[null]."<br/>";
				echo "C'est $tab[null]<br/>";	// Même chose
				// echo "La technologie est $tab[\"technologie\"].<br/>";	// Error
				echo "La technologie est {$tab["technologie"]}<br/>";	// Juste Bon OK
			?>
		</div>
		<hr>
		<div>
			Les objets<br>
			Cf. <a href="./poo.php">la partie sur la programmation objet.</a>.
		</div><hr>
		<div>
			Les ressources<br>
			Cf. <a href="./ressources.php">la partie sur les ressources.</a>.
		</div>
	</body>
</html>
